  $('.status-display').hide();  

     jQuery(function($) {
    //initiate dataTables plugin
        $('#example1').DataTable({
          'paging'      : true,
          'lengthChange': true,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : true,
          'order': [[ 3, "desc" ]],
        initComplete: function () {
            this.api().columns().every( function () {
                    var that = this;
                    $( 'input', this.footer() ).on( 'keyup change', function () {
                        if ( that.search() !== this.value ) {
                            that
                                .search( this.value )
                                .draw();
                        }
                    } );

                    var that = this;
                    $( 'select', this.footer() ).on( 'keyup change', function () {
                        if ( that.search() !== this.value ) {
                            that
                                .search( this.value )
                                .draw();
                        }
                    } );
            } );
        }
        });
    });


   	$('.gradeleveldiv').hide();
	$(document).on("change", "#sy", function(e){
		if($(this).val() != ""){
   			$('.gradeleveldiv').show();
   		}
   		else{
   			$('.gradeleveldiv').hide();
   		}
	});

	var isSHS=false;
	var isNull = true;
	$('.categorydiv').hide();
	$(document).on("change", "#gradeLevelFrom", function(e){
		if($('#gradeLevelTo').val()>11){
   			$('.categorydiv').show();
   			isSHS = true;
   		}
   		else{
   			$('.categorydiv').hide();
   			isSHS = false;
   		}
	});

	$(document).on("change", "#gradeLevelTo", function(e){
		//alert($(this).val());
		if($('#gradeLevelTo').val()>11){
   			$('.categorydiv').show();
   			isSHS = true;
   		}
   		else{
   			$('.categorydiv').hide();
   			isSHS = false;
   		}
	});

   	function onFormNextSubmit(){
   		if($('#sy').val()==null){
   			alert("Select a school year.");
   			return false;
   		}
   		if($('#gradeLevelFrom').val()==null){
   			alert("Select desired grade levels");
   			return false;
   		}
   		else{
   			
	   		if(parseInt($('#gradeLevelFrom').val()) > parseInt($('#gradeLevelTo').val())) {
	   			alert("GRADE TO must be greater or equal than GRADE FROM");
	   			return false;
	   		}
	   		else if(isSHS==true && $('#category').val()==0){
	   			alert("Select a category for SHS Application");
	   			return false;
	   		}
	   		else{
	   			return true;	
	   		}   			

	   	}
   	}

//-------------------- upload and delete files---------------------------------
  $(document).on('click', '.btn-upload-file', function(e){
    $('#mainReqid').val($(this).closest('tr').find('.mainReq').attr('mainReqId'));
    $('#documentTitle_tmp').val($(this).closest('tr').find('.mainReq').attr('mainRedDesc'));
    $('#documentTitle').val($(this).closest('tr').find('.mainReq').attr('mainRedDesc'));
  });

  $(document).on('click', '.btn-delete-file', function(e){
    $('#fileName').val($(this).attr('fileName'));
    $('#targetFile').val($(this).attr('targetFile'));
    $('#fileToBeDeleted').text($(this).attr('fileName'));
    $('#mainReqid_del').val($(this).closest('tr').find('.mainReq').attr('mainReqId'));
  });


  $(document).on('click', '.btn-submit-application', function(e){
    //var href = "{{ route('submitapplication', [" + $(this).attr('prStatusId') + ", " + $(this).attr('applicationTypeId') + "]) }}";
    //$('#form_submit').attr('action', href);
  });



//--------------------- check box -------------------------------
  $(document).on('change', '.reqChkBox', function(e){
    var val = 0;
    if($(this).prop('checked')==true){
      val = 1;
    }
    else{
      val = 0;
    }
    e.preventDefault();
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    $.ajax({
      type:'POST',
      url:"\evaluationChkUpdate",
      data: {
        docReviewId: $(this).attr('prDocReviewId'),
        reqLevel: $(this).attr('reqLevel'),
        statusType: $(this).attr('statusType'),
        oneorall: $(this).attr('oneorall'),
        value: val
      },
      success:function(data){
        window.location.replace(data);
        //var x = document.getElementById("snackbar");
        //x.className = "show";
        //$('#snackbar').text("Requirement has been updated.");
        //setTimeout(function(){ x.className = x.className.replace("show", ""); }, 10000);
      }
    });

  });

//-------------- for comments/remarks -----------------------------
  $(document).on('click', '.fa-comment', function(e){
    $('#reqRemarks').text($(this).attr('title'));
    $('#remarksId').val($(this).attr('remarksId'));
    $('#statusType').val($(this).attr('statusType'));
  });


  $(document).on('click', '.fa-comment-o', function(e){
    //$('#reqRemarks').text($(this).attr('data-original-title'));
    $('#reqRemarks').text($(this).attr('title'));
    $('#remarksId').val($(this).attr('remarksId'));
    $('#statusType').val($(this).attr('statusType'));
  });







//-------------------for private schools --------------------------
  $(document).on('click', '.btn-submit-application', function(e){
    //alert($('.mainReq').length + "\n" + $('.fileUploaded').length);
    if($('.mainReq').length != $('.fileUploaded').length){
      $('.submitNotification').html('All Requirements should have an uploaded file.<br><br>Kindly check your application.');
      $('.submit-completion').hide();
      $('.submit-completion-no').text('Close');
    }
    else{
      $('.submitNotification').html('Are you sure you want to submit the application?'); 
      $('.submit-completion').show();
      $('.submit-completion-no').text('No');
    }
  });






//----------------FOR SDO ------------------
  //------------------- notify compliance during assessment ----------------------------
  $(document).on('click', '.btn-notify-for-compliance-sdo-assessment', function(e){
    if($('.mainReq').length == $('.assesment-indicator-complete').length){
      $('.for-compliance-assessment').html('At least one requirement under Assessment/Evaluation must be incomplete for compliance notification.<br><br>Kindly check your assessment.');
      $('.for-compliance-assessment-btn-yes').hide();
      $('.for-compliance-assessment-btn-no').text('Close');
    }
    else{
      $('.for-compliance-assessment').html('Notify the private school for incomplete submitted documents. <br><br>Are you sure you want to continue?'); 
      $('.for-compliance-assessment-btn-yes').show();
      $('.for-compliance-assessment-btn-no').text('No');
    }
  });

  //------------------- notify compliance during visit ----------------------------
  $(document).on('click', '.btn-notify-for-compliance-sdo-visit', function(e){
    if($('.mainReq').length == $('.sdo-ocular-visit-indicator-complete').length){
      $('.for-compliance-visit').html('At least one requirement under SDO Ocular Visit must be incomplete for compliance notification.<br><br>Kindly check your assessment.');
      $('.for-compliance-visit-btn-yes').hide();
      $('.for-compliance-visit-btn-no').text('Close');
    }
    else{
      $('.for-compliance-visit').html('Notify the private school for incomplete submitted documents. <br><br>Are you sure you want to continue?'); 
      $('.for-compliance-visit-btn-yes').show();
      $('.for-compliance-visit-btn-no').text('No');
    }
  });

  //------------------- complete document ----------------------------
  $(document).on('click', '.btn-with-complete-docu', function(e){
    if($('.mainReq').length != $('.assesment-indicator-complete').length){
      $('.with-complete-docu-notification').html('All Requirements should have a Complete assessment/evaluation status.<br><br>Kindly check your assessment.');
      $('.with-complete-docu').hide();
      $('.with-complete-docu-no').text('Close');
    }
    else{
      $('.with-complete-docu-notification').html('Tag application for completeness of document submitted during assessment and evaluation by SDO. <br><br>Are you sure you want to continue?'); 
      $('.with-complete-docu').show();
      $('.with-complete-docu-no').text('No');
    }
  });

  //------------------- for inspection ----------------------------
  $(document).on('click','.btn-for-inspection', function(e){
    if($('.mainReq').length != $('.assesment-indicator-complete').length){
      $('.submitForInspection').html('All Requirements under assessment should be evaluated first before ocular visit. <br><br>Kindly check the requirements of the application.');
      $('.for-inspection-btn-yes').hide();
      $('.for-inspection-btn-no').text('Close');
    }
    else{
      $('.submitForInspection').html('For SDO ocular visit. <br><br>Set proposed schedule for inspection and upload filled-up application assessment form'); 
      $('.for-inspection-btn-yes').show();
      $('.for-inspection-btn-no').text('No');
    }
  });

  //------------------- for endorsement and ro review ----------------------------
  $(document).on('click','.btn-compliant-for-endorsement', function(e){
    if($('.mainReq').length != $('.sdo-ocular-visit-indicator-complete').length){
      $('.for-endorsement-review').html('All Requirements should be evaluated first during the ocular visit before endorsing to the Regional Office for review.<br><br>Kindly check the requirements of the application.');
      $('.for-endorsement-btn-yes').hide();
      $('.for-endorsement-btn-no').text('Close');
    }
    else{
      $('.for-endorsement-review').html('Application is tagged to be compliant for endorsement to the Regional Office for review.<br><br>Upload filled-up ocular visit form and letter of endorsement to RO'); 
      $('.for-endorsement-btn-yes').show();
      $('.for-endorsement-btn-no').text('No');
    }
  });  

  //------------------- for re-inspection ----------------------------
  $(document).on('click','.btn-for-reinspection', function(e){
    if($('.mainReq').length != $('.assesment-indicator-complete').length){
      $('.for-reinspection').html('All Requirements should be evaluated first before ocular revisit. <br><br>Kindly check the requirements of the application.');
      $('.for-reinspection-btn-yes').hide();
      $('.for-reinspection-btn-no').text('Close');
    }
    else{
      $('.for-reinspection').html('For Revisit by SDO. <br><br>Set proposed schedule for revisit.?'); 
      $('.for-reinspection-btn-yes').show();
      $('.for-reinspection-btn-no').text('No');
    }
  });

  //------------------- for ro ocular visit ----------------------------
  $(document).on('click','.btn-back-to-assessment', function(e){
      $('.back-to-assessment').html('Go back to Assessment / Evaluation?');
  });
  






//----------------FOR RO ------------------
  //------------------- notify compliance for review ----------------------------
  $(document).on('click', '.btn-notify-for-compliance-ro-review', function(e){
    if($('.mainReq').length == $('.ro-for-review-indicator-complete').length){
      $('.for-compliance-ro-review').html('At least one requirement under RO Review must be incomplete for compliance notification.<br><br>Kindly check your assessment.');
      $('.for-compliance-ro-review-btn-yes').hide();
      $('.for-compliance-ro-review-btn-no').text('Close');
    }
    else{
      $('.for-compliance-ro-review').html('Notify the private school for incomplete submitted documents. <br><br>Are you sure you want to continue?'); 
      $('.for-compliance-ro-review-btn-yes').show();
      $('.for-compliance-ro-review-btn-no').text('No');
    }
  });

  //------------------- notify compliance during ro visit ----------------------------
  $(document).on('click', '.btn-notify-for-compliance-ro-visit', function(e){
    if($('.mainReq').length == $('.ro-ocular-visit-indicator-complete').length){
      $('.for-compliance-ro-visit').html('At least one requirement under RO Ocular Visit must be incomplete for compliance notification.<br><br>Kindly check your assessment.');
      $('.for-compliance-ro-visit-btn-yes').hide();
      $('.for-compliance-ro-visit-btn-no').text('Close');
    }
    else{
      $('.for-compliance-ro-visit').html('Notify the private school for incomplete submitted documents. <br><br>Are you sure you want to continue?'); 
      $('.for-compliance-ro-visit-btn-yes').show();
      $('.for-compliance-ro-visit-btn-no').text('No');
    }
  });

  //------------------- for ro ocular visit ----------------------------
  $(document).on('click','.btn-for-inspection-ro', function(e){
    if($('.mainReq').length != $('.ro-for-review-indicator-complete').length){
      $('.for-inspection-ro').html('All Requirements under RO review should be evaluated first before ocular visit. <br><br>Kindly check the requirements of the application.');
      $('.for-inspection-ro-btn-yes').hide();
      $('.for-inspection-ro-btn-no').text('Close');
    }
    else{
      $('.for-inspection-ro').html('For RO ocular visit. <br><br>Set proposed schedule for inspection and upload filled-up RO review form.'); 
      $('.for-inspection-ro-btn-yes').show();
      $('.for-inspection-ro-btn-no').text('No');
    }
  });

  //------------------- for ro compliant permit ----------------------------
  $(document).on('click','.btn-for-compliant-permit-ro', function(e){
    if($('.mainReq').length != $('.ro-for-review-indicator-complete').length){
      $('.compliant-issuance-permit').html('All Requirements under RO review should be evaluated first before ocular visit. <br><br>Kindly check the requirements of the application.');
      $('.for-compliant-permit-ro-btn-yes').hide();
      $('.for-compliant-permit-ro-btn-no').text('Close');
    }
    else{
      $('.compliant-issuance-permit').html('Compliant for Issuance of Permit to Operate. <br>Click Yes to generate certificate.'); 
      $('.for-compliant-permit-ro-btn-yes').show();
      $('.for-compliant-permit-ro-btn-no').text('No');
    }
  });

  //------------------- for ro compliant recognition ----------------------------
  $(document).on('click','.btn-for-compliant-recognition-ro', function(e){
    if($('.mainReq').length != $('.ro-for-review-indicator-complete').length){
      $('.compliant-issuance-recognition').html('All Requirements under RO review should be evaluated first before ocular visit. <br><br>Kindly check the requirements of the application.');
      $('.for-compliant-recognition-ro-btn-yes').hide();
      $('.for-compliant-recognition-ro-btn-no').text('Close');
    }
    else{
      $('.compliant-issuance-recognition').html('Compliant for Issuance of Recognition. <br>Click Yes to generate certificate.'); 
      $('.for-compliant-recognition-ro-btn-yes').show();
      $('.for-compliant-recognition-ro-btn-no').text('No');
    }
  });

  //------------------- for ro ocular visit ----------------------------
  $(document).on('click','.btn-back-for-ro-review', function(e){
      $('.ro-review-application').html('Go back to RO Review?');
  });
  

  // file upload assessment

      function validateFormSdoRo(){
        if($(".fileUploadSdoRo").val()==null || $(".fileUploadSdoRo").val()==""){
            alert("No File to be Uploaded");
            return false;
        }
        else{
            return true;
        }
      }

      function validateFormSdoOcular(){
        if($(".fileUploadSdoOcular").val()==null || $(".fileUploadSdoOcular").val()==""){
            alert("No File to be Uploaded");
            return false;
        }
        else{
            return true;
        }
      }
      
      function validateFormRoReview(){
        if($(".fileUploadRoReview").val()==null || $(".fileUploadRoReview").val()==""){
            alert("No File to be Uploaded");
            return false;
        }
        else{
            return true;
        }
      }

      function validateFormRoOcular(){
        if($(".fileUploadRoOcular").val()==null || $(".fileUploadRoOcular").val()==""){
            alert("No File to be Uploaded");
            return false;
        }
        else{
            return true;
        }
      }