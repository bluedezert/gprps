    $("ul.sidebar-menu a")
        .click(function(e) {
            var link = $(this);

            var item = link.parent("li");
            
            if (item.hasClass("active")) {
                item.removeClass("active").children("a").removeClass("active");
            } else {
                item.addClass("active").children("a").addClass("active");
            }

            if (item.children("ul").length > 0) {
                var href = link.attr("href");
                link.attr("href", "#");
                setTimeout(function () { 
                    link.attr("href", href);
                }, 300);
                e.preventDefault();
            }
        })
        .each(function() {
            var link = $(this);
            if (link.get(0).href === location.href) {
                link.addClass("active").parents("li").addClass("active");
                return false;
            }
        });


    $(document).on("click", ".btn-upload-file", function(e){
        $("#mainReqid").val($(this).closest("tr").find(".mainReq").attr("mainReqId"));
        $("#requirement").val($(this).closest("tr").find(".mainReq").text());
    });

    function validateForm(){
        if($("#fileUpload").val()==null || $("#fileUpload").val()==""){
            alert("No File to be Uploaded");
            return false;
        }
        else{
            return true;
        }
        
    }
