     jQuery(function($) {
        var x = document.getElementById("snackbar");
        // Add the "show" class to DIV
        x.className = "show";
        //$('#snackbarmanageuser').text($('.status-display').text());
        // After 3 seconds, remove the show class from DIV
        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 10000);
    //initiate dataTables plugin
        $('#usersTable').DataTable({
          'paging'      : true,
          'lengthChange': true,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'order': [[ 4, "desc" ]],
           initComplete: function () {
               this.api().columns().every( function () {
                       var that = this;
                       $( 'input.filterps', this.footer() ).on( 'keyup change', function () {
                           if ( that.search() !== this.value ) {
                               that
                                   .search( this.value )
                                   .draw();
                           }
                       } );

                       var that = this;
                       $( 'select.filterps', this.footer() ).on( 'keyup change', function () {
                           if ( that.search() !== this.value ) {
                               that
                                   .search( this.value )
                                   .draw();
                           }
                       } );
               } );
           }
        });

    //initiate dataTables plugin
        $('#usersTableSDO').DataTable({
          'paging'      : true,
          'lengthChange': true,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'order': [[ 4, "desc" ]],
           initComplete: function () {
               this.api().columns().every( function () {
                       var that1 = this;
                       $( 'input.filtersdo', this.footer() ).on( 'keyup change', function () {
                           if ( that1.search() !== this.value ) {
                               that1
                                   .search( this.value )
                                   .draw();
                           }
                       } );

                       var that2 = this;
                       $( 'select.filtersdo', this.footer() ).on( 'keyup change', function () {
                           if ( that2.search() !== this.value ) {
                               that2
                                   .search( this.value )
                                   .draw();
                           }
                       } );
               } );
           }
        });

    //initiate dataTables plugin
        $('#usersTableRO').DataTable({
          'paging'      : true,
          'lengthChange': true,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'order': [[ 4, "desc" ]],
           initComplete: function () {
               this.api().columns().every( function () {
                       var that3 = this;
                       $( 'input.filterro', this.footer() ).on( 'keyup change', function () {
                           if ( that3.search() !== this.value ) {
                               that3
                                   .search( this.value )
                                   .draw();
                           }
                       } );

                       var that4 = this;
                       $( 'select.filterro', this.footer() ).on( 'keyup change', function () {
                           if ( that4.search() !== this.value ) {
                               that4
                                   .search( this.value )
                                   .draw();
                           }
                       } );
               } );
           }
        });

    //initiate dataTables plugin
        $('#schoolListElem').DataTable({
          'paging'      : true,
          'lengthChange': true,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true
        });

        $('#schoolListSec').DataTable({
          'paging'      : true,
          'lengthChange': true,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true
        });
    });

    $('.for-school').hide();
    $('.for-ro-sdo').hide();
    $('.passwords').hide();
    $('.mb-0').hide();

    if($('#officeId').val()==3){
        $('.for-school').show();
        $('.for-ro-sdo').hide();
        $('.passwords').show();
        $('.mb-0').show();

        $('#employeeID').removeAttr('required');
        $('#firstName').removeAttr('required');
        $('#middleName').removeAttr('required');
        $('#lastName').removeAttr('required');
        $('#schoolsDivisionId_ro_sdo').removeAttr('required');
        $('#email_ro_sdo').removeAttr('required');
        
        $('#schoolId').attr('required');
        $('#name').attr('required');
        $('#email').attr('required');
        $('#schoolsDivisionId').attr('required');
    }
    else if($('#officeId').val()==2 || $('#officeId').val()==1){
        $('.for-school').hide();
        $('.for-ro-sdo').show();
        $('.passwords').show();
        $('.mb-0').show();

        $('#schoolId').removeAttr('required');
        $('#name').removeAttr('required');
        $('#email').removeAttr('required');
        $('#schoolsDivisionId').removeAttr('required');

        $('#employeeID').attr('required');
        $('#firstName').attr('required');
        $('#middleName').attr('required');
        $('#lastName').attr('required');
        $('#schoolsDivisionId_ro_sdo').attr('required');
        $('#email_ro_sdo').attr('required');
    }
    else{
        $('.for-school').hide();
        $('.for-ro-sdo').hide();
        $('.passwords').hide();
        $('.mb-0').hide();
    }

    $(document).on('change', '#officeId', function(e){
      if($(this).val()==3){
        $('.for-school').fadeIn();
        $('.for-ro-sdo').hide();
        $('.passwords').fadeIn();
        $('.mb-0').fadeIn();

        $('#employeeID').removeAttr('required');
        $('#firstName').removeAttr('required');
        $('#middleName').removeAttr('required');
        $('#lastName').removeAttr('required');
        $('#schoolsDivisionId_ro_sdo').removeAttr('required');
        $('#email_ro_sdo').removeAttr('required');

        $('#schoolId').attr('required');
        $('#name').attr('required');
        $('#email').attr('required');
        $('#schoolsDivisionId').attr('required');
      }
      else if($('#officeId').val()==2 || $('#officeId').val()==1){
        $('.for-school').hide();
        $('.for-ro-sdo').fadeIn();
        $('.passwords').fadeIn();
        $('.mb-0').fadeIn();

        $('#schoolId').removeAttr('required');
        $('#name').removeAttr('required');
        $('#email').removeAttr('required');
        $('#schoolsDivisionId').removeAttr('required');

        $('#employeeID').attr('required');
        $('#firstName').attr('required');
        $('#middleName').attr('required');
        $('#lastName').attr('required');
        $('#schoolsDivisionId_ro_sdo').attr('required');
        $('#email_ro_sdo').attr('required');
      }
      else{
        $('.for-school').hide();
        $('.for-ro-sdo').hide();
        $('.passwords').hide();
        $('.mb-0').hide();
      }
    });

    function validateRegistration(){
   		if($('#password').val() != $('#password-confirm').val()){
   			alert("Password do not match.");
   			return false;
   		}
   		else{
   			return true;	
   		}
   	}

    function validatePassword(){
      if($('#password').val() != $('#password-confirm').val()){
        alert("Password do not match.");
        return false;
      }
      else{
        return true;  
      }
    }
    
   	$(document).on('click', '.btn-deactivate-user', function(e){
   		$('#userid').val($(this).attr('id'));
   		$('#isActive').val($(this).attr('isActive'));
   		if($(this).attr('isActive')==0){
   			$('.btn-isactive').html('<span class="fa fa-check"></span> Activate');
   			$('.btn-isactive').removeClass('btn-danger').addClass('btn-warning');
   		}
   		else{
   			$('.btn-isactive').html('<span class="fa fa-ban"></span> Deactivate');
   			$('.btn-isactive').removeClass('btn-warning').addClass('btn-danger');
   		}
   	});