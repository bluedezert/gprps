	$(document).on('change', '#requirementslistsSY', function(e){
      $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
      });
      $.ajax({
         type:'GET',
         url:"requirementsSY",
         data: {
            applicationTypeId: $(this).attr('applicationTypeId'),
            levelId: $(this).attr('levelId'),
            schoolYear: $(this).val()
         },
         success:function(data){

         }
      });
	});

   $(document).on('click', '.main-requirement', function(e){
      $('#maindescription').text($(this).text());
      $('#mainReqId').val($(this).attr('mainReqId'));

      if($(this).attr('isActive')==1){
         $('#maindescriptionToggle').prop('checked', true);
         $('#maindescriptionToggle').parent('div').removeClass('off');
      }
      else{
         $('#maindescriptionToggle').prop('checked', false);
         $('#maindescriptionToggle').parent('div').addClass('off');
      }
   });

   $(document).on('click', '.sub1-requirement', function(e){
      $('#sub1description').text($.trim($(this).text()));
      $('#mainReqId_sub1').val($(this).attr('mainReqId'));
      $('#subReq1Id_sub1').val($(this).attr('subReq1Id'));
      if($(this).attr('isActive')==1){
         $('#sub1descriptionToggle').prop('checked', true);
         $('#sub1descriptionToggle').parent('div').removeClass('off');
      }
      else{
         $('#sub1descriptionToggle').prop('checked', false);
         $('#sub1descriptionToggle').parent('div').addClass('off');
      }
   });

   $(document).on('click', '.sub2-requirement', function(e){
      $('#sub2description').text($.trim($(this).text()));
      $('#mainReqId_sub2').val($(this).attr('mainReqId'));
      $('#subReq1Id_sub2').val($(this).attr('subReq1Id'));
      $('#subReq2Id_sub2').val($(this).attr('subReq2Id'));
      if($(this).attr('isActive')==1){
         $('#sub2descriptionToggle').prop('checked', true);
         $('#sub2descriptionToggle').parent('div').removeClass('off');
      }
      else{
         $('#sub2descriptionToggle').prop('checked', false);
         $('#sub2descriptionToggle').parent('div').addClass('off');
      }
   });