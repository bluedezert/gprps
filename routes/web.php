<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// for logout
Route::redirect('/','/gprps/public/login');

Route::prefix('admin')->group(function() {
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/home', 'AdminController@index')->name('admin.home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//routes for New Application
Route::get('privateschoolnewapplication/{applicationTypeId}', 'NewApplication\PrivateSchoolNewApplication@index')->name('privateschoolnewapplication');
Route::get('sdoapplicationmonitoring/{applicationTypeId}', 'NewApplication\SdoApplicationMonitoring@index')->name('sdoapplicationmonitoring');
Route::get('roapplicationmonitoring/{applicationTypeId}', 'NewApplication\RoApplicationMonitoring@index')->name('roapplicationmonitoring');

// routes for school
Route::post('draftnewapplication', 'NewApplication\PrivateSchoolNewApplication@draftNewApplication')->name('draftnewapplication');
//route for editing draft application
Route::post('editdraftnewapplication', 'NewApplication\PrivateSchoolNewApplication@editDraftNewApplication')->name('editdraftnewapplication');
Route::get('/viewapplication/{prStatusId}/{applicationTypeId}/{status}', 'NewApplication\PrivateSchoolNewApplication@viewapplication')->name('viewapplication');
//route for file upload
Route::post('uploadfile', 'FileUpload@uploadFile')->name('uploadfile');
Route::post('uploadfilesdoro', 'FileUpload@uploadFileSdoRo')->name('uploadfilesdoro');
Route::post('deletefile', 'FileUpload@deleteFile')->name('deletefile');
//submit application
Route::post('submitapplication', 'NewApplication\PrivateSchoolNewApplication@submitapplication')->name('submitapplication');
Route::post('submitcompletion', 'NewApplication\PrivateSchoolNewApplication@submitcompletion')->name('submitcompletion');
Route::post('cancelapplication', 'NewApplication\PrivateSchoolNewApplication@cancelapplication')->name('cancelapplication');

//routes for SDO
Route::post('assessapplication', 'NewApplication\SdoApplicationMonitoring@assessapplication')->name('assessapplication');
Route::post('notifyforcpmpliancesdo', 'NewApplication\SdoApplicationMonitoring@notifyforcpmpliancesdo')->name('notifyforcpmpliancesdo');
Route::post('completedocuments', 'NewApplication\SdoApplicationMonitoring@completedocuments')->name('completedocuments');
Route::post('forinspectionsdo', 'NewApplication\SdoApplicationMonitoring@forinspectionsdo')->name('forinspectionsdo');
Route::post('forreinspectionsdo', 'NewApplication\SdoApplicationMonitoring@forreinspectionsdo')->name('forreinspectionsdo');
Route::post('compliantforendorsement', 'NewApplication\SdoApplicationMonitoring@compliantforendorsement')->name('compliantforendorsement');
Route::post('applicationforreview', 'NewApplication\SdoApplicationMonitoring@applicationforreview')->name('applicationforreview');

//routes for RO
Route::post('roreviewapplication', 'NewApplication\RoApplicationMonitoring@roreviewapplication')->name('roreviewapplication');
Route::post('notifyforcpmpliancero', 'NewApplication\RoApplicationMonitoring@notifyforcpmpliancero')->name('notifyforcpmpliancero');
Route::post('forinspectionro', 'NewApplication\RoApplicationMonitoring@forinspectionro')->name('forinspectionro');
Route::post('compliantfornewissuance', 'NewApplication\RoApplicationMonitoring@compliantfornewissuance')->name('compliantfornewissuance');
Route::post('compliantforrenewissuance', 'NewApplication\RoApplicationMonitoring@compliantforrenewissuance')->name('compliantforrenewissuance');
Route::post('compliantforrecognitionissuance', 'NewApplication\RoApplicationMonitoring@compliantforrecognitionissuance')->name('compliantforrecognitionissuance'); 
Route::post('forpaymentandissunce', 'NewApplication\RoApplicationMonitoring@forpaymentandissunce')->name('forpaymentandissunce');
Route::post('payment', 'NewApplication\RoApplicationMonitoring@payment')->name('payment');
Route::post('issuedpermit', 'NewApplication\RoApplicationMonitoring@issuedpermit')->name('issuedpermit');
Route::post('issuedrecognition', 'NewApplication\RoApplicationMonitoring@issuedrecognition')->name('issuedrecognition');
Route::post('disapproveapplication', 'NewApplication\RoApplicationMonitoring@disapproveapplication')->name('disapproveapplication');
Route::post('approveapplication', 'NewApplication\RoApplicationMonitoring@approveapplication')->name('approveapplication');

//routes for Requirements Managments
Route::get('/requirements/{applicationTypeId}/{levelId}/{status}', 'ManageRequirements\ManageRequirements@requirements')->name("requirements");
Route::post('addmainrequirement', 'ManageRequirements\ManageRequirements@addMainRequirement')->name('addmainrequirement');
Route::post('updaterequirement', 'ManageRequirements\ManageRequirements@updateRequirement')->name('updaterequirement');
Route::post('deactivaterequirement', 'ManageRequirements\ManageRequirements@deactivateRequirement')->name('deactivaterequirement');

//routes for User Management
Route::get('manageusers', 'ManageUsers\ManageUsers@index')->name('manageusers');
Route::post('registeruser', 'ManageUsers\ManageUsers@registeruser')->name('registeruser');
Route::get('registration', 'ManageUsers\ManageUsers@registration')->name('registration');
Route::post('deactivateuser', 'ManageUsers\ManageUsers@deactivateuser')->name('deactivateuser');
Route::get('editprofile/{userid}', 'ManageUsers\ManageUsers@editprofile')->name('editprofile');
Route::post('saveschoolprofile', 'ManageUsers\ManageUsers@saveschoolprofile')->name('saveschoolprofile');
Route::post('savesdoroprofile', 'ManageUsers\ManageUsers@savesdoroprofile')->name('savesdoroprofile');
Route::get('changepassword/{userid}', 'ManageUsers\ManageUsers@changepassword')->name('changepassword');
Route::post('savenewpassword', 'ManageUsers\ManageUsers@savenewpassword')->name('savenewpassword');
Route::get('viewprofile/{userid}', 'ManageUsers\ManageUsers@viewprofile')->name('viewprofile');

//routes for SDO evaluation
Route::post('/viewapplication/{prStatusId}/{applicationTypeId}/evaluationChkUpdate', 'NewApplication\SdoApplicationMonitoring@evaluationChkUpdate')->name('evaluationChkUpdate');
Route::post('saveRemarks', 'NewApplication\SdoApplicationMonitoring@saveRemarks')->name('saveRemarks');

//routes for generation of certificates
Route::get('/createPermit/{prStatusId}/{applicationTypeId}/{schoolUserId}', 'GenerateCertificates\GenerateCertificate@createPermit')->name('createPermit');
Route::post('generatepermit', 'GenerateCertificates\GenerateCertificate@generatepermit')->name('generatepermit');
Route::get('/createRecognition/{prStatusId}/{applicationTypeId}/{schoolUserId}', 'GenerateCertificates\GenerateCertificate@createRecognition')->name('createRecognition');
Route::post('generateRecognition', 'GenerateCertificates\GenerateCertificate@generateRecognition')->name('generateRecognition');

//dashboard more info links
Route::get('dashboardmoreinfo/{applicationTypeId}', 'HomeController@dashboardmoreinfo')->name('dashboardmoreinfo');