<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStagesTimelineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stagestimeline', function (Blueprint $table) {
            $table->increments('stageTimelineId');
            $table->integer('prStatusIdfk');
            $table->integer('stageIdfk');
            $table->integer('stageStatusIdfk')->default(0);
            $table->integer('updatedBy');
            $table->dateTime('lastDateUpdated');
            $table->string('remarks')->nullable();
            $table->integer('docReviewFileIdfk')->nullable();
            $table->integer('fileIdfk')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stagestimeline');
    }
}
