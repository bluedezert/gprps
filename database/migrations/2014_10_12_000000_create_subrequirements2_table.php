<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubRequirements2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('subrequirements2');
        Schema::create('subrequirements2', function (Blueprint $table) {
            $table->increments('subReq2Id');
            $table->string('description', 255);
            $table->integer('subReq1Idfk');
            $table->string('schoolYear');
            $table->integer('isActive')->default(1);
        });

        // Document Review Form for Application of Government Permit/Recognition
        $description = array('Far from existing schools and fire hazad areas',
                            'Free from traffic and noise',
                            'Location map');

        $subReq1Idfk = array(6, 
                            6,
                            6);

        $schoolYear = '2019 - 2020';

        $conn1 = DB::connection('mysql')->getPDO();
        $stmt = $conn1 -> prepare('INSERT INTO `subrequirements2`
                                    (`description`, `subReq1Idfk`, `schoolYear`)
                                    VALUES (:description, :subReq1Idfk, :schoolYear)');
        $i = 0;
        foreach($description as $row){
            $stmt->bindParam(':description',$row);
            $stmt->bindParam(':subReq1Idfk',$subReq1Idfk[$i]);
            $stmt->bindParam(':schoolYear',$schoolYear);
            $stmt->execute();
            $i++;
        }   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subrequirements2');
    }
}
