<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubRequirements2SHSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('subrequirements2_shs');
        Schema::create('subrequirements2_shs', function (Blueprint $table) {
            $table->increments('subReq2Id');
            $table->string('description', 255);
            $table->integer('subReq1Idfk');
            $table->string('schoolYear');
            $table->integer('isActive')->default(1);
        });

        // Document Review Form for Senior High School
        $description = array('FAAP recognized accreditting agencies',
                            'Asia Pacific Accreditation and Certification (APACC)',

                            'AFA',
                            'IA',
                            'HE',
                            'ICT',

                            'Performing Arts',
                            'Arts Production',

                            'Computer',
                            'Science (for STEM minimum of 3 labs)',
                            'Workshop Room/Studios');

        $subReq1Idfk = array(7, 
                            7,

                            14,
                            14,
                            14,
                            14,

                            15,
                            15,

                            18,
                            18,
                            18);

        $schoolYear = '2019 - 2020';

        $conn1 = DB::connection('mysql')->getPDO();
        $stmt = $conn1 -> prepare('INSERT INTO `subrequirements2_shs`
                                    (`description`, `subReq1Idfk`, `schoolYear`)
                                    VALUES (:description, :subReq1Idfk, :schoolYear)');
        $i = 0;
        foreach($description as $row){
            $stmt->bindParam(':description',$row);
            $stmt->bindParam(':subReq1Idfk',$subReq1Idfk[$i]);
            $stmt->bindParam(':schoolYear',$schoolYear);
            $stmt->execute();
            $i++;
        }        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subrequirements2_shs');
    }
}
