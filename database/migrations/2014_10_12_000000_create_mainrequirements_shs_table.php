<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMainRequirementsSHSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('mainrequirements_shs');
        Schema::create('mainrequirements_shs', function (Blueprint $table) {
            $table->increments('mainReqId');
            $table->string('description', 255);
            $table->integer('applicationTypeIdfk')->default(0);
            $table->string('schoolYear');
            $table->integer('isActive')->default(1);
        });

        // Document Review Form for Senior High School
        $description = array('Letter of Intent', 
                            'Board Resolution certified by the secretary and approved by the Board of Directors/Board of Trustees',
                            'Certificate of Recognition of any of the Following',
                            'Proposed tuition and other fees',
                            'Proposed school calendar',
                            'Proposed list of academin and non-academic personnel',
                            'Curriculum Offering',
                            'Minimum program requirements for the SHS tracks/strands',
                            'A copy of Memorandum/Memoranda of Agreement/Memorandum of Unserstanding for partnership and arrangements relative to the SHS Program Implementation. These arrangements may include:',
                            'Articles of Incorporation and by-laws for private schools only',
                            'Document of ownership of school sites under the name of the school, or deed of usufruct',
                            'Proposed annual budget and annual expenditures');

        $applicationTypeIdfk = 1;
        $schoolYear = '2019 - 2020';

        $conn1 = DB::connection('mysql')->getPDO();
        $stmt = $conn1 -> prepare('INSERT INTO `mainrequirements_shs`
                                    (`description`, `applicationTypeIdfk`, `schoolYear`)
                                    VALUES (:description, :applicationTypeIdfk, :schoolYear)');
        $i = 0;
        foreach($description as $row){
            $stmt->bindParam(':description',$row);
            $stmt->bindParam(':applicationTypeIdfk',$applicationTypeIdfk);
            $stmt->bindParam(':schoolYear',$schoolYear);
            $stmt->execute();
            $i++;
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mainrequirements_shs');
    }
}
