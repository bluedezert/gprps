<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesSHSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categoriesshs', function (Blueprint $table) {
            $table->increments('categoryId');
            $table->string('category', 5);
            $table->text('description', 255)->nullable();
            $table->integer('isActive')->default(1);
        });

        $category = array('A', 'B', 'C', 'D');

        $description = array('Private schools, which have been granted at least Level II accreditation by any of the accrediting agencieas under the Federation of Accrediting Agencies in the Philippines.',
            'Non-DepED Schools, which have been issued a permit or government recognition by Commission on Higher Education (CHED) to offer any higher education program.',
            'Private schools, which have been granted recognition by the DepED to offer secondary education (Year I-IV/Grade 7 to 10).',
            'Non-DepED schools, shich have been issued a permit or recognition by the Technical Education and Skills Development Authority (TESDA) to offer any training course, and other individual, corporations, foundations or organization duly recognized by the Securities and Exchange Commission');

        $conn1 = DB::connection('mysql')->getPDO();
        $stmt = $conn1 -> prepare('INSERT INTO `categoriesshs`
                                    (`category`, `description`)
                                    VALUES (:category, :description)');
        $i = 0;
        foreach($category as $row){
            $stmt->bindParam(':category',$row);
            $stmt->bindParam(':description',$description[$i]);
            $stmt->execute();
            $i++;
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categoriesshs');
    }
}
