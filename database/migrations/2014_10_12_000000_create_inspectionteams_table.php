<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInspectionTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inspectionteams', function (Blueprint $table) {
            $table->increments('inspectionTeamId');
            $table->string('shortName');
            $table->string('description', 255);
        });

        $shortName = array('DAIT', 'RVT');
        $description = array('Division Assessment and Inspection Team', 'Regional Validation Team');

        $conn1 = DB::connection('mysql')->getPDO();
        $stmt = $conn1 -> prepare('INSERT INTO `inspectionteams`
                                    (`shortName`, `description`)
                                    VALUES (:shortName, :description)');
        $i = 0;
        foreach($shortName as $row){
            $stmt->bindParam(':shortName',$row);
            $stmt->bindParam(':description',$description[$i]);
            $stmt->execute();
            $i++;
        }
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inspectionteams');
    }
}
