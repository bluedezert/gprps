<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeneratedCertificatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('generatedcertificates', function (Blueprint $table) {
            $table->increments('certificateId');
            $table->integer('applicationTypeIdfk');
            $table->string('governmentPRNumberfk');
            $table->dateTime('dateIssued');
            $table->integer('issuedBy');
            $table->string('regionalDirectorName', 255)->nullable();
            $table->string('position', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('generatedcertificates');
    }
}
