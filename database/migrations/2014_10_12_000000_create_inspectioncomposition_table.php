<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInspectionCompositionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inspectioncomposition', function (Blueprint $table) {
            $table->increments('inspectionCompositionId');
            $table->integer('inspectionTeamIdfk');
            $table->string('CM', 255);
            $table->string('description', 255);
            $table->integer('number');
        });

        $description = array('Assistant Schools Division Superintendent', 
                            'Division Focal Person for Private School',
                            'EPS/PSDS from the CID',
                            'Division Engineer',
                            'Legal Officer',
                            'QAD Chief',
                            'EPS from the QAD',
                            'EPS from CLMD',
                            'Regional Engineer',
                            'Legal Officer');
        $CM = array('C', 
                    'M',
                    'M',
                    'M',
                    'M',
                    'C', 
                    'M',
                    'M',
                    'M',
                    'M');

        $num = array(1, 
                    1,
                    1,
                    1,
                    1,
                    1, 
                    1,
                    1,
                    1,
                    1);

        $inspectionTeamIdfk = array(1, 
                                    1,
                                    1,
                                    1,
                                    1,
                                    2,
                                    2,
                                    2,
                                    2,
                                    2);

        $conn1 = DB::connection('mysql')->getPDO();
        $stmt = $conn1 -> prepare('INSERT INTO `inspectioncomposition`
                                    (`inspectionTeamIdfk`, `CM`, `description`, `number`)
                                    VALUES (:inspectionTeamIdfk, :CM, :description, :num)');
        $i = 0;
        foreach($description as $row){
            $stmt->bindParam(':inspectionTeamIdfk',$inspectionTeamIdfk[$i]);
            $stmt->bindParam(':description',$row);
            $stmt->bindParam(':CM',$CM[$i]);
            $stmt->bindParam(':num',$num[$i]);
            $stmt->execute();
            $i++;
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inspectioncomposition');
    }
}
