<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubRequirements1SHSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('subrequirements1_shs');
        Schema::create('subrequirements1_shs', function (Blueprint $table) {
            $table->increments('subReq1Id');
            $table->string('description', 255);
            $table->integer('mainReqIdfk');
            $table->string('schoolYear');
            $table->integer('isActive')->default(1);
        });

        // Document Review Form for Senior High School
        $description = array('Purpose',
                            'School year of intended operation',
                            'SHS Curriculum for the track/s and strand/s to be offered',

                            'Secondary Education Program - DepED',
                            'Training program - TESDA',
                            'Higher Education Program - CHED',
                            'Others:',

                            'Qualifications',
                            'Job Descriptions',
                            'Teaching Load',
                            'Number of working hours per week',
                            'Certification from recognized national/international agencies (TESDA, ABA and others)',

                            'Academic Track',
                            'Tech-Voc Track',
                            'Arts and Design Track',
                            'Sports Track',

                            'Instructional rooms',
                            'Laboratories',
                            'Athletic Facilities',
                            'Learner\'s resource canter or Library',
                            'Internet Facilities',
                            'Ancillary Services',

                            'Engagement of stakeholders in the localization of the curriculum',
                            'Work Immersion',
                            'Apprenticeship',
                            'Research',
                            'Provision of equipment and laboratories, workshop, and other facilities',
                            'Organization of career guidance and youth formation activities',
                            'Other');

        $mainReqIdfk = array(2, 
                            2,
                            2,

                            3,
                            3,
                            3,
                            3,

                            6,
                            6,
                            6,
                            6,
                            6,

                            7,
                            7,
                            7,
                            7,

                            8,
                            8,
                            8,
                            8,
                            8,
                            8,

                            9,
                            9,
                            9,
                            9,
                            9,
                            9,
                            9);

        $schoolYear = '2019 - 2020';

        $conn1 = DB::connection('mysql')->getPDO();
        $stmt = $conn1 -> prepare('INSERT INTO `subrequirements1_shs`
                                    (`description`, `mainReqIdfk`, `schoolYear`)
                                    VALUES (:description, :mainReqIdfk, :schoolYear)');
        $i = 0;
        foreach($description as $row){
            $stmt->bindParam(':description',$row);
            $stmt->bindParam(':mainReqIdfk',$mainReqIdfk[$i]);
            $stmt->bindParam(':schoolYear',$schoolYear);
            $stmt->execute();
            $i++;
        }        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subrequirements1_shs');
    }
}
