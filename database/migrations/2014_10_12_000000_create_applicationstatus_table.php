<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('applicationstatus');
        Schema::create('applicationstatus', function (Blueprint $table) {
            $table->increments('applicationStatusId');
            $table->string('description', 255)->nullable();
            $table->integer('valueNow', 0);
            $table->string('progressClass')->nullable();
        });

        $applicationstatus = array('Draft', 'Cancelled', 'Submitted', 'On Process', 'Approved', 'Disapproved', 'Expired', 'On Compliance');

        $valueNow = array(10, 100, 25, 50, 100, 100, 100, 50);

        $progressClass = array('progress-bar-draft', 'progress-bar-danger', 'progress-bar-submitted', 'progress-bar-info', 'progress-bar-success', 'progress-bar-warning', 'progress-bar-danger', 'progress-bar-draft');

        $conn1 = DB::connection('mysql')->getPDO();
        $stmt = $conn1 -> prepare('INSERT INTO `applicationstatus`
                                    (`description`, `valueNow`, `progressClass`)
                                    VALUES (:description, :valueNow, :progressClass)');
        $i=0;
        foreach($applicationstatus as $row){
            $stmt->bindParam(':description',$row);
            $stmt->bindParam(':valueNow',$valueNow[$i]);
            $stmt->bindParam(':progressClass',$progressClass[$i]);
            $stmt->execute();
            $i++;
        }
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applicationstatus');
    }
}
