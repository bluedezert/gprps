<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStagesStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('stagesstatus');
        Schema::create('stagesstatus', function (Blueprint $table) {
            $table->increments('stageStatusId');
            $table->string('description');
            $table->integer('stageIdfk');
        });

        $description = array('Draft Application', 'Application Cancelled', 'Submitted for Evaluation', 'On Assessment', 'Notified For Compliance', 'With Complete Documents', 'For Inspection', 'Compliant for Endorsement', 'Endorsed to Regional Office For Review', 'Compliant for Endorsement', 'For Re-Inspection', 'Endorsed to Regional Office For Review', 'On Review', 'Notified For Compliance', 'For Inspection', 'Compliant for Issuance of Permit to Operate', 'Compliant for Issuance of Recognition', 'For Payment and Issuance', 'Paid', 'Issued Government Permit', 'Issued Government Recognition', 'Approved', 'Disapproved', 'Submitted for Completion');
        $stageIdfk = array(1, 1, 1, 2, 2, 2, 2, 2, 2, 3, 3, 3, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 1);

        $conn1 = DB::connection('mysql')->getPDO();
        $stmt = $conn1 -> prepare('INSERT INTO `stagesstatus`
                                    (`description`, `stageIdfk`)
                                    VALUES (:description, :stageIdfk)');
        $i = 0;
        foreach($description as $row){
            $stmt->bindParam(':description',$row);
            $stmt->bindParam(':stageIdfk',$stageIdfk[$i]);
            $stmt->execute();
            $i++;
        }
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stagesstatus');
    }
}
