<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMainRequirementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('mainrequirements');
        Schema::create('mainrequirements', function (Blueprint $table) {
            $table->increments('mainReqId');
            $table->string('description', 255);
            $table->integer('applicationTypeIdfk')->default(0);
            $table->string('schoolYear');
            $table->integer('isActive')->default(1);
        });

        // Document Review Form for Application of Government Permit/Recognition
        $description = array('Board Resolution', 
                            'Feasibility Study',
                            'Articles of Incorporation',
                            'Copies of Transfer Certificate of Title of the School Site',
                            'Campus Development and Landscaping Plans',
                            'Documents of Ownership and School Building',
                            'Certificat of Occupancy of School Building',
                            'Picture of School Building, Classrooms, Laboratories, Library, Medical and Dental Health Facilities, Canteen, etc.',
                            'Proposed Budget for the Succeding School Year Approved by the Board of Trustees/Directors',
                            'Copy of the Latest Financial Statement of the School Certified by an Independent Certified Public Accountant',
                            'Proposed/Approved Curriculum',
                            'Approved Tuition and Other School Fees',
                            'Retirement Plans for Teachers and Other Personnel',
                            'List of School Administrators (President, Vice-President, Deans and Department Heads',
                            'List of Non-academic Personnel (Registrat, Librarian, Guidance Counselor, Researchers, etc.',
                            'List of Teaching/Academic Staff',
                            'List of Laboratory Facilities, Equipment, Furniture, Supplies and Materials Classified by Subject Area',
                            'List of Library Holdings',
                            'List of Athletics Facilities, Equipment, Supplies and Materials',
                            'Additional Requirements');

        $applicationTypeIdfk = 1;
        $schoolYear = '2019 - 2020';

        $conn1 = DB::connection('mysql')->getPDO();
        $stmt = $conn1 -> prepare('INSERT INTO `mainrequirements`
                                    (`description`, `applicationTypeIdfk`, `schoolYear`)
                                    VALUES (:description, :applicationTypeIdfk, :schoolYear)');
        $i = 0;
        foreach($description as $row){
            $stmt->bindParam(':description',$row);
            $stmt->bindParam(':applicationTypeIdfk',$applicationTypeIdfk);
            $stmt->bindParam(':schoolYear',$schoolYear);
            $stmt->execute();
            $i++;
        }

        // Processing Checklist for Renewal of Government Permit
        $description = array('Letter of Request',
                            'Division Ocular Inspection Report',
                            'Proposed Budget signed by the Board of Trustees/Directors',
                            'Copy of the Latest Audited Financial Statement Signed by an Independent Auditor (CPA)',
                            'Copy of SEC Certification of Corporate Filing/Information',
                            'Approved Tuition and Other Fees by the Board',
                            'Photocopy of Official Receipts (payments of the latest) of Employees',
                            'List of School Administrators with:',
                            'List of New Academic and Non-academic Personnel with Necessary Information',
                            'List of New Laboratory Facilities, Equipment, Furniture & Supplies',
                            'List of New additional Facilities, Equipment & Supplies',
                            'List of New Library Holdings',
                            'Documents Submitted in Four (4) copies/folders');
        $applicationTypeIdfk = 2;
        $schoolYear = '2019 - 2020';
        $stmt = $conn1 -> prepare('INSERT INTO `mainrequirements`
                                    (`description`, `applicationTypeIdfk`, `schoolYear`)
                                    VALUES (:description, :applicationTypeIdfk, :schoolYear)');
        $i = 0;
        foreach($description as $row){
            $stmt->bindParam(':description',$row);
            $stmt->bindParam(':applicationTypeIdfk',$applicationTypeIdfk);
            $stmt->bindParam(':schoolYear',$schoolYear);
            $stmt->execute();
            $i++;
        }
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mainrequirements');
    }
}
