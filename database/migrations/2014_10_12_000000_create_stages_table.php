<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('stages');
        Schema::create('stages', function (Blueprint $table) {
            $table->increments('stageId');
            $table->integer('sequence');
            $table->string('description', 255)->nullable();
            $table->string('schedule', 255)->nullable();
            $table->string('bgColor', 255)->nullable();
        });

        $description = array('Application', 'Document Assessment & Evaluation', 'SDO Ocular Inspection', 'RO Review', 'Payment and Issuance');
        $schedule = array('On or before August 30', 'August 30 to September 30', 'On or before October 30', 'On or before November 30', 'On or before February 28');
        $sequence = array(1, 2, 3, 4, 5);

        $bgColor = array('bg-purple', 'bg-blue', 'bg-aqua', 'bg-yellow', 'bg-green');

        $conn1 = DB::connection('mysql')->getPDO();
        $stmt = $conn1 -> prepare('INSERT INTO `stages`
                                    (`sequence`,`description`,`schedule`,`bgColor`)
                                    VALUES (:sequence, :description, :schedule, :bgColor)');
        $i=0;
        foreach($description as $row){
            $stmt->bindParam(':sequence',$sequence[$i]);
            $stmt->bindParam(':description',$row);
            $stmt->bindParam(':schedule',$schedule[$i]);
            $stmt->bindParam(':bgColor',$bgColor[$i]);
            $stmt->execute();
            $i++;
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stages');
    }
}
