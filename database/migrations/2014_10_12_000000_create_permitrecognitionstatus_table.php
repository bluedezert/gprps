<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermitRecognitionStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permitrecognitionstatus', function (Blueprint $table) {
            $table->increments('prStatusId');
            $table->integer('applicationTypeIdfk');
            $table->integer('schoolUserIdfk');
            $table->integer('gradeLevelIdfkFrom');
            $table->integer('gradeLevelIdfkTo');
            $table->string('governmentPRNumber', 255)->nullable();
            $table->integer('seriesYear');
            $table->string('schoolYear');
            $table->integer('applicationStatusIdfk');
            $table->integer('categoryIdfk')->default(0);
            $table->dateTime('lastDateUpdated');
            $table->string('controlNumber', 255);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permitrecognitionstatus');
    }
}
