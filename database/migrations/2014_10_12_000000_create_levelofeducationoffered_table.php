<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLevelofeducationofferedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('levelofeducationoffered', function (Blueprint $table) {
            $table->increments('levelId');
            $table->string('description', 255)->nullable();
        });

        $levelofeducationoffered = array('Kindergarten', 'Elementary', 'Junior High School', 'Senior High School');

        $conn1 = DB::connection('mysql')->getPDO();
        $stmt = $conn1 -> prepare('INSERT INTO `levelofeducationoffered`
                                    (`description`)
                                    VALUES (:description)');
        foreach($levelofeducationoffered as $row){
            $stmt->bindParam(':description',$row);
            $stmt->execute();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('levelofeducationoffered');
    }
}
