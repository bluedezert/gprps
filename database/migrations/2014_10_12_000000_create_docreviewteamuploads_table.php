<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocReviewTeamUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('docreviewteamuploads', function (Blueprint $table) {
            $table->increments('docReviewFileId');
            $table->integer('docReviewTeamIdfk');
            $table->string('fileName');
            $table->integer('fileSize');
            $table->dateTime('dateUploaded');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('docreviewteamuploads');
    }
}
