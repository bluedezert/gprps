<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubRequirements1Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('subrequirements1');
        Schema::create('subrequirements1', function (Blueprint $table) {
            $table->increments('subReq1Id');
            $table->string('description', 255);
            $table->integer('mainReqIdfk');
            $table->string('schoolYear');
            $table->integer('isActive')->default(1);
        });

        // Document Review Form for Application of Government Permit/Recognition
        $description = array('Certified by Corporate Secretary', 

                            'Purpose',
                            'Need and relevance of school applied for',
                            'Resources and capabilities of the Corporate/School',
                            'Programs and Projects',
                            'Location of School in Relation to its Environment',

                            'in the Name of the School',
                            'Registered with the Securities and Exchange Commission (SEC)',
                            'Non-stock/Stock',
                            'registration Certificate Number',

                            'in the name of the School',
                            'Total Area',

                            'Fully Implementd',
                            'Partially Implemented',
                            'Not Implemented',

                            'in the name of the School',
                            'Total floor area with adequate staff',

                            'Signed nu proper City/Municipal Authorities',

                            'Adequate',
                            'Clean',

                            'In accordance with the DepED standards and requirements (K-12 Curriculum)',

                            'Approved by the Department of Education',
                            'In accordance with the guidelines',

                            'Terms and benefits',

                            'Educationally qualified',
                            'Salaries in accordance with the prescribed standards',

                            'Educationally Qualified',
                            'Subject assignment in accordance with qualifications',
                            'Salary/Other benefits in accordance with prescribed standards',
                            'Valid contracts/appointments',
                            'Ratio of full-time, part-time in accordance with DepED policies',
                            'Full-time',

                            'Educationally Qualified',
                            'Subject assignment in accordance with qualifications',
                            'Salary/Other benefits in accordance with prescribed standards',
                            'Valid contracts/appointments',
                            'Ratio of full-time, part-time in accordance with DepED policies',

                            'Complied with prescribed standards and requirements',

                            'List of textbooks per subject area (Sec. 160 of the Revised Manual of Regulations for Private Schools-RMRPS',
                            'List of book reference per subject area (minimum of 5 authors per subject area',

                            'Gymnasium, Playground, and the likes',
                            'Indoor and outdoor play/sports implements',

                            'Letter of Request (Separate for Elementary and Secondary)',
                            'Submit in four (4) folders',
                            'Indorsement from the Schools Division Supirentendent',
                            'Inspection Report prepared by the Division Inspection Team with the findings and recommendations',
                            'Photocopy pf the latest Government Permit Issued');

        $mainReqIdfk = array(1, 

                            2,
                            2,
                            2,
                            2,
                            2,

                            3,
                            3,
                            3,
                            3,

                            4,
                            4,

                            5,
                            5,
                            5,

                            6,
                            6,

                            7,

                            8,
                            8,

                            11,

                            12,
                            12,

                            13,

                            14,
                            14,

                            15,
                            15,
                            15,
                            15,
                            15,
                            15,

                            16,
                            16,
                            16,
                            16,
                            16,

                            17,

                            18,
                            18,

                            19,
                            19,

                            20,
                            20,
                            20,
                            20,
                            20);

        $schoolYear = '2019 - 2020';

        $conn1 = DB::connection('mysql')->getPDO();
        $stmt = $conn1 -> prepare('INSERT INTO `subrequirements1`
                                    (`description`, `mainReqIdfk`, `schoolYear`)
                                    VALUES (:description, :mainReqIdfk, :schoolYear)');
        $i = 0;
        foreach($description as $row){
            $stmt->bindParam(':description',$row);
            $stmt->bindParam(':mainReqIdfk',$mainReqIdfk[$i]);
            $stmt->bindParam(':schoolYear',$schoolYear);
            $stmt->execute();
            $i++;
        }

        // Processing Checklist for Renewal of Government Permit
        $description = array('SSS',
                            'Pag-Ibig',
                            'PhilHealth',

                            'Educational Qualifications',
                            'Salary Rate/moth',
                            'Employment Status',

                            'Job description',
                            'Educational Qualifications',
                            'Field of Specifications',
                            'Transcript of Records (Certified True Copy)',
                            'Certificates of Training',
                            'PRC Licence (Photocopy)',
                            'Employment Contract duly notarized',
                            'Salaries (per hour/month) and benefits',
                            'Full time/Part-time (Number of Hours)',
                            'Teaching Load');

        // Processing Checklist for Renewal of Government Permit
        $mainReqIdfk = array(27,
                            27,
                            27,

                            28,
                            28,
                            28,

                            29,
                            29,
                            29,
                            29,
                            29,
                            29,
                            29,
                            29,
                            29,
                            29);

        $schoolYear = '2019 - 2020';

        $conn1 = DB::connection('mysql')->getPDO();
        $stmt = $conn1 -> prepare('INSERT INTO `subrequirements1`
                                    (`description`, `mainReqIdfk`, `schoolYear`)
                                    VALUES (:description, :mainReqIdfk, :schoolYear)');
        $i = 0;
        foreach($description as $row){
            $stmt->bindParam(':description',$row);
            $stmt->bindParam(':mainReqIdfk',$mainReqIdfk[$i]);
            $stmt->bindParam(':schoolYear',$schoolYear);
            $stmt->execute();
            $i++;
        }
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subrequirements1');
    }
}
