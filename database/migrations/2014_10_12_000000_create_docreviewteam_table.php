<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocReviewTeamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('docreviewteam', function (Blueprint $table) {
            $table->increments('docReviewTeamId');
            $table->integer('prStatusIdfk');
            $table->integer('stagesIdfk');
            $table->integer('inspectionMemberCIdfk');
            $table->string('inspectionMemberIdfk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('docreviewteam');
    }
}
