<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('fileuploads');
        Schema::create('fileuploads', function (Blueprint $table) {
            $table->increments('fileId');
            $table->string('originalFileName');
            $table->string('documentTitle');
            $table->string('newFileName');
            $table->integer('fileSize');
            $table->integer('uploadedBy');
            $table->dateTime('dateUploaded');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fileuploads');
    }
}
