<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermitRecognitionDocReviewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('permitrecognitiondocreview');
        Schema::create('permitrecognitiondocreview', function (Blueprint $table) {
            $table->increments('prDocReviewId');
            $table->integer('prStatusIdfk');
            $table->integer('mainReqidfk');
            $table->integer('subReq1Idfk')->default(0);
            $table->integer('subReq2Idfk')->default(0);
            $table->integer('fileUploadStatus')->default(0);
            $table->text('requirementRemarks')->nullable();
            $table->integer('evaluationStatus')->default(0);
            $table->text('evaluationRemarks')->nullable();
            $table->integer('inspectionStatus')->default(0);
            $table->text('inspectionRemarks')->nullable();
            $table->integer('roReviewStatus')->default(0);
            $table->text('roReviewRemarks')->nullable();
            $table->integer('inspectionROStatus')->default(0);
            $table->text('inspectionRORemarks')->nullable();
            $table->integer('complete')->default(0);
            $table->string('remarks', 255)->nullable();
            $table->integer('fileIdfk')->default(0);
            $table->integer('updatedBy')->default(0);
            $table->dateTime('lastDateUpdated');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permitrecognitiondocreview');
    }
}
