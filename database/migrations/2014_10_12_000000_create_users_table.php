<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('users');
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('schoolId', 255)->nullable();
            $table->string('name', 255);
            $table->string('previousName', 255)->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password', 255);
            $table->string('region', 255)->default('CAR');
            $table->integer('officeIDfk')->default('0');
            $table->integer('schoolsDivisionIdfk')->default('0');
            $table->string('district', 255)->nullable();
            $table->string('legislativeDistrict', 255)->nullable();
            $table->string('municipality', 255)->nullable();
            $table->string('telNumber', 255)->nullable();
            $table->string('mobileNumber', 255)->nullable();
            $table->string('faxNo', 255)->nullable();
            $table->string('schoolHead', 255)->nullable();
            $table->string('designation', 255)->nullable();
            $table->date('dateEstablished')->nullable();
            $table->integer('isActive')->default('1');
            $table->integer('firstTimeLogin')->default('1');
            $table->integer('accountType')->default('3');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
