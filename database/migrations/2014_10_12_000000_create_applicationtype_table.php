<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('applicationtype');
        Schema::create('applicationtype', function (Blueprint $table) {
            $table->increments('applicationTypeId');
            $table->string('description', 255)->nullable();
        });

        $applicationtype = array('DepEd Permit (New)', 'DepEd Permit (Renewal)', 'Recognition (New)', 'Recognition (Renewal)', 'Offer Additional Course', 'Offer Night Classes', 'Tuition and Other School Fee Increase', 'Certification of Private School for Tax Exemption Status');

        $conn1 = DB::connection('mysql')->getPDO();
        $stmt = $conn1 -> prepare('INSERT INTO `applicationtype`
                                    (`description`)
                                    VALUES (:description)');
        foreach($applicationtype as $row){
            $stmt->bindParam(':description',$row);
            $stmt->execute();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applicationtype');
    }
}
