<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGradeLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gradeLevels', function (Blueprint $table) {
            $table->increments('gradeLevelId');
            $table->string('description', 255)->nullable();
            $table->integer('levelIdfk')->default('0');
        });

        $gradeLevels = array('Kindergarten', 'Grade 1', 'Grade 2', 'Grade 3', 'Grade 4', 'Grade 5', 'Grade 6', 'Grade 7', 'Grade 8', 'Grade 9', 'Grade 10', 'Grade 11', 'Grade 12');
        $levelIdfk  = array(1, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4);

        $conn1 = DB::connection('mysql')->getPDO();
        $stmt = $conn1 -> prepare('INSERT INTO `gradeLevels`
                                    (`description`,`levelIdfk`)
                                    VALUES (:description, :levelIdfk)');
        $i=0;
        foreach($gradeLevels as $row){
            $stmt->bindParam(':description',$row);
            $stmt->bindParam(':levelIdfk',$levelIdfk[$i]);
            $stmt->execute();
            $i++;
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gradeLevels');
    }
}
