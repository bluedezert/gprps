-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 07, 2019 at 08:29 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `depedcarprivateschools`
--

-- --------------------------------------------------------

--
-- Table structure for table `applicationstatus`
--

CREATE TABLE `applicationstatus` (
  `applicationStatusId` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valueNow` int(11) NOT NULL,
  `progressClass` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `applicationstatus`
--

INSERT INTO `applicationstatus` (`applicationStatusId`, `description`, `valueNow`, `progressClass`) VALUES
(1, 'Draft', 10, 'progress-bar-draft'),
(2, 'Cancelled', 100, 'progress-bar-danger'),
(3, 'Submitted', 25, 'progress-bar-submitted'),
(4, 'On Process', 50, 'progress-bar-info'),
(5, 'Approved', 100, 'progress-bar-success'),
(6, 'Disapproved', 100, 'progress-bar-warning'),
(7, 'Expired', 100, 'progress-bar-danger'),
(8, 'On Compliance', 50, 'progress-bar-draft'),
(9, 'Removed', 100, 'progress-bar-danger');

-- --------------------------------------------------------

--
-- Table structure for table `applicationtype`
--

CREATE TABLE `applicationtype` (
  `applicationTypeId` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `applicationtype`
--

INSERT INTO `applicationtype` (`applicationTypeId`, `description`) VALUES
(1, 'DepEd Permit (New)'),
(2, 'DepEd Permit (Renewal)'),
(3, 'Recognition (New)'),
(4, 'Recognition (Renewal)'),
(5, 'Offer Additional Course'),
(6, 'Offer Night Classes'),
(7, 'Tuition and Other School Fee Increase'),
(8, 'Certification of Private School for Tax Exemption Status'),
(9, 'Recognized');

-- --------------------------------------------------------

--
-- Table structure for table `categoriesshs`
--

CREATE TABLE `categoriesshs` (
  `categoryId` int(10) UNSIGNED NOT NULL,
  `category` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `isActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categoriesshs`
--

INSERT INTO `categoriesshs` (`categoryId`, `category`, `description`, `isActive`) VALUES
(1, 'A', 'Private schools, which have been granted at least Level II accreditation by any of the accrediting agencieas under the Federation of Accrediting Agencies in the Philippines.', 1),
(2, 'B', 'Non-DepED Schools, which have been issued a permit or government recognition by Commission on Higher Education (CHED) to offer any higher education program.', 1),
(3, 'C', 'Private schools, which have been granted recognition by the DepED to offer secondary education (Year I-IV/Grade 7 to 10).', 1),
(4, 'D', 'Non-DepED schools, shich have been issued a permit or recognition by the Technical Education and Skills Development Authority (TESDA) to offer any training course, and other individual, corporations, foundations or organization duly recognized by the Securities and Exchange Commission', 1);

-- --------------------------------------------------------

--
-- Table structure for table `docreviewteam`
--

CREATE TABLE `docreviewteam` (
  `docReviewTeamId` int(10) UNSIGNED NOT NULL,
  `prStatusIdfk` int(11) NOT NULL,
  `stagesIdfk` int(11) NOT NULL,
  `inspectionMemberCIdfk` int(11) NOT NULL,
  `inspectionMemberIdfk` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `docreviewteamuploads`
--

CREATE TABLE `docreviewteamuploads` (
  `docReviewFileId` int(10) UNSIGNED NOT NULL,
  `docReviewTeamIdfk` int(11) NOT NULL,
  `fileName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fileSize` int(11) NOT NULL,
  `dateUploaded` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fileuploads`
--

CREATE TABLE `fileuploads` (
  `fileId` int(10) UNSIGNED NOT NULL,
  `applicationTypeIdfk` int(11) DEFAULT NULL,
  `prStatusIdfk` int(11) DEFAULT NULL,
  `mainReqIdfk` int(11) DEFAULT NULL,
  `originalFileName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `documentTitle` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `newFileName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fileSize` int(11) NOT NULL,
  `uploadedBy` int(11) NOT NULL,
  `dateUploaded` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fileuploads`
--

INSERT INTO `fileuploads` (`fileId`, `applicationTypeIdfk`, `prStatusIdfk`, `mainReqIdfk`, `originalFileName`, `documentTitle`, `newFileName`, `fileSize`, `uploadedBy`, `dateUploaded`) VALUES
(3, 2, 1, 56, 'Program of Activities.pdf', 'Proposed Budget signed by the Board of Trustees/Directors', '69-2-1-56.pdf', 104686, 69, '2019-10-08 12:13:55'),
(4, 2, 1, 54, 'Training of Pilot Users.pdf', 'Letter of Request', '69-2-1-54.pdf', 358804, 69, '2019-10-08 12:14:03'),
(8, 2, 1, 60, 'Training of Pilot Users.pdf', 'Photocopy of Official Receipts (payments of the latest) of Employees', '69-2-1-60.pdf', 358804, 69, '2019-10-08 12:14:40'),
(9, 2, 1, 61, 'Training of Pilot Users.pdf', 'List of School Administrators with:', '69-2-1-61.pdf', 358804, 69, '2019-10-08 12:14:51'),
(10, 2, 1, 62, 'Training of Pilot Users.pdf', 'List of New Academic and Non-academic Personnel with Necessary Information', '69-2-1-62.pdf', 358804, 69, '2019-10-08 12:15:00'),
(11, 2, 1, 63, 'Training of Pilot Users.pdf', 'List of New Laboratory Facilities, Equipment, Furniture & Supplies', '69-2-1-63.pdf', 358804, 69, '2019-10-08 12:15:10'),
(12, 2, 1, 64, 'Training of Pilot Users.pdf', 'List of New additional Facilities, Equipment & Supplies', '69-2-1-64.pdf', 358804, 69, '2019-10-08 12:15:20'),
(13, 2, 1, 65, 'Training of Pilot Users.pdf', 'List of New Library Holdings', '69-2-1-65.pdf', 358804, 69, '2019-10-08 12:15:29'),
(14, 2, 1, 66, 'Training of Pilot Users.pdf', 'Documents Submitted in Four (4) copies/folders', '69-2-1-66.pdf', 358804, 69, '2019-10-08 12:15:39'),
(15, 2, 1, 55, 'Program of Activities.pdf', 'Division Ocular Inspection Report', '69-2-1-55.pdf', 104686, 69, '2019-10-08 12:18:55'),
(18, 2, 1, 58, 'Program of Activities.pdf', 'Copy of SEC Certification of Corporate Filing/Information', '69-2-1-58.pdf', 104686, 69, '2019-10-08 12:24:13'),
(19, 2, 1, 59, 'Training of Pilot Users.pdf', 'Approved Tuition and Other Fees by the Board', '69-2-1-59.pdf', 358804, 69, '2019-10-08 12:24:22'),
(21, 2, 1, 57, 'rm_no._045_s._2019.pdf', 'Copy of the Latest Audited Financial Statement Signed by an Independent Auditor (CPA)', '69-2-1-57.pdf', 835092, 69, '2019-10-08 12:40:43'),
(24, 2, 1, 0, 'Program of Activities.pdf', 'SDO Assessment', '69-2-1-SDO Assessment.pdf', 104686, 9, '2019-10-08 01:07:49'),
(25, 2, 1, 0, 'Training of Pilot Users.pdf', 'SDO Ocular Visit', '69-2-1-SDO Ocular Visit.pdf', 358804, 9, '2019-10-08 01:07:56'),
(26, 2, 1, 0, 'Program of Activities.pdf', 'RO Review', '69-2-1-RO Review.pdf', 104686, 3, '2019-10-08 01:08:18'),
(27, 3, 2, 34, 'Program of Activities.pdf', 'Board Resolution', '69-3-2-34.pdf', 104686, 69, '2019-10-08 01:18:47'),
(28, 3, 2, 35, 'Program of Activities.pdf', 'Feasibility Study', '69-3-2-35.pdf', 104686, 69, '2019-10-08 01:18:55'),
(29, 3, 2, 36, 'Training of Pilot Users.pdf', 'Articles of Incorporation', '69-3-2-36.pdf', 358804, 69, '2019-10-08 01:19:04'),
(30, 3, 2, 37, 'Program of Activities.pdf', 'Copies of Transfer Certificate of Title of the School Site', '69-3-2-37.pdf', 104686, 69, '2019-10-08 01:19:14'),
(31, 3, 2, 38, 'Training of Pilot Users.pdf', 'Campus Development and Landscaping Plans', '69-3-2-38.pdf', 358804, 69, '2019-10-08 01:19:23'),
(32, 3, 2, 39, 'Program of Activities.pdf', 'Documents of Ownership and School Building', '69-3-2-39.pdf', 104686, 69, '2019-10-08 01:19:32'),
(33, 3, 2, 40, 'Training of Pilot Users.pdf', 'Certificat of Occupancy of School Building', '69-3-2-40.pdf', 358804, 69, '2019-10-08 01:19:41'),
(34, 3, 2, 41, 'Training of Pilot Users.pdf', 'Picture of School Building, Classrooms, Laboratories, Library, Medical and Dental Health Facilities, Canteen, etc.', '69-3-2-41.pdf', 358804, 69, '2019-10-08 01:19:51'),
(35, 3, 2, 42, 'Program of Activities.pdf', 'Proposed Budget for the Succeeding School Year Approved by the Board of Trustees or Directors', '69-3-2-42.pdf', 104686, 69, '2019-10-08 01:20:00'),
(36, 3, 2, 43, 'Training of Pilot Users.pdf', 'Copy of the Latest Financial Statement of the School Certified by an Independent Certified Public Accountant', '69-3-2-43.pdf', 358804, 69, '2019-10-08 01:20:10'),
(37, 3, 2, 44, 'Training of Pilot Users.pdf', 'Proposed or Approved Curriculum', '69-3-2-44.pdf', 358804, 69, '2019-10-08 01:20:18'),
(38, 3, 2, 45, 'Training of Pilot Users.pdf', 'Approved Tuition and Other School Fees', '69-3-2-45.pdf', 358804, 69, '2019-10-08 01:20:28'),
(39, 3, 2, 53, 'Training of Pilot Users.pdf', 'Additional Requirements', '69-3-2-53.pdf', 358804, 69, '2019-10-08 01:20:40'),
(40, 3, 2, 46, 'Program of Activities.pdf', 'Retirement Plans for Teachers and Other Personnel', '69-3-2-46.pdf', 104686, 69, '2019-10-08 01:20:52'),
(41, 3, 2, 47, 'Program of Activities.pdf', 'List of School Administrators (President, Vice-President, Deans and Department Heads', '69-3-2-47.pdf', 104686, 69, '2019-10-08 01:21:02'),
(42, 3, 2, 48, 'Training of Pilot Users.pdf', 'List of Non-academic Personnel (Registrat, Librarian, Guidance Counselor, Researchers, etc.', '69-3-2-48.pdf', 358804, 69, '2019-10-08 01:21:11'),
(43, 3, 2, 49, 'Training of Pilot Users.pdf', 'List of Teaching or Academic Staff', '69-3-2-49.pdf', 358804, 69, '2019-10-08 01:21:22'),
(44, 3, 2, 50, 'Training of Pilot Users.pdf', 'List of Laboratory Facilities, Equipment, Furniture, Supplies and Materials Classified by Subject Area', '69-3-2-50.pdf', 358804, 69, '2019-10-08 01:21:32'),
(45, 3, 2, 51, 'Training of Pilot Users.pdf', 'List of Library Holdings', '69-3-2-51.pdf', 358804, 69, '2019-10-08 01:21:45'),
(46, 3, 2, 52, 'Training of Pilot Users.pdf', 'List of Athletics Facilities, Equipment, Supplies and Materials', '69-3-2-52.pdf', 358804, 69, '2019-10-08 01:21:55');

-- --------------------------------------------------------

--
-- Table structure for table `generatedcertificates`
--

CREATE TABLE `generatedcertificates` (
  `certificateId` int(10) UNSIGNED NOT NULL,
  `seriesYear` int(10) DEFAULT NULL,
  `prStatusIdfk` int(11) DEFAULT NULL,
  `applicationTypeIdfk` int(11) DEFAULT NULL,
  `governmentPRNumberfk` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `number` int(11) DEFAULT '0',
  `dateApproved` datetime DEFAULT NULL,
  `dateIssued` datetime DEFAULT NULL,
  `issuedBy` int(11) DEFAULT NULL,
  `certGenerated` int(11) DEFAULT '0',
  `regionalDirectorName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `regionalDirectorPosition` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `taskForceMember1Name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `taskForceMember1Position` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `taskForceMember2Name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `taskForceMember2Position` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `taskForceChairName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `taskForceChairPosition` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `generatedcertificates`
--

INSERT INTO `generatedcertificates` (`certificateId`, `seriesYear`, `prStatusIdfk`, `applicationTypeIdfk`, `governmentPRNumberfk`, `number`, `dateApproved`, `dateIssued`, `issuedBy`, `certGenerated`, `regionalDirectorName`, `regionalDirectorPosition`, `taskForceMember1Name`, `taskForceMember1Position`, `taskForceMember2Name`, `taskForceMember2Position`, `taskForceChairName`, `taskForceChairPosition`) VALUES
(8, 2019, 1, 2, 'QAD - P - AB - 001, s. 2019', 1, '2019-10-08 01:08:24', '2019-10-08 01:16:19', 3, 1, 'Regional Director', 'Regional Director', 'Maksim Botilas', 'EPS', 'Florence Balictan', 'EPS', 'AIDA L. PAYANG, Ed.D.', 'Chief, Quality Assurance Division');

-- --------------------------------------------------------

--
-- Table structure for table `gradelevels`
--

CREATE TABLE `gradelevels` (
  `gradeLevelId` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `levelIdfk` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gradelevels`
--

INSERT INTO `gradelevels` (`gradeLevelId`, `description`, `levelIdfk`) VALUES
(1, 'Kindergarten', 1),
(2, 'Grade 1', 2),
(3, 'Grade 2', 2),
(4, 'Grade 3', 2),
(5, 'Grade 4', 2),
(6, 'Grade 5', 2),
(7, 'Grade 6', 2),
(8, 'Grade 7', 3),
(9, 'Grade 8', 3),
(10, 'Grade 9', 3),
(11, 'Grade 10', 3),
(12, 'Grade 11', 4),
(13, 'Grade 12', 4);

-- --------------------------------------------------------

--
-- Table structure for table `inspectioncomposition`
--

CREATE TABLE `inspectioncomposition` (
  `inspectionCompositionId` int(10) UNSIGNED NOT NULL,
  `inspectionTeamIdfk` int(11) NOT NULL,
  `CM` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `inspectioncomposition`
--

INSERT INTO `inspectioncomposition` (`inspectionCompositionId`, `inspectionTeamIdfk`, `CM`, `description`, `number`) VALUES
(1, 1, 'C', 'Assistant Schools Division Superintendent', 1),
(2, 1, 'M', 'Division Focal Person for Private School', 1),
(3, 1, 'M', 'EPS/PSDS from the CID', 1),
(4, 1, 'M', 'Division Engineer', 1),
(5, 1, 'M', 'Legal Officer', 1),
(6, 2, 'C', 'QAD Chief', 1),
(7, 2, 'M', 'EPS from the QAD', 1),
(8, 2, 'M', 'EPS from CLMD', 1),
(9, 2, 'M', 'Regional Engineer', 1),
(10, 2, 'M', 'Legal Officer', 1),
(11, 1, 'M', 'Others', 1),
(12, 2, 'M', 'Others', 1);

-- --------------------------------------------------------

--
-- Table structure for table `inspectionmembers`
--

CREATE TABLE `inspectionmembers` (
  `inspectionMemberId` int(10) UNSIGNED NOT NULL,
  `inspectionCompositionId` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `inspectionteams`
--

CREATE TABLE `inspectionteams` (
  `inspectionTeamId` int(10) UNSIGNED NOT NULL,
  `shortName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `inspectionteams`
--

INSERT INTO `inspectionteams` (`inspectionTeamId`, `shortName`, `description`) VALUES
(1, 'DAIT', 'Division Assessment and Inspection Team'),
(2, 'RVT', 'Regional Validation Team');

-- --------------------------------------------------------

--
-- Table structure for table `levelofeducationoffered`
--

CREATE TABLE `levelofeducationoffered` (
  `levelId` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `levelofeducationoffered`
--

INSERT INTO `levelofeducationoffered` (`levelId`, `description`) VALUES
(1, 'Kindergarten'),
(2, 'Elementary'),
(3, 'Junior High School'),
(4, 'Senior High School');

-- --------------------------------------------------------

--
-- Table structure for table `mainrequirements`
--

CREATE TABLE `mainrequirements` (
  `mainReqId` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `applicationTypeIdfk` int(11) NOT NULL DEFAULT '0',
  `schoolYear` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mainrequirements`
--

INSERT INTO `mainrequirements` (`mainReqId`, `description`, `applicationTypeIdfk`, `schoolYear`, `isActive`) VALUES
(1, 'Board Resolution', 1, '2019 - 2020', 1),
(2, 'Feasibility Study', 1, '2019 - 2020', 1),
(3, 'Articles of Incorporation', 1, '2019 - 2020', 1),
(4, 'Copies of Transfer Certificate of Title of the School Site', 1, '2019 - 2020', 1),
(5, 'Campus Development and Landscaping Plans', 1, '2019 - 2020', 1),
(6, 'Documents of Ownership and School Building', 1, '2019 - 2020', 1),
(7, 'Certificat of Occupancy of School Building', 1, '2019 - 2020', 1),
(8, 'Picture of School Building, Classrooms, Laboratories, Library, Medical and Dental Health Facilities, Canteen, etc.', 1, '2019 - 2020', 1),
(9, 'Proposed Budget for the Succeding School Year Approved by the Board of Trustees/Directors', 1, '2019 - 2020', 1),
(10, 'Copy of the Latest Financial Statement of the School Certified by an Independent Certified Public Accountant', 1, '2019 - 2020', 1),
(11, 'Proposed/Approved Curriculum', 1, '2019 - 2020', 1),
(12, 'Approved Tuition and Other School Fees', 1, '2019 - 2020', 1),
(13, 'Retirement Plans for Teachers and Other Personnel', 1, '2019 - 2020', 1),
(14, 'List of School Administrators (President, Vice-President, Deans and Department Heads', 1, '2019 - 2020', 1),
(15, 'List of Non-academic Personnel (Registrat, Librarian, Guidance Counselor, Researchers, etc.', 1, '2019 - 2020', 1),
(16, 'List of Teaching/Academic Staff', 1, '2019 - 2020', 1),
(17, 'List of Laboratory Facilities, Equipment, Furniture, Supplies and Materials Classified by Subject Area', 1, '2019 - 2020', 1),
(18, 'List of Library Holdings', 1, '2019 - 2020', 1),
(19, 'List of Athletics Facilities, Equipment, Supplies and Materials', 1, '2019 - 2020', 1),
(20, 'Additional Requirements', 1, '2019 - 2020', 1),
(21, 'Letter of Request', 2, '2019 - 2020', 1),
(22, 'Division Ocular Inspection Report', 2, '2019 - 2020', 1),
(23, 'Proposed Budget signed by the Board of Trustees/Directors', 2, '2019 - 2020', 1),
(24, 'Copy of the Latest Audited Financial Statement Signed by an Independent Auditor (CPA)', 2, '2019 - 2020', 1),
(25, 'Copy of SEC Certification of Corporate Filing/Information', 2, '2019 - 2020', 1),
(26, 'Approved Tuition and Other Fees by the Board', 2, '2019 - 2020', 1),
(27, 'Photocopy of Official Receipts (payments of the latest) of Employees', 2, '2019 - 2020', 1),
(28, 'List of School Administrators with:', 2, '2019 - 2020', 1),
(29, 'List of New Academic and Non-academic Personnel with Necessary Information', 2, '2019 - 2020', 1),
(30, 'List of New Laboratory Facilities, Equipment, Furniture & Supplies', 2, '2019 - 2020', 1),
(31, 'List of New additional Facilities, Equipment & Supplies', 2, '2019 - 2020', 1),
(32, 'List of New Library Holdings', 2, '2019 - 2020', 1),
(33, 'Documents Submitted in Four (4) copies/folders', 2, '2019 - 2020', 1),
(34, 'Board Resolution', 1, '2020 - 2021', 1),
(35, 'Feasibility Study', 1, '2020 - 2021', 1),
(36, 'Articles of Incorporation', 1, '2020 - 2021', 1),
(37, 'Copies of Transfer Certificate of Title of the School Site', 1, '2020 - 2021', 1),
(38, 'Campus Development and Landscaping Plans', 1, '2020 - 2021', 1),
(39, 'Documents of Ownership and School Building', 1, '2020 - 2021', 1),
(40, 'Certificat of Occupancy of School Building', 1, '2020 - 2021', 1),
(41, 'Picture of School Building, Classrooms, Laboratories, Library, Medical and Dental Health Facilities, Canteen, etc.', 1, '2020 - 2021', 1),
(42, 'Proposed Budget for the Succeeding School Year Approved by the Board of Trustees or Directors', 1, '2020 - 2021', 1),
(43, 'Copy of the Latest Financial Statement of the School Certified by an Independent Certified Public Accountant', 1, '2020 - 2021', 1),
(44, 'Proposed or Approved Curriculum', 1, '2020 - 2021', 1),
(45, 'Approved Tuition and Other School Fees', 1, '2020 - 2021', 1),
(46, 'Retirement Plans for Teachers and Other Personnel', 1, '2020 - 2021', 1),
(47, 'List of School Administrators (President, Vice-President, Deans and Department Heads', 1, '2020 - 2021', 1),
(48, 'List of Non-academic Personnel (Registrat, Librarian, Guidance Counselor, Researchers, etc.', 1, '2020 - 2021', 1),
(49, 'List of Teaching or Academic Staff', 1, '2020 - 2021', 1),
(50, 'List of Laboratory Facilities, Equipment, Furniture, Supplies and Materials Classified by Subject Area', 1, '2020 - 2021', 1),
(51, 'List of Library Holdings', 1, '2020 - 2021', 1),
(52, 'List of Athletics Facilities, Equipment, Supplies and Materials', 1, '2020 - 2021', 1),
(53, 'Additional Requirements', 1, '2020 - 2021', 1),
(54, 'Letter of Request', 2, '2020 - 2021', 1),
(55, 'Division Ocular Inspection Report', 2, '2020 - 2021', 1),
(56, 'Proposed Budget signed by the Board of Trustees/Directors', 2, '2020 - 2021', 1),
(57, 'Copy of the Latest Audited Financial Statement Signed by an Independent Auditor (CPA)', 2, '2020 - 2021', 1),
(58, 'Copy of SEC Certification of Corporate Filing/Information', 2, '2020 - 2021', 1),
(59, 'Approved Tuition and Other Fees by the Board', 2, '2020 - 2021', 1),
(60, 'Photocopy of Official Receipts (payments of the latest) of Employees', 2, '2020 - 2021', 1),
(61, 'List of School Administrators with:', 2, '2020 - 2021', 1),
(62, 'List of New Academic and Non-academic Personnel with Necessary Information', 2, '2020 - 2021', 1),
(63, 'List of New Laboratory Facilities, Equipment, Furniture & Supplies', 2, '2020 - 2021', 1),
(64, 'List of New additional Facilities, Equipment & Supplies', 2, '2020 - 2021', 1),
(65, 'List of New Library Holdings', 2, '2020 - 2021', 1),
(66, 'Documents Submitted in Four (4) copies/folders', 2, '2020 - 2021', 1),
(67, 'testing1', 1, '2020 - 2021', 0);

-- --------------------------------------------------------

--
-- Table structure for table `mainrequirements_shs`
--

CREATE TABLE `mainrequirements_shs` (
  `mainReqId` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `applicationTypeIdfk` int(11) NOT NULL DEFAULT '0',
  `schoolYear` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mainrequirements_shs`
--

INSERT INTO `mainrequirements_shs` (`mainReqId`, `description`, `applicationTypeIdfk`, `schoolYear`, `isActive`) VALUES
(1, 'Letter of Intent', 1, '2019 - 2020', 1),
(2, 'Board Resolution certified by the secretary and approved by the Board of Directors/Board of Trustees', 1, '2019 - 2020', 1),
(3, 'Certificate of Recognition of any of the Following', 1, '2019 - 2020', 1),
(4, 'Proposed tuition and other fees', 1, '2019 - 2020', 1),
(5, 'Proposed school calendar', 1, '2019 - 2020', 1),
(6, 'Proposed list of academin and non-academic personnel', 1, '2019 - 2020', 1),
(7, 'Curriculum Offering', 1, '2019 - 2020', 1),
(8, 'Minimum program requirements for the SHS tracks/strands', 1, '2019 - 2020', 1),
(9, 'A copy of Memorandum/Memoranda of Agreement/Memorandum of Unserstanding for partnership and arrangements relative to the SHS Program Implementation. These arrangements may include:', 1, '2019 - 2020', 1),
(10, 'Articles of Incorporation and by-laws for private schools only', 1, '2019 - 2020', 1),
(11, 'Document of ownership of school sites under the name of the school, or deed of usufruct', 1, '2019 - 2020', 1),
(12, 'Proposed annual budget and annual expenditures', 1, '2019 - 2020', 1),
(13, 'Letter of Intent', 1, '2020 - 2021', 1),
(14, 'Board Resolution certified by the secretary and approved by the Board of Directors/Board of Trustees', 1, '2020 - 2021', 1),
(15, 'Certificate of Recognition of any of the Following', 1, '2020 - 2021', 1),
(16, 'Proposed tuition and other fees', 1, '2020 - 2021', 1),
(17, 'Proposed school calendar', 1, '2020 - 2021', 1),
(18, 'Proposed list of academin and non-academic personnel', 1, '2020 - 2021', 1),
(19, 'Curriculum Offering', 1, '2020 - 2021', 1),
(20, 'Minimum program requirements for the SHS tracks/strands', 1, '2020 - 2021', 1),
(21, 'A copy of Memorandum/Memoranda of Agreement/Memorandum of Unserstanding for partnership and arrangements relative to the SHS Program Implementation. These arrangements may include:', 1, '2020 - 2021', 1),
(22, 'Articles of Incorporation and by-laws for private schools only', 1, '2020 - 2021', 1),
(23, 'Document of ownership of school sites under the name of the school, or deed of usufruct', 1, '2020 - 2021', 1),
(24, 'Proposed annual budget and annual expenditures', 1, '2020 - 2021', 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(28, '2014_10_12_000000_create_categoriesshs_table', 1),
(29, '2014_10_12_000000_create_docreviewteam_table', 1),
(30, '2014_10_12_000000_create_docreviewteamuploads_table', 1),
(32, '2014_10_12_000000_create_generatedcertificates_table', 1),
(33, '2014_10_12_000000_create_gradelevels_table', 1),
(34, '2014_10_12_000000_create_inspectioncomposition_table', 1),
(35, '2014_10_12_000000_create_inspectionmembers_table', 1),
(36, '2014_10_12_000000_create_inspectionteams_table', 1),
(37, '2014_10_12_000000_create_levelofeducationoffered_table', 1),
(41, '2014_10_12_000000_create_permitrecognitionstatus_table', 1),
(44, '2014_10_12_000000_create_stagestimeline_table', 1),
(50, '2014_10_12_100000_create_password_resets_table', 1),
(53, '2014_10_12_000000_create_users_table', 3),
(56, '2014_10_12_000000_create_stages_table', 5),
(57, '2014_10_12_000000_create_applicationtype_table', 6),
(58, '2014_10_12_000000_create_fileuploads_table', 7),
(60, '2014_10_12_000000_create_mainrequirements_table', 8),
(61, '2014_10_12_000000_create_subrequirements1_shs_table', 8),
(62, '2014_10_12_000000_create_subrequirements1_table', 8),
(63, '2014_10_12_000000_create_subrequirements2_shs_table', 8),
(64, '2014_10_12_000000_create_subrequirements2_table', 8),
(66, '2014_10_12_000000_create_mainrequirements_shs_table', 9),
(68, '2014_10_12_000000_create_permitrecognitiondocreview_table', 11),
(71, '2014_10_12_000000_create_applicationstatus_table', 12),
(72, '2014_10_12_000000_create_stagesstatus_table', 12);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permitrecognitiondocreview`
--

CREATE TABLE `permitrecognitiondocreview` (
  `prDocReviewId` int(10) UNSIGNED NOT NULL,
  `prStatusIdfk` int(11) NOT NULL,
  `mainReqidfk` int(11) NOT NULL,
  `subReq1Idfk` int(11) NOT NULL DEFAULT '0',
  `subReq2Idfk` int(11) NOT NULL DEFAULT '0',
  `fileUploadStatus` int(11) NOT NULL DEFAULT '0',
  `requirementRemarks` text COLLATE utf8mb4_unicode_ci,
  `evaluationStatus` int(11) NOT NULL DEFAULT '0',
  `evaluationRemarks` text COLLATE utf8mb4_unicode_ci,
  `inspectionStatus` int(11) NOT NULL DEFAULT '0',
  `inspectionRemarks` text COLLATE utf8mb4_unicode_ci,
  `roReviewStatus` int(11) NOT NULL DEFAULT '0',
  `roReviewRemarks` text COLLATE utf8mb4_unicode_ci,
  `inspectionROStatus` int(11) NOT NULL DEFAULT '0',
  `inspectionRORemarks` text COLLATE utf8mb4_unicode_ci,
  `complete` int(11) NOT NULL DEFAULT '0',
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fileIdfk` int(11) NOT NULL DEFAULT '0',
  `updatedBy` int(11) NOT NULL DEFAULT '0',
  `lastDateUpdated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permitrecognitiondocreview`
--

INSERT INTO `permitrecognitiondocreview` (`prDocReviewId`, `prStatusIdfk`, `mainReqidfk`, `subReq1Idfk`, `subReq2Idfk`, `fileUploadStatus`, `requirementRemarks`, `evaluationStatus`, `evaluationRemarks`, `inspectionStatus`, `inspectionRemarks`, `roReviewStatus`, `roReviewRemarks`, `inspectionROStatus`, `inspectionRORemarks`, `complete`, `remarks`, `fileIdfk`, `updatedBy`, `lastDateUpdated`) VALUES
(3, 1, 56, 0, 0, 1, NULL, 1, NULL, 1, NULL, 1, NULL, 0, NULL, 0, 'File Uploaded: 69-2-1-56.pdf', 3, 69, '2019-10-08 12:13:55'),
(4, 1, 54, 0, 0, 1, NULL, 1, NULL, 1, NULL, 1, NULL, 0, NULL, 0, 'File Uploaded: 69-2-1-54.pdf', 4, 69, '2019-10-08 12:14:03'),
(8, 1, 60, 0, 0, 1, NULL, 1, NULL, 1, NULL, 1, NULL, 0, NULL, 0, 'File Uploaded: 69-2-1-60.pdf', 8, 69, '2019-10-08 12:14:40'),
(9, 1, 60, 111, 0, 1, NULL, 1, NULL, 1, NULL, 1, NULL, 0, NULL, 0, 'File Uploaded: 69-2-1-60.pdf', 8, 69, '2019-10-08 12:14:40'),
(10, 1, 60, 112, 0, 1, NULL, 1, NULL, 1, NULL, 1, NULL, 0, NULL, 0, 'File Uploaded: 69-2-1-60.pdf', 8, 69, '2019-10-08 12:14:40'),
(11, 1, 60, 113, 0, 1, NULL, 1, NULL, 1, NULL, 1, NULL, 0, NULL, 0, 'File Uploaded: 69-2-1-60.pdf', 8, 69, '2019-10-08 12:14:40'),
(12, 1, 61, 0, 0, 1, NULL, 1, NULL, 1, NULL, 1, NULL, 0, NULL, 0, 'File Uploaded: 69-2-1-61.pdf', 9, 69, '2019-10-08 12:14:51'),
(13, 1, 61, 114, 0, 1, NULL, 1, NULL, 1, NULL, 1, NULL, 0, NULL, 0, 'File Uploaded: 69-2-1-61.pdf', 9, 69, '2019-10-08 12:14:51'),
(14, 1, 61, 115, 0, 1, NULL, 1, NULL, 1, NULL, 1, NULL, 0, NULL, 0, 'File Uploaded: 69-2-1-61.pdf', 9, 69, '2019-10-08 12:14:51'),
(15, 1, 61, 116, 0, 1, NULL, 1, NULL, 1, NULL, 1, NULL, 0, NULL, 0, 'File Uploaded: 69-2-1-61.pdf', 9, 69, '2019-10-08 12:14:51'),
(16, 1, 62, 0, 0, 1, NULL, 1, NULL, 1, NULL, 1, NULL, 0, NULL, 0, 'File Uploaded: 69-2-1-62.pdf', 10, 69, '2019-10-08 12:15:00'),
(17, 1, 62, 117, 0, 1, NULL, 1, NULL, 1, NULL, 1, NULL, 0, NULL, 0, 'File Uploaded: 69-2-1-62.pdf', 10, 69, '2019-10-08 12:15:00'),
(18, 1, 62, 118, 0, 1, NULL, 1, NULL, 1, NULL, 1, NULL, 0, NULL, 0, 'File Uploaded: 69-2-1-62.pdf', 10, 69, '2019-10-08 12:15:00'),
(19, 1, 62, 119, 0, 1, NULL, 1, NULL, 1, NULL, 1, NULL, 0, NULL, 0, 'File Uploaded: 69-2-1-62.pdf', 10, 69, '2019-10-08 12:15:00'),
(20, 1, 62, 120, 0, 1, NULL, 1, NULL, 1, NULL, 1, NULL, 0, NULL, 0, 'File Uploaded: 69-2-1-62.pdf', 10, 69, '2019-10-08 12:15:00'),
(21, 1, 62, 121, 0, 1, NULL, 1, NULL, 1, NULL, 1, NULL, 0, NULL, 0, 'File Uploaded: 69-2-1-62.pdf', 10, 69, '2019-10-08 12:15:00'),
(22, 1, 62, 122, 0, 1, NULL, 1, NULL, 1, NULL, 1, NULL, 0, NULL, 0, 'File Uploaded: 69-2-1-62.pdf', 10, 69, '2019-10-08 12:15:00'),
(23, 1, 62, 123, 0, 1, NULL, 1, NULL, 1, NULL, 1, NULL, 0, NULL, 0, 'File Uploaded: 69-2-1-62.pdf', 10, 69, '2019-10-08 12:15:00'),
(24, 1, 62, 124, 0, 1, NULL, 1, NULL, 1, NULL, 1, NULL, 0, NULL, 0, 'File Uploaded: 69-2-1-62.pdf', 10, 69, '2019-10-08 12:15:00'),
(25, 1, 62, 125, 0, 1, NULL, 1, NULL, 1, NULL, 1, NULL, 0, NULL, 0, 'File Uploaded: 69-2-1-62.pdf', 10, 69, '2019-10-08 12:15:00'),
(26, 1, 62, 126, 0, 1, NULL, 1, NULL, 1, NULL, 1, NULL, 0, NULL, 0, 'File Uploaded: 69-2-1-62.pdf', 10, 69, '2019-10-08 12:15:00'),
(27, 1, 63, 0, 0, 1, NULL, 1, NULL, 1, NULL, 1, NULL, 0, NULL, 0, 'File Uploaded: 69-2-1-63.pdf', 11, 69, '2019-10-08 12:15:10'),
(28, 1, 64, 0, 0, 1, NULL, 1, NULL, 1, NULL, 1, NULL, 0, NULL, 0, 'File Uploaded: 69-2-1-64.pdf', 12, 69, '2019-10-08 12:15:20'),
(29, 1, 65, 0, 0, 1, NULL, 1, NULL, 1, NULL, 1, NULL, 0, NULL, 0, 'File Uploaded: 69-2-1-65.pdf', 13, 69, '2019-10-08 12:15:29'),
(30, 1, 66, 0, 0, 1, NULL, 1, NULL, 1, NULL, 1, NULL, 0, NULL, 0, 'File Uploaded: 69-2-1-66.pdf', 14, 69, '2019-10-08 12:15:39'),
(31, 1, 55, 0, 0, 1, NULL, 1, NULL, 1, NULL, 1, NULL, 0, NULL, 0, 'File Uploaded: 69-2-1-55.pdf', 15, 69, '2019-10-08 12:18:55'),
(33, 1, 58, 0, 0, 1, NULL, 1, NULL, 1, NULL, 1, NULL, 0, NULL, 0, 'File Uploaded: 69-2-1-58.pdf', 18, 69, '2019-10-08 12:24:13'),
(34, 1, 59, 0, 0, 1, NULL, 1, NULL, 1, NULL, 1, NULL, 0, NULL, 0, 'File Uploaded: 69-2-1-59.pdf', 19, 69, '2019-10-08 12:24:22'),
(35, 1, 57, 0, 0, 1, NULL, 1, NULL, 1, NULL, 1, NULL, 0, NULL, 0, 'File Uploaded: 69-2-1-57.pdf', 21, 69, '2019-10-08 12:40:43'),
(36, 2, 34, 0, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-34.pdf', 27, 69, '2019-10-08 01:18:47'),
(37, 2, 34, 64, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-34.pdf', 27, 69, '2019-10-08 01:18:47'),
(38, 2, 35, 0, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-35.pdf', 28, 69, '2019-10-08 01:18:55'),
(39, 2, 35, 65, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-35.pdf', 28, 69, '2019-10-08 01:18:55'),
(40, 2, 35, 66, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-35.pdf', 28, 69, '2019-10-08 01:18:55'),
(41, 2, 35, 67, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-35.pdf', 28, 69, '2019-10-08 01:18:55'),
(42, 2, 35, 68, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-35.pdf', 28, 69, '2019-10-08 01:18:55'),
(43, 2, 35, 69, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-35.pdf', 28, 69, '2019-10-08 01:18:55'),
(44, 2, 35, 69, 4, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-35.pdf', 28, 69, '2019-10-08 01:18:55'),
(45, 2, 35, 69, 5, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-35.pdf', 28, 69, '2019-10-08 01:18:55'),
(46, 2, 35, 69, 6, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-35.pdf', 28, 69, '2019-10-08 01:18:55'),
(47, 2, 36, 0, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-36.pdf', 29, 69, '2019-10-08 01:19:04'),
(48, 2, 36, 70, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-36.pdf', 29, 69, '2019-10-08 01:19:04'),
(49, 2, 36, 71, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-36.pdf', 29, 69, '2019-10-08 01:19:04'),
(50, 2, 36, 72, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-36.pdf', 29, 69, '2019-10-08 01:19:04'),
(51, 2, 36, 73, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-36.pdf', 29, 69, '2019-10-08 01:19:04'),
(52, 2, 37, 0, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-37.pdf', 30, 69, '2019-10-08 01:19:14'),
(53, 2, 37, 74, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-37.pdf', 30, 69, '2019-10-08 01:19:14'),
(54, 2, 37, 75, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-37.pdf', 30, 69, '2019-10-08 01:19:14'),
(55, 2, 38, 0, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-38.pdf', 31, 69, '2019-10-08 01:19:23'),
(56, 2, 38, 76, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-38.pdf', 31, 69, '2019-10-08 01:19:23'),
(57, 2, 38, 77, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-38.pdf', 31, 69, '2019-10-08 01:19:23'),
(58, 2, 38, 78, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-38.pdf', 31, 69, '2019-10-08 01:19:23'),
(59, 2, 39, 0, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-39.pdf', 32, 69, '2019-10-08 01:19:32'),
(60, 2, 39, 79, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-39.pdf', 32, 69, '2019-10-08 01:19:32'),
(61, 2, 39, 80, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-39.pdf', 32, 69, '2019-10-08 01:19:32'),
(62, 2, 40, 0, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-40.pdf', 33, 69, '2019-10-08 01:19:41'),
(63, 2, 40, 81, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-40.pdf', 33, 69, '2019-10-08 01:19:41'),
(64, 2, 41, 0, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-41.pdf', 34, 69, '2019-10-08 01:19:51'),
(65, 2, 41, 82, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-41.pdf', 34, 69, '2019-10-08 01:19:51'),
(66, 2, 41, 83, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-41.pdf', 34, 69, '2019-10-08 01:19:51'),
(67, 2, 42, 0, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-42.pdf', 35, 69, '2019-10-08 01:20:00'),
(68, 2, 43, 0, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-43.pdf', 36, 69, '2019-10-08 01:20:10'),
(69, 2, 44, 0, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-44.pdf', 37, 69, '2019-10-08 01:20:18'),
(70, 2, 44, 84, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-44.pdf', 37, 69, '2019-10-08 01:20:18'),
(71, 2, 45, 0, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-45.pdf', 38, 69, '2019-10-08 01:20:28'),
(72, 2, 45, 85, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-45.pdf', 38, 69, '2019-10-08 01:20:28'),
(73, 2, 45, 86, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-45.pdf', 38, 69, '2019-10-08 01:20:28'),
(74, 2, 53, 0, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-53.pdf', 39, 69, '2019-10-08 01:20:40'),
(75, 2, 53, 106, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-53.pdf', 39, 69, '2019-10-08 01:20:40'),
(76, 2, 53, 107, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-53.pdf', 39, 69, '2019-10-08 01:20:40'),
(77, 2, 53, 108, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-53.pdf', 39, 69, '2019-10-08 01:20:40'),
(78, 2, 53, 109, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-53.pdf', 39, 69, '2019-10-08 01:20:40'),
(79, 2, 53, 110, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-53.pdf', 39, 69, '2019-10-08 01:20:40'),
(80, 2, 46, 0, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-46.pdf', 40, 69, '2019-10-08 01:20:52'),
(81, 2, 46, 87, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-46.pdf', 40, 69, '2019-10-08 01:20:52'),
(82, 2, 47, 0, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-47.pdf', 41, 69, '2019-10-08 01:21:02'),
(83, 2, 47, 88, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-47.pdf', 41, 69, '2019-10-08 01:21:02'),
(84, 2, 47, 89, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-47.pdf', 41, 69, '2019-10-08 01:21:02'),
(85, 2, 48, 0, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-48.pdf', 42, 69, '2019-10-08 01:21:11'),
(86, 2, 48, 90, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-48.pdf', 42, 69, '2019-10-08 01:21:11'),
(87, 2, 48, 91, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-48.pdf', 42, 69, '2019-10-08 01:21:11'),
(88, 2, 48, 92, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-48.pdf', 42, 69, '2019-10-08 01:21:11'),
(89, 2, 48, 93, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-48.pdf', 42, 69, '2019-10-08 01:21:11'),
(90, 2, 48, 94, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-48.pdf', 42, 69, '2019-10-08 01:21:11'),
(91, 2, 48, 95, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-48.pdf', 42, 69, '2019-10-08 01:21:11'),
(92, 2, 49, 0, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-49.pdf', 43, 69, '2019-10-08 01:21:22'),
(93, 2, 49, 96, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-49.pdf', 43, 69, '2019-10-08 01:21:22'),
(94, 2, 49, 97, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-49.pdf', 43, 69, '2019-10-08 01:21:22'),
(95, 2, 49, 98, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-49.pdf', 43, 69, '2019-10-08 01:21:22'),
(96, 2, 49, 99, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-49.pdf', 43, 69, '2019-10-08 01:21:22'),
(97, 2, 49, 100, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-49.pdf', 43, 69, '2019-10-08 01:21:22'),
(98, 2, 50, 0, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-50.pdf', 44, 69, '2019-10-08 01:21:32'),
(99, 2, 50, 101, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-50.pdf', 44, 69, '2019-10-08 01:21:32'),
(100, 2, 51, 0, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-51.pdf', 45, 69, '2019-10-08 01:21:45'),
(101, 2, 51, 102, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-51.pdf', 45, 69, '2019-10-08 01:21:45'),
(102, 2, 51, 103, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-51.pdf', 45, 69, '2019-10-08 01:21:45'),
(103, 2, 52, 0, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-52.pdf', 46, 69, '2019-10-08 01:21:55'),
(104, 2, 52, 104, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-52.pdf', 46, 69, '2019-10-08 01:21:55'),
(105, 2, 52, 105, 0, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 'File Uploaded: 69-3-2-52.pdf', 46, 69, '2019-10-08 01:21:55');

-- --------------------------------------------------------

--
-- Table structure for table `permitrecognitionstatus`
--

CREATE TABLE `permitrecognitionstatus` (
  `prStatusId` int(10) UNSIGNED NOT NULL,
  `applicationTypeIdfk` int(11) NOT NULL,
  `schoolUserIdfk` int(11) NOT NULL,
  `gradeLevelIdfkFrom` int(11) NOT NULL,
  `gradeLevelIdfkTo` int(11) NOT NULL,
  `governmentPRNumber` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seriesYear` int(11) NOT NULL,
  `schoolYear` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `applicationStatusIdfk` int(11) NOT NULL,
  `categoryIdfk` int(11) NOT NULL DEFAULT '0',
  `lastDateUpdated` datetime NOT NULL,
  `controlNumber` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permitrecognitionstatus`
--

INSERT INTO `permitrecognitionstatus` (`prStatusId`, `applicationTypeIdfk`, `schoolUserIdfk`, `gradeLevelIdfkFrom`, `gradeLevelIdfkTo`, `governmentPRNumber`, `seriesYear`, `schoolYear`, `applicationStatusIdfk`, `categoryIdfk`, `lastDateUpdated`, `controlNumber`) VALUES
(1, 2, 69, 1, 7, 'QAD - P - AB - 001, s. 2019', 2019, '2020 - 2021', 5, 0, '2019-10-08 00:15:43', 'PR-406102-2020 - 2021-0-6'),
(2, 3, 69, 1, 7, '', 2019, '2020 - 2021', 3, 0, '2019-10-08 01:22:01', 'RN-406102-2020 - 2021-0-6');

-- --------------------------------------------------------

--
-- Table structure for table `stages`
--

CREATE TABLE `stages` (
  `stageId` int(10) UNSIGNED NOT NULL,
  `sequence` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `schedule` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bgColor` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stages`
--

INSERT INTO `stages` (`stageId`, `sequence`, `description`, `schedule`, `bgColor`) VALUES
(1, 1, 'Application', 'On or before August 30', 'bg-purple'),
(2, 2, 'Document Assessment & Evaluation', 'August 30 to September 30', 'bg-blue'),
(3, 3, 'SDO Ocular Inspection', 'On or before October 30', 'bg-aqua'),
(4, 4, 'RO Review', 'On or before November 30', 'bg-yellow'),
(5, 5, 'Payment and Issuance', 'On or before February 28', 'bg-green');

-- --------------------------------------------------------

--
-- Table structure for table `stagesstatus`
--

CREATE TABLE `stagesstatus` (
  `stageStatusId` int(10) UNSIGNED NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stageIdfk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stagesstatus`
--

INSERT INTO `stagesstatus` (`stageStatusId`, `description`, `stageIdfk`) VALUES
(1, 'Draft Application', 1),
(2, 'Application Cancelled', 1),
(3, 'Submitted for Evaluation', 1),
(4, 'On Assessment', 2),
(5, 'Notified For Compliance', 2),
(6, 'With Complete Documents', 2),
(7, 'For Inspection', 2),
(8, 'Compliant for Endorsement', 2),
(9, 'Endorsed to Regional Office For Review', 2),
(10, 'Compliant for Endorsement', 3),
(11, 'For Re-Inspection', 3),
(12, 'Endorsed to Regional Office For Review', 3),
(13, 'On Review', 4),
(14, 'Notified For Compliance', 4),
(15, 'For Inspection', 4),
(16, 'Compliant for Issuance of Permit to Operate', 4),
(17, 'Compliant for Issuance of Recognition', 4),
(18, 'For Payment and Issuance', 4),
(19, 'Paid', 5),
(20, 'Issued Government Permit', 5),
(21, 'Issued Government Recognition', 5),
(22, 'Approved', 5),
(23, 'Disapproved', 5),
(24, 'Submitted for Completion', 1),
(25, 'Generated Certificate', 1);

-- --------------------------------------------------------

--
-- Table structure for table `stagestimeline`
--

CREATE TABLE `stagestimeline` (
  `stageTimelineId` int(10) UNSIGNED NOT NULL,
  `prStatusIdfk` int(11) NOT NULL,
  `stageIdfk` int(11) NOT NULL,
  `stageStatusIdfk` int(11) NOT NULL DEFAULT '0',
  `updatedBy` int(11) NOT NULL,
  `lastDateUpdated` datetime NOT NULL,
  `remarks` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `docReviewFileIdfk` int(11) DEFAULT NULL,
  `fileIdfk` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stagestimeline`
--

INSERT INTO `stagestimeline` (`stageTimelineId`, `prStatusIdfk`, `stageIdfk`, `stageStatusIdfk`, `updatedBy`, `lastDateUpdated`, `remarks`, `docReviewFileIdfk`, `fileIdfk`) VALUES
(1, 1, 1, 1, 69, '2019-10-08 00:13:15', 'Draft Application', 0, 0),
(2, 1, 1, 3, 69, '2019-10-08 00:15:43', 'Submitted Application', 0, 0),
(3, 1, 2, 4, 9, '2019-10-08 00:17:24', 'Assess Application', 0, 0),
(4, 1, 2, 5, 9, '2019-10-08 00:18:20', 'Notify for Compliance', 0, 0),
(5, 1, 1, 24, 69, '2019-10-08 00:19:00', 'Submitted Completion', 0, 0),
(6, 1, 2, 4, 9, '2019-10-08 00:19:33', 'Assess Application', 0, 0),
(7, 1, 2, 6, 9, '2019-10-08 00:20:44', 'With Complete Documents', 0, 0),
(8, 1, 2, 7, 9, '2019-10-08 00:20:53', 'For Inspection', 0, 0),
(9, 1, 2, 5, 9, '2019-10-08 00:22:24', 'Notify for Compliance', 0, 0),
(10, 1, 1, 24, 69, '2019-10-08 00:24:27', 'Submitted Completion', 0, 0),
(11, 1, 2, 4, 9, '2019-10-08 00:36:34', 'Assess Application', 0, 0),
(12, 1, 2, 6, 9, '2019-10-08 00:37:14', 'With Complete Documents', 0, 0),
(13, 1, 2, 7, 9, '2019-10-08 00:38:22', 'For Inspection', 0, 0),
(14, 1, 2, 5, 9, '2019-10-08 00:39:45', 'Notify for Compliance', 0, 0),
(15, 1, 1, 24, 69, '2019-10-08 00:40:50', 'Submitted Completion', 0, 0),
(16, 1, 2, 4, 9, '2019-10-08 00:41:15', 'Assess Application', 0, 0),
(17, 1, 2, 6, 9, '2019-10-08 00:41:59', 'With Complete Documents', 0, 0),
(18, 1, 2, 7, 9, '2019-10-08 00:44:43', 'For Inspection', 0, 0),
(19, 1, 3, 10, 9, '2019-10-08 00:52:18', 'Compliant for Endorsement to the Regional Office', 0, 0),
(20, 1, 3, 12, 9, '2019-10-08 00:52:18', 'Endorsed to the Regional Office for Review', 0, 0),
(21, 1, 4, 13, 3, '2019-10-08 01:03:49', 'Review Application', 0, 0),
(22, 1, 4, 14, 3, '2019-10-08 01:04:46', 'Notify for Compliance', 0, 0),
(23, 1, 4, 13, 69, '2019-10-08 01:06:23', 'Submitted Completion', 0, 0),
(24, 1, 5, 16, 3, '2019-10-08 01:08:24', 'Compliant for Issuance of Permit to Operate', 0, 0),
(25, 1, 5, 22, 3, '2019-10-08 01:08:24', 'Approved Application', 0, 0),
(26, 1, 5, 25, 3, '2019-10-08 01:09:13', 'Generated Certificate', 0, 0),
(27, 1, 5, 18, 3, '2019-10-08 01:09:46', 'For Payment and Issuance of Certification', 0, 0),
(28, 1, 5, 19, 3, '2019-10-08 01:10:24', 'Paid', 0, 0),
(29, 1, 5, 20, 3, '2019-10-08 01:10:34', 'Issued Government Permit to Operate', 0, 0),
(30, 1, 5, 20, 3, '2019-10-08 01:16:19', 'Issued Government Permit to Operate', 0, 0),
(31, 2, 1, 1, 69, '2019-10-08 01:18:20', 'Draft Application', 0, 0),
(32, 2, 1, 3, 69, '2019-10-08 01:22:01', 'Submitted Application', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `subrequirements1`
--

CREATE TABLE `subrequirements1` (
  `subReq1Id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mainReqIdfk` int(11) NOT NULL,
  `schoolYear` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subrequirements1`
--

INSERT INTO `subrequirements1` (`subReq1Id`, `description`, `mainReqIdfk`, `schoolYear`, `isActive`) VALUES
(1, 'Certified by Corporate Secretary', 1, '2019 - 2020', 1),
(2, 'Purpose', 2, '2019 - 2020', 1),
(3, 'Need and relevance of school applied for', 2, '2019 - 2020', 1),
(4, 'Resources and capabilities of the Corporate/School', 2, '2019 - 2020', 1),
(5, 'Programs and Projects', 2, '2019 - 2020', 1),
(6, 'Location of School in Relation to its Environment', 2, '2019 - 2020', 1),
(7, 'in the Name of the School', 3, '2019 - 2020', 1),
(8, 'Registered with the Securities and Exchange Commission (SEC)', 3, '2019 - 2020', 1),
(9, 'Non-stock/Stock', 3, '2019 - 2020', 1),
(10, 'registration Certificate Number', 3, '2019 - 2020', 1),
(11, 'in the name of the School', 4, '2019 - 2020', 1),
(12, 'Total Area', 4, '2019 - 2020', 1),
(13, 'Fully Implementd', 5, '2019 - 2020', 1),
(14, 'Partially Implemented', 5, '2019 - 2020', 1),
(15, 'Not Implemented', 5, '2019 - 2020', 1),
(16, 'in the name of the School', 6, '2019 - 2020', 1),
(17, 'Total floor area with adequate staff', 6, '2019 - 2020', 1),
(18, 'Signed nu proper City/Municipal Authorities', 7, '2019 - 2020', 1),
(19, 'Adequate', 8, '2019 - 2020', 1),
(20, 'Clean', 8, '2019 - 2020', 1),
(21, 'In accordance with the DepED standards and requirements (K-12 Curriculum)', 11, '2019 - 2020', 1),
(22, 'Approved by the Department of Education', 12, '2019 - 2020', 1),
(23, 'In accordance with the guidelines', 12, '2019 - 2020', 1),
(24, 'Terms and benefits', 13, '2019 - 2020', 1),
(25, 'Educationally qualified', 14, '2019 - 2020', 1),
(26, 'Salaries in accordance with the prescribed standards', 14, '2019 - 2020', 1),
(27, 'Educationally Qualified', 15, '2019 - 2020', 1),
(28, 'Subject assignment in accordance with qualifications', 15, '2019 - 2020', 1),
(29, 'Salary/Other benefits in accordance with prescribed standards', 15, '2019 - 2020', 1),
(30, 'Valid contracts/appointments', 15, '2019 - 2020', 1),
(31, 'Ratio of full-time, part-time in accordance with DepED policies', 15, '2019 - 2020', 1),
(32, 'Full-time', 15, '2019 - 2020', 1),
(33, 'Educationally Qualified', 16, '2019 - 2020', 1),
(34, 'Subject assignment in accordance with qualifications', 16, '2019 - 2020', 1),
(35, 'Salary/Other benefits in accordance with prescribed standards', 16, '2019 - 2020', 1),
(36, 'Valid contracts/appointments', 16, '2019 - 2020', 1),
(37, 'Ratio of full-time, part-time in accordance with DepED policies', 16, '2019 - 2020', 1),
(38, 'Complied with prescribed standards and requirements', 17, '2019 - 2020', 1),
(39, 'List of textbooks per subject area (Sec. 160 of the Revised Manual of Regulations for Private Schools-RMRPS', 18, '2019 - 2020', 1),
(40, 'List of book reference per subject area (minimum of 5 authors per subject area', 18, '2019 - 2020', 1),
(41, 'Gymnasium, Playground, and the likes', 19, '2019 - 2020', 1),
(42, 'Indoor and outdoor play/sports implements', 19, '2019 - 2020', 1),
(43, 'Letter of Request (Separate for Elementary and Secondary)', 20, '2019 - 2020', 1),
(44, 'Submit in four (4) folders', 20, '2019 - 2020', 1),
(45, 'Indorsement from the Schools Division Supirentendent', 20, '2019 - 2020', 1),
(46, 'Inspection Report prepared by the Division Inspection Team with the findings and recommendations', 20, '2019 - 2020', 1),
(47, 'Photocopy pf the latest Government Permit Issued', 20, '2019 - 2020', 1),
(48, 'SSS', 27, '2019 - 2020', 1),
(49, 'Pag-Ibig', 27, '2019 - 2020', 1),
(50, 'PhilHealth', 27, '2019 - 2020', 1),
(51, 'Educational Qualifications', 28, '2019 - 2020', 1),
(52, 'Salary Rate/moth', 28, '2019 - 2020', 1),
(53, 'Employment Status', 28, '2019 - 2020', 1),
(54, 'Job description', 29, '2019 - 2020', 1),
(55, 'Educational Qualifications', 29, '2019 - 2020', 1),
(56, 'Field of Specifications', 29, '2019 - 2020', 1),
(57, 'Transcript of Records (Certified True Copy)', 29, '2019 - 2020', 1),
(58, 'Certificates of Training', 29, '2019 - 2020', 1),
(59, 'PRC Licence (Photocopy)', 29, '2019 - 2020', 1),
(60, 'Employment Contract duly notarized', 29, '2019 - 2020', 1),
(61, 'Salaries (per hour/month) and benefits', 29, '2019 - 2020', 1),
(62, 'Full time/Part-time (Number of Hours)', 29, '2019 - 2020', 1),
(63, 'Teaching Load', 29, '2019 - 2020', 1),
(64, 'Certified by Corporate Secretary', 34, '2020 - 2021', 1),
(65, 'Purpose', 35, '2020 - 2021', 1),
(66, 'Need and relevance of school applied for', 35, '2020 - 2021', 1),
(67, 'Resources and capabilities of the Corporate/School', 35, '2020 - 2021', 1),
(68, 'Programs and Projects', 35, '2020 - 2021', 1),
(69, 'Location of School in Relation to its Environment', 35, '2020 - 2021', 1),
(70, 'in the Name of the School', 36, '2020 - 2021', 1),
(71, 'Registered with the Securities and Exchange Commission (SEC)', 36, '2020 - 2021', 1),
(72, 'Non-stock/Stock', 36, '2020 - 2021', 1),
(73, 'registration Certificate Number', 36, '2020 - 2021', 1),
(74, 'in the name of the School', 37, '2020 - 2021', 1),
(75, 'Total Area', 37, '2020 - 2021', 1),
(76, 'Fully Implementd', 38, '2020 - 2021', 1),
(77, 'Partially Implemented', 38, '2020 - 2021', 1),
(78, 'Not Implemented', 38, '2020 - 2021', 1),
(79, 'in the name of the School', 39, '2020 - 2021', 1),
(80, 'Total floor area with adequate staff', 39, '2020 - 2021', 1),
(81, 'Signed nu proper City/Municipal Authorities', 40, '2020 - 2021', 1),
(82, 'Adequate', 41, '2020 - 2021', 1),
(83, 'Clean', 41, '2020 - 2021', 1),
(84, 'In accordance with the DepED standards and requirements (K-12 Curriculum)', 44, '2020 - 2021', 1),
(85, 'Approved by the Department of Education', 45, '2020 - 2021', 1),
(86, 'In accordance with the guidelines', 45, '2020 - 2021', 1),
(87, 'Terms and benefits', 46, '2020 - 2021', 1),
(88, 'Educationally qualified', 47, '2020 - 2021', 1),
(89, 'Salaries in accordance with the prescribed standards', 47, '2020 - 2021', 1),
(90, 'Educationally Qualified', 48, '2020 - 2021', 1),
(91, 'Subject assignment in accordance with qualifications', 48, '2020 - 2021', 1),
(92, 'Salary/Other benefits in accordance with prescribed standards', 48, '2020 - 2021', 1),
(93, 'Valid contracts/appointments', 48, '2020 - 2021', 1),
(94, 'Ratio of full-time, part-time in accordance with DepED policies', 48, '2020 - 2021', 1),
(95, 'Full-time', 48, '2020 - 2021', 1),
(96, 'Educationally Qualified', 49, '2020 - 2021', 1),
(97, 'Subject assignment in accordance with qualifications', 49, '2020 - 2021', 1),
(98, 'Salary/Other benefits in accordance with prescribed standards', 49, '2020 - 2021', 1),
(99, 'Valid contracts/appointments', 49, '2020 - 2021', 1),
(100, 'Ratio of full-time, part-time in accordance with DepED policies', 49, '2020 - 2021', 1),
(101, 'Complied with prescribed standards and requirements', 50, '2020 - 2021', 1),
(102, 'List of textbooks per subject area (Sec. 160 of the Revised Manual of Regulations for Private Schools-RMRPS', 51, '2020 - 2021', 1),
(103, 'List of book reference per subject area (minimum of 5 authors per subject area', 51, '2020 - 2021', 1),
(104, 'Gymnasium, Playground, and the likes', 52, '2020 - 2021', 1),
(105, 'Indoor and outdoor play/sports implements', 52, '2020 - 2021', 1),
(106, 'Letter of Request (Separate for Elementary and Secondary)', 53, '2020 - 2021', 1),
(107, 'Submit in four (4) folders', 53, '2020 - 2021', 1),
(108, 'Indorsement from the Schools Division Supirentendent', 53, '2020 - 2021', 1),
(109, 'Inspection Report prepared by the Division Inspection Team with the findings and recommendations', 53, '2020 - 2021', 1),
(110, 'Photocopy pf the latest Government Permit Issued', 53, '2020 - 2021', 1),
(111, 'SSS', 60, '2020 - 2021', 1),
(112, 'Pag-Ibig', 60, '2020 - 2021', 1),
(113, 'PhilHealth', 60, '2020 - 2021', 1),
(114, 'Educational Qualifications', 61, '2020 - 2021', 1),
(115, 'Salary Rate/moth', 61, '2020 - 2021', 1),
(116, 'Employment Status', 61, '2020 - 2021', 1),
(117, 'Job description', 62, '2020 - 2021', 1),
(118, 'Educational Qualifications', 62, '2020 - 2021', 1),
(119, 'Field of Specifications', 62, '2020 - 2021', 1),
(120, 'Transcript of Records (Certified True Copy)', 62, '2020 - 2021', 1),
(121, 'Certificates of Training', 62, '2020 - 2021', 1),
(122, 'PRC Licence (Photocopy)', 62, '2020 - 2021', 1),
(123, 'Employment Contract duly notarized', 62, '2020 - 2021', 1),
(124, 'Salaries (per hour/month) and benefits', 62, '2020 - 2021', 1),
(125, 'Full time/Part-time (Number of Hours)', 62, '2020 - 2021', 1),
(126, 'Teaching Load', 62, '2020 - 2021', 1),
(127, 'another2', 35, '2020 - 2021', 0);

-- --------------------------------------------------------

--
-- Table structure for table `subrequirements1_shs`
--

CREATE TABLE `subrequirements1_shs` (
  `subReq1Id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mainReqIdfk` int(11) NOT NULL,
  `schoolYear` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subrequirements1_shs`
--

INSERT INTO `subrequirements1_shs` (`subReq1Id`, `description`, `mainReqIdfk`, `schoolYear`, `isActive`) VALUES
(1, 'Purpose', 2, '2019 - 2020', 1),
(2, 'School year of intended operation', 2, '2019 - 2020', 1),
(3, 'SHS Curriculum for the track/s and strand/s to be offered', 2, '2019 - 2020', 1),
(4, 'Secondary Education Program - DepED', 3, '2019 - 2020', 1),
(5, 'Training program - TESDA', 3, '2019 - 2020', 1),
(6, 'Higher Education Program - CHED', 3, '2019 - 2020', 1),
(7, 'Others:', 3, '2019 - 2020', 1),
(8, 'Qualifications', 6, '2019 - 2020', 1),
(9, 'Job Descriptions', 6, '2019 - 2020', 1),
(10, 'Teaching Load', 6, '2019 - 2020', 1),
(11, 'Number of working hours per week', 6, '2019 - 2020', 1),
(12, 'Certification from recognized national/international agencies (TESDA, ABA and others)', 6, '2019 - 2020', 1),
(13, 'Academic Track', 7, '2019 - 2020', 1),
(14, 'Tech-Voc Track', 7, '2019 - 2020', 1),
(15, 'Arts and Design Track', 7, '2019 - 2020', 1),
(16, 'Sports Track', 7, '2019 - 2020', 1),
(17, 'Instructional rooms', 8, '2019 - 2020', 1),
(18, 'Laboratories', 8, '2019 - 2020', 1),
(19, 'Athletic Facilities', 8, '2019 - 2020', 1),
(20, 'Learner\'s resource canter or Library', 8, '2019 - 2020', 1),
(21, 'Internet Facilities', 8, '2019 - 2020', 1),
(22, 'Ancillary Services', 8, '2019 - 2020', 1),
(23, 'Engagement of stakeholders in the localization of the curriculum', 9, '2019 - 2020', 1),
(24, 'Work Immersion', 9, '2019 - 2020', 1),
(25, 'Apprenticeship', 9, '2019 - 2020', 1),
(26, 'Research', 9, '2019 - 2020', 1),
(27, 'Provision of equipment and laboratories, workshop, and other facilities', 9, '2019 - 2020', 1),
(28, 'Organization of career guidance and youth formation activities', 9, '2019 - 2020', 1),
(29, 'Other', 9, '2019 - 2020', 1),
(30, 'Purpose', 14, '2020 - 2021', 1),
(31, 'School year of intended operation', 14, '2020 - 2021', 1),
(32, 'SHS Curriculum for the track/s and strand/s to be offered', 14, '2020 - 2021', 1),
(33, 'Secondary Education Program - DepED', 15, '2020 - 2021', 1),
(34, 'Training program - TESDA', 15, '2020 - 2021', 1),
(35, 'Higher Education Program - CHED', 15, '2020 - 2021', 1),
(36, 'Others:', 15, '2020 - 2021', 1),
(37, 'Qualifications', 18, '2020 - 2021', 1),
(38, 'Job Descriptions', 18, '2020 - 2021', 1),
(39, 'Teaching Load', 18, '2020 - 2021', 1),
(40, 'Number of working hours per week', 18, '2020 - 2021', 1),
(41, 'Certification from recognized national/international agencies (TESDA, ABA and others)', 18, '2020 - 2021', 1),
(42, 'Academic Track', 19, '2020 - 2021', 1),
(43, 'Tech-Voc Track', 19, '2020 - 2021', 1),
(44, 'Arts and Design Track', 19, '2020 - 2021', 1),
(45, 'Sports Track', 19, '2020 - 2021', 1),
(46, 'Instructional rooms', 20, '2020 - 2021', 1),
(47, 'Laboratories', 20, '2020 - 2021', 1),
(48, 'Athletic Facilities', 20, '2020 - 2021', 1),
(49, 'Learner\'s resource canter or Library', 20, '2020 - 2021', 1),
(50, 'Internet Facilities', 20, '2020 - 2021', 1),
(51, 'Ancillary Services', 20, '2020 - 2021', 1),
(52, 'Engagement of stakeholders in the localization of the curriculum', 21, '2020 - 2021', 1),
(53, 'Work Immersion', 21, '2020 - 2021', 1),
(54, 'Apprenticeship', 21, '2020 - 2021', 1),
(55, 'Research', 21, '2020 - 2021', 1),
(56, 'Provision of equipment and laboratories, workshop, and other facilities', 21, '2020 - 2021', 1),
(57, 'Organization of career guidance and youth formation activities', 21, '2020 - 2021', 1),
(58, 'Other', 21, '2020 - 2021', 1);

-- --------------------------------------------------------

--
-- Table structure for table `subrequirements2`
--

CREATE TABLE `subrequirements2` (
  `subReq2Id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subReq1Idfk` int(11) NOT NULL,
  `schoolYear` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subrequirements2`
--

INSERT INTO `subrequirements2` (`subReq2Id`, `description`, `subReq1Idfk`, `schoolYear`, `isActive`) VALUES
(1, 'Far from existing schools and fire hazad areas', 6, '2019 - 2020', 1),
(2, 'Free from traffic and noise', 6, '2019 - 2020', 1),
(3, 'Location map', 6, '2019 - 2020', 1),
(4, 'Far from existing schools and fire hazad areas', 69, '2020 - 2021', 1),
(5, 'Free from traffic and noise', 69, '2020 - 2021', 1),
(6, 'Location map', 69, '2020 - 2021', 1),
(7, 'test', 127, '2020 - 2021', 0);

-- --------------------------------------------------------

--
-- Table structure for table `subrequirements2_shs`
--

CREATE TABLE `subrequirements2_shs` (
  `subReq2Id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subReq1Idfk` int(11) NOT NULL,
  `schoolYear` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subrequirements2_shs`
--

INSERT INTO `subrequirements2_shs` (`subReq2Id`, `description`, `subReq1Idfk`, `schoolYear`, `isActive`) VALUES
(1, 'FAAP recognized accreditting agencies', 7, '2019 - 2020', 1),
(2, 'Asia Pacific Accreditation and Certification (APACC)', 7, '2019 - 2020', 1),
(3, 'AFA', 14, '2019 - 2020', 1),
(4, 'IA', 14, '2019 - 2020', 1),
(5, 'HE', 14, '2019 - 2020', 1),
(6, 'ICT', 14, '2019 - 2020', 1),
(7, 'Performing Arts', 15, '2019 - 2020', 1),
(8, 'Arts Production', 15, '2019 - 2020', 1),
(9, 'Computer', 18, '2019 - 2020', 1),
(10, 'Science (for STEM minimum of 3 labs)', 18, '2019 - 2020', 1),
(11, 'Workshop Room/Studios', 18, '2019 - 2020', 1),
(12, 'FAAP recognized accreditting agencies', 36, '2020 - 2021', 1),
(13, 'Asia Pacific Accreditation and Certification (APACC)', 36, '2020 - 2021', 1),
(14, 'AFA', 43, '2020 - 2021', 1),
(15, 'IA', 43, '2020 - 2021', 1),
(16, 'HE', 43, '2020 - 2021', 1),
(17, 'ICT', 43, '2020 - 2021', 1),
(18, 'Performing Arts', 44, '2020 - 2021', 1),
(19, 'Arts Production', 44, '2020 - 2021', 1),
(20, 'Computer', 47, '2020 - 2021', 1),
(21, 'Science (for STEM minimum of 3 labs)', 47, '2020 - 2021', 1),
(22, 'Workshop Room/Studios', 47, '2020 - 2021', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `employeeID` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firstName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middleName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sex` varchar(6) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthDate` date DEFAULT NULL,
  `schoolId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `previousName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `schoolAddress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'CAR',
  `officeIDfk` int(11) NOT NULL DEFAULT '0',
  `schoolsDivisionIdfk` int(11) NOT NULL DEFAULT '0',
  `district` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `legislativeDistrict` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `municipality` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telNumber` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobileNumber` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `faxNo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `schoolHead` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `designation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dateEstablished` date DEFAULT NULL,
  `isActive` int(11) NOT NULL DEFAULT '1',
  `firstTimeLogin` int(11) NOT NULL DEFAULT '1',
  `accountType` int(11) NOT NULL DEFAULT '3',
  `inspectionCompositionIdfk` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `employeeID`, `firstName`, `middleName`, `lastName`, `sex`, `birthDate`, `schoolId`, `name`, `previousName`, `email`, `email_verified_at`, `password`, `schoolAddress`, `region`, `officeIDfk`, `schoolsDivisionIdfk`, `district`, `legislativeDistrict`, `municipality`, `telNumber`, `mobileNumber`, `faxNo`, `schoolHead`, `designation`, `dateEstablished`, `isActive`, `firstTimeLogin`, `accountType`, `inspectionCompositionIdfk`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, '', '', '', '', '', '0000-00-00', '481572', '4th Watch Maranatha Christian Academy of Baguio City, Inc.', NULL, 'baguiops@gmail.com', NULL, '$2y$10$pc3X0Hxzc62MY5AhA0df6e.g7QnVmeEVY9iVgXSCoFILsAavmErru', NULL, 'CAR', 3, 3, NULL, NULL, 'Baguio City', '074-4450982', '0921479490', NULL, 'Princess Jean L. Cabantog', 'Principal', '2000-09-01', 1, 1, 3, 0, 'SqbMYaDhQCHUDbjHKtRUMLSBJZOnFyZl5nLiwJqKc8AvDX4xLlWe9Iqj4aNI', '2019-01-22 21:55:33', '2019-09-18 08:40:10'),
(3, '000001', 'Maksim', 'a', 'Botilas', 'Male', '2019-09-19', NULL, NULL, NULL, 'ro@gmail.com', NULL, '$2y$10$YjBLm6hG6osD31DUXpATq.FiUXKuPKqgPOz7Nzv/Goeikhxo3au5q', NULL, 'CAR', 1, 0, NULL, NULL, NULL, NULL, '1', NULL, NULL, '1', NULL, 1, 1, 3, 0, 'CxveZkexci9Pr0md9NWQw030VYY0LSjwdpQkXb9vyr6aPJIdEGbYJgzGgH0u', '2019-02-06 00:35:12', '2019-09-19 03:02:08'),
(4, '', '', '', '', '', '0000-00-00', '407980', 'Alfonso Lista Christian Academy, Inc.', NULL, 'ifugaops@gmail.com', NULL, '$2y$10$pc3X0Hxzc62MY5AhA0df6e.g7QnVmeEVY9iVgXSCoFILsAavmErru', NULL, 'CAR', 3, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 3, 0, 'xZXhVelzKStprcyKZnGjJZOmxdZcuvlhKhktAXq9r5hNl5H6NaIS5sd7l1nj', '2019-02-06 04:30:30', '2019-02-06 04:30:30'),
(5, '', '', '', '', '', '0000-00-00', '406114', '	Abra District Adventist Multigrade School (ADAMS)', NULL, 'abraps@gmail.com', NULL, '$2y$10$pc3X0Hxzc62MY5AhA0df6e.g7QnVmeEVY9iVgXSCoFILsAavmErru', NULL, 'CAR', 3, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 3, 0, 'GT1PnUJXYlIJUASE2WHrFnf1FFvgQChD9X5sXRUf62e39tl5Q8UIrRx2SvPU', '2019-02-06 04:33:40', '2019-02-06 04:33:40'),
(6, '', '', '', '', '', '0000-00-00', '406135', 'Apayao Community Learning Center', NULL, 'apayaops@gmail.com', NULL, '$2y$10$pc3X0Hxzc62MY5AhA0df6e.g7QnVmeEVY9iVgXSCoFILsAavmErru', NULL, 'CAR', 3, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 3, 0, 'HSC9wSSJwuNN4ko6pdKhVYh0rOPKtwBPnPH23WK6swbxufy8hh7K18gctHz7', '2019-02-06 04:35:20', '2019-02-06 04:35:20'),
(7, '', '', '', '', '', '0000-00-00', '1479503', 'Benguet Learning Center', NULL, 'benguetps@gmail.com', NULL, '$2y$10$pc3X0Hxzc62MY5AhA0df6e.g7QnVmeEVY9iVgXSCoFILsAavmErru', NULL, 'CAR', 3, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 3, 0, '2PMMRJAYnXq0vFaH7YIhlOsJUjg1mEfKhkomOalOT7Itj1x7zpIlSloBPGh2', '2019-02-06 04:40:19', '2019-02-06 04:40:19'),
(8, '', '', '', '', '', '0000-00-00', '406184', 'KALINGA ACADEMY', NULL, 'kalingaps@gmail.com', NULL, '$2y$10$pc3X0Hxzc62MY5AhA0df6e.g7QnVmeEVY9iVgXSCoFILsAavmErru', NULL, 'CAR', 3, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 3, 0, 'ovXiYMbQWotZfR4PgflTBckc8lvCGJ2Khf6J3LInnuUZNLFjM6uByGSmnZ6u', '2019-02-06 04:33:40', NULL),
(9, '100001', 'Juan', 'Dela', 'Cruz', 'Male', '2019-10-02', NULL, 'sdoabra', NULL, 'sdoabra@gmail.com', NULL, '$2y$10$IavTJY1Ru26WcQSZFLn3V.7ylb8Jz.jYFIkXNHgU8zQWqQUCCGLde', NULL, 'CAR', 2, 1, NULL, NULL, NULL, NULL, '09991234567', NULL, NULL, NULL, NULL, 1, 1, 3, 2, 'mYDdvxv6ckJT9uJaZQ9gtq44UO8FZlKh9OSWJARTvhJBxWHdxBU4glXaAnA9', '2019-09-01 16:00:00', '2019-10-03 06:20:31'),
(10, '', '', '', '', '', '0000-00-00', NULL, 'sdoapayao', NULL, 'sdoapayao@gmail.com', NULL, '$2y$10$pc3X0Hxzc62MY5AhA0df6e.g7QnVmeEVY9iVgXSCoFILsAavmErru', NULL, 'CAR', 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 3, 0, 'aW3Huenof46xrl0UhOkzzo0yTv7k9VRw9GxjUcdRrQ4o5XBIIK6Ikyykn7h5', '2019-09-01 16:00:00', NULL),
(12, '', '', '', '', '', '0000-00-00', NULL, 'sdobenguet', NULL, 'sdobenguet@gmail.com', NULL, '$2y$10$pc3X0Hxzc62MY5AhA0df6e.g7QnVmeEVY9iVgXSCoFILsAavmErru', NULL, 'CAR', 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 3, 0, '1zBSr9vInXA2PdLaGvDmJqCM0CbwGBsSKIqbwNuq5mMsWVt6xlec5aZaGEU6', '2019-02-06 04:33:40', NULL),
(13, '', '', '', '', '', '0000-00-00', NULL, 'sdoifugao', NULL, 'sdoifugao@gmail.com', NULL, '$2y$10$pc3X0Hxzc62MY5AhA0df6e.g7QnVmeEVY9iVgXSCoFILsAavmErru', NULL, 'CAR', 2, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 3, 0, 'LehYxJbE5GgwUkzR3icvzrKN3vJF5BZXn9xyBe3T6WOKghAtNs8vAtqOECiE', '2019-02-06 04:33:40', NULL),
(14, '', '', '', '', '', '0000-00-00', NULL, 'sdokalinga', NULL, 'sdokalinga@gmail.com', NULL, '$2y$10$pc3X0Hxzc62MY5AhA0df6e.g7QnVmeEVY9iVgXSCoFILsAavmErru', NULL, 'CAR', 2, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 3, 0, 'X1kstfRMYXEGNb8MNtQCIzKOFB0jgb1Jl2K7YhVcFuRMIlBr9FmA7ifznap3', '2019-02-04 16:00:00', NULL),
(15, '', '', '', '', '', '0000-00-00', NULL, 'sdomp', NULL, 'sdomp@gmail.com', NULL, '$2y$10$pc3X0Hxzc62MY5AhA0df6e.g7QnVmeEVY9iVgXSCoFILsAavmErru', NULL, 'CAR', 2, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 3, 0, 'LtyRf4wrUsJ5YRKdmjmiWvbAHCMiLsGUMkQfQfojEifppZWK5rr1iZlUTipC', '2019-09-01 16:00:00', NULL),
(16, '', '', '', '', '', '0000-00-00', NULL, 'sdotabuk', NULL, 'sdotabuk@gmail.com', NULL, '$2y$10$pc3X0Hxzc62MY5AhA0df6e.g7QnVmeEVY9iVgXSCoFILsAavmErru', NULL, 'CAR', 2, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 3, 0, 'JPPFDS4m1WLroPuXnTrdFrGxAFbtBxhNnH0NRNuvR2qn459ueUeC5VmCucHu', '2019-09-01 16:00:00', NULL),
(17, '', '', '', '', '', '0000-00-00', '406199', 'Bauko Catholic School', NULL, 'mtprovinceps@gmail.com', NULL, '$2y$10$pc3X0Hxzc62MY5AhA0df6e.g7QnVmeEVY9iVgXSCoFILsAavmErru', NULL, 'CAR', 3, 7, NULL, NULL, NULL, NULL, '0921479491', NULL, 'Juan Dela Cruz', 'Principal1', '2019-09-17', 1, 1, 3, 0, 'x6Yez0BzC1zBxgU3ktAMs8K9ubdlOBLfy82kjjd6XowZU2TuHmATPoFKlfyF', '2019-09-02 16:00:00', '2019-09-20 01:29:15'),
(18, '', '', '', '', '', '0000-00-00', '400939', 'Cordillera A+ Computer Technology College', NULL, 'tabukps@gmail.com', NULL, '$2y$10$pc3X0Hxzc62MY5AhA0df6e.g7QnVmeEVY9iVgXSCoFILsAavmErru', NULL, 'CAR', 3, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 3, 0, 'G4DNg2kfvrtbAFqrCCL4IBuqj05cH51Y75nYPaO39FazgyMTQtkMO11DMUWV', '2019-09-01 16:00:00', NULL),
(42, '123456', 'dummy', '.', 'division', 'Male', '1980-09-18', NULL, NULL, NULL, 'dummy.division@deped.gov.ph', NULL, '$2y$10$BJXjnvxYPNFaIW6NVucpE.YGP0B3XJaovF.eXDQNBWV62xuzx43fu', NULL, 'CAR', 2, 3, NULL, NULL, NULL, NULL, '0999123456789', NULL, NULL, 'Private School Coordinator', NULL, 1, 1, 3, 0, NULL, '2019-09-16 08:49:46', '2019-09-16 08:49:46'),
(43, '101010', 'sdo', '.', 'baguio city', 'Male', '2019-09-19', NULL, NULL, NULL, 'sdobaguio@gmail.com', NULL, '$2y$10$d5MsY4zShVgpWY7OISF5deq2XEcswuxgayjBHPw4de013w2SnSqS.', NULL, 'CAR', 2, 3, NULL, NULL, NULL, NULL, '0921479490', NULL, NULL, 'Private School Coordinator', NULL, 1, 1, 3, 2, 'QzwV9DUKC3kCttqNYMWS8hRyUSfqFfvX6K9Vh0SaoJUwjt0bhRIkz2mOw4QQ', '2019-09-19 00:36:21', '2019-10-03 05:20:43'),
(44, NULL, NULL, NULL, NULL, NULL, NULL, '1481509', 'Academia de Sophia International Inc.', NULL, 'baguiops1@gmail.com', NULL, '$2y$10$e7jFnwZDv4MN.R6fsvSLBe6udMMZxPU3Ky/ThkvdNvLqeq/UQdiy.', NULL, 'CAR', 3, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 3, 0, NULL, '2019-09-19 02:05:19', '2019-09-19 02:05:19'),
(68, '200001', 'Juan', 'Dela', 'Cruz', NULL, NULL, NULL, NULL, NULL, 'vannflora1@gmail.com', NULL, '$2y$10$scHXj4kaUUPqaz8LIQIvrOdLqLEKyz4eCK8nyrnyao16c2b9h3u2q', NULL, 'CAR', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 3, 0, 'RBtrdcCOou2JPRmDd4JY1xuY9vUiFMIeTVqn6m44g41Cz9tWYUaREy3hrT5q', '2019-10-01 05:38:37', '2019-10-01 07:45:14'),
(69, NULL, NULL, NULL, NULL, NULL, NULL, '406102', 'Abra Valley Colleges', NULL, 'vannflora@gmail.com', NULL, '$2y$10$bncEeb9sYwSsebayvFkZourmH5O6tkdbeM6u6BuBW8NP8Mkwhb9yi', 'Address Except Division 1', 'CAR', 3, 1, NULL, NULL, NULL, NULL, '09214794911', NULL, 'Juan Dela Cruz', 'Principal1', '2019-10-02', 1, 1, 3, 0, 'Je6yZGd0lBs0YjTRvl4eKcPSz1dAADcym93vijlpBYbz4FvddpFX59cEohET', '2019-10-01 08:37:43', '2019-10-02 06:18:53');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `applicationstatus`
--
ALTER TABLE `applicationstatus`
  ADD PRIMARY KEY (`applicationStatusId`);

--
-- Indexes for table `applicationtype`
--
ALTER TABLE `applicationtype`
  ADD PRIMARY KEY (`applicationTypeId`);

--
-- Indexes for table `categoriesshs`
--
ALTER TABLE `categoriesshs`
  ADD PRIMARY KEY (`categoryId`);

--
-- Indexes for table `docreviewteam`
--
ALTER TABLE `docreviewteam`
  ADD PRIMARY KEY (`docReviewTeamId`);

--
-- Indexes for table `docreviewteamuploads`
--
ALTER TABLE `docreviewteamuploads`
  ADD PRIMARY KEY (`docReviewFileId`);

--
-- Indexes for table `fileuploads`
--
ALTER TABLE `fileuploads`
  ADD PRIMARY KEY (`fileId`);

--
-- Indexes for table `generatedcertificates`
--
ALTER TABLE `generatedcertificates`
  ADD PRIMARY KEY (`certificateId`);

--
-- Indexes for table `gradelevels`
--
ALTER TABLE `gradelevels`
  ADD PRIMARY KEY (`gradeLevelId`);

--
-- Indexes for table `inspectioncomposition`
--
ALTER TABLE `inspectioncomposition`
  ADD PRIMARY KEY (`inspectionCompositionId`);

--
-- Indexes for table `inspectionmembers`
--
ALTER TABLE `inspectionmembers`
  ADD PRIMARY KEY (`inspectionMemberId`);

--
-- Indexes for table `inspectionteams`
--
ALTER TABLE `inspectionteams`
  ADD PRIMARY KEY (`inspectionTeamId`);

--
-- Indexes for table `levelofeducationoffered`
--
ALTER TABLE `levelofeducationoffered`
  ADD PRIMARY KEY (`levelId`);

--
-- Indexes for table `mainrequirements`
--
ALTER TABLE `mainrequirements`
  ADD PRIMARY KEY (`mainReqId`);

--
-- Indexes for table `mainrequirements_shs`
--
ALTER TABLE `mainrequirements_shs`
  ADD PRIMARY KEY (`mainReqId`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permitrecognitiondocreview`
--
ALTER TABLE `permitrecognitiondocreview`
  ADD PRIMARY KEY (`prDocReviewId`);

--
-- Indexes for table `permitrecognitionstatus`
--
ALTER TABLE `permitrecognitionstatus`
  ADD PRIMARY KEY (`prStatusId`);

--
-- Indexes for table `stages`
--
ALTER TABLE `stages`
  ADD PRIMARY KEY (`stageId`);

--
-- Indexes for table `stagesstatus`
--
ALTER TABLE `stagesstatus`
  ADD PRIMARY KEY (`stageStatusId`);

--
-- Indexes for table `stagestimeline`
--
ALTER TABLE `stagestimeline`
  ADD PRIMARY KEY (`stageTimelineId`);

--
-- Indexes for table `subrequirements1`
--
ALTER TABLE `subrequirements1`
  ADD PRIMARY KEY (`subReq1Id`);

--
-- Indexes for table `subrequirements1_shs`
--
ALTER TABLE `subrequirements1_shs`
  ADD PRIMARY KEY (`subReq1Id`);

--
-- Indexes for table `subrequirements2`
--
ALTER TABLE `subrequirements2`
  ADD PRIMARY KEY (`subReq2Id`);

--
-- Indexes for table `subrequirements2_shs`
--
ALTER TABLE `subrequirements2_shs`
  ADD PRIMARY KEY (`subReq2Id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `applicationstatus`
--
ALTER TABLE `applicationstatus`
  MODIFY `applicationStatusId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `applicationtype`
--
ALTER TABLE `applicationtype`
  MODIFY `applicationTypeId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `categoriesshs`
--
ALTER TABLE `categoriesshs`
  MODIFY `categoryId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `docreviewteam`
--
ALTER TABLE `docreviewteam`
  MODIFY `docReviewTeamId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `docreviewteamuploads`
--
ALTER TABLE `docreviewteamuploads`
  MODIFY `docReviewFileId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fileuploads`
--
ALTER TABLE `fileuploads`
  MODIFY `fileId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `generatedcertificates`
--
ALTER TABLE `generatedcertificates`
  MODIFY `certificateId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `gradelevels`
--
ALTER TABLE `gradelevels`
  MODIFY `gradeLevelId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `inspectioncomposition`
--
ALTER TABLE `inspectioncomposition`
  MODIFY `inspectionCompositionId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `inspectionmembers`
--
ALTER TABLE `inspectionmembers`
  MODIFY `inspectionMemberId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `inspectionteams`
--
ALTER TABLE `inspectionteams`
  MODIFY `inspectionTeamId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `levelofeducationoffered`
--
ALTER TABLE `levelofeducationoffered`
  MODIFY `levelId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `mainrequirements`
--
ALTER TABLE `mainrequirements`
  MODIFY `mainReqId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `mainrequirements_shs`
--
ALTER TABLE `mainrequirements_shs`
  MODIFY `mainReqId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `permitrecognitiondocreview`
--
ALTER TABLE `permitrecognitiondocreview`
  MODIFY `prDocReviewId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- AUTO_INCREMENT for table `permitrecognitionstatus`
--
ALTER TABLE `permitrecognitionstatus`
  MODIFY `prStatusId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `stages`
--
ALTER TABLE `stages`
  MODIFY `stageId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `stagesstatus`
--
ALTER TABLE `stagesstatus`
  MODIFY `stageStatusId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `stagestimeline`
--
ALTER TABLE `stagestimeline`
  MODIFY `stageTimelineId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `subrequirements1`
--
ALTER TABLE `subrequirements1`
  MODIFY `subReq1Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;

--
-- AUTO_INCREMENT for table `subrequirements1_shs`
--
ALTER TABLE `subrequirements1_shs`
  MODIFY `subReq1Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `subrequirements2`
--
ALTER TABLE `subrequirements2`
  MODIFY `subReq2Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `subrequirements2_shs`
--
ALTER TABLE `subrequirements2_shs`
  MODIFY `subReq2Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
