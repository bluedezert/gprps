<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use App\Services\PayUService\Exception;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\PDO;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home')
            ->with('currentSY', Config::get('constants.gprpsconst.application_school_year'))
            ->with('submitted', self::countApplicationStatus(3))
            ->with('onprocess', self::countApplicationStatus(4))
            ->with('approved', self::countApplicationStatus(5))
            ->with('disapproved', self::countApplicationStatus(6))
            ->with('privateschoolselem', self::privateSchoolElemList())
            ->with('privateschoolssec', self::privateSchoolSecList());
    }

    public function getApplicationStatusDesc($applicationStatusId){
      $conn1 = DB::connection('mysql')->getPdo();
      $sql = "SELECT
                  `applicationStatusId`,
                  `description`,
                  `valueNow`,
                  `progressClass`
              FROM
                  `applicationstatus`
              WHERE
                  applicationStatusId=:applicationStatusId";
      $stmt = $conn1 -> prepare($sql);
      $stmt -> bindParam(":applicationStatusId", $applicationStatusId);
      $stmt->execute();
      $result=$stmt->fetch(\PDO::FETCH_ASSOC);
      return $result["description"];
    }

    public function dashboardmoreinfo($applicationStatusId)
    {
        return view('dashboardmoreinfo')
            ->with('currentSY', Config::get('constants.gprpsconst.application_school_year'))
            ->with('submitted', self::countApplicationStatus(3))
            ->with('onprocess', self::countApplicationStatus(4))
            ->with('approved', self::countApplicationStatus(5))
            ->with('disapproved', self::countApplicationStatus(6))
            ->with('getApplicationStatusDesc', self::getApplicationStatusDesc($applicationStatusId))
            ->with('applicationlistelem', self::applicationlistelem($applicationStatusId))
            ->with('applicationlistsec', self::applicationlistsec($applicationStatusId));
    }

    public function applicationlistelem($applicationStatusId){
        $applicationSY = Config::get('constants.gprpsconst.application_school_year');
        $schoolsDivisionIdfk = Auth::user()->schoolsDivisionIdfk;
        $divisionName = self::getDivisionName($schoolsDivisionIdfk);
        $conn1 = DB::connection('mysql')->getPdo();
        if($schoolsDivisionIdfk==0){//RO
          $sql = "SELECT DISTINCT (prStatusId),
                                  applicationTypeIdfk,
                                  schoolUserIdfk,
                                  gradeLevelIdfkFrom,
                                  gradeLevelIdfkTo,
                                  governmentPRNumber,
                                  seriesYear,
                                  permitrecognitionstatus.schoolYear,
                                  applicationStatusIdfk,
                                  categoryIdfk,
                                  lastDateUpdated,
                                  controlNumber,
                                  elementary_schools.SCHOOLID,
                                 elementary_schools.SCHOOLNAME,
                                 elementary_schools.DIVISION,
                                 applicationtype.description AS applicationType
                  FROM permitrecognitionstatus
                       LEFT JOIN users ON permitrecognitionstatus.schoolUserIdfk = users.id
                       LEFT JOIN depedcar.elementary_schools
                          ON users.schoolId = depedcar.elementary_schools.SCHOOLID
                       LEFT JOIN applicationtype ON permitrecognitionstatus.applicationTypeIdfk = applicationtype.applicationTypeId
                  WHERE permitrecognitionstatus.applicationStatusIdfk = :applicationStatusIdfk  AND elementary_schools.SCHOOLID IS NOT null";
          $stmt = $conn1 -> prepare($sql);
          $stmt -> bindParam(":applicationStatusIdfk", $applicationStatusId);
        }
        else{//SDO
          $sql = "SELECT DISTINCT (prStatusId),
                                  applicationTypeIdfk,
                                  schoolUserIdfk,
                                  gradeLevelIdfkFrom,
                                  gradeLevelIdfkTo,
                                  governmentPRNumber,
                                  seriesYear,
                                  permitrecognitionstatus.schoolYear,
                                  applicationStatusIdfk,
                                  categoryIdfk,
                                  lastDateUpdated,
                                  controlNumber,
                                 elementary_schools.SCHOOLID,
                                 elementary_schools.SCHOOLNAME,
                                 elementary_schools.DIVISION,
                                 applicationtype.description AS applicationType
                  FROM permitrecognitionstatus
                       LEFT JOIN users ON permitrecognitionstatus.schoolUserIdfk = users.id
                       LEFT JOIN depedcar.elementary_schools
                          ON users.schoolId = depedcar.elementary_schools.SCHOOLID
                       LEFT JOIN applicationtype ON permitrecognitionstatus.applicationTypeIdfk = applicationtype.applicationTypeId
                  WHERE permitrecognitionstatus.applicationStatusIdfk = :applicationStatusIdfk AND users.schoolsDivisionIdfk=:schoolsDivisionIdfk  AND elementary_schools.SCHOOLID IS NOT null";
          $stmt = $conn1 -> prepare($sql);
          $stmt -> bindParam(":applicationStatusIdfk", $applicationStatusId);
          $stmt -> bindParam(":schoolsDivisionIdfk", $schoolsDivisionIdfk);
        }
        $stmt->execute();
        $result=$stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    public function applicationlistsec($applicationStatusId){
        $applicationSY = Config::get('constants.gprpsconst.application_school_year');
        $schoolsDivisionIdfk = Auth::user()->schoolsDivisionIdfk;
        $divisionName = self::getDivisionName($schoolsDivisionIdfk);
        $conn1 = DB::connection('mysql')->getPdo();
        if($schoolsDivisionIdfk==0){//RO
          $sql = "SELECT DISTINCT (prStatusId),
                                  applicationTypeIdfk,
                                  schoolUserIdfk,
                                  gradeLevelIdfkFrom,
                                  gradeLevelIdfkTo,
                                  governmentPRNumber,
                                  seriesYear,
                                  permitrecognitionstatus.schoolYear,
                                  applicationStatusIdfk,
                                  categoryIdfk,
                                  lastDateUpdated,
                                  controlNumber,
                                  secondary_schools.SCHOOLID,
                                 secondary_schools.SCHOOLNAME,
                                 secondary_schools.DIVISION,
                                 applicationtype.description AS applicationType
                  FROM permitrecognitionstatus
                       LEFT JOIN users ON permitrecognitionstatus.schoolUserIdfk = users.id
                       LEFT JOIN depedcar.secondary_schools
                          ON users.schoolId = depedcar.secondary_schools.SCHOOLID
                       LEFT JOIN applicationtype ON permitrecognitionstatus.applicationTypeIdfk = applicationtype.applicationTypeId
                  WHERE permitrecognitionstatus.applicationStatusIdfk = :applicationStatusIdfk  AND secondary_schools.SCHOOLID IS NOT null";
          $stmt = $conn1 -> prepare($sql);
          $stmt -> bindParam(":applicationStatusIdfk", $applicationStatusId);
        }
        else{//SDO
          $sql = "SELECT DISTINCT (prStatusId),
                                  applicationTypeIdfk,
                                  schoolUserIdfk,
                                  gradeLevelIdfkFrom,
                                  gradeLevelIdfkTo,
                                  governmentPRNumber,
                                  seriesYear,
                                  permitrecognitionstatus.schoolYear,
                                  applicationStatusIdfk,
                                  categoryIdfk,
                                  lastDateUpdated,
                                  controlNumber,
                                 secondary_schools.SCHOOLID,
                                 secondary_schools.SCHOOLNAME,
                                 secondary_schools.DIVISION,
                                 applicationtype.description AS applicationType
                  FROM permitrecognitionstatus
                       LEFT JOIN users ON permitrecognitionstatus.schoolUserIdfk = users.id
                       LEFT JOIN depedcar.secondary_schools
                          ON users.schoolId = depedcar.secondary_schools.SCHOOLID
                       LEFT JOIN applicationtype ON permitrecognitionstatus.applicationTypeIdfk = applicationtype.applicationTypeId
                  WHERE permitrecognitionstatus.applicationStatusIdfk = :applicationStatusIdfk AND users.schoolsDivisionIdfk=:schoolsDivisionIdfk  AND secondary_schools.SCHOOLID IS NOT null";
          $stmt = $conn1 -> prepare($sql);
          $stmt -> bindParam(":applicationStatusIdfk", $applicationStatusId);
          $stmt -> bindParam(":schoolsDivisionIdfk", $schoolsDivisionIdfk);
        }
        $stmt->execute();
        $result=$stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    public function countApplicationStatus($applicationStatusId){
        $conn1 = DB::connection('mysql')->getPdo();
        if(Auth::user()->officeIDfk==1){
            $sql = "SELECT COUNT(*) AS total FROM `permitrecognitionstatus` WHERE applicationStatusIdfk=:applicationStatusIdfk";

            $stmt = $conn1->prepare($sql);
            $stmt->bindParam('applicationStatusIdfk', $applicationStatusId);
            $stmt->execute();
            $result=$stmt->fetch(\PDO::FETCH_ASSOC);
        }
        else if(Auth::user()->officeIDfk==2){
            $schoolsDivisionIdfk = Auth::user()->schoolsDivisionIdfk;
            $sql = "SELECT COUNT(*) AS total 
                                FROM permitrecognitionstatus
                                     LEFT JOIN users
                                        ON permitrecognitionstatus.schoolUserIdfk = users.id
                                     LEFT JOIN depedcarprofilesdb.tbl_schoolsdivisionoffices AS SDO
                                        ON users.schoolsDivisionIdfk = SDO.schoolsDivisionID
                                WHERE users.schoolsDivisionIdfk = :schoolsDivisionIdfk
                                AND applicationStatusIdfk = :applicationStatusIdfk";

            $stmt = $conn1->prepare($sql);
            $stmt->bindParam('applicationStatusIdfk', $applicationStatusId);
            $stmt->bindParam('schoolsDivisionIdfk', $schoolsDivisionIdfk);
            $stmt->execute();
            $result=$stmt->fetch(\PDO::FETCH_ASSOC);
        }
        else{
            $stmt="";
            $result="";
        }

        return $result;
    }

    public function privateSchoolElemList(){
        $applicationSY = Config::get('constants.gprpsconst.application_school_year');
        $schoolsDivisionIdfk = Auth::user()->schoolsDivisionIdfk;
        $divisionName = self::getDivisionName($schoolsDivisionIdfk);
        $conn2 = DB::connection('mysql2')->getPdo();
        if($schoolsDivisionIdfk==0){
            $stmt = $conn2 -> prepare("SELECT 
                                           elementary_schools.SCHOOLID,
                                           elementary_schools.SCHOOLNAME,
                                           elementary_schools.DIVISION,
                                           IF(USERS.created_at IS NULL,0,1) AS acctstat,
                                           PR.applicationStatusIdfk,
                                           APPSTAT.description,
                                           APPSTAT.progressClass,
                                           PR.lastDateUpdated,
                                           PR.schoolYear
                                    FROM depedcar.elementary_schools
                                         LEFT JOIN depedcarprofilesdb.tbl_schoolsdivisionoffices AS SDO
                                            ON depedcar.elementary_schools.DIVISION = SDO.schoolsDivision
                                        LEFT JOIN depedcarprivateschools.users AS USERS
                                            ON depedcar.elementary_schools.SCHOOLID = USERS.schoolId
                                         LEFT JOIN depedcarprivateschools.permitrecognitionstatus AS PR
                                            ON     PR.schoolUserIdfk = USERS.id
                                               AND PR.lastDateUpdated =
                                                   (SELECT MAX(lastDateUpdated)
                                                    FROM depedcarprivateschools.permitrecognitionstatus
                                                    WHERE     schoolUserIdfk = USERS.id
                                                          AND schoolYear =:applicationSY)
                                         LEFT JOIN depedcarprivateschools.applicationstatus AS APPSTAT
                                            ON APPSTAT.applicationStatusId = PR.applicationStatusIdfk
                                    WHERE  depedcar.elementary_schools.SUBCLASSIFICATION IN
                                                 ('Sectarian', 'Non-Sectarian')
                                    GROUP BY elementary_schools.SCHOOLID,
                                           elementary_schools.SCHOOLNAME,
                                           elementary_schools.DIVISION,
                                           acctstat,
                                           PR.applicationStatusIdfk,
                                           APPSTAT.description,
                                           APPSTAT.progressClass,
                                           PR.lastDateUpdated,
                                           PR.schoolYear");

            $stmt->bindParam(":applicationSY", $applicationSY);
        }
        else{
            $stmt = $conn2 -> prepare("SELECT 
                                           elementary_schools.SCHOOLID,
                                           elementary_schools.SCHOOLNAME,
                                           elementary_schools.DIVISION,
                                           IF(USERS.created_at IS NULL,0,1) AS acctstat,
                                           PR.applicationStatusIdfk,
                                           APPSTAT.description,
                                           APPSTAT.progressClass,
                                           PR.lastDateUpdated,
                                           PR.schoolYear
                                    FROM depedcar.elementary_schools
                                         LEFT JOIN depedcarprofilesdb.tbl_schoolsdivisionoffices AS SDO
                                            ON depedcar.elementary_schools.DIVISION = SDO.schoolsDivision
                                        LEFT JOIN depedcarprivateschools.users AS USERS
                                            ON depedcar.elementary_schools.SCHOOLID = USERS.schoolId
                                         LEFT JOIN depedcarprivateschools.permitrecognitionstatus AS PR
                                            ON     PR.schoolUserIdfk = USERS.id
                                               AND PR.lastDateUpdated =
                                                   (SELECT MAX(lastDateUpdated)
                                                    FROM depedcarprivateschools.permitrecognitionstatus
                                                    WHERE     schoolUserIdfk = USERS.id
                                                          AND schoolYear =:applicationSY)
                                         LEFT JOIN depedcarprivateschools.applicationstatus AS APPSTAT
                                            ON APPSTAT.applicationStatusId = PR.applicationStatusIdfk
                                    WHERE     depedcar.elementary_schools.DIVISION =:DIVISION
                                          AND depedcar.elementary_schools.SUBCLASSIFICATION IN
                                                 ('Sectarian', 'Non-Sectarian')
                                    GROUP BY elementary_schools.SCHOOLID,
                                           elementary_schools.SCHOOLNAME,
                                           elementary_schools.DIVISION,
                                           acctstat,
                                           PR.applicationStatusIdfk,
                                           APPSTAT.description,
                                           APPSTAT.progressClass,
                                           PR.lastDateUpdated,
                                           PR.schoolYear");
            $stmt->bindParam(":DIVISION",$divisionName);
            $stmt->bindParam(":applicationSY",$applicationSY);
        }
        $stmt->execute();
        $result=$stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }
    public function privateSchoolSecList(){
        $applicationSY = Config::get('constants.gprpsconst.application_school_year');
        $schoolsDivisionIdfk = Auth::user()->schoolsDivisionIdfk;
        $divisionName = self::getDivisionName($schoolsDivisionIdfk);
        $conn2 = DB::connection('mysql2')->getPdo();
        if($schoolsDivisionIdfk==0){
            $stmt = $conn2 -> prepare("SELECT 
                                           secondary_schools.SCHOOLID,
                                           secondary_schools.SCHOOLNAME,
                                           secondary_schools.DIVISION,
                                           IF(USERS.created_at IS NULL,0,1) AS acctstat,
                                           PR.applicationStatusIdfk,
                                           APPSTAT.description,
                                           APPSTAT.progressClass,
                                           PR.lastDateUpdated,
                                           PR.schoolYear
                                    FROM depedcar.secondary_schools
                                         LEFT JOIN depedcarprofilesdb.tbl_schoolsdivisionoffices AS SDO
                                            ON depedcar.secondary_schools.DIVISION = SDO.schoolsDivision
                                        LEFT JOIN depedcarprivateschools.users AS USERS
                                            ON depedcar.secondary_schools.SCHOOLID = USERS.schoolId
                                         LEFT JOIN depedcarprivateschools.permitrecognitionstatus AS PR
                                            ON     PR.schoolUserIdfk = USERS.id
                                               AND PR.lastDateUpdated =
                                                   (SELECT MAX(lastDateUpdated)
                                                    FROM depedcarprivateschools.permitrecognitionstatus
                                                    WHERE     schoolUserIdfk = USERS.id
                                                          AND schoolYear =:applicationSY)
                                         LEFT JOIN depedcarprivateschools.applicationstatus AS APPSTAT
                                            ON APPSTAT.applicationStatusId = PR.applicationStatusIdfk
                                    WHERE  depedcar.secondary_schools.SUBCLASSIFICATION IN
                                                 ('Sectarian', 'Non-Sectarian')
                                    GROUP BY secondary_schools.SCHOOLID,
                                           secondary_schools.SCHOOLNAME,
                                           secondary_schools.DIVISION,
                                           acctstat,
                                           PR.applicationStatusIdfk,
                                           APPSTAT.description,
                                           APPSTAT.progressClass,
                                           PR.lastDateUpdated,
                                           PR.schoolYear");
            $stmt->bindParam(":applicationSY",$applicationSY);
        }
        else{
            $stmt = $conn2 -> prepare("SELECT 
                                           secondary_schools.SCHOOLID,
                                           secondary_schools.SCHOOLNAME,
                                           secondary_schools.DIVISION,
                                           IF(USERS.created_at IS NULL,0,1) AS acctstat,
                                           PR.applicationStatusIdfk,
                                           APPSTAT.description,
                                           APPSTAT.progressClass,
                                           PR.lastDateUpdated,
                                           PR.schoolYear
                                    FROM depedcar.secondary_schools
                                         LEFT JOIN depedcarprofilesdb.tbl_schoolsdivisionoffices AS SDO
                                            ON depedcar.secondary_schools.DIVISION = SDO.schoolsDivision
                                        LEFT JOIN depedcarprivateschools.users AS USERS
                                            ON depedcar.secondary_schools.SCHOOLID = USERS.schoolId
                                         LEFT JOIN depedcarprivateschools.permitrecognitionstatus AS PR
                                            ON     PR.schoolUserIdfk = USERS.id
                                               AND PR.lastDateUpdated =
                                                   (SELECT MAX(lastDateUpdated)
                                                    FROM depedcarprivateschools.permitrecognitionstatus
                                                    WHERE     schoolUserIdfk = USERS.id
                                                          AND schoolYear =:applicationSY)
                                         LEFT JOIN depedcarprivateschools.applicationstatus AS APPSTAT
                                            ON APPSTAT.applicationStatusId = PR.applicationStatusIdfk
                                    WHERE     depedcar.secondary_schools.DIVISION =:DIVISION
                                          AND depedcar.secondary_schools.SUBCLASSIFICATION IN
                                                 ('Sectarian', 'Non-Sectarian')
                                    GROUP BY secondary_schools.SCHOOLID,
                                           secondary_schools.SCHOOLNAME,
                                           secondary_schools.DIVISION,
                                           acctstat,
                                           PR.applicationStatusIdfk,
                                           APPSTAT.description,
                                           APPSTAT.progressClass,
                                           PR.lastDateUpdated,
                                           PR.schoolYear");
            $stmt->bindParam(":DIVISION",$divisionName);
            $stmt->bindParam(":applicationSY",$applicationSY);
        }
        $stmt->execute();
        $result=$stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }
}
