<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Services\PayUService\Exception;
use App\Http\Controllers\PDO;

	global $conn;
	global $conn1;
	global $conn2;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	public function __construct(){
		$this->middleware('auth');
	    try{
	        $conn = DB::connection('mysql1')->getPdo();
	    }catch(\Exception $e){
			die ("Could not connect to the GK database. Please check your configuration. Error: "); // for depedcarprofilesdb database
	    }

	    try{
	        $conn2 = DB::connection('mysql2')->getPdo();
	    }catch(\Exception $e){
			die ("Could not connect to the SUB database. Please check your configuration. Error: "); //for depedcar database
	    }

	    try{
	        $conn1 = DB::connection('mysql')->getPdo();
	    }catch(\Exception $e){
			die ("Could not connect to the MAIN database. Please check your configuration. Error: "); // for depedcarpfpmt database
	    }
	}

//get functions
    public function gradeLevels(){
        $conn1 = DB::connection('mysql')->getPdo();
        $stmt = $conn1 -> prepare("SELECT * FROM gradelevels ORDER BY gradeLevelId");
        $stmt->execute();
        $result=$stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    public function gradeLevelsDescription($gradeLevelId){
        $conn1 = DB::connection('mysql')->getPdo();
        $stmt = $conn1 -> prepare("SELECT * FROM gradelevels WHERE gradeLevelId=:gradeLevelId");
        $stmt->bindParam(":gradeLevelId", $gradeLevelId);
        $stmt->execute();
        $result=$stmt->fetch(\PDO::FETCH_ASSOC);
        return $result["description"];
    }

    public function categoriesshs(){
        $conn1 = DB::connection('mysql')->getPdo();
        $stmt = $conn1 -> prepare("SELECT * FROM categoriesshs ORDER BY categoryId");
        $stmt->execute();
        $result=$stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    public function getSchoolsDivisions(){
        $conn = DB::connection('mysql1')->getPdo();
        $stmt = $conn -> prepare("SELECT * FROM depedcarprofilesdb.tbl_schoolsdivisionoffices");
        $stmt->execute();
        $result=$stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    public function getDivisionName($schoolsDivisionIdfk){
        $conn = DB::connection('mysql1')->getPdo();
        $stmt = $conn -> prepare("SELECT * FROM depedcarprofilesdb.tbl_schoolsdivisionoffices WHERE depedcarprofilesdb.tbl_schoolsdivisionoffices.schoolsDivisionID = :schoolsDivisionIdfk");
        $stmt->bindParam(":schoolsDivisionIdfk",$schoolsDivisionIdfk);
        $stmt->execute();
        $result=$stmt->fetch(\PDO::FETCH_ASSOC);
        return $result["schoolsDivision"];
    }

    public function getDivisionId(){
    	$userId = Auth::id();
        $conn1 = DB::connection('mysql')->getPdo();
        $stmt = $conn1 -> prepare("SELECT * FROM users WHERE id = :id");
        $stmt->bindParam(":id", $userId);
        $stmt->execute();
        $result=$stmt->fetch(\PDO::FETCH_ASSOC);
        return $result["schoolsDivisionIdfk"];
    }

    public function getFileUploadStatus($prStatusId, $reqId, $level){
    	if($level==1){
    		$condition = "`permitrecognitiondocreview`.mainReqidfk=:reqId";
    	}
    	elseif($level==2){
    		$condition = "`permitrecognitiondocreview`.subReq1Idfk=:reqId";	
    	}
    	elseif($level==3){
    		$condition = "`permitrecognitiondocreview`.subReq2Idfk=:reqId";
    	}

    	$conn1 = DB::connection('mysql')->getPdo();
    	$sql = $conn1 -> prepare("SELECT
								    `prDocReviewId`,
								    `permitrecognitiondocreview`.prStatusIdfk,
								    `permitrecognitiondocreview`.mainReqidfk,
								    `subReq1Idfk`,
								    `subReq2Idfk`,
								    `fileUploadStatus`,
								    `evaluationStatus`,
								    `inspectionStatus`,
								    `roReviewStatus`,
                                    `inspectionROStatus`,
								    `complete`,
								    `remarks`,
								    `fileIdfk`,
								    `updatedBy`,
								    `lastDateUpdated`,
								    `fileuploads`.newFileName,
								    `fileuploads`.fileSize,
								    `fileuploads`.dateUploaded
								FROM
								    `permitrecognitiondocreview`
								LEFT JOIN `fileuploads`
									ON `permitrecognitiondocreview`.fileIdfk = `fileuploads`.fileId     
								WHERE
								    `permitrecognitiondocreview`.prStatusIdfk=:prStatusIdfk 
								    AND ".$condition ." 
                                ORDER BY `lastDateUpdated` DESC");
    	$sql->bindParam(":prStatusIdfk", $prStatusId);
    	$sql->bindParam(":reqId", $reqId);
    	$sql->execute();
    	$result=$sql->fetch(\PDO::FETCH_ASSOC);
    	return $result;
    }

    public function getFileUploadStatusFindings($prStatusId, $applicationTypeIdfk, $documentTitle){

        $conn1 = DB::connection('mysql')->getPdo();
        $sql = $conn1 -> prepare("SELECT
                                    `fileId`,
                                    `applicationTypeIdfk`,
                                    `prStatusIdfk`,
                                    `mainReqIdfk`,
                                    `originalFileName`,
                                    `documentTitle`,
                                    `newFileName`,
                                    `fileSize`,
                                    `uploadedBy`,
                                    `dateUploaded`
                                FROM
                                    `fileuploads`
                                WHERE
                                    `prStatusIdfk`=:prStatusIdfk AND `applicationTypeIdfk`=:applicationTypeIdfk AND `documentTitle`=:documentTitle");
        $sql->bindParam(":prStatusIdfk", $prStatusId);
        $sql->bindParam(":applicationTypeIdfk", $applicationTypeIdfk);
        $sql->bindParam(":documentTitle", $documentTitle);
        $sql->execute();
        $result=$sql->fetch(\PDO::FETCH_ASSOC);
        return $result;
    }

    public function getDocReviewStatus($prStatusId, $mainReqId, $subReq1Id, $subReq2Id){
    	$conn1 = DB::connection('mysql')->getPdo();
    	$sql = $conn1->prepare("
							SELECT
							    `prDocReviewId`,
							    `prStatusIdfk`,
							    `mainReqidfk`,
							    `subReq1Idfk`,
							    `subReq2Idfk`,
							    `fileUploadStatus`,
							    `requirementRemarks`,
							    `evaluationStatus`,
							    `evaluationRemarks`,
							    `inspectionStatus`,
							    `inspectionRemarks`,
							    `roReviewStatus`,
							    `roReviewRemarks`,
							    `inspectionROStatus`,
							    `inspectionRORemarks`,
							    `complete`,
							    `remarks`,
							    `fileIdfk`,
							    `updatedBy`,
							    `lastDateUpdated`
							FROM
							    `permitrecognitiondocreview`
							WHERE
							    `prStatusIdfk`=:prStatusIdfk 
							    AND `mainReqidfk`=:mainReqidfk
							    AND `subReq1Idfk`=:subReq1Idfk
							    AND `subReq2Idfk`=:subReq2Idfk
                            ORDER BY `lastDateUpdated` DESC
    					");

    	$sql->bindParam('prStatusIdfk', $prStatusId);
    	$sql->bindParam('mainReqidfk', $mainReqId);
    	$sql->bindParam('subReq1Idfk', $subReq1Id);
    	$sql->bindParam('subReq2Idfk', $subReq2Id);
        $sql->execute();
    	$result=$sql->fetch(\PDO::FETCH_ASSOC);
    	return $result;
    }

    public function getInspectionComposition($inspectionTeamId){
        $conn = DB::connection('mysql')->getPdo();
        $stmt = $conn -> prepare("SELECT * FROM `inspectioncomposition` WHERE `inspectionTeamIdfk`=:inspectionTeamIdfk");
        $stmt->bindParam(":inspectionTeamIdfk", $inspectionTeamId);
        $stmt->execute();
        $result=$stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    public function getSdoPrivateSchoolFocal($schoolsDivisionId){
        $conn1 = DB::connection('mysql')->getPdo();
        $stmt = $conn1 -> prepare("SELECT * FROM `users` WHERE `schoolsDivisionIdfk` = :schoolsDivisionIdfk AND `inspectionCompositionIdfk`=2 AND `isActive`=1");
        $stmt->bindParam(":schoolsDivisionIdfk", $schoolsDivisionId);
        $stmt->execute();
        $result=$stmt->fetch(\PDO::FETCH_ASSOC);
        //print_r($result);
        return $result["email"];
    }

// pinakamahirap function
    public function newApplicationRequirements($prStatusId){
    	$appInfo = self::applicationInfo($prStatusId);
    	$latestStagesTimeLineInfo = self::latestStagesTimeLineInfo($prStatusId);

            $sdoIdfk = $appInfo["schoolsDivisionIdfk"];
            $divisionName = self::getDivisionName($sdoIdfk);

            $sdoAssessment = self::getFileUploadStatusFindings($prStatusId, $appInfo["applicationTypeIdfk"], "SDO Assessment");
            $target_file = '/gprps/public/uploads/' .$divisionName.'/'. $prStatusId . '/'.$sdoAssessment["newFileName"];

            $sdoOcularVisit = self::getFileUploadStatusFindings($prStatusId, $appInfo["applicationTypeIdfk"], "SDO Ocular Visit");
            $target_file_sdo_ocular_visit = '/gprps/public/uploads/' .$divisionName.'/'. $prStatusId . '/'.$sdoOcularVisit["newFileName"];

            $roReview = self::getFileUploadStatusFindings($prStatusId, $appInfo["applicationTypeIdfk"], "RO Review");
            $target_file_ro_review = '/gprps/public/uploads/' .$divisionName.'/'. $prStatusId . '/'.$roReview["newFileName"];

            $roOcularVisit = self::getFileUploadStatusFindings($prStatusId, $appInfo["applicationTypeIdfk"], "RO Ocular Visit");
            $target_file_ro_ocular_visit = '/gprps/public/uploads/' .$divisionName.'/'. $prStatusId . '/'.$roOcularVisit["newFileName"];
            //$target_file_windows = "\uploads\\" . $divisionName."\\". $prStatusId . "\\".$sdoAssessment["newFileName"];

    	if($appInfo["applicationTypeIdfk"]==3){
    		$applicationId = 1;
    	}
    	else{
    		$applicationId = $appInfo["applicationTypeIdfk"];	
    	}

    	$returnValue = "";
    	$returnValue .='
                        <thead>
                            <tr>';
                                if(Auth::user()->officeIDfk==3 && ($appInfo["applicationStatusIdfk"]==1 || $appInfo["applicationStatusIdfk"]==8)){
                                    $returnValue.='<th colspan="3" style="text-align: center;border-bottom: #dd4b39 3px solid;">School</th>';
                                }
                                else{
                                    $returnValue.='<th colspan="2" style="text-align: center;border-bottom: #dd4b39 3px solid;">School</th>';   
                                }
                            $returnValue.='
                                <th colspan="2" style="text-align: center;border-bottom: #ff851b 3px solid;">Schools Division Office</th>
                                <th colspan="2" style="text-align: center;border-bottom: #00a65a 3px solid;">Regional Office</th>
                            </tr>
                            <tr>';
                                if(Auth::user()->officeIDfk==3 && ($appInfo["applicationStatusIdfk"]==1 || $appInfo["applicationStatusIdfk"]==8)){
                                    $returnValue.='<th colspan="3" style="text-align: center;border-left: #dd4b39 3px solid;border-right: #dd4b39 3px solid;border-bottom:0px;"></th>';
                                }
                                else{
                                    $returnValue.='<th colspan="2" style="text-align: center;border-left: #dd4b39 3px solid;border-right: #dd4b39 3px solid;border-bottom:0px;"></th>';   
                                }
                            $returnValue.='
                                <th colspan="2" style="text-align: center;border-left: #ff851b 3px solid;border-right: #ff851b 3px solid;border-bottom:0px;"></th>
                                <th colspan="2" style="text-align: center;border-left: #00a65a 3px solid;border-right: #00a65a 3px solid;border-bottom:0px;"></th>
                            </tr>

                            <tr valign="top">';
                                if(Auth::user()->officeIDfk==3 && ($appInfo["applicationStatusIdfk"]==1 || $appInfo["applicationStatusIdfk"]==8)){
                                    $returnValue .='<th width="60px;" style="vertical-align: middle;border-top:0px;">Upload</th>';
                                }
                            $returnValue .='                                
                                <th  style="vertical-align: middle;border-top:0px;"></th>
                                <th width="150px" class="text-center" style="vertical-align: middle;border-top:0px;"></span>
                                </th>
                                <th width="130px" class="text-center" style="vertical-align: top;border-top:0px;">';
                                if(Auth::user()->officeIDfk==2){
                                    $returnValue .='
                                    <button type="button" class="btn btn-xs bg-purple btn-flat btn-upload-file" data-toggle="modal" data-target="#modal-upload-sdo-assessment">
                                        <span class="fa fa-upload TT" data-toggle="tooltip" data-html="true" title="Upload Assessment File"></span>
                                    </button>&nbsp';
                                }
                                if($sdoAssessment["newFileName"]!="" || $sdoAssessment["newFileName"]!=null){
                                    $returnValue.= '<span class="label label-info fileUploaded-sdo-ro">File uploaded</span>
                                    <a href="'. $target_file .'" class="TT" target="_blank" data-toggle="tooltip" title="Download File"><i class="fa fa-download">
                                    </i></a>';
                                }
                                else{
                                    $returnValue.= '<span class="label label-default fileUploaded-sdo-ro">No File uploaded</span><br>';
                                }

                                    $returnValue .='
                                </th>
                                <th width="130px" class="text-center" style="vertical-align: top;border-top:0px;">';
                                if(Auth::user()->officeIDfk==2){
                                    $returnValue .='
                                    <button type="button" class="btn btn-xs bg-purple btn-flat btn-upload-file" data-toggle="modal" data-target="#modal-upload-sdo-ocular-visit">
                                        <span class="fa fa-upload TT" data-toggle="tooltip" data-html="true" title="Upload<br>Ocular Visit File"></span>
                                    </button>&nbsp;';
                                }
                                if($sdoOcularVisit["newFileName"]!="" || $sdoOcularVisit["newFileName"]!=null){
                                    $returnValue.= '<span class="label label-info fileUploaded-sdo-ro">File uploaded</span>
                                    <a href="'. $target_file_sdo_ocular_visit .'" class="TT" target="_blank" data-toggle="tooltip" title="Download File"><i class="fa fa-download">
                                    </i></a>';
                                }
                                else{
                                    $returnValue.= '<span class="label label-default fileUploaded-sdo-ro">No File uploaded</span><br>';
                                }
                                    $returnValue .='

                                </th>
                                <th width="130px" class="text-center"  style="vertical-align: top;border-top:0px;">';
                                if(Auth::user()->officeIDfk==1){
                                    $returnValue .='
                                    <button type="button" class="btn btn-xs bg-purple btn-flat btn-upload-file" data-toggle="modal" data-target="#modal-upload-ro-review">
                                        <span class="fa fa-upload TT" data-toggle="tooltip" data-html="true" title="Upload<br>RO Review File"></span>
                                    </button>&nbsp;';
                                }
                                if($roReview["newFileName"]!="" || $roReview["newFileName"]!=null){
                                    $returnValue.= '<span class="label label-info fileUploaded-sdo-ro">File uploaded</span>
                                    <a href="'. $target_file_ro_review .'" class="TT" target="_blank" data-toggle="tooltip" title="Download File"><i class="fa fa-download">
                                    </i></a>';
                                }
                                else{
                                    $returnValue.= '<span class="label label-default fileUploaded-sdo-ro">No File uploaded</span><br>';
                                }
                                    $returnValue .='

                                </th>
                                <th width="130px" class="text-center" style="vertical-align: top;border-top:0px;">';
                                if(Auth::user()->officeIDfk==1){
                                    $returnValue .='
                                    <button type="button" class="btn btn-xs bg-purple btn-flat btn-upload-file" data-toggle="modal" data-target="#modal-upload-ro-ocular-visit">
                                        <span class="fa fa-upload TT" data-toggle="tooltip" data-html="true" title="Upload<br>RO Ocular Visit File"></span>
                                    </button>&nbsp;';
                                }
                                if($roOcularVisit["newFileName"]!="" || $roOcularVisit["newFileName"]!=null){
                                    $returnValue.= '<span class="label label-info fileUploaded-sdo-ro">File uploaded</span>
                                    <a href="'. $target_file_ro_ocular_visit .'" class="TT" target="_blank" data-toggle="tooltip" title="Download File"><i class="fa fa-download">
                                    </i></a>';
                                }
                                else{
                                    $returnValue.= '<span class="label label-default fileUploaded-sdo-ro">No File uploaded</span><br>';
                                }
                                    $returnValue .='

                                </th>
                            </tr>

                            <tr valign="top">';
                                if(Auth::user()->officeIDfk==3 && ($appInfo["applicationStatusIdfk"]==1 || $appInfo["applicationStatusIdfk"]==8)){
                                    $returnValue .='<th width="60px;" style="vertical-align: middle;border-top:0px;">Upload</th>';
                                }
                            $returnValue .='                                
                                <th  style="vertical-align: top;border-top:0px;">Requirements</th>
                                <th width="150px" class="text-center" style="vertical-align: top;border-top:0px;"><span class="fa fa-upload fa-3x behind-icons" style="margin-left: -20px;"></span><span class="front-text">Uploading<br>Status</span>
                                </th>
                                <th width="150px" class="text-center" style="vertical-align: top;border-top:0px;">';
                                    $returnValue .='
                                    <span class="fa fa-tasks fa-3x behind-icons" style="margin-left: -18px;"></span>
                                    <span class="front-text">Assessment /<br>Evaluation</span>
                                </th>
                                <th width="150px" class="text-center" style="vertical-align: top;border-top:0px;">';

                                    $returnValue .='
                                    <span class="fa fa-search fa-3x behind-icons" style="margin-left: -16px;"></span></span>
                                    <span class="front-text">SDO Ocular<br>Visit</span>
                                </th>
                                <th width="150px" class="text-center"  style="vertical-align: top;border-top:0px;">';
                                    $returnValue .='
                                    <span class="fa fa-eye fa-3x behind-icons" style="margin-left: -45px;"></span></span>
                                    <span class="front-text">RO<br>Review</span>
                                </th>
                                <th width="150px" class="text-center" style="vertical-align: top;border-top:0px;">';
                                    $returnValue .='
                                    <span class="fa fa-search fa-3x behind-icons" style="margin-left: -18px;"></span></span>
                                    <span class="front-text">RO Ocular<br>Visit</span>
                                </th>
                            </tr>


                        </thead>
                        <tbody>';
    	if($appInfo["levelIdfk"]==4 && $appInfo["applicationTypeIdfk"]!=2){
    		$tableName = "mainrequirements_shs";	
    	}
    	else{
    		$tableName = "mainrequirements";
    	}
    	
        //-------------------------------------------------- main requirements -----------------------------------------------
        $conn1 = DB::connection('mysql')->getPdo();

        $stmt = $conn1 -> prepare("SELECT * FROM ". $tableName ." WHERE applicationTypeIdfk=:applicationTypeIdfk AND schoolYear=:schoolYear AND isActive=1");
        $stmt->bindParam(":applicationTypeIdfk",$applicationId);
        $stmt->bindParam(":schoolYear",$appInfo["schoolYear"]);
        $stmt->execute();
        $result=$stmt->fetchAll(\PDO::FETCH_ASSOC);
        $startMain = 1;
        foreach($result as $mainReq){
        	$resultMainReq = self::getFileUploadStatus($prStatusId, $mainReq["mainReqId"], 1);
        	$resultDocReviewMain = self::getDocReviewStatus($prStatusId, $mainReq["mainReqId"], 0, 0);
            $sdoIdfk = $appInfo["schoolsDivisionIdfk"];
        	$divisionName = self::getDivisionName($sdoIdfk);
            $hasSubReq = self::hasSubReq($mainReq["mainReqId"], $appInfo["schoolYear"]);
        	$target_file = '/gprps/public/uploads/' .$divisionName.'/'. $prStatusId . '/'.$resultMainReq["newFileName"];
            $target_file_windows = "\uploads\\" . $divisionName."\\". $prStatusId . "\\".$resultMainReq["newFileName"];

        	$returnValue.= '
        		<tr>';
            			if(Auth::user()->officeIDfk==3 && ($appInfo["applicationStatusIdfk"]==1 || $appInfo["applicationStatusIdfk"]==8)){
            				$returnValue.= '
            			<td style="text-align: center;">
            				<button type="button" class="btn btn-xs bg-purple btn-flat btn-upload-file" data-toggle="modal" data-target="#modal-upload-requirement-for-new-application">
                                <span class="fa fa-upload TT" data-toggle="tooltip" title="Upload File"></span>
                            </button>
                        </td>';
                    	}
                $returnValue.= '
        			<td>
						<ol style="list-style-type: decimal; margin-bottom:0px; margin-left:-20px;" start="'. $startMain .'">
							<strong><li class="mainReq" mainReqId="'. $mainReq["mainReqId"] .'" mainRedDesc="'. $mainReq["description"] .'">'. $mainReq["description"] .'</li></strong>
						</ol>
					</td>
					<td class="text-center">';
						if($resultMainReq["fileUploadStatus"]==1){
							$returnValue.= '<span class="label label-success fileUploaded">File uploaded</span>';
							if(Auth::user()->officeIDfk==3 || (Auth::user()->officeIDfk==1 || Auth::user()->officeIDfk==2 && ($appInfo["applicationStatusIdfk"]!=3 && $appInfo["applicationStatusIdfk"]!=1 && $appInfo["applicationStatusIdfk"]!=8))) {
								$returnValue.= '
								<a href="'. $target_file .'" class="TT" target="_blank" data-toggle="tooltip" title="Download File"><i class="fa fa-download">
									</i></a>';
							}
							if(Auth::user()->officeIDfk==3){
								$returnValue.= '
								<a href="#" prStatusId="'. $appInfo["prStatusId"] .'" applicationTypeId="'. $appInfo["applicationTypeIdfk"] .'" targetFile="'. $target_file_windows .'" fileName="'. $resultMainReq["newFileName"] .'" class="TT btn-delete-file"  data-toggle="modal" data-target="#modal-delete-requirement-for-new-application"><i class="fa fa-trash TT" data-toggle="tooltip" title="Remove File" style="color: red;"></i></a>
									';
							}
						}
						else{
							$returnValue.= '<span class="label label-danger">No file uploaded</span>';
						}
			$returnValue.= '			
					</td>
					<td class="text-center">';
//----- check box indicator Assessment / Evaluation
                       // if(!$hasSubReq){
                            if(Auth::user()->officeIDfk==2 && $appInfo["applicationStatusIdfk"]==4 && ($latestStagesTimeLineInfo["stageStatusIdfk"]==4 || $latestStagesTimeLineInfo["stageStatusIdfk"]==5 || $latestStagesTimeLineInfo["stageStatusIdfk"]==6 || $latestStagesTimeLineInfo["stageStatusIdfk"]==24)){
                                if($resultMainReq["evaluationStatus"]==1){
                                    $returnValue.= '
                                    <input type="checkbox" class="reqChkBox" name="reqChkBox" oneorall="all" reqLevel="main" statusType="evaluationStatus" prDocReviewId="'. $resultDocReviewMain["prDocReviewId"] .'" checked="checked">';
                                }
                                else{
                                    $returnValue.= '
                                    <input type="checkbox" class="reqChkBox" name="reqChkBox" oneorall="all" reqLevel="main" statusType="evaluationStatus" prDocReviewId="'. $resultDocReviewMain["prDocReviewId"] .'">';
                                }
                            }
                      //  }

						if($resultMainReq["evaluationStatus"]==1 && $resultMainReq["fileUploadStatus"]==1){
							$returnValue.= '<span class="label label-success assesment-indicator-complete">Complete</span>';
						}
						elseif($resultMainReq["evaluationStatus"]==0 && $resultMainReq["fileUploadStatus"]==1){
							$returnValue.= '<span class="label label-warning">Incomplete</span>';
						}
						else{
							$returnValue.= '<span class="label label-warning">Pending</span>';
						}

//------ comment indicator SDO Assessment / Evaluation------------------
                        if($latestStagesTimeLineInfo["stageStatusIdfk"]>=4){
                            if($resultDocReviewMain["evaluationRemarks"]!=""){
                                $returnValue.= '&nbsp;<span class="fa fa-comment text-warning for-read-write TT pointer" data-toggle="modal" data-target="#modal-remarks" data-toggle="tooltip" data-placement="top" title="'. $resultDocReviewMain["evaluationRemarks"] .'" remarksId='. $resultDocReviewMain["prDocReviewId"] .' statusType="evaluationRemarks"></span>';
                            }
                            else{
                                $returnValue.= '&nbsp;<span class="fa fa-comment-o text-warning for-read-write TT pointer" data-toggle="modal" data-target="#modal-remarks" data-toggle="tooltip" data-placement="top" title="'. $resultDocReviewMain["evaluationRemarks"] .'" remarksId='. $resultDocReviewMain["prDocReviewId"] .' statusType="evaluationRemarks"></span>';
                            }
                        }
			$returnValue.= '			
					</td>
					<td class="text-center">';
//----- check box indicator sdo ocular visit
                        //if(!$hasSubReq){
                            if(Auth::user()->officeIDfk==2 && $appInfo["applicationStatusIdfk"]==4 && ($latestStagesTimeLineInfo["stageStatusIdfk"]==7 || $latestStagesTimeLineInfo["stageStatusIdfk"]==11)){
                                if($resultMainReq["inspectionStatus"]==1){
                $returnValue.= '
                            <input type="checkbox" class="reqChkBox" name="reqChkBox" oneorall="all" reqLevel="main" statusType="inspectionStatus" prDocReviewId="'. $resultDocReviewMain["prDocReviewId"] .'" checked="checked">';
                                }
                                else{
                $returnValue.= '
                            <input type="checkbox" class="reqChkBox" name="reqChkBox" oneorall="all" reqLevel="main" statusType="inspectionStatus" prDocReviewId="'. $resultDocReviewMain["prDocReviewId"] .'">';
                                }
                            }
                       // }

						if($resultMainReq["inspectionStatus"]==1 && $resultMainReq["evaluationStatus"]==1 && $resultMainReq["fileUploadStatus"]==1){
							$returnValue.= '<span class="label label-success sdo-ocular-visit-indicator-complete">Complete</span>';
						}
						elseif($resultMainReq["inspectionStatus"]==0 && $resultMainReq["evaluationStatus"]==1 && $resultMainReq["fileUploadStatus"]==1){
							$returnValue.= '<span class="label label-warning">Incomplete</span>';
						}
						else{
							$returnValue.= '<span class="label label-warning">Pending</span>';
						}

//------ comment indicator SDO Ocular Visit------------------
                        if($latestStagesTimeLineInfo["stageStatusIdfk"]>=7){
                            if($resultDocReviewMain["inspectionRemarks"]!=""){
                                $returnValue.= '&nbsp;<span class="fa fa-comment text-warning for-read-write TT pointer" data-toggle="modal" data-target="#modal-remarks" data-toggle="tooltip" data-placement="top" title="'. $resultDocReviewMain["inspectionRemarks"] .'" remarksId='. $resultDocReviewMain["prDocReviewId"] .' statusType="inspectionRemarks"></span>';
                            }
                            else{
                                $returnValue.= '&nbsp;<span class="fa fa-comment-o text-warning for-read-write TT pointer" data-toggle="modal" data-target="#modal-remarks" data-toggle="tooltip" data-placement="top" title="'. $resultDocReviewMain["inspectionRemarks"] .'" remarksId='. $resultDocReviewMain["prDocReviewId"] .' statusType="inspectionRemarks"></span>';
                            }
                        }

			$returnValue.= '			
					</td>
					<td class="text-center">';

//----- check box indicator RO Review
                        if(Auth::user()->officeIDfk==1 && $appInfo["applicationStatusIdfk"]==4 && ($latestStagesTimeLineInfo["stageStatusIdfk"]==13 || $latestStagesTimeLineInfo["stageStatusIdfk"]==14 || $latestStagesTimeLineInfo["stageStatusIdfk"]==16 || $latestStagesTimeLineInfo["stageStatusIdfk"]==17)){
                            if($resultMainReq["roReviewStatus"]==1){
            $returnValue.= '
                        <input type="checkbox" class="reqChkBox" name="reqChkBox" oneorall="all" reqLevel="main" statusType="roReviewStatus" prDocReviewId="'. $resultDocReviewMain["prDocReviewId"] .'" checked="checked">';
                            }
                            else{
            $returnValue.= '
                        <input type="checkbox" class="reqChkBox" name="reqChkBox" oneorall="all" reqLevel="main" statusType="roReviewStatus" prDocReviewId="'. $resultDocReviewMain["prDocReviewId"] .'">';
                            }
                        }

						if($resultMainReq["roReviewStatus"]==1 && $resultMainReq["inspectionStatus"]==1 && $resultMainReq["evaluationStatus"]==1 && $resultMainReq["fileUploadStatus"]==1){
							$returnValue.= '<span class="label label-success ro-for-review-indicator-complete">Complete</span>';
						}
						elseif($resultMainReq["roReviewStatus"]==0 && $resultMainReq["inspectionStatus"]==1 && $resultMainReq["evaluationStatus"]==1 && $resultMainReq["fileUploadStatus"]==1){
							$returnValue.= '<span class="label label-warning">Incomplete</span>';
						}
						else{
							$returnValue.= '<span class="label label-warning">Pending</span>';
						}

//------comment indicator RO Review ------------------
                        if($latestStagesTimeLineInfo["stageStatusIdfk"]>=13){
                            if($resultDocReviewMain["roReviewRemarks"]!=""){
                                $returnValue.= '&nbsp;<span class="fa fa-comment text-warning for-read-write TT pointer" data-toggle="modal" data-target="#modal-remarks" data-toggle="tooltip" data-placement="top" title="'. $resultDocReviewMain["roReviewRemarks"] .'" remarksId='. $resultDocReviewMain["prDocReviewId"] .' statusType="roReviewRemarks"></span>';
                            }
                            else{
                                $returnValue.= '&nbsp;<span class="fa fa-comment-o text-warning for-read-only TT pointer" data-toggle="modal" data-target="#modal-remarks" data-toggle="tooltip" data-placement="top" title="'. $resultDocReviewMain["roReviewRemarks"] .'" remarksId='. $resultDocReviewMain["prDocReviewId"] .' statusType="roReviewRemarks"></span>';
                            }
                        }

			$returnValue.= '
					</td>
					<td class="text-center">';
						if($resultMainReq["inspectionROStatus"]==1 && $resultMainReq["roReviewStatus"]==1 && $resultMainReq["inspectionStatus"]==1 && $resultMainReq["evaluationStatus"]==1 && $resultMainReq["fileUploadStatus"]==1){
							$returnValue.= '<span class="label label-success ro-ocular-visit-indicator-complete">Complete</span>';
						}
						elseif($resultMainReq["inspectionROStatus"]==0 && $resultMainReq["roReviewStatus"]==1 && $resultMainReq["inspectionStatus"]==1 && $resultMainReq["evaluationStatus"]==1 && $resultMainReq["fileUploadStatus"]==1){
							$returnValue.= '<span class="label label-warning">Incomplete</span>';
						}
						else{
							$returnValue.= '<span class="label label-warning">Pending</span>';
						}


//------ comment indicator RO RO Ocular Visit ------------------
                        if($latestStagesTimeLineInfo["stageStatusIdfk"]>=15){
                            if($resultDocReviewMain["inspectionRORemarks"]!=""){
                                $returnValue.= '&nbsp;<span class="fa fa-comment text-warning for-read-write TT" data-toggle="modal" data-target="#modal-remarks" data-toggle="tooltip" data-placement="top" title="'. $resultDocReviewMain["inspectionRORemarks"] .'" remarksId='. $resultDocReviewMain["prDocReviewId"] .' statusType="inspectionRORemarks"></span>';
                            }
                            else{
                                $returnValue.= '&nbsp;<span class="fa fa-comment-o text-warning for-read-only TT" data-toggle="modal" data-target="#modal-remarks" data-toggle="tooltip" data-placement="top" title="'. $resultDocReviewMain["inspectionRORemarks"] .'" remarksId='. $resultDocReviewMain["prDocReviewId"] .' statusType="inspectionRORemarks"></span>';
                            }
                        }

			$returnValue.= '
					</td>
				</tr>';
			$startMain++;

            //-------------------------------------------------- sub requirements 1 -----------------------------------------------
	    	if($appInfo["levelIdfk"]==4 && $appInfo["applicationTypeIdfk"]!=2){
	    		$tableName = "subrequirements1_shs";	
	    	}
	    	else{
	    		$tableName = "subrequirements1";
	    	}
			$stmt1 = $conn1 -> prepare("SELECT * FROM ". $tableName ." WHERE mainReqIdfk=:mainReqIdfk AND isActive=1");
	        $stmt1->bindParam(":mainReqIdfk",$mainReq["mainReqId"]);
	        $stmt1->execute();
	        $result1=$stmt1->fetchAll(\PDO::FETCH_ASSOC);
	        if(count($result1)!=0){
		        $startSub1 = 1;
		        foreach($result1 as $subReq1){
		        	$resultSub1Req = self::getFileUploadStatus($prStatusId, $subReq1["subReq1Id"], 2);
		        	$resultDocReviewSub1 = self::getDocReviewStatus($prStatusId, $mainReq["mainReqId"], $subReq1["subReq1Id"], 0);
		        	$returnValue.= '
		        		<tr>';
		        		if(Auth::user()->officeIDfk==3 && ($appInfo["applicationStatusIdfk"]==1 || $appInfo["applicationStatusIdfk"]==8)){
		        			$returnValue.= '
		        			<td>
		        				&nbsp;
		                    </td>';
		                }
		                $returnValue.= '
		        			<td>
								<ol style="list-style-type: lower-alpha; margin-bottom:0px; margin-left: 0px;" start="'. $startSub1 .'">
									<li class="subReq1" subReq1Id="'. $subReq1["subReq1Id"] .'">'. $subReq1["description"] .'</li>
								</ol>
							</td>
							<td class="text-center">';
							if($resultSub1Req["fileUploadStatus"]==1){
								$returnValue.= '<span class="fa fa-check text-success"></span>';
							}
							else{
								$returnValue.= '<span class="fa fa-times text-danger"></span>';
							}
					$returnValue.= '
							</td>
							<td class="text-center">';
//------check marks indicator SDO Assessment / Evaluation------------------
							if(Auth::user()->officeIDfk==1 || Auth::user()->officeIDfk==3 || (Auth::user()->officeIDfk==2 && $latestStagesTimeLineInfo["stageStatusIdfk"]>=7) && $resultMainReq["fileUploadStatus"]==1){
								if($resultDocReviewSub1["evaluationStatus"]==1){
									$returnValue.= '<span class="fa fa-check text-success"></span>';
								}
								else{
									$returnValue.= '<span class="fa fa-times text-warning"></span>';
								}
							}
							else{
							  if(Auth::user()->officeIDfk==2 && $appInfo["applicationStatusIdfk"]==4 && ($latestStagesTimeLineInfo["stageStatusIdfk"]==4 || $latestStagesTimeLineInfo["stageStatusIdfk"]==5 || $latestStagesTimeLineInfo["stageStatusIdfk"]==6)){
								if($resultDocReviewSub1["evaluationStatus"]==1){
									$returnValue.= '<input type="checkbox" class="reqChkBox" oneorall="one" reqLevel="sub1" name="reqChkBox" statusType="evaluationStatus" prDocReviewId="'. $resultDocReviewSub1["prDocReviewId"] .'" checked="checked">';
								}
								else{
									$returnValue.= '<input type="checkbox" class="reqChkBox" oneorall="one" reqLevel="sub1" name="reqChkBox" statusType="evaluationStatus" prDocReviewId="'. $resultDocReviewSub1["prDocReviewId"] .'">';
								}
							  }
							}
							

					$returnValue.= '
							</td>
							<td class="text-center">';
//------check marks indicator SDO Ocular Visit------------------
							if(Auth::user()->officeIDfk==1 || Auth::user()->officeIDfk==3 || (Auth::user()->officeIDfk==2 && $latestStagesTimeLineInfo["stageStatusIdfk"]>=12)){
								if($resultDocReviewSub1["inspectionStatus"]==1){
									$returnValue.= '<span class="fa fa-check text-success"></span>';
								}
								else{
									$returnValue.= '<span class="fa fa-times text-warning"></span>';
								}
							}
							else{
							  if(Auth::user()->officeIDfk==2 && $appInfo["applicationStatusIdfk"]==4 && ($latestStagesTimeLineInfo["stageStatusIdfk"]==7 || $latestStagesTimeLineInfo["stageStatusIdfk"]==10 || $latestStagesTimeLineInfo["stageStatusIdfk"]==11 || $latestStagesTimeLineInfo["stageStatusIdfk"]==12)){
								if($resultDocReviewSub1["inspectionStatus"]==1){
									$returnValue.= '<input type="checkbox" class="reqChkBox" name="reqChkBox" oneorall="one" reqLevel="sub1" statusType="inspectionStatus" prDocReviewId="'. $resultDocReviewSub1["prDocReviewId"] .'" checked="checked">';
								}
								else{
									$returnValue.= '<input type="checkbox" class="reqChkBox" name="reqChkBox" oneorall="one" reqLevel="sub1" statusType="inspectionStatus" prDocReviewId="'. $resultDocReviewSub1["prDocReviewId"] .'">';
								}
							  }
							}

					$returnValue.= '
							</td>
							<td class="text-center">';
//------check marks indicator RO Review ------------------
							if(Auth::user()->officeIDfk==2 || Auth::user()->officeIDfk==3 || (Auth::user()->officeIDfk==1 && $latestStagesTimeLineInfo["stageStatusIdfk"]>=18)){
								if($resultDocReviewSub1["roReviewStatus"]==1){
									$returnValue.= '<span class="fa fa-check text-success"></span>';
								}
								else{
									$returnValue.= '<span class="fa fa-times text-warning"></span>';
								}
							}
							else{
							  if(Auth::user()->officeIDfk==1 && $appInfo["applicationStatusIdfk"]==4 && ($latestStagesTimeLineInfo["stageStatusIdfk"]==13 || $latestStagesTimeLineInfo["stageStatusIdfk"]==14 || $latestStagesTimeLineInfo["stageStatusIdfk"]==16 || $latestStagesTimeLineInfo["stageStatusIdfk"]==17)){
								if($resultDocReviewSub1["roReviewStatus"]==1){
									$returnValue.= '<input type="checkbox" class="reqChkBox" oneorall="one" name="reqChkBox" reqLevel="sub1" statusType="roReviewStatus" prDocReviewId="'. $resultDocReviewSub1["prDocReviewId"] .'" checked="checked">';
								}
								else{
									$returnValue.= '<input type="checkbox" class="reqChkBox" oneorall="one" name="reqChkBox" reqLevel="sub1" statusType="roReviewStatus" prDocReviewId="'. $resultDocReviewSub1["prDocReviewId"] .'">';
								}
							  }
							}

					$returnValue.= '
							</td>
							</td>
							<td class="text-center">';
//------check marks indicator RO RO Ocular Visit ------------------
							if(Auth::user()->officeIDfk==2 || Auth::user()->officeIDfk==3 || (Auth::user()->officeIDfk==1 && $latestStagesTimeLineInfo["stageStatusIdfk"]>=18)){
								if($resultDocReviewSub1["inspectionROStatus"]==1){
									$returnValue.= '<span class="fa fa-check text-success"></span>';
								}
								else{
									$returnValue.= '<span class="fa fa-times text-warning"></span>';
								}
							}
							else{
							  if(Auth::user()->officeIDfk==1 && $appInfo["applicationStatusIdfk"]==4 && ($latestStagesTimeLineInfo["stageStatusIdfk"]==15)){
								if($resultDocReviewSub1["inspectionROStatus"]==1){
									$returnValue.= '<input type="checkbox" class="reqChkBox" oneorall="one" name="reqChkBox" reqLevel="sub1" statusType="inspectionROStatus" prDocReviewId="'. $resultDocReviewSub1["prDocReviewId"] .'" checked="checked">';
								}
								else{
									$returnValue.= '<input type="checkbox" class="reqChkBox" oneorall="one" name="reqChkBox" reqLevel="sub1" statusType="inspectionROStatus" prDocReviewId="'. $resultDocReviewSub1["prDocReviewId"] .'">';
								}
							  }
							}

					$returnValue.= '
							</td>
						</tr>';
					$startSub1++;

                    //-------------------------------------------------- sub requirements 2 -----------------------------------------------
			    	if($appInfo["levelIdfk"]==4 && $appInfo["applicationTypeIdfk"]!=2){
			    		$tableName = "subrequirements2_shs";	
			    	}
			    	else{
			    		$tableName = "subrequirements2";
			    	}
					$stmt2 = $conn1 -> prepare("SELECT * FROM ". $tableName ." WHERE subReq1Idfk=:subReq1Idfk AND isActive=1");
			        $stmt2->bindParam(":subReq1Idfk",$subReq1["subReq1Id"]);
			        $stmt2->execute();
			        $result2=$stmt2->fetchAll(\PDO::FETCH_ASSOC);
					if(count($result1)!=0){		       
				        $startSub2 = 1;
				        foreach($result2 as $subReq2){
				        	$resultSub2Req = self::getFileUploadStatus($prStatusId, $subReq2["subReq2Id"], 3);
				        	$resultDocReviewSub2 = self::getDocReviewStatus($prStatusId, $mainReq["mainReqId"], $subReq1["subReq1Id"], $subReq2["subReq2Id"]);
				        	$returnValue.= '
				        		<tr>';
				        		if(Auth::user()->officeIDfk==3 && ($appInfo["applicationStatusIdfk"]==1 || $appInfo["applicationStatusIdfk"]==8)){
				        			$returnValue.= '
				        			<td>
				        				&nbsp;
				                    </td>';
				                }
				                $returnValue.= '
				        			<td>
										<ul style="list-style-type: disc; margin-bottom:0px; margin-left: 20px;">
											<li class="subReq2" subReq2Id="'. $subReq2["subReq2Id"] .'">'. $subReq2["description"] .'</li>
										</ul>
									</td>
									<td class="text-center">';
									if($resultSub2Req["fileUploadStatus"]==1){
										$returnValue.= '<span class="fa fa-check text-success"></span>';
									}
									else{
										$returnValue.= '<span class="fa fa-times text-danger"></span>';
									}
							$returnValue.= '
									</td>
									<td class="text-center">';
									if(Auth::user()->officeIDfk==1 || Auth::user()->officeIDfk==3 || (Auth::user()->officeIDfk==2 && $latestStagesTimeLineInfo["stageStatusIdfk"]>=7)){
										if($resultDocReviewSub2["evaluationStatus"]==1){
											$returnValue.= '<span class="fa fa-check text-success"></span>';
										}
										else{
											$returnValue.= '<span class="fa fa-times text-warning"></span>';
										}
									}
									else{
									  if(Auth::user()->officeIDfk==2 && $appInfo["applicationStatusIdfk"]==4  && ($latestStagesTimeLineInfo["stageStatusIdfk"]==4 || $latestStagesTimeLineInfo["stageStatusIdfk"]==5 || $latestStagesTimeLineInfo["stageStatusIdfk"]==6)){
										if($resultDocReviewSub2["evaluationStatus"]==1){
											$returnValue.= '<input type="checkbox" class="reqChkBox" oneorall="one" reqLevel="sub2" name="reqChkBox" statusType="evaluationStatus" prDocReviewId="'. $resultDocReviewSub2["prDocReviewId"] .'" checked="checked">';
										}
										else{
											$returnValue.= '<input type="checkbox" class="reqChkBox" oneorall="one" name="reqChkBox" reqLevel="sub2" statusType="evaluationStatus" prDocReviewId="'. $resultDocReviewSub2["prDocReviewId"] .'">';
										}
									  }
									}

							$returnValue.= '</td>
									<td class="text-center">';
									if(Auth::user()->officeIDfk==1 || Auth::user()->officeIDfk==3 || (Auth::user()->officeIDfk==2 && $latestStagesTimeLineInfo["stageStatusIdfk"]>=12)){
										if($resultDocReviewSub2["inspectionStatus"]==1){
											$returnValue.= '<span class="fa fa-check text-success"></span>';
										}
										else{
											$returnValue.= '<span class="fa fa-times text-warning"></span>';
										}
									}
									else{
									  if(Auth::user()->officeIDfk==2 && $appInfo["applicationStatusIdfk"]==4 && ($latestStagesTimeLineInfo["stageStatusIdfk"]==7 || $latestStagesTimeLineInfo["stageStatusIdfk"]==10 || $latestStagesTimeLineInfo["stageStatusIdfk"]==11 || $latestStagesTimeLineInfo["stageStatusIdfk"]==12)){
										if($resultDocReviewSub2["inspectionStatus"]==1){
											$returnValue.= '<input type="checkbox" class="reqChkBox" oneorall="one" name="reqChkBox" oneorall="one" reqLevel="sub2" statusType="inspectionStatus" prDocReviewId="'. $resultDocReviewSub2["prDocReviewId"] .'" checked="checked">';
										}
										else{
											$returnValue.= '<input type="checkbox" class="reqChkBox" oneorall="one" name="reqChkBox" reqLevel="sub2" statusType="inspectionStatus" prDocReviewId="'. $resultDocReviewSub2["prDocReviewId"] .'">';
										}
									  }
									}

							$returnValue.= '</td>
									<td class="text-center">';
									if(Auth::user()->officeIDfk==2 || Auth::user()->officeIDfk==3 || (Auth::user()->officeIDfk==1 && $latestStagesTimeLineInfo["stageStatusIdfk"]>=18)){
										if($resultDocReviewSub2["roReviewStatus"]==1){
											$returnValue.= '<span class="fa fa-check text-success"></span>';
										}
										else{
											$returnValue.= '<span class="fa fa-times text-warning"></span>';
										}
									}
									else{
									  if(Auth::user()->officeIDfk==1 && $appInfo["applicationStatusIdfk"]==4 && ($latestStagesTimeLineInfo["stageStatusIdfk"]==13 || $latestStagesTimeLineInfo["stageStatusIdfk"]==14 || $latestStagesTimeLineInfo["stageStatusIdfk"]==16 || $latestStagesTimeLineInfo["stageStatusIdfk"]==17)){
										if($resultDocReviewSub2["roReviewStatus"]==1){
											$returnValue.= '<input type="checkbox" class="reqChkBox" oneorall="one" name="reqChkBox" reqLevel="sub2" statusType="roReviewStatus" prDocReviewId="'. $resultDocReviewSub2["prDocReviewId"] .'" checked="checked">';
										}
										else{
											$returnValue.= '<input type="checkbox" class="reqChkBox" oneorall="one" name="reqChkBox" reqLevel="sub2" statusType="roReviewStatus" prDocReviewId="'. $resultDocReviewSub2["prDocReviewId"] .'">';
										}
									  }
									}

							$returnValue.= '</td>
									<td class="text-center">';
									if(Auth::user()->officeIDfk==2 || Auth::user()->officeIDfk==3 || (Auth::user()->officeIDfk==1 && $latestStagesTimeLineInfo["stageStatusIdfk"]>=18)){
										if($resultDocReviewSub2["inspectionROStatus"]==1){
											$returnValue.= '<span class="fa fa-check text-success"></span>';
										}
										else{
											$returnValue.= '<span class="fa fa-times text-warning"></span>';
										}
									}
									else{
									  if(Auth::user()->officeIDfk==1 && $appInfo["applicationStatusIdfk"]==4 && ($latestStagesTimeLineInfo["stageStatusIdfk"]==15)){
										if($resultDocReviewSub2["inspectionROStatus"]==1){
											$returnValue.= '<input type="checkbox" class="reqChkBox" oneorall="one" name="reqChkBox" reqLevel="sub2" statusType="inspectionROStatus" prDocReviewId="'. $resultDocReviewSub2["prDocReviewId"] .'" checked="checked">';
										}
										else{
											$returnValue.= '<input type="checkbox" class="reqChkBox" oneorall="one" name="reqChkBox" reqLevel="sub2" statusType="inspectionROStatus" prDocReviewId="'. $resultDocReviewSub2["prDocReviewId"] .'">';
										}
									  }
									}

							$returnValue.= '</td>
								</tr>';
						}
					}
	        	}
	        }
        }
        $returnValue.= "</tbody>";

        return $returnValue;
    }

    public function stagesTimeLine($prStatusId){
    	$returnValue="";
        $conn1 = DB::connection('mysql')->getPdo();
        $stmt = $conn1 -> prepare("
				        	SELECT stageId,
							       sequence,
							       description,
							       schedule,
							       bgColor
							FROM stages
							ORDER BY sequence DESC");
        $stmt->execute();
        $result=$stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach($result as $stage){
        	$returnValue.='
                <li class="time-label">
                      <span class="'. $stage["bgColor"] .'">
                        '. $stage["description"] .'
                      </span>
                      <span class="time pull-right"><i class="fa fa-calendar"></i> <strong>Schedule: </strong>'. $stage["schedule"] .'</span>
                </li>
        	';
	    	//$conn1 = DB::connection('mysql')->getPDO();
	    	$stmt1 = $conn1 -> prepare("
									SELECT stageTimelineId,
									       prStatusIdfk,
									       stagestimeline.stageIdfk,
									       stages.description AS stage,
									       stages.schedule AS stageSchedule,
									       stageStatusIdfk,
									       stagesstatus.description AS stagesStatus,
									       updatedBy,
									       users.name,
									       lastDateUpdated,
									       remarks,
									       docReviewFileIdfk
									FROM stagestimeline
									LEFT JOIN stages
									ON stagestimeline.stageIdfk = stages.stageId
									LEFT JOIN stagesstatus
									ON stagestimeline.stageStatusIdfk = stagesstatus.stageStatusId
									LEFT JOIN users
									ON stagestimeline.updatedBy = users.id
									WHERE prStatusIdfk = :prStatusIdfk AND stagestimeline.stageIdfk = :stageIdfk ORDER BY lastDateUpdated DESC
	    						");
	    	$stmt1 -> bindParam('prStatusIdfk', $prStatusId);
	    	$stmt1 -> bindParam('stageIdfk', $stage["stageId"]);
	    	$stmt1 -> execute();
	    	$result1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);
	    	foreach($result1 as $stageTimeLine){
	    		$returnValue.='
                            <!-- timeline item -->
                            <li>
                              <i class="fa fa-tasks bg-red"></i>

                              <div class="timeline-item">
                                <span class="time"><i class="fa fa-clock-o"></i> '. $stageTimeLine["lastDateUpdated"] .'</span>

                                <h3 class="timeline-header"><a href="#">'. $stageTimeLine["stagesStatus"] .'</a> <span style="font-size: smaller;"> (updated by <i class="fa fa-user"></i>: '. $stageTimeLine["name"] .')</span></h3>

                                <div class="timeline-body">
                                  '. $stageTimeLine["remarks"] .'
                                </div>
                              </div>
                            </li>
	    		';
	    	}
    	}
    	return $returnValue;
    }

//info functions
    public function applicationInfo($prStatusId){
        $conn1 = DB::connection('mysql')->getPdo();
        $stmt = $conn1 -> prepare("
								SELECT prStatusId,
								       applicationTypeIdfk,
								       schoolUserIdfk,
								       gradeLevelIdfkFrom,
								       gradeLevelIdfkTo,
								       governmentPRNumber,
								       seriesYear,
								       schoolYear,
								       applicationStatusIdfk,
                                       controlNumber,
								       applicationstatus.description AS applicationStatus,
								       applicationstatus.valueNow,
								       applicationstatus.progressClass,
								       categoryIdfk,
								       categoriesshs.category,
                                       tblFrom.gradeLevelId as applicationIdFrom,
								       tblFrom.description as applicationFrom,
                                       tblTo.gradeLevelId as applicationIdTo,
								       tblTo.description as applicationTo,
								       tblFrom.levelIdfk,
                                       users.schoolId as schoolId,
								       users.name as schoolName,
                                       users.email as schoolEmailAddress,
                                       users.schoolAddress,
								       users.municipality,
								       users.telNumber,
								       users.mobileNumber,
                                       users.schoolsDivisionIdfk,
								       users.schoolHead
								FROM permitrecognitionstatus
									LEFT JOIN categoriesshs
										ON permitrecognitionstatus.categoryIdfk = categoriesshs.categoryId
									LEFT JOIN gradelevels as tblFrom
										ON permitrecognitionstatus.gradeLevelIdfkFrom = tblFrom.gradeLevelId
									LEFT JOIN gradelevels as tblTo
										ON permitrecognitionstatus.gradeLevelIdfkTo = tblTo.gradeLevelId
                                    LEFT JOIN applicationstatus
                                        ON permitrecognitionstatus.applicationStatusIdfk = applicationstatus.applicationStatusId
                                    LEFT JOIN users
                                    	ON permitrecognitionstatus.schoolUserIdfk = users.id
								WHERE permitrecognitionstatus.prStatusId = :prStatusId
        					");
       	$stmt -> bindParam(':prStatusId', $prStatusId);
        $stmt->execute();
        $result=$stmt->fetch(\PDO::FETCH_ASSOC);
        return $result;
    }

    public function latestStagesTimeLineInfo($prStatusId){
    	$conn1 = DB::Connection('mysql')->getPdo();
    	$stmt = $conn1->prepare("
							SELECT
							    `stageTimelineId`,
							    `prStatusIdfk`,
							    `stageIdfk`,
							    `stageStatusIdfk`,
							    `updatedBy`,
							    `lastDateUpdated`,
							    `remarks`,
							    `docReviewFileIdfk`
							FROM
							    `stagestimeline`
							WHERE
							    prStatusIdfk = :prStatusIdfk
							ORDER BY
							    `lastDateUpdated` DESC, `stageTimelineId` DESC
							LIMIT 1
    		");
       	$stmt -> bindParam(':prStatusIdfk', $prStatusId);
        $stmt->execute();
        $result=$stmt->fetch(\PDO::FETCH_ASSOC);
        return $result;
    }

//updates functions
    public function updateApplicationStatus($prStatusId, $applicationStatusId, $lastDateUpdated){
        $conn1 = DB::connection('mysql')->getPdo();
        $sql = $conn1->prepare("
                        UPDATE
                            `permitrecognitionstatus`
                        SET
                            `applicationStatusIdfk` = :applicationStatusIdfk
                        WHERE
                            `prStatusId` = :prStatusId
                    ");
        $sql->bindParam(':prStatusId', $prStatusId);
        $sql->bindParam(':applicationStatusIdfk', $applicationStatusId);
        $sql->execute();
    }

    public function updateStagesTimeline($prStatusId, $stageIdfk, $stageStatusIdfk, $updatedBy, $lastDateUpdated, $remarks, $docReviewFileIdfk){
            $conn1 = DB::connection('mysql')->getPdo();
            $stmt = $conn1 -> prepare("
                                INSERT INTO `stagestimeline`(
                                    `stageTimelineId`,
                                    `prStatusIdfk`,
                                    `stageIdfk`,
                                    `stageStatusIdfk`,
                                    `updatedBy`,
                                    `lastDateUpdated`,
                                    `remarks`,
                                    `docReviewFileIdfk`
                                )
                                VALUES(
                                    null,
                                    :prStatusId,
                                    :stageIdfk,
                                    :stageStatusIdfk,
                                    :updatedBy,
                                    :lastDateUpdated,
                                    :remarks,
                                    :docReviewFileIdfk
                                )
                                ");
            $stmt->bindParam(':prStatusId', $prStatusId);
            $stmt->bindParam('stageIdfk', $stageIdfk);
            $stmt->bindParam('stageStatusIdfk', $stageStatusIdfk);
            $stmt->bindParam('updatedBy', $updatedBy);
            $stmt->bindParam('lastDateUpdated', $lastDateUpdated);
            $stmt->bindParam('remarks', $remarks);
            $stmt->bindParam('docReviewFileIdfk', $docReviewFileIdfk);
            $stmt->execute();
    }

// function requirementsLists
    public function requirementsLists($applicationId, $levelId, $schoolYear){
        $returnValue = "";
        $returnValue .='
                        <thead>
                            <tr>
                                <th> 

                                </th>
                            </tr>
                        </thead>
                        <tbody>';
        if($levelId==4){
            $tableName = "mainrequirements_shs";    
        }
        else{
            $tableName = "mainrequirements";
        }
        //-------------------------------------------------- main requirements -----------------------------------------------
        $conn1 = DB::connection('mysql')->getPdo();

        $stmt = $conn1 -> prepare("SELECT * FROM ". $tableName ." WHERE applicationTypeIdfk=:applicationTypeIdfk AND schoolYear=:schoolYear");
        $stmt->bindParam(":applicationTypeIdfk",$applicationId);
        $stmt->bindParam(":schoolYear",$schoolYear);
        $stmt->execute();
        $result=$stmt->fetchAll(\PDO::FETCH_ASSOC);
        $startMain = 1;
        foreach($result as $mainReq){
            if($mainReq["isActive"]==0){
                $style = "text-decoration: line-through;";
            }
            else{
                $style = "text-decoration: none;";   
            }
            $returnValue.= '
                <tr>
                    <td>
                        <ol style="list-style-type: decimal; margin-bottom:0px; margin-left:-20px;" start="'. $startMain .'">
                            <strong>
                                <li class="mainReq">
                                    <span data-toggle="modal" data-target="#modal-edit-main-requirement">
                                        <a href="#" isActive="'. $mainReq["isActive"] .'" mainReqId="'. $mainReq["mainReqId"] .'" class="main-requirement TT" data-placement="left" title="Edit Requirement" data-toggle="tooltip" style="'. $style .'">'. $mainReq["description"] .'</a>
                                    </span>
                                </li>
                            </strong>
                        </ol>
                    </td>
                </tr>';
            $startMain++;

            //-------------------------------------------------- sub requirements 1 -----------------------------------------------
            if($levelId==4){
                $tableName = "subrequirements1_shs";    
            }
            else{
                $tableName = "subrequirements1";
            }

            $stmt1 = $conn1 -> prepare("SELECT * FROM ". $tableName ." WHERE mainReqIdfk=:mainReqIdfk");
            $stmt1->bindParam(":mainReqIdfk",$mainReq["mainReqId"]);
            $stmt1->execute();
            $result1=$stmt1->fetchAll(\PDO::FETCH_ASSOC);
            if(count($result1)!=0){
                $startSub1 = 1;
                foreach($result1 as $subReq1){
                    if($subReq1["isActive"]==0){
                        $style = "text-decoration: line-through;";
                    }
                    else{
                        $style = "text-decoration: none;";   
                    }
                    $returnValue.= '
                        <tr>
                            <td>
                                <ol style="list-style-type: lower-alpha; margin-bottom:0px; margin-left: 0px;" start="'. $startSub1 .'">
                                    <li class="subReq1">
                                        <span data-toggle="modal" data-target="#modal-edit-sub1-requirement">
                                            <a href="#" isActive="'. $subReq1["isActive"] .'" mainReqId="'. $mainReq["mainReqId"] .'" subReq1Id="'. $subReq1["subReq1Id"] .'" class="sub1-requirement TT" data-placement="left" title="Edit Sub Requirement" data-toggle="tooltip" style="'. $style .'">' . $subReq1["description"] .'
                                            </a>
                                        </span>
                                    </button>
                                    </li>
                                </ol>
                            </td>

                        </tr>';
                    $startSub1++;

                    //-------------------------------------------------- sub requirements 2 -----------------------------------------------
                    if($levelId==4){
                        $tableName = "subrequirements2_shs";    
                    }
                    else{
                        $tableName = "subrequirements2";
                    }

                    $stmt2 = $conn1 -> prepare("SELECT * FROM ". $tableName ." WHERE subReq1Idfk=:subReq1Idfk");
                    $stmt2->bindParam(":subReq1Idfk",$subReq1["subReq1Id"]);
                    $stmt2->execute();
                    $result2=$stmt2->fetchAll(\PDO::FETCH_ASSOC);
                    if(count($result1)!=0){            
                        $startSub2 = 1;
                        foreach($result2 as $subReq2){
                            if($subReq2["isActive"]==0){
                                $style = "text-decoration: line-through;";
                            }
                            else{
                                $style = "text-decoration: none;";   
                            }
                            $returnValue.= '
                                <tr>
                                    <td>
                                        <ul style="list-style-type: disc; margin-bottom:0px; margin-left: 20px;">
                                            <li class="subReq2">
                                                <span data-toggle="modal" data-target="#modal-edit-sub2-requirement">
                                                    <a href="#" isActive="'. $subReq2["isActive"] .'" mainReqId="'. $mainReq["mainReqId"] .'" subReq1Id="'. $subReq1["subReq1Id"] .'" subReq2Id="'. $subReq2["subReq2Id"] .'" class="sub2-requirement TT" data-placement="left" title="Edit Sub Requirement" data-toggle="tooltip" style="'. $style .'">'. $subReq2["description"] .' 
                                                    </a>
                                                </span>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>';
                        }
                    }
                }
            }
        }
        $returnValue.= "</tbody>";
        return $returnValue;
    }

    public function hasSubReq($mainReqId, $schoolYear){
        $conn1 = DB::connection('mysql')->getPdo();
        $stmt = $conn1 -> prepare("SELECT COUNT(*) AS numberReq FROM `subrequirements1` WHERE `mainReqIdfk` = :mainReqIdfk AND `schoolYear`=:schoolYear");
        $stmt->bindParam(":mainReqIdfk", $mainReqId);
        $stmt->bindParam(":schoolYear", $schoolYear);
        $stmt->execute();
        $result=$stmt->fetch(\PDO::FETCH_ASSOC);
        if($result["numberReq"]>0){
            return true;
        }
        else{
            return false;
        }
    }
}
