<?php

namespace App\Http\Controllers\ManageRequirements;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use App\Services\PayUService\Exception;
use App\Http\Controllers\PDO;

class ManageRequirements extends Controller
{
    //prevents accessing the page without logging in
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function requirements($applicationTypeId, $levelId, $status){
        //latest SY
    	$schoolYear = Config::get('constants.gprpsconst.application_school_year');
    	if($applicationTypeId==1 && $levelId<=3){
    		$applicationType = "Government Permit / Recognition - Requirements (K-10)";
    	}
    	else if($applicationTypeId==1 && $levelId==4){
    		$applicationType = "Government Permit / Recognition - Requirements (SHS)";
    	}
    	if($applicationTypeId==2 && $levelId<=3){
    		$applicationType = "Renewal of Government Permit / Recognition - Requirements (K-10)";
    	}
    	else if($applicationTypeId==2 && $levelId==4){
    		$applicationType = "Renewal of Government Permit / Recognition - Requirements (SHS)";
    	}

        return view('managerequirements/requirements')
            ->with('applicationTypeId', $applicationTypeId)
            ->with('applicationType', $applicationType)
            ->with('levelId', $levelId)
            ->with('schoolYear', $schoolYear)
            ->with('requirementslists', Controller::requirementsLists($applicationTypeId, $levelId, $schoolYear))
            ->with('status', $status)
            ->render();
    }

    public function requirementsSY($applicationTypeId, $levelId, $schoolYear){
            return redirect()->route('managerequirements/requirements', [$applicationTypeId, $levelId]);
    }

    public function addmainrequirement(Request $request){
        $applicationTypeId = $request->input("applicationTypeId");
        $levelId = $request->input("levelId");
        $schoolYear = $request->input("schoolYear");
        $description = trim($request->input("description"));
        $status = "Successfully Added New Requirement";

        $idValTemp = 0;
        if($levelId==3){
            $tableName = "mainrequirements";
        }
        else if($levelId==4){
            $tableName = "mainrequirements_shs";
        }

        $conn1 = DB::connection('mysql')->getPdo();
        $sql = "INSERT INTO " . $tableName . " (
                        `mainReqId`,
                        `description`,
                        `applicationTypeIdfk`,
                        `schoolYear`,
                        `isActive`
                    )
                    VALUES(
                        null,
                        :description,
                        :applicationTypeIdfk,
                        :schoolYear,
                        1
                    )";
        $stmt = $conn1 -> prepare($sql);
        $stmt->bindParam('description', $description);
        $stmt->bindParam('applicationTypeIdfk', $applicationTypeId);
        $stmt->bindParam('schoolYear', $schoolYear);
        $stmt->execute();
        return redirect()->route('requirements', [$applicationTypeId, $levelId, $status]);
    }

    public function updaterequirement(Request $request){

        $applicationTypeId = $request->input("applicationTypeId");
        $levelId = $request->input("levelId");
        $schoolYear = $request->input("schoolYear");
        $mainReqId = $request->input("mainReqId");
        $subReq1Id = $request->input("subReq1Id");
        $subReq2Id = $request->input("subReq2Id");
        $type = $request->input("type");
        $isActive = $request->input("isActive");
        $description = trim($request->input("description"));
        $status = "Successfully Updated a Requirement";
        
        $idValTemp = 0;


        if($isActive=="on"){
            $isActive = 1;
        }
        else{
            $isActive = 0;
        }

        if($levelId==3 && $type=="main"){
            $tableName = "mainrequirements";
            $subTableName = "subrequirements1";
            $subMainId = "subReq1Id";
            $idName = "mainReqId";
            $idValTemp = $mainReqId;
        }
        else if($levelId==3 && $type=="sub1"){
            $tableName = "subrequirements1";
            $subTableName = "subrequirements2";
            $subMainId = "subReq2Id";
            $idName = "subReq1Id";
            $idValTemp = $subReq1Id;
        }
        else if($levelId==3 && $type=="sub2"){
            $tableName = "subrequirements2";
            $idName = "subReq2Id";
            $idValTemp = $subReq2Id;
        }
        else if($levelId==4 && $type=="main"){
            $tableName = "mainrequirements_shs";
            $subTableName = "subrequirements1_shs";
            $subMainId = "subReq1Id";
            $idName = "mainReqId";
            $idValTemp = $mainReqId;
        }
        else if($levelId==4 && $type=="sub1"){
            $tableName = "subrequirements1_shs";
            $subTableName = "subrequirements2_shs";
            $subMainId = "subReq2Id";
            $idName = "subReq1Id";
            $idValTemp = $subReq1Id;
        }
        else if($levelId==4 && $type=="sub2"){
            $tableName = "subrequirements2_shs";
            $idName = "subReq2Id";
            $idValTemp = $subReq2Id;
        }


        $conn1 = DB::connection('mysql')->getPdo();
        $sql = "UPDATE " . $tableName . " SET `description`=:description, isActive =:isActive WHERE ". $idName ."=:idValTemp";
        $stmt = $conn1 -> prepare($sql);
        $stmt->bindParam('description', $description);
        $stmt->bindParam('isActive', $isActive);
        $stmt->bindParam('idValTemp', $idValTemp);
        $stmt->execute();
        //

        if(trim($request->input("subdescription"))!=null) {
            $subdescription = $request->input("subdescription");
            $schoolYear = $request->input("schoolYear");

            $sql = "INSERT INTO ". $subTableName ."(
                        ". $subMainId .",
                        description,
                        ". $idName ."fk,
                        `schoolYear`,
                        `isActive`
                    )
                    VALUES(
                        null,
                        :subdescription,
                        :idFk,
                        :schoolYear,
                        1
                    )";
            $stmt = $conn1 -> prepare($sql);
            $stmt->bindParam('subdescription', $subdescription);
            $stmt->bindParam('idFk', $idValTemp);
            $stmt->bindParam('schoolYear', $schoolYear);
            $stmt->execute();
            
        }
        return redirect()->route('requirements', [$applicationTypeId, $levelId, $status]);
    }

    public function deactivaterequirement(Request $request){
    	
    }

}
