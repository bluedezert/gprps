<?php

namespace App\Http\Controllers\NewApplication;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;
use App\Services\PayUService\Exception;
use App\Http\Controllers\PDO;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use Illuminate\Support\Facades\Config;

class SdoApplicationMonitoring extends Controller
{
	//prevents accessing the page without logging in
    public function __construct()
    {
        $this->middleware('auth');
    }

    //url to be called
    public function index($applicationTypeId)
    {
        if($applicationTypeId==1){
            $applicationType = "New Application <small>Government Permit</small>";
            $snackbar = "Displaying List of New Application for Government Permit";
            //$newBtnText = "New Application";
        }
        elseif($applicationTypeId==2){
            $applicationType = "Renewal <small>Government Permit</small>";
            $snackbar = "Displaying List of Renewal Application for Government Permit";
            //$newBtnText = "Renew Permit";   
        }
        elseif($applicationTypeId==9){
            $applicationType = "Government Recognition - Recognized Schools";
            $snackbar = "Displaying List of Recognized Schools";
            //$newBtnText = "";
        }
        elseif($applicationTypeId==3){
            $applicationType = "Applicants for Recognition <small>Government Recognition</small>";
            $snackbar = "Displaying List of Applicants for Government Recognition";
            //$newBtnText = "Applying for Recognition";
        }
        elseif($applicationTypeId==4){
            $applicationType = "Renewal of Recognition <small>Government Recognition</small>";
            $snackbar = "Displaying List of Applicants for Renewal of Government Recognition";
            //$newBtnText = "Renewal of Recognition";
        }

        return view('newapplication/sdoapplicationmonitoring')
            ->with('userid', Auth::id())
            ->with('applicationTypeId', $applicationTypeId)
            ->with('applicationType', $applicationType)
            ->with('status', $snackbar)
            ->with('gradeLevels', Controller::gradeLevels())
            ->with('categoriesshs', Controller::categoriesshs())
            ->with('applicationlists', self::sdoNewApplicationLists($applicationTypeId));
    }

    public function sdoNewApplicationLists($applicationTypeId){
        $conn1 = DB::connection('mysql')->getPdo();
        $schoolUserIdfk = Auth::id();
        $stmt = $conn1 -> prepare("
                                SELECT prStatusId,
                                       applicationTypeIdfk,
                                       schoolUserIdfk,
                                       gradeLevelIdfkFrom,
                                       gradeLevelIdfkTo,
                                       governmentPRNumber,
                                       seriesYear,
                                       schoolYear,
                                       applicationStatusIdfk,
                                       applicationstatus.description AS applicationStatus,
                                       applicationstatus.valueNow,
                                       applicationstatus.progressClass,
                                       categoryIdfk,
                                       permitrecognitionstatus.lastDateUpdated,
                                       categoriesshs.category,
                                       tblFrom.description AS applicationFrom,
                                       tblTo.description AS applicationTo,
                                       users.officeIDfk,
                                       users.name AS schoolName,
                                       users.schoolsDivisionIdfk,
                                       SDO.schoolsDivision
                                FROM permitrecognitionstatus
                                     LEFT JOIN categoriesshs
                                        ON permitrecognitionstatus.categoryIdfk = categoriesshs.categoryId
                                     LEFT JOIN gradelevels AS tblFrom
                                        ON permitrecognitionstatus.gradeLevelIdfkFrom = tblFrom.gradeLevelId
                                     LEFT JOIN gradelevels AS tblTo
                                        ON permitrecognitionstatus.gradeLevelIdfkTo = tblTo.gradeLevelId
                                     LEFT JOIN applicationstatus
                                        ON permitrecognitionstatus.applicationStatusIdfk =
                                           applicationstatus.applicationStatusId
                                     LEFT JOIN users
                                        ON permitrecognitionstatus.schoolUserIdfk = users.id
                                     LEFT JOIN depedcarprofilesdb.tbl_schoolsdivisionoffices AS SDO
                                        ON users.schoolsDivisionIdfk = SDO.schoolsDivisionID
                                WHERE users.schoolsDivisionIdfk = :schoolsDivisionIdfk
                                AND applicationTypeIdfk = :applicationTypeIdfk
                                AND permitrecognitionstatus.applicationStatusIdfk <> 1
                                AND permitrecognitionstatus.applicationStatusIdfk <> 9
                            ");
        $schoolsDivisionIdfk = Controller::getDivisionId();
        $stmt->bindParam(":schoolsDivisionIdfk",$schoolsDivisionIdfk);
        $stmt->bindParam(":applicationTypeIdfk",$applicationTypeId);
        $stmt->execute();
        $result=$stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    public function assessapplication(Request $request){
      //update Application Status
      $applicationStatusId = 4;
      $prStatusId = $request->input('prStatusId');
      $lastDateUpdated = date("Y-m-d H:i:s");

      Controller::updateApplicationStatus($prStatusId, $applicationStatusId, $lastDateUpdated);
      //update Stages Timeline
          $stageIdfk = 2;
          $stageStatusIdfk = 4;
          $updatedBy = Auth::id();
          $remarks = "Assess Application";
          $docReviewFileIdfk = 0;
          Controller::updateStagesTimeline($prStatusId, $stageIdfk, $stageStatusIdfk, $updatedBy, $lastDateUpdated, $remarks, $docReviewFileIdfk);

      //email generation
          $subject = "GPRPS - Application Monitoring and Management (Application Assessment - SDO)";
          $greeting = "Hello!";
          $firstParagraph = "Schools Division Office started assessing the application of the following private school.<br>";
          $fixedInfo = self::getFixedInfo($prStatusId);
          $generatedEmail = self::generateEmail($subject, $greeting, $fixedInfo, $firstParagraph, $lastDateUpdated);

        //recipients
          $email = Auth::user()->email;//email of SDO DAIT
          $schoolEmailAddress = $fixedInfo["schoolEmailAddress"];
          $getSdoPrivateSchoolFocal = Controller::getSdoPrivateSchoolFocal(Auth::user()->schoolsDivisionIdfk);
          self::emailRecipients($email, $schoolEmailAddress, $getSdoPrivateSchoolFocal, $generatedEmail); 

      //return to view application page
      $applicationTypeId = $request->input('applicationType');
      $status = "Application assessment started";
      return redirect()->route('viewapplication', [$prStatusId, $applicationTypeId, $status]);
    }

    public function notifyforcpmpliancesdo(Request $request){
      //update Application Status
      $applicationStatusId = 8;
      $prStatusId = $request->input('prStatusId');
      $lastDateUpdated = date("Y-m-d H:i:s");

      Controller::updateApplicationStatus($prStatusId, $applicationStatusId, $lastDateUpdated);
      //update Stages Timeline
          $stageIdfk = 2;
          $stageStatusIdfk = 5;
          $updatedBy = Auth::id();
          $remarks = "Notify for Compliance";
          $docReviewFileIdfk = 0;
          Controller::updateStagesTimeline($prStatusId, $stageIdfk, $stageStatusIdfk, $updatedBy, $lastDateUpdated, $remarks, $docReviewFileIdfk);

//-------------- update database for compliance notif ------------------------

      //email generation
          $subject = "GPRPS - Application Monitoring and Management (Application Assessment - SDO)";
          $greeting = "Hello!";
          $firstParagraph = "Schools Division Office notified the following private school for compliance of incomplete documents.<br>";
          $fixedInfo = self::getFixedInfo($prStatusId);
          $generatedEmail = self::generateEmail($subject, $greeting, $fixedInfo, $firstParagraph, $lastDateUpdated);

        //recipients
          $email = Auth::user()->email;//email of SDO DAIT
          $schoolEmailAddress = $fixedInfo["schoolEmailAddress"];
          $getSdoPrivateSchoolFocal = Controller::getSdoPrivateSchoolFocal(Auth::user()->schoolsDivisionIdfk);
          self::emailRecipients($email, $schoolEmailAddress, $getSdoPrivateSchoolFocal, $generatedEmail); 

      //return to view application page
      $applicationTypeId = $request->input('applicationType');
      $status = "Private school has been notified for compliance";

      return redirect()->route('viewapplication', [$prStatusId, $applicationTypeId, $status]);
    }

    public function completedocuments(Request $request){
      //update Application Status
      $applicationStatusId = 4;
      $prStatusId = $request->input('prStatusId');
      $lastDateUpdated = date("Y-m-d H:i:s");
      
      Controller::updateApplicationStatus($prStatusId, $applicationStatusId, $lastDateUpdated);
      //update Stages Timeline
        $stageIdfk = 2;
        $stageStatusIdfk = 6;
        $updatedBy = Auth::id();
        $remarks = "With Complete Documents";
        $docReviewFileIdfk = 0;
        Controller::updateStagesTimeline($prStatusId, $stageIdfk, $stageStatusIdfk, $updatedBy, $lastDateUpdated, $remarks, $docReviewFileIdfk);
        
      //return to view application page
            $applicationTypeId = $request->input('applicationType');
            $status = "Private school has been tagged with complete application documents";

            return redirect()->route('viewapplication', [$prStatusId, $applicationTypeId, $status]);
    }


    public function getFixedInfo($prStatusId){
      $applicationInfo = Controller::applicationInfo($prStatusId);
      $fixedInfo = array(
        'schoolYear' => $applicationInfo["schoolYear"],
        'schoolId' => $applicationInfo["schoolId"],
        'schoolName' => $applicationInfo["schoolName"],
        'schoolEmailAddress' => $applicationInfo["schoolEmailAddress"],
        'gradeFrom' => $applicationInfo["gradeLevelIdfkFrom"],
        'gradeTo' => $applicationInfo["gradeLevelIdfkTo"]
      );

      return $fixedInfo;
    }

    public function generateEmail($subject, $greeting, $emailFixedData, $firstParagraph, $lastDateUpdated)
    {
        $message = "";
        $message .= $firstParagraph;
        $message .= "<br><strong>School ID:</strong> " . $emailFixedData["schoolId"];
        $message .= "<br><strong>School Name:</strong> " . $emailFixedData["schoolName"];
        $message .= "<br><strong>School Year:</strong> " . $emailFixedData["schoolYear"];
        $message .= "<br><strong>Grade Levels Applying for:</strong> From " .  Controller::gradeLevelsDescription($emailFixedData["gradeFrom"]) . " to " .  Controller::gradeLevelsDescription($emailFixedData["gradeTo"]);
        $message .= "<br><strong>Date:</strong> " . $lastDateUpdated;

      $generatedEmail = array(
          'subject'   =>  $subject,
          'greeting'  =>  $greeting,
          'message'   =>  $message
      );
      return $generatedEmail;
    }

    public function emailRecipients($email, $schoolEmailAddress, $getSdoPrivateSchoolFocal, $generatedEmail){
      $status = "";
      Mail::to($email)->send(new SendMail($generatedEmail)); // send to SDO
      Mail::to($schoolEmailAddress)->send(new SendMail($generatedEmail)); // send to school
      $roEmail = "ictu.depedcar@gmail.com";
      Mail::to($roEmail)->send(new SendMail($generatedEmail)); // send to RO (qad.depedcar@gmail.com)
      if(isset($getSdoPrivateSchoolFocal)){
          Mail::to($getSdoPrivateSchoolFocal)->send(new SendMail($generatedEmail)); // send to SDO Focal Person
      }
      else{
          $status .= ". No email sent to SDO";
          //return $status;
      }
    }

    public function forinspectionsdo(Request $request){
      //update Application Status
      $applicationStatusId = 4;
      $prStatusId = $request->input('prStatusId');
      $lastDateUpdated = date("Y-m-d H:i:s");
      $fixedInfo = self::getFixedInfo($prStatusId);

      Controller::updateApplicationStatus($prStatusId, $applicationStatusId, $lastDateUpdated);
      //update Stages Timeline
          $stageIdfk = 2;
          $stageStatusIdfk = 7;
          $updatedBy = Auth::id();
          $remarks = "For Inspection";
          $docReviewFileIdfk = 0;
          Controller::updateStagesTimeline($prStatusId, $stageIdfk, $stageStatusIdfk, $updatedBy, $lastDateUpdated, $remarks, $docReviewFileIdfk);

//------------- DAIT to set schedule of visit ------------------- notify RO only, not to school
      //email generation
          $subject = "GPRPS - Application Monitoring and Management (Ocular Visit - SDO)";
          $greeting = "Hello!";
          $firstParagraph = "Schools Division Office tagged the following private school for ocular visit.<br>";
          $fixedInfo = self::getFixedInfo($prStatusId);
          $generatedEmail = self::generateEmail($subject, $greeting, $fixedInfo, $firstParagraph, $lastDateUpdated);

        //recipients
          $email = Auth::user()->email;//email of SDO DAIT
          $schoolEmailAddress = $fixedInfo["schoolEmailAddress"];
          $getSdoPrivateSchoolFocal = Controller::getSdoPrivateSchoolFocal(Auth::user()->schoolsDivisionIdfk);
          self::emailRecipients($email, $schoolEmailAddress, $getSdoPrivateSchoolFocal, $generatedEmail);

      //return to view application page
      $applicationTypeId = $request->input('applicationType');
      $status = "Application for SDO Ocular Visit";
      return redirect()->route('viewapplication', [$prStatusId, $applicationTypeId, $status]);
    }

    public function forreinspectionsdo(Request $request){
      //update Application Status
      $applicationStatusId = 4;
      $prStatusId = $request->input('prStatusId');
      $lastDateUpdated = date("Y-m-d H:i:s");

      Controller::updateApplicationStatus($prStatusId, $applicationStatusId, $lastDateUpdated);
      //update Stages Timeline
          $stageIdfk = 3;
          $stageStatusIdfk = 11;
          $updatedBy = Auth::id();
          $remarks = "For Re-Visit";
          $docReviewFileIdfk = 0;
          Controller::updateStagesTimeline($prStatusId, $stageIdfk, $stageStatusIdfk, $updatedBy, $lastDateUpdated, $remarks, $docReviewFileIdfk);

//------------- DAIT to set schedule of revisit ------------------- notify RO only, not to school

      //email generation
          $subject = "GPRPS - Application Monitoring and Management (Ocular Visit - SDO)";
          $greeting = "Hello!";
          $firstParagraph = "Schools Division Office tagged the following private school for ocular revisit.<br>";
          $fixedInfo = self::getFixedInfo($prStatusId);
          $generatedEmail = self::generateEmail($subject, $greeting, $fixedInfo, $firstParagraph, $lastDateUpdated);

        //recipients
          $email = Auth::user()->email;//email of SDO DAIT
          $schoolEmailAddress = $fixedInfo["schoolEmailAddress"];
          $getSdoPrivateSchoolFocal = Controller::getSdoPrivateSchoolFocal(Auth::user()->schoolsDivisionIdfk);
          self::emailRecipients($email, $schoolEmailAddress, $getSdoPrivateSchoolFocal, $generatedEmail);

      //return to view application page
      $applicationTypeId = $request->input('applicationType');
      $status = "Application for SDO Ocular Visit (Re)";
      return redirect()->route('viewapplication', [$prStatusId, $applicationTypeId, $status]);
    }

    public function compliantforendorsement(Request $request){
      //update Application Status
      $applicationStatusId = 4;
      $prStatusId = $request->input('prStatusId');
      $lastDateUpdated = date("Y-m-d H:i:s");

      Controller::updateApplicationStatus($prStatusId, $applicationStatusId, $lastDateUpdated);
      //update Stages Timeline
          $stageIdfk = 3;
          $stageStatusIdfk = 10;
          $updatedBy = Auth::id();
          $remarks = "Compliant for Endorsement to the Regional Office";
          $docReviewFileIdfk = 0;
          Controller::updateStagesTimeline($prStatusId, $stageIdfk, $stageStatusIdfk, $updatedBy, $lastDateUpdated, $remarks, $docReviewFileIdfk);

          $stageIdfk = 3;
          $stageStatusIdfk = 12;
          $updatedBy = Auth::id();
          $remarks = "Endorsed to the Regional Office for Review";
          $docReviewFileIdfk = 0;
          Controller::updateStagesTimeline($prStatusId, $stageIdfk, $stageStatusIdfk, $updatedBy, $lastDateUpdated, $remarks, $docReviewFileIdfk);

      //email generation
          $subject = "GPRPS - Application Monitoring and Management (Endorsement to Regional Office for Review)";
          $greeting = "Hello!";
          $firstParagraph = "Schools Division Office tagged the following private school to be compliant for endorsement to the Regional Office for review.<br>";
          $fixedInfo = self::getFixedInfo($prStatusId);
          $generatedEmail = self::generateEmail($subject, $greeting, $fixedInfo, $firstParagraph, $lastDateUpdated);

        //recipients
          $email = Auth::user()->email;//email of SDO DAIT
          $schoolEmailAddress = $fixedInfo["schoolEmailAddress"];
          $getSdoPrivateSchoolFocal = Controller::getSdoPrivateSchoolFocal(Auth::user()->schoolsDivisionIdfk);
          self::emailRecipients($email, $schoolEmailAddress, $getSdoPrivateSchoolFocal, $generatedEmail);

      //return to view application page
      $applicationTypeId = $request->input('applicationType');
      $status = "Application is compliant for endorsement to the Regional Office for Review.";
      return redirect()->route('viewapplication', [$prStatusId, $applicationTypeId, $status]);
    }

    public function applicationforreview(Request $request){
      //update Application Status
      $applicationStatusId = 4;
      $prStatusId = $request->input('prStatusId');
      $lastDateUpdated = date("Y-m-d H:i:s");

      Controller::updateApplicationStatus($prStatusId, $applicationStatusId, $lastDateUpdated);
      //update Stages Timeline
          $stageIdfk = 3;
          $stageStatusIdfk = 12;
          $updatedBy = Auth::id();
          $remarks = "Endorsed to Regional Office for Review";
          $docReviewFileIdfk = 0;
          Controller::updateStagesTimeline($prStatusId, $stageIdfk, $stageStatusIdfk, $updatedBy, $lastDateUpdated, $remarks, $docReviewFileIdfk);

      //return to view application page
      $applicationTypeId = $request->input('applicationType');
      $status = "Application has been endorsed ro the regional office for review";
      return redirect()->route('viewapplication', [$prStatusId, $applicationTypeId, $status]);
    }

    public function evaluationChkUpdate(Request $request){
      $userIdfk = Auth::id();
      $docReviewId = $request->input('docReviewId');
      $oneorall = $request->input('oneorall');
      $reqLevel = $request->input('reqLevel');
      $statusType = $request->input('statusType');
      $value = $request->input('value');
      $conn1 = DB::connection('mysql')->getPdo();
      
      $returnMsg = "";

      $stmt = $conn1 -> prepare("UPDATE `permitrecognitiondocreview` SET " . $statusType . " = :value WHERE prDocReviewId = :docReviewId");
      $stmt->bindParam(':value', $value);
      $stmt->bindParam(':docReviewId', $docReviewId);
      $stmt->execute();

      $sql="SELECT prStatusIdfk, mainReqidfk FROM permitrecognitiondocreview WHERE prDocReviewId=:docReviewId";
      $stmt=$conn1->prepare($sql);
      $stmt->bindParam(':docReviewId', $docReviewId);
      $stmt->execute();
      $result=$stmt->fetch(\PDO::FETCH_ASSOC);      
      $prStatusIdfk = $result["prStatusIdfk"];
      $mainReqidfk = $result["mainReqidfk"];
      //update all with mainReqidfk
      if($oneorall=="all"){
        $stmt = $conn1 -> prepare("UPDATE `permitrecognitiondocreview` SET " . $statusType . " = :value WHERE mainReqidfk = :mainReqidfk");
        $stmt->bindParam(':value', $value);
        $stmt->bindParam(':mainReqidfk', $mainReqidfk);
        $stmt->execute();
      }
      else{
        $sql1="SELECT COUNT(*) as totAll
                FROM permitrecognitiondocreview
                WHERE mainReqidfk =:mainReqidfk AND prStatusIdfk =:prStatusIdfk";
        $stmt=$conn1->prepare($sql1);
        $stmt->bindParam(':mainReqidfk', $mainReqidfk);
        $stmt->bindParam(':prStatusIdfk', $prStatusIdfk);
        $stmt->execute();
        $result=$stmt->fetch(\PDO::FETCH_ASSOC);      
        $totAll = $result["totAll"];

        $sql2="SELECT COUNT(*) as totYes
                FROM permitrecognitiondocreview
                WHERE " . $statusType . " = 1 AND mainReqidfk =:mainReqidfk AND prStatusIdfk =:prStatusIdfk";
        $stmt=$conn1->prepare($sql2);
        $stmt->bindParam(':mainReqidfk', $mainReqidfk);
        $stmt->bindParam(':prStatusIdfk', $prStatusIdfk);
        $stmt->execute();
        $result=$stmt->fetch(\PDO::FETCH_ASSOC);      
        $totYes = $result["totYes"];

        $sql3="SELECT COUNT(*) as totLvl1
                FROM permitrecognitiondocreview
                WHERE mainReqidfk =:mainReqidfk AND prStatusIdfk =:prStatusIdfk
                      AND subReq1Idfk <> 0
                      AND subReq2Idfk = 0";
        $stmt=$conn1->prepare($sql3);
        $stmt->bindParam(':mainReqidfk', $mainReqidfk);
        $stmt->bindParam(':prStatusIdfk', $prStatusIdfk);
        $stmt->execute();
        $result=$stmt->fetch(\PDO::FETCH_ASSOC);      
        $totLvl1 = $result["totLvl1"];

        $sql4="SELECT COUNT(*) AS totLvl2
                FROM permitrecognitiondocreview
                WHERE mainReqidfk =:mainReqidfk AND prStatusIdfk =:prStatusIdfk
                      AND subReq1Idfk <> 0
                      AND subReq2Idfk <> 0";
        $stmt=$conn1->prepare($sql4);
        $stmt->bindParam(':mainReqidfk', $mainReqidfk);
        $stmt->bindParam(':prStatusIdfk', $prStatusIdfk);
        $stmt->execute();
        $result=$stmt->fetch(\PDO::FETCH_ASSOC);      
        $totLvl2 = $result["totLvl2"];

        $sqlId="SELECT prDocReviewId 
        FROM permitrecognitiondocreview
        WHERE mainReqidfk =:mainReqidfk AND prStatusIdfk =:prStatusIdfk
              AND subReq1Idfk = 0
              AND subReq2Idfk = 0";
        $stmt=$conn1->prepare($sqlId);
        $stmt->bindParam(':mainReqidfk', $mainReqidfk);
        $stmt->bindParam(':prStatusIdfk', $prStatusIdfk);
        $stmt->execute();
        $result=$stmt->fetch(\PDO::FETCH_ASSOC);      
        $prDocReviewId = $result["prDocReviewId"];

        if($totYes==($totLvl1+$totLvl2)){
          //update status base on status type
          $stmt = $conn1 -> prepare("UPDATE `permitrecognitiondocreview` SET " . $statusType . " = :value WHERE prDocReviewId = :docReviewId");
          $stmt->bindParam(':value', $value);
          $stmt->bindParam(':docReviewId', $prDocReviewId);
          $stmt->execute();
          //return "Good";
        }
        else{
          //return "Cant update"; 
        }
      }      
      $prStatusId = session('prStatusId');
      $applicationTypeId = session('applicationTypeId');
      $status = "Requirement has been updated";
      return route('viewapplication', [$prStatusId, $applicationTypeId, $status]);

    }

    public function saveRemarks(Request $request){
      $userIdfk = Auth::id();
      $docReviewId = $request->input('remarksId');
      $statusType = $request->input('statusType');
      $remarks = $request->input('reqRemarks');

      $conn1 = DB::connection('mysql')->getPdo();
      $stmt = $conn1 -> prepare("UPDATE `permitrecognitiondocreview` SET " . $statusType . " = :remarks WHERE prDocReviewId = :docReviewId");
      $stmt->bindParam(':remarks', $remarks);
      $stmt->bindParam(':docReviewId', $docReviewId);
      if($docReviewId!=""){
        $stmt->execute();

        $prStatusId = $request->input('prStatusId');
        $applicationTypeId = $request->input('applicationType');
        $status = "Remarks saved.";
        return redirect()->route('viewapplication', [$prStatusId, $applicationTypeId, $status]);
      }
      else{
        $prStatusId = $request->input('prStatusId');
        $applicationTypeId = $request->input('applicationType');
        $status = "Remarks Not saved.";
        return redirect()->route('viewapplication', [$prStatusId, $applicationTypeId, $status]);
      }

    }


}
