<?php

namespace App\Http\Controllers\NewApplication;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;
use App\Services\PayUService\Exception;
use App\Http\Controllers\PDO;

class PrivateSchoolNewApplication extends Controller
{
	//prevents accessing the page without logging in
    public function __construct()
    {
        $this->middleware('auth');
    }

    //url to be called
    public function index($applicationTypeId)
    {
        if($applicationTypeId==1){
            $applicationType = "New Application";
            $newBtnText = "New Application";
        }
        elseif($applicationTypeId==2){
            $applicationType = "Renewal";
            $newBtnText = "Renew Permit";   
        }
        elseif($applicationTypeId==3){
            $applicationType = "Recognition";
            $newBtnText = "Apply for Recognition";
        }

        return view('newapplication/privateschoolnewapplication')
            ->with('userid', Auth::id())
            ->with('applicationTypeId', $applicationTypeId)
            ->with('applicationType', $applicationType)
            ->with('newBtnText', $newBtnText)
            ->with('gradeLevels', Controller::gradeLevels())
            ->with('categoriesshs', Controller::categoriesshs())
            ->with('applicationlists', self::newApplicationLists($applicationTypeId));
    }

    public function draftNewApplication(Request $request){
        $applicationTypeIdfk = $request->input('applicationType');
        $applicationTypeId = $applicationTypeIdfk;
        $userIdfk = Auth::id();
        $gradeLevelIdfkFrom = $request->input('gradeLevelFrom');
        $gradeLevelIdfkTo = $request->input('gradeLevelTo');
        $governmentPRNumber = "";
        $seriesYear = date("Y");
        $schoolYear = $request->input('sy');
        $applicationStatusIdfk = 1; // draft status
        $categoryIdfk = $request->input('category');
        
        $conn1 = DB::connection('mysql')->getPdo();
        $stmt = $conn1 -> prepare("
                            INSERT INTO `permitrecognitionstatus`(
                                `prStatusId`,
                                `applicationTypeIdfk`,
                                `schoolUserIdfk`,
                                `gradeLevelIdfkFrom`,
                                `gradeLevelIdfkTo`,
                                `governmentPRNumber`,
                                `seriesYear`,
                                `schoolYear`,
                                `applicationStatusIdfk`,
                                `categoryIdfk`
                            )
                            VALUES(
                                null,
                                :applicationTypeIdfk,
                                :schoolUserIdfk,
                                :gradeLevelIdfkFrom,
                                :gradeLevelIdfkTo,
                                :governmentPRNumber,
                                :seriesYear,
                                :schoolYear,
                                :applicationStatusIdfk,
                                :categoryIdfk
                            )
            ");
        $stmt->bindParam(':applicationTypeIdfk', $applicationTypeIdfk);
        $stmt->bindParam('schoolUserIdfk', $userIdfk);
        $stmt->bindParam('gradeLevelIdfkFrom', $gradeLevelIdfkFrom);
        $stmt->bindParam('gradeLevelIdfkTo', $gradeLevelIdfkTo);
        $stmt->bindParam('governmentPRNumber', $governmentPRNumber);
        $stmt->bindParam('seriesYear', $seriesYear);
        $stmt->bindParam('schoolYear', $schoolYear);
        $stmt->bindParam('applicationStatusIdfk', $applicationStatusIdfk);
        $stmt->bindParam('categoryIdfk', $categoryIdfk);

        $stmt->execute();

            $stmt = $conn1->prepare("SELECT LAST_INSERT_ID()");
            $stmt->execute();

            $prStatusId = $stmt->fetchColumn();
            $stageIdfk = 1;
            $stageStatusIdfk = 1;
            $updatedBy = $userIdfk;
            $lastDateUpdated = date("Y-m-d h:i:s");
            $remarks = "";
            $docReviewFileIdfk = 0;

            $stmt = $conn1 -> prepare("
                                INSERT INTO `stagestimeline`(
                                    `stageTimelineId`,
                                    `prStatusIdfk`,
                                    `stageIdfk`,
                                    `stageStatusIdfk`,
                                    `updatedBy`,
                                    `lastDateUpdated`,
                                    `remarks`,
                                    `docReviewFileIdfk`
                                )
                                VALUES(
                                    null,
                                    :prStatusId,
                                    :stageIdfk,
                                    :stageStatusIdfk,
                                    :updatedBy,
                                    :lastDateUpdated,
                                    :remarks,
                                    :docReviewFileIdfk
                                )
                                ");
            $stmt->bindParam(':prStatusId', $prStatusId);
            $stmt->bindParam('stageIdfk', $stageIdfk);
            $stmt->bindParam('stageStatusIdfk', $stageStatusIdfk);
            $stmt->bindParam('updatedBy', $updatedBy);
            $stmt->bindParam('lastDateUpdated', $lastDateUpdated);
            $stmt->bindParam('remarks', $remarks);
            $stmt->bindParam('docReviewFileIdfk', $docReviewFileIdfk);
            $stmt->execute();

            $status = "Draft New Application";
        return redirect()->route('viewapplication', [$prStatusId, $applicationTypeId, $status]);
    }

    public function editDraftNewApplication(Request $request){
        $applicationTypeId = $request->input('applicationTypeEdit');
        $prStatusId = $request->input('prStatusIdEdit');
        $userIdfk = Auth::id();
        $gradeLevelIdfkFrom = $request->input('gradeLevelFrom');
        $gradeLevelIdfkTo = $request->input('gradeLevelTo');
        $schoolYear = $request->input('sy');
        $categoryIdfk = $request->input('category');
        
        $conn1 = DB::connection('mysql')->getPdo();
        $stmt = $conn1 -> prepare("
                                UPDATE
                                    `permitrecognitionstatus`
                                SET
                                    `gradeLevelIdfkFrom` = :gradeLevelIdfkFrom,
                                    `gradeLevelIdfkTo` = :gradeLevelIdfkTo,
                                    `schoolYear` = :schoolYear,
                                    `categoryIdfk` = :categoryIdfk
                                WHERE
                                    `prStatusId` = :prStatusId
            ");

        $stmt->bindParam('gradeLevelIdfkFrom', $gradeLevelIdfkFrom);
        $stmt->bindParam('gradeLevelIdfkTo', $gradeLevelIdfkTo);
        $stmt->bindParam('schoolYear', $schoolYear);
        $stmt->bindParam('categoryIdfk', $categoryIdfk);
        $stmt->bindParam('prStatusId', $prStatusId);

        $stmt->execute();

            $stageIdfk = 1;
            $stageStatusIdfk = 1;
            $updatedBy = $userIdfk;
            $lastDateUpdated = date("Y-m-d h:i:s");
            $remarks = "Editted Application";
            $docReviewFileIdfk = 0;

            $stmt = $conn1 -> prepare("
                                INSERT INTO `stagestimeline`(
                                    `stageTimelineId`,
                                    `prStatusIdfk`,
                                    `stageIdfk`,
                                    `stageStatusIdfk`,
                                    `updatedBy`,
                                    `lastDateUpdated`,
                                    `remarks`,
                                    `docReviewFileIdfk`
                                )
                                VALUES(
                                    null,
                                    :prStatusId,
                                    :stageIdfk,
                                    :stageStatusIdfk,
                                    :updatedBy,
                                    :lastDateUpdated,
                                    :remarks,
                                    :docReviewFileIdfk
                                )
                                ");
            $stmt->bindParam(':prStatusId', $prStatusId);
            $stmt->bindParam('stageIdfk', $stageIdfk);
            $stmt->bindParam('stageStatusIdfk', $stageStatusIdfk);
            $stmt->bindParam('updatedBy', $updatedBy);
            $stmt->bindParam('lastDateUpdated', $lastDateUpdated);
            $stmt->bindParam('remarks', $remarks);
            $stmt->bindParam('docReviewFileIdfk', $docReviewFileIdfk);
            $stmt->execute();

            $status = "Application has been updated.";
        return redirect()->route('viewapplication', [$prStatusId, $applicationTypeId, $status]);
    }


    public function newApplicationLists($applicationTypeId){
        $conn1 = DB::connection('mysql')->getPdo();
        $schoolUserIdfk = Auth::id();
        $stmt = $conn1 -> prepare("
                                SELECT prStatusId,
                                       applicationTypeIdfk,
                                       schoolUserIdfk,
                                       gradeLevelIdfkFrom,
                                       gradeLevelIdfkTo,
                                       governmentPRNumber,
                                       seriesYear,
                                       schoolYear,
                                       applicationStatusIdfk,
                                       applicationstatus.description AS applicationStatus,
                                       applicationstatus.valueNow,
                                       applicationstatus.progressClass,
                                       categoryIdfk,
                                       categoriesshs.category,
                                       tblFrom.description AS applicationFrom,
                                       tblTo.description AS applicationTo
                                FROM permitrecognitionstatus
                                     LEFT JOIN categoriesshs
                                        ON permitrecognitionstatus.categoryIdfk = categoriesshs.categoryId
                                     LEFT JOIN gradelevels AS tblFrom
                                        ON permitrecognitionstatus.gradeLevelIdfkFrom = tblFrom.gradeLevelId
                                     LEFT JOIN gradelevels AS tblTo
                                        ON permitrecognitionstatus.gradeLevelIdfkTo = tblTo.gradeLevelId
                                     LEFT JOIN applicationstatus
                                        ON permitrecognitionstatus.applicationStatusIdfk =
                                           applicationstatus.applicationStatusId
                                WHERE permitrecognitionstatus.schoolUserIdfk = :schoolUserIdfk
                                AND applicationTypeIdfk = :applicationTypeIdfk
                            ");
        $stmt->bindParam(":schoolUserIdfk",$schoolUserIdfk);
        $stmt->bindParam(":applicationTypeIdfk",$applicationTypeId);
        $stmt->execute();
        $result=$stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    public function viewapplication($prStatusId, $applicationTypeId, $status){
        return view('newapplication\viewapplication')
            ->with('prStatusId', $prStatusId)
            ->with('applicationTypeId', $applicationTypeId)
            ->with('gradeLevels', Controller::gradeLevels())
            ->with('newApplicationRequirements', Controller::newApplicationRequirements($prStatusId))
            ->with('applicationInfo', Controller::applicationInfo($prStatusId))
            ->with('getDivisionName', Controller::getDivisionName(Controller::getDivisionid()))
            ->with('categoriesshs', Controller::categoriesshs())
            ->with('stagesTimeLine', Controller::stagesTimeLine($prStatusId))
            ->with('status', $status)
            ->render();
    }

    public function submitapplication(Request $request){
        $applicationStatusId = 3;
        $prStatusId = $request->input('prStatusId');
        $applicationTypeId = $request->input('applicationType');

        $conn1 = DB::connection('mysql')->getPdo();
        $sql = $conn1->prepare("
                        UPDATE
                            `permitrecognitionstatus`
                        SET
                            `applicationStatusIdfk` = :applicationStatusIdfk
                        WHERE
                            `prStatusId` = :prStatusId
                    ");
        $sql->bindParam(':prStatusId', $prStatusId);
        $sql->bindParam(':applicationStatusIdfk', $applicationStatusId);
        $sql->execute();

            $stageIdfk = 1;
            $stageStatusIdfk = 3;
            $updatedBy = Auth::id();
            $lastDateUpdated = date("Y-m-d h:i:s");
            $remarks = "Submitted Application";
            $docReviewFileIdfk = 0;

            $stmt = $conn1 -> prepare("
                                INSERT INTO `stagestimeline`(
                                    `stageTimelineId`,
                                    `prStatusIdfk`,
                                    `stageIdfk`,
                                    `stageStatusIdfk`,
                                    `updatedBy`,
                                    `lastDateUpdated`,
                                    `remarks`,
                                    `docReviewFileIdfk`
                                )
                                VALUES(
                                    null,
                                    :prStatusId,
                                    :stageIdfk,
                                    :stageStatusIdfk,
                                    :updatedBy,
                                    :lastDateUpdated,
                                    :remarks,
                                    :docReviewFileIdfk
                                )
                                ");
            $stmt->bindParam(':prStatusId', $prStatusId);
            $stmt->bindParam('stageIdfk', $stageIdfk);
            $stmt->bindParam('stageStatusIdfk', $stageStatusIdfk);
            $stmt->bindParam('updatedBy', $updatedBy);
            $stmt->bindParam('lastDateUpdated', $lastDateUpdated);
            $stmt->bindParam('remarks', $remarks);
            $stmt->bindParam('docReviewFileIdfk', $docReviewFileIdfk);
            $stmt->execute();

        $status = "Submitted Application";
        return redirect()->route('viewapplication', [$prStatusId, $applicationTypeId, $status]);
    }

    public function cancelapplication(Request $request){
        $applicationStatusId = 2;
        $prStatusId = $request->input('prStatusId');
        $applicationTypeId = $request->input('applicationType');
        
        $conn1 = DB::connection('mysql')->getPdo();
        $sql = $conn1->prepare("
                        UPDATE
                            `permitrecognitionstatus`
                        SET
                            `applicationStatusIdfk` = :applicationStatusIdfk
                        WHERE
                            `prStatusId` = :prStatusId
                    ");
        $sql->bindParam(':prStatusId', $prStatusId);
        $sql->bindParam(':applicationStatusIdfk', $applicationStatusId);
        $sql->execute();

            $stageIdfk = 1;
            $stageStatusIdfk = 2;
            $updatedBy = Auth::id();
            $lastDateUpdated = date("Y-m-d h:i:s");
            $remarks = "Cancelled Application";
            $docReviewFileIdfk = 0;

            $stmt = $conn1 -> prepare("
                                INSERT INTO `stagestimeline`(
                                    `stageTimelineId`,
                                    `prStatusIdfk`,
                                    `stageIdfk`,
                                    `stageStatusIdfk`,
                                    `updatedBy`,
                                    `lastDateUpdated`,
                                    `remarks`,
                                    `docReviewFileIdfk`
                                )
                                VALUES(
                                    null,
                                    :prStatusId,
                                    :stageIdfk,
                                    :stageStatusIdfk,
                                    :updatedBy,
                                    :lastDateUpdated,
                                    :remarks,
                                    :docReviewFileIdfk
                                )
                                ");
            $stmt->bindParam(':prStatusId', $prStatusId);
            $stmt->bindParam('stageIdfk', $stageIdfk);
            $stmt->bindParam('stageStatusIdfk', $stageStatusIdfk);
            $stmt->bindParam('updatedBy', $updatedBy);
            $stmt->bindParam('lastDateUpdated', $lastDateUpdated);
            $stmt->bindParam('remarks', $remarks);
            $stmt->bindParam('docReviewFileIdfk', $docReviewFileIdfk);
            $stmt->execute();

        $status = "Application has been cancelled";
        return redirect()->route('viewapplication', [$prStatusId, $applicationTypeId, $status]);
    }
}
