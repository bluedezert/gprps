<?php

namespace App\Http\Controllers\NewApplication;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;
use App\Services\PayUService\Exception;
use App\Http\Controllers\PDO;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use Illuminate\Support\Facades\Config;

class RoApplicationMonitoring extends Controller
{
    //prevents accessing the page without logging in
    public function __construct()
    {
        $this->middleware('auth');
    }

    //url to be called
    public function index($applicationTypeId)
    {
        if($applicationTypeId==1){
            $applicationType = "New Application <small>Government Permit</small>";
            $snackbar = "Displaying List of New Application for Government Permit";
            //$newBtnText = "New Application";
        }
        elseif($applicationTypeId==2){
            $applicationType = "Renewal <small>Government Permit</small>";
            $snackbar = "Displaying List of Renewal Application for Government Permit";
            //$newBtnText = "Renew Permit";   
        }
        elseif($applicationTypeId==9){
            $applicationType = "Government Recognition - Recognized Schools";
            $snackbar = "Displaying List of Recognized Schools";
            //$newBtnText = "";
        }
        elseif($applicationTypeId==3){
            $applicationType = "Applicants for Recognition <small>Government Recognition</small>";
            $snackbar = "Displaying List of Applicants for Government Recognition";
            //$newBtnText = "Applying for Recognition";
        }

        elseif($applicationTypeId==4){
            $applicationType = "Renewal of Recognition <small>Government Recognition</small>";
            $snackbar = "Displaying List of Applicants for Renewal of Government Recognition";
            //$newBtnText = "Renewal of Recognition";
        }

        return view('newapplication/roapplicationmonitoring')
            ->with('userid', Auth::id())
            ->with('applicationTypeId', $applicationTypeId)
            ->with('applicationType', $applicationType)
            ->with('status', $snackbar)
            ->with('gradeLevels', Controller::gradeLevels())
            ->with('categoriesshs', Controller::categoriesshs())
            ->with('applicationlists', self::roApplicationLists($applicationTypeId));
    }

    public function roApplicationLists($applicationTypeId){
        $conn1 = DB::connection('mysql')->getPdo();
        $schoolUserIdfk = Auth::id();
        $stmt = $conn1 -> prepare("
                                SELECT prStatusId,
                                       applicationTypeIdfk,
                                       schoolUserIdfk,
                                       gradeLevelIdfkFrom,
                                       gradeLevelIdfkTo,
                                       governmentPRNumber,
                                       seriesYear,
                                       schoolYear,
                                       applicationStatusIdfk,
                                       applicationstatus.description AS applicationStatus,
                                       applicationstatus.valueNow,
                                       applicationstatus.progressClass,
                                       categoryIdfk,
                                       permitrecognitionstatus.lastDateUpdated,
                                       categoriesshs.category,
                                       tblFrom.description AS applicationFrom,
                                       tblTo.description AS applicationTo,
                                       users.officeIDfk,
                                       users.name AS schoolName,
                                       users.schoolsDivisionIdfk,
                                       SDO.schoolsDivision
                                FROM permitrecognitionstatus
                                     LEFT JOIN categoriesshs
                                        ON permitrecognitionstatus.categoryIdfk = categoriesshs.categoryId
                                     LEFT JOIN gradelevels AS tblFrom
                                        ON permitrecognitionstatus.gradeLevelIdfkFrom = tblFrom.gradeLevelId
                                     LEFT JOIN gradelevels AS tblTo
                                        ON permitrecognitionstatus.gradeLevelIdfkTo = tblTo.gradeLevelId
                                     LEFT JOIN applicationstatus
                                        ON permitrecognitionstatus.applicationStatusIdfk =
                                           applicationstatus.applicationStatusId
                                     LEFT JOIN users
                                        ON permitrecognitionstatus.schoolUserIdfk = users.id
                                     LEFT JOIN depedcarprofilesdb.tbl_schoolsdivisionoffices AS SDO
                                        ON users.schoolsDivisionIdfk = SDO.schoolsDivisionID
                                WHERE applicationTypeIdfk = :applicationTypeIdfk
                                AND permitrecognitionstatus.applicationStatusIdfk <> 1
                            ");
        $stmt->bindParam(":applicationTypeIdfk",$applicationTypeId);
        $stmt->execute();
        $result=$stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    public function roreviewapplication(Request $request){
      //update Application Status
      $applicationStatusId = 4;
      $prStatusId = $request->input('prStatusId');
      $lastDateUpdated = date("Y-m-d H:i:s");

      Controller::updateApplicationStatus($prStatusId, $applicationStatusId, $lastDateUpdated);

      //update Stages Timeline
          $stageIdfk = 4;
          $stageStatusIdfk = 13;
          $updatedBy = Auth::id();
          $remarks = "Review Application";
          $docReviewFileIdfk = 0;
          Controller::updateStagesTimeline($prStatusId, $stageIdfk, $stageStatusIdfk, $updatedBy, $lastDateUpdated, $remarks, $docReviewFileIdfk);

      //return to view application page
      $applicationTypeId = $request->input('applicationType');
      $status = "Application for Regional Office review started";
      return redirect()->route('viewapplication', [$prStatusId, $applicationTypeId, $status]);
    }


    public function getFixedInfo($prStatusId){
      $applicationInfo = Controller::applicationInfo($prStatusId);
      $fixedInfo = array(
        'schoolYear' => $applicationInfo["schoolYear"],
        'schoolId' => $applicationInfo["schoolId"],
        'schoolName' => $applicationInfo["schoolName"],
        'schoolEmailAddress' => $applicationInfo["schoolEmailAddress"],
        'gradeFrom' => $applicationInfo["gradeLevelIdfkFrom"],
        'gradeTo' => $applicationInfo["gradeLevelIdfkTo"]
      );

      return $fixedInfo;
    }

    public function generateEmail($subject, $greeting, $emailFixedData, $firstParagraph, $lastDateUpdated)
    {
        $message = "";
        $message .= $firstParagraph;
        $message .= "<br><strong>School ID:</strong> " . $emailFixedData["schoolId"];
        $message .= "<br><strong>School Name:</strong> " . $emailFixedData["schoolName"];
        $message .= "<br><strong>School Year:</strong> " . $emailFixedData["schoolYear"];
        $message .= "<br><strong>Grade Levels Applying for:</strong> From " .  Controller::gradeLevelsDescription($emailFixedData["gradeFrom"]) . " to " .  Controller::gradeLevelsDescription($emailFixedData["gradeTo"]);
        $message .= "<br><strong>Date:</strong> " . $lastDateUpdated;

      $generatedEmail = array(
          'subject'   =>  $subject,
          'greeting'  =>  $greeting,
          'message'   =>  $message
      );
      return $generatedEmail;
    }

    public function emailRecipients($email, $schoolEmailAddress, $getSdoPrivateSchoolFocal, $generatedEmail){
      $status = "";      
      Mail::to($email)->send(new SendMail($generatedEmail)); // send to SDO
      Mail::to($schoolEmailAddress)->send(new SendMail($generatedEmail)); // send to school
      $roEmail = "ictu.depedcar@gmail.com";
      Mail::to($roEmail)->send(new SendMail($generatedEmail)); // send to RO (qad.depedcar@gmail.com)
      if(isset($getSdoPrivateSchoolFocal)){
          Mail::to($getSdoPrivateSchoolFocal)->send(new SendMail($generatedEmail)); // send to SDO Focal Person
      }
      else{
          $status .= ". No email sent to SDO";
          //return $status; 
      }
    }

    public function notifyforcpmpliancero(Request $request){
      //update Application Status
      $applicationStatusId = 8;
      $prStatusId = $request->input('prStatusId');
      $lastDateUpdated = date("Y-m-d H:i:s");
      Controller::updateApplicationStatus($prStatusId, $applicationStatusId, $lastDateUpdated);

      //update Stages Timeline
          $stageIdfk = 4;
          $stageStatusIdfk = 14;
          $updatedBy = Auth::id();
          $remarks = "Notify for Compliance";
          $docReviewFileIdfk = 0;
          Controller::updateStagesTimeline($prStatusId, $stageIdfk, $stageStatusIdfk, $updatedBy, $lastDateUpdated, $remarks, $docReviewFileIdfk);


      //email generation
          $subject = "GPRPS - Application Monitoring and Management (Review - Regional Office)";
          $greeting = "Hello!";
          $firstParagraph = "Regional Office notified the following private school for compliance of incomplete documents.<br>";
          $fixedInfo = self::getFixedInfo($prStatusId);
          $generatedEmail = self::generateEmail($subject, $greeting, $fixedInfo, $firstParagraph, $lastDateUpdated);

        //recipients
          $email = Auth::user()->email;//email of SDO DAIT
          $schoolEmailAddress = $fixedInfo["schoolEmailAddress"];
          $getSdoPrivateSchoolFocal = Controller::getSdoPrivateSchoolFocal(Auth::user()->schoolsDivisionIdfk);
          self::emailRecipients($email, $schoolEmailAddress, $getSdoPrivateSchoolFocal, $generatedEmail); 

      //return to view application page
      $applicationTypeId = $request->input('applicationType');
      $status = "Private school has been notified for compliance";
      return redirect()->route('viewapplication', [$prStatusId, $applicationTypeId, $status]);
    }

    public function forinspectionro(Request $request){
      //update Application Status
      $applicationStatusId = 4;
      $prStatusId = $request->input('prStatusId');
      $lastDateUpdated = date("Y-m-d H:i:s");
      Controller::updateApplicationStatus($prStatusId, $applicationStatusId, $lastDateUpdated);

      //update Stages Timeline
          $stageIdfk = 4;
          $stageStatusIdfk = 15;
          $updatedBy = Auth::id();
          $remarks = "For Inspection";
          $docReviewFileIdfk = 0;
          Controller::updateStagesTimeline($prStatusId, $stageIdfk, $stageStatusIdfk, $updatedBy, $lastDateUpdated, $remarks, $docReviewFileIdfk);


      //email generation
          $subject = "GPRPS - Application Monitoring and Management (Ocular Inspection - Regional Office)";
          $greeting = "Hello!";
          $firstParagraph = "Regional Office tagged the following private school for ocular visit.<br>";
          $fixedInfo = self::getFixedInfo($prStatusId);
          $generatedEmail = self::generateEmail($subject, $greeting, $fixedInfo, $firstParagraph, $lastDateUpdated);

        //recipients
          $email = Auth::user()->email;//email of SDO DAIT
          $schoolEmailAddress = $fixedInfo["schoolEmailAddress"];
          $getSdoPrivateSchoolFocal = Controller::getSdoPrivateSchoolFocal(Auth::user()->schoolsDivisionIdfk);
          self::emailRecipients($email, $schoolEmailAddress, $getSdoPrivateSchoolFocal, $generatedEmail); 


      //return to view application page
      $applicationTypeId = $request->input('applicationType');
      $status = "Application for RO Ocular Visit";
      return redirect()->route('viewapplication', [$prStatusId, $applicationTypeId, $status]);
    }

    public function compliantfornewissuance(Request $request){
      //update Application Status
      $applicationStatusId = 5;
      $prStatusId = $request->input('prStatusId');
      $lastDateUpdated = date("Y-m-d H:i:s");
      Controller::updateApplicationStatus($prStatusId, $applicationStatusId, $lastDateUpdated);

      //update Stages Timeline
          $stageIdfk = 5;
          $stageStatusIdfk = 16;
          $updatedBy = Auth::id();
          $remarks = "Compliant for Issuance of Permit to Operate";
          $docReviewFileIdfk = 0;
          Controller::updateStagesTimeline($prStatusId, $stageIdfk, $stageStatusIdfk, $updatedBy, $lastDateUpdated, $remarks, $docReviewFileIdfk);


      //update Stages Timeline
          $stageIdfk = 5;
          $stageStatusIdfk = 22;
          $updatedBy = Auth::id();
          $remarks = "Approved Application";
          $docReviewFileIdfk = 0;
          Controller::updateStagesTimeline($prStatusId, $stageIdfk, $stageStatusIdfk, $updatedBy, $lastDateUpdated, $remarks, $docReviewFileIdfk);

      //get application Info
          $applicationInfo = Controller::applicationInfo($prStatusId);
          $pre = "QAD";
          $applicationTypeIdfk = $applicationInfo["applicationTypeIdfk"]; // Permit or Renewal
            if($applicationTypeIdfk==1 || $applicationTypeIdfk==2){
              $certType = "P";
            }
            elseif($applicationTypeIdfk==3 || $applicationTypeIdfk==4){
              $certType = "R";
            }
          $schoolsDivisionIdfk = $applicationInfo["schoolsDivisionIdfk"]; // AB AP BAG BENG IF KAL MP TAB
          $sdo = "";
            switch($schoolsDivisionIdfk){
              case 1:
                $sdo = "AB";
                break;
              case 2:
                $sdo = "AP";
                break;
              case 3:
                $sdo = "BAG";
                break;
              case 4:
                $sdo = "BENG";
                break;
              case 5:
                $sdo = "IF";
                break;
              case 6:
                $sdo = "KAL";
                break;
              case 7:
                $sdo = "MP";
                break;
              case 8:
                $sdo = "TAB";
                break;
              default;
                $sdo = "ERR";
            }

          $number = 0;

          $seriesYear = $applicationInfo["seriesYear"];
          $lastNumber = self::getLatestCertificateNumber($seriesYear, $applicationTypeIdfk);
          $number = $lastNumber + 1;
          $newNumber = sprintf("%03d", $number);
          $governmentPRNumberfk = $pre . " - " . $certType . " - " . $sdo . " - " . $newNumber . ", s. " . $seriesYear;
      //insert record on generate certificate
          $conn1 = DB::connection('mysql')->getPdo();
          $sql = "INSERT INTO `generatedcertificates`(
                      `certificateId`,
                      `seriesYear`,
                      `prStatusIdfk`,
                      `applicationTypeIdfk`,
                      `governmentPRNumberfk`,
                      `number`,
                      `dateApproved`,
                      `certGenerated`
                  )
                  VALUES(
                      null,
                      :seriesYear,
                      :prStatusIdfk,
                      :applicationTypeIdfk,
                      :governmentPRNumberfk,
                      :newNumber,
                      :dateApproved,
                      0
                  )";
          $stmt=$conn1->prepare($sql);
          $stmt->bindParam(":seriesYear", $seriesYear);
          $stmt->bindParam(":prStatusIdfk", $prStatusId);
          $stmt->bindParam(":applicationTypeIdfk", $applicationTypeIdfk);
          $stmt->bindParam(":governmentPRNumberfk", $governmentPRNumberfk);
          $stmt->bindParam(":newNumber", $number);
          $stmt->bindParam(":dateApproved", $lastDateUpdated);
          $stmt->execute();

          $sql = "UPDATE permitrecognitionstatus SET `governmentPRNumber`=:governmentPRNumber WHERE prStatusId=:prStatusId";
          $stmt=$conn1->prepare($sql);
          $stmt->bindParam(":governmentPRNumber", $governmentPRNumberfk);
          $stmt->bindParam(":prStatusId", $prStatusId);
          $stmt->execute();

      //email generation
          $subject = "GPRPS - Application Monitoring and Management (Compliant for issuance of permit / recognition)";
          $greeting = "Hello!";
          $firstParagraph = "Regional Office tagged the following private school to be compliant fore permit / recognition issuance.<br>";
          $fixedInfo = self::getFixedInfo($prStatusId);
          $generatedEmail = self::generateEmail($subject, $greeting, $fixedInfo, $firstParagraph, $lastDateUpdated);

        //recipients
          $email = Auth::user()->email;//email of SDO DAIT
          $schoolEmailAddress = $fixedInfo["schoolEmailAddress"];
          $getSdoPrivateSchoolFocal = Controller::getSdoPrivateSchoolFocal(Auth::user()->schoolsDivisionIdfk);
          self::emailRecipients($email, $schoolEmailAddress, $getSdoPrivateSchoolFocal, $generatedEmail); 

      //return to view application page
      $applicationTypeId = $request->input('applicationType');
      $status = "Application is compliant for issuance of permit to operate.";
      return redirect()->route('viewapplication', [$prStatusId, $applicationTypeId, $status]);
    }

    public function compliantforrenewissuance(Request $request){
      //update Application Status
      $applicationStatusId = 5;
      $prStatusId = $request->input('prStatusId');
      $lastDateUpdated = date("Y-m-d H:i:s");
      Controller::updateApplicationStatus($prStatusId, $applicationStatusId, $lastDateUpdated);

      //update Stages Timeline
          $stageIdfk = 5;
          $stageStatusIdfk = 16;
          $updatedBy = Auth::id();
          $remarks = "Compliant for Issuance of Permit to Operate";
          $docReviewFileIdfk = 0;
          Controller::updateStagesTimeline($prStatusId, $stageIdfk, $stageStatusIdfk, $updatedBy, $lastDateUpdated, $remarks, $docReviewFileIdfk);

      //update Stages Timeline
          $stageIdfk = 5;
          $stageStatusIdfk = 22;
          $updatedBy = Auth::id();
          $remarks = "Approved Application";
          $docReviewFileIdfk = 0;
          Controller::updateStagesTimeline($prStatusId, $stageIdfk, $stageStatusIdfk, $updatedBy, $lastDateUpdated, $remarks, $docReviewFileIdfk);


      //get application Info
          $applicationInfo = Controller::applicationInfo($prStatusId);
          $pre = "QAD";
          $applicationTypeIdfk = $applicationInfo["applicationTypeIdfk"]; // Permit or Renewal
            if($applicationTypeIdfk==1 || $applicationTypeIdfk==2){
              $certType = "P";
            }
            elseif($applicationTypeIdfk==3 || $applicationTypeIdfk==4){
              $certType = "R";
            }
          $schoolsDivisionIdfk = $applicationInfo["schoolsDivisionIdfk"]; // AB AP BAG BENG IF KAL MP TAB
          $sdo = "";
            switch($schoolsDivisionIdfk){
              case 1:
                $sdo = "AB";
                break;
              case 2:
                $sdo = "AP";
                break;
              case 3:
                $sdo = "BAG";
                break;
              case 4:
                $sdo = "BENG";
                break;
              case 5:
                $sdo = "IF";
                break;
              case 6:
                $sdo = "KAL";
                break;
              case 7:
                $sdo = "MP";
                break;
              case 8:
                $sdo = "TAB";
                break;
              default;
                $sdo = "ERR";
            }

          $number = 0;

          $seriesYear = $applicationInfo["seriesYear"];
          $lastNumber = self::getLatestCertificateNumber($seriesYear, $applicationTypeIdfk);
          $number = $lastNumber + 1;
          $newNumber = sprintf("%03d", $number);
          $governmentPRNumberfk = $pre . " - " . $certType . " - " . $sdo . " - " . $newNumber . ", s. " . $seriesYear;
      //insert record on generate certificate
          $conn1 = DB::connection('mysql')->getPdo();
          $sql = "INSERT INTO `generatedcertificates`(
                      `certificateId`,
                      `seriesYear`,
                      `prStatusIdfk`,
                      `applicationTypeIdfk`,
                      `governmentPRNumberfk`,
                      `number`,
                      `dateApproved`,
                      `certGenerated`
                  )
                  VALUES(
                      null,
                      :seriesYear,
                      :prStatusIdfk,
                      :applicationTypeIdfk,
                      :governmentPRNumberfk,
                      :newNumber,
                      :dateApproved,
                      0
                  )";
          $stmt=$conn1->prepare($sql);
          $stmt->bindParam(":seriesYear", $seriesYear);
          $stmt->bindParam(":prStatusIdfk", $prStatusId);
          $stmt->bindParam(":applicationTypeIdfk", $applicationTypeIdfk);
          $stmt->bindParam(":governmentPRNumberfk", $governmentPRNumberfk);
          $stmt->bindParam(":newNumber", $number);
          $stmt->bindParam(":dateApproved", $lastDateUpdated);
          $stmt->execute();

          $sql = "UPDATE permitrecognitionstatus SET `governmentPRNumber`=:governmentPRNumber WHERE prStatusId=:prStatusId";
          $stmt=$conn1->prepare($sql);
          $stmt->bindParam(":governmentPRNumber", $governmentPRNumberfk);
          $stmt->bindParam(":prStatusId", $prStatusId);
          $stmt->execute();

      //email generation
          $subject = "GPRPS - Application Monitoring and Management (Compliant for issuance of permit / recognition)";
          $greeting = "Hello!";
          $firstParagraph = "Regional Office tagged the following private school to be compliant fore permit / recognition issuance.<br>";
          $fixedInfo = self::getFixedInfo($prStatusId);
          $generatedEmail = self::generateEmail($subject, $greeting, $fixedInfo, $firstParagraph, $lastDateUpdated);

        //recipients
          $email = Auth::user()->email;//email of SDO DAIT
          $schoolEmailAddress = $fixedInfo["schoolEmailAddress"];
          $getSdoPrivateSchoolFocal = Controller::getSdoPrivateSchoolFocal(Auth::user()->schoolsDivisionIdfk);
          self::emailRecipients($email, $schoolEmailAddress, $getSdoPrivateSchoolFocal, $generatedEmail); 

      //return to view application page
      $applicationTypeId = $request->input('applicationType');
      $status = "Application is compliant for issuance of permit to operate.";
      return redirect()->route('viewapplication', [$prStatusId, $applicationTypeId, $status]);
    }

    public function compliantforrecognitionissuance(Request $request){
      //update Application Status
      $applicationStatusId = 5;
      $prStatusId = $request->input('prStatusId');
      $lastDateUpdated = date("Y-m-d H:i:s");
      Controller::updateApplicationStatus($prStatusId, $applicationStatusId, $lastDateUpdated);

      //update Stages Timeline
          $stageIdfk = 5;
          $stageStatusIdfk = 17;
          $updatedBy = Auth::id();
          $remarks = "Compliant for Issuance of Permit to Operate";
          $docReviewFileIdfk = 0;
          Controller::updateStagesTimeline($prStatusId, $stageIdfk, $stageStatusIdfk, $updatedBy, $lastDateUpdated, $remarks, $docReviewFileIdfk);


      //update Stages Timeline
          $stageIdfk = 5;
          $stageStatusIdfk = 22;
          $updatedBy = Auth::id();
          $remarks = "Approved Application";
          $docReviewFileIdfk = 0;
          Controller::updateStagesTimeline($prStatusId, $stageIdfk, $stageStatusIdfk, $updatedBy, $lastDateUpdated, $remarks, $docReviewFileIdfk);


      //get application Info
          $applicationInfo = Controller::applicationInfo($prStatusId);
          $pre = "QAD";
          $applicationTypeIdfk = $applicationInfo["applicationTypeIdfk"]; // Permit or Renewal
            if($applicationTypeIdfk==1 || $applicationTypeIdfk==2){
              $certType = "P";
            }
            elseif($applicationTypeIdfk==3 || $applicationTypeIdfk==4){
              $certType = "R";
            }
          $schoolsDivisionIdfk = $applicationInfo["schoolsDivisionIdfk"]; // AB AP BAG BENG IF KAL MP TAB
          $sdo = "";
            switch($schoolsDivisionIdfk){
              case 1:
                $sdo = "AB";
                break;
              case 2:
                $sdo = "AP";
                break;
              case 3:
                $sdo = "BAG";
                break;
              case 4:
                $sdo = "BENG";
                break;
              case 5:
                $sdo = "IF";
                break;
              case 6:
                $sdo = "KAL";
                break;
              case 7:
                $sdo = "MP";
                break;
              case 8:
                $sdo = "TAB";
                break;
              default;
                $sdo = "ERR";
            }

          $number = 0;

          $seriesYear = $applicationInfo["seriesYear"];
          $lastNumber = self::getLatestCertificateNumber($seriesYear, $applicationTypeIdfk);
          $number = $lastNumber + 1;
          $newNumber = sprintf("%03d", $number);
          $governmentPRNumberfk = $pre . " - " . $certType . " - " . $sdo . " - " . $newNumber . ", s. " . $seriesYear;
      //insert record on generate certificate
          $conn1 = DB::connection('mysql')->getPdo();
          $sql = "INSERT INTO `generatedcertificates`(
                      `certificateId`,
                      `seriesYear`,
                      `prStatusIdfk`,
                      `applicationTypeIdfk`,
                      `governmentPRNumberfk`,
                      `number`,
                      `dateApproved`,
                      `certGenerated`
                  )
                  VALUES(
                      null,
                      :seriesYear,
                      :prStatusIdfk,
                      :applicationTypeIdfk,
                      :governmentPRNumberfk,
                      :newNumber,
                      :dateApproved,
                      0
                  )";
          $stmt=$conn1->prepare($sql);
          $stmt->bindParam(":seriesYear", $seriesYear);
          $stmt->bindParam(":prStatusIdfk", $prStatusId);
          $stmt->bindParam(":applicationTypeIdfk", $applicationTypeIdfk);
          $stmt->bindParam(":governmentPRNumberfk", $governmentPRNumberfk);
          $stmt->bindParam(":newNumber", $number);
          $stmt->bindParam(":dateApproved", $lastDateUpdated);
          $stmt->execute();

          $sql = "UPDATE permitrecognitionstatus SET `governmentPRNumber`=:governmentPRNumber WHERE prStatusId=:prStatusId";
          $stmt=$conn1->prepare($sql);
          $stmt->bindParam(":governmentPRNumber", $governmentPRNumberfk);
          $stmt->bindParam(":prStatusId", $prStatusId);
          $stmt->execute();

      //email generation
          $subject = "GPRPS - Application Monitoring and Management (Compliant for issuance of permit / recognition)";
          $greeting = "Hello!";
          $firstParagraph = "Regional Office tagged the following private school to be compliant fore permit / recognition issuance.<br>";
          $fixedInfo = self::getFixedInfo($prStatusId);
          $generatedEmail = self::generateEmail($subject, $greeting, $fixedInfo, $firstParagraph, $lastDateUpdated);

        //recipients
          $email = Auth::user()->email;//email of SDO DAIT
          $schoolEmailAddress = $fixedInfo["schoolEmailAddress"];
          $getSdoPrivateSchoolFocal = Controller::getSdoPrivateSchoolFocal(Auth::user()->schoolsDivisionIdfk);
          self::emailRecipients($email, $schoolEmailAddress, $getSdoPrivateSchoolFocal, $generatedEmail); 

      //return to view application page
      $applicationTypeId = $request->input('applicationType');
      $status = "Application is compliant for issuance of permit to operate.";
      return redirect()->route('viewapplication', [$prStatusId, $applicationTypeId, $status]);
    }

    public function forpaymentandissunce(Request $request){
      //update Application Status
      $applicationStatusId = 5;
      $prStatusId = $request->input('prStatusId');
      $lastDateUpdated = date("Y-m-d H:i:s");
      Controller::updateApplicationStatus($prStatusId, $applicationStatusId, $lastDateUpdated);

      //update Stages Timeline
          $stageIdfk = 5;
          $stageStatusIdfk = 18;
          $updatedBy = Auth::id();
          $remarks = "For Payment and Issuance of Certification";
          $docReviewFileIdfk = 0;
          Controller::updateStagesTimeline($prStatusId, $stageIdfk, $stageStatusIdfk, $updatedBy, $lastDateUpdated, $remarks, $docReviewFileIdfk);

      //email generation
          $subject = "GPRPS - Application Monitoring and Management (For Pick-up)";
          $greeting = "Hello!";
          $firstParagraph = "Permit / Recognition is now available for pick-up at the Regional Office<br>";
          $fixedInfo = self::getFixedInfo($prStatusId);
          $generatedEmail = self::generateEmail($subject, $greeting, $fixedInfo, $firstParagraph, $lastDateUpdated);

        //recipients
          $email = Auth::user()->email;//email of SDO DAIT
          $schoolEmailAddress = $fixedInfo["schoolEmailAddress"];
          $getSdoPrivateSchoolFocal = Controller::getSdoPrivateSchoolFocal(Auth::user()->schoolsDivisionIdfk);
          self::emailRecipients($email, $schoolEmailAddress, $getSdoPrivateSchoolFocal, $generatedEmail); 

      //return to view application page
      $applicationTypeId = $request->input('applicationType');
      $status = "Application is set for payment and issuance of certification.";
      return redirect()->route('viewapplication', [$prStatusId, $applicationTypeId, $status]);
    }

    public function payment(Request $request){
      //update Application Status
      $applicationStatusId = 5;
      $prStatusId = $request->input('prStatusId');
      $lastDateUpdated = date("Y-m-d H:i:s");
      Controller::updateApplicationStatus($prStatusId, $applicationStatusId, $lastDateUpdated);

      //update Stages Timeline
          $stageIdfk = 5;
          $stageStatusIdfk = 19;
          $updatedBy = Auth::id();
          $remarks = "Paid";
          $docReviewFileIdfk = 0;
          Controller::updateStagesTimeline($prStatusId, $stageIdfk, $stageStatusIdfk, $updatedBy, $lastDateUpdated, $remarks, $docReviewFileIdfk);

      //return to view application page
      $applicationTypeId = $request->input('applicationType');
      $status = "Paid application.";
      return redirect()->route('viewapplication', [$prStatusId, $applicationTypeId, $status]);
    }

    public function issuedpermit(Request $request){
      //update Application Status
      $applicationStatusId = 5;
      $prStatusId = $request->input('prStatusId');
      $lastDateUpdated = date("Y-m-d H:i:s");
      Controller::updateApplicationStatus($prStatusId, $applicationStatusId, $lastDateUpdated);

      $applicationInfo = Controller::applicationInfo($prStatusId);
      $governmentPRNumberfk = $applicationInfo["governmentPRNumber"];
      //update Stages Timeline
          $stageIdfk = 5;
          $stageStatusIdfk = 20;
          $updatedBy = Auth::id();
          $remarks = "Issued Government Permit to Operate";
          $docReviewFileIdfk = 0;
          Controller::updateStagesTimeline($prStatusId, $stageIdfk, $stageStatusIdfk, $updatedBy, $lastDateUpdated, $remarks, $docReviewFileIdfk);

          $conn1 = DB::connection('mysql')->getPdo();
          $sql = "UPDATE generatedcertificates SET `dateIssued`=:dateIssued, `issuedBy`=:issuedBy WHERE governmentPRNumberfk=:governmentPRNumberfk";
          $stmt=$conn1->prepare($sql);
          $stmt->bindParam(":dateIssued", $lastDateUpdated);
          $stmt->bindParam(":issuedBy", $updatedBy);
          $stmt->bindParam(":governmentPRNumberfk", $governmentPRNumberfk);
          $stmt->execute();

      //email generation
          $subject = "GPRPS - Application Monitoring and Management (Issuance of Permit to Operate)";
          $greeting = "Hello!";
          $firstParagraph = "Regional Office issued certificate of permit to operate to the following private school. <strong>Permit Number : ". $governmentPRNumberfk ."</strong><br>";
          $fixedInfo = self::getFixedInfo($prStatusId);
          $generatedEmail = self::generateEmail($subject, $greeting, $fixedInfo, $firstParagraph, $lastDateUpdated);

        //recipients
          $email = Auth::user()->email;//email of SDO DAIT
          $schoolEmailAddress = $fixedInfo["schoolEmailAddress"];
          $getSdoPrivateSchoolFocal = Controller::getSdoPrivateSchoolFocal(Auth::user()->schoolsDivisionIdfk);
          self::emailRecipients($email, $schoolEmailAddress, $getSdoPrivateSchoolFocal, $generatedEmail); 

      //return to view application page
      $applicationTypeId = $request->input('applicationType');
      $status = "School has been issued a government permit to operate";
      return redirect()->route('viewapplication', [$prStatusId, $applicationTypeId, $status]);
    }

    public function issuedrecognition(Request $request){
      //update Application Status
      $applicationStatusId = 5;
      $prStatusId = $request->input('prStatusId');
      $lastDateUpdated = date("Y-m-d H:i:s");
      Controller::updateApplicationStatus($prStatusId, $applicationStatusId, $lastDateUpdated);

      $applicationInfo = Controller::applicationInfo($prStatusId);
      $governmentPRNumberfk = $applicationInfo["governmentPRNumber"];

      //update Stages Timeline
          $stageIdfk = 5;
          $stageStatusIdfk = 21;
          $updatedBy = Auth::id();
          $remarks = "Issued Government Recognition";
          $docReviewFileIdfk = 0;
          Controller::updateStagesTimeline($prStatusId, $stageIdfk, $stageStatusIdfk, $updatedBy, $lastDateUpdated, $remarks, $docReviewFileIdfk);

          $conn1 = DB::connection('mysql')->getPdo();
          $sql = "UPDATE generatedcertificates SET `dateIssued`=:dateIssued, `issuedBy`=:issuedBy WHERE governmentPRNumberfk=:governmentPRNumberfk";
          $stmt=$conn1->prepare($sql);
          $stmt->bindParam(":dateIssued", $lastDateUpdated);
          $stmt->bindParam(":issuedBy", $updatedBy);
          $stmt->bindParam(":governmentPRNumberfk", $governmentPRNumberfk);
          $stmt->execute();

      //email generation
          $subject = "GPRPS - Application Monitoring and Management (Issuance of Recognition)";
          $greeting = "Hello!";
          $firstParagraph = "Regional Office issued certificate of recognition to the following private school. <strong>Permit Number : ". $governmentPRNumberfk ."</strong><br>";
          $fixedInfo = self::getFixedInfo($prStatusId);
          $generatedEmail = self::generateEmail($subject, $greeting, $fixedInfo, $firstParagraph, $lastDateUpdated);

        //recipients
          $email = Auth::user()->email;//email of SDO DAIT
          $schoolEmailAddress = $fixedInfo["schoolEmailAddress"];
          $getSdoPrivateSchoolFocal = Controller::getSdoPrivateSchoolFocal(Auth::user()->schoolsDivisionIdfk);
          self::emailRecipients($email, $schoolEmailAddress, $getSdoPrivateSchoolFocal, $generatedEmail); 


      //return to view application page
      $applicationTypeId = $request->input('applicationType');
      $status = "School has been issued a government recognition.";
      return redirect()->route('viewapplication', [$prStatusId, $applicationTypeId, $status]);
    }

    public function disapproveapplication(Request $request){
      //update Application Status
      $applicationStatusId = 6;
      $prStatusId = $request->input('prStatusId');
      $lastDateUpdated = date("Y-m-d H:i:s");
      Controller::updateApplicationStatus($prStatusId, $applicationStatusId, $lastDateUpdated);

      //update Stages Timeline
          $stageIdfk = 5;
          $stageStatusIdfk = 23;
          $updatedBy = Auth::id();
          $remarks = "Disapproved Application";
          $docReviewFileIdfk = 0;
          Controller::updateStagesTimeline($prStatusId, $stageIdfk, $stageStatusIdfk, $updatedBy, $lastDateUpdated, $remarks, $docReviewFileIdfk);

      //email generation
          $subject = "GPRPS - Application Monitoring and Management (Disapproved Application)";
          $greeting = "Hello!";
          $firstParagraph = "Regional Office disapproved the application of the following private school.<br>";
          $fixedInfo = self::getFixedInfo($prStatusId);
          $generatedEmail = self::generateEmail($subject, $greeting, $fixedInfo, $firstParagraph, $lastDateUpdated);

        //recipients
          $email = Auth::user()->email;//email of SDO DAIT
          $schoolEmailAddress = $fixedInfo["schoolEmailAddress"];
          $getSdoPrivateSchoolFocal = Controller::getSdoPrivateSchoolFocal(Auth::user()->schoolsDivisionIdfk);
          self::emailRecipients($email, $schoolEmailAddress, $getSdoPrivateSchoolFocal, $generatedEmail); 

      //return to view application page
      $applicationTypeId = $request->input('applicationType');
      $status = "Application has been disapproved.";
      return redirect()->route('viewapplication', [$prStatusId, $applicationTypeId, $status]);
    }

    public function getLatestCertificateNumber($seriesYear, $applicationTypeIdfk){

      if($applicationTypeIdfk==1 || $applicationTypeIdfk==2){
        $conn1 = DB::connection('mysql')->getPdo();
        $sql = "SELECT MAX(`number`) AS lastNumber FROM `generatedcertificates` WHERE `seriesYear`=:seriesYear AND (applicationTypeIdfk=1 || applicationTypeIdfk=2)";
        $stmt=$conn1->prepare($sql);
        $stmt->bindParam(":seriesYear", $seriesYear);
        $stmt->execute();
        $result=$stmt->fetch(\PDO::FETCH_ASSOC);
        $lastNumber = $result["lastNumber"];
        if($lastNumber==null || $lastNumber==0){
          $lastNumber = 0;
        }        
      }
      elseif($applicationTypeIdfk==3 || $applicationTypeIdfk==4){
        $conn1 = DB::connection('mysql')->getPdo();
        $sql = "SELECT MAX(`number`) AS lastNumber FROM `generatedcertificates` WHERE `seriesYear`=:seriesYear AND (applicationTypeIdfk=3 || applicationTypeIdfk=4)";
        $stmt=$conn1->prepare($sql);
        $stmt->bindParam(":seriesYear", $seriesYear);
        $stmt->execute();
        $result=$stmt->fetch(\PDO::FETCH_ASSOC);
        $lastNumber = $result["lastNumber"];
        if($lastNumber==null || $lastNumber==0){
          $lastNumber = 0;
        }        
      }

      return $lastNumber;
    }

}
