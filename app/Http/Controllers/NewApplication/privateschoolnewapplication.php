<?php

namespace App\Http\Controllers\NewApplication;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;
use App\Services\PayUService\Exception;
use App\Http\Controllers\PDO;

use App\Mail\DemoMail;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use Illuminate\Support\Facades\Config;

class PrivateSchoolNewApplication extends Controller
{
    //prevents accessing the page without logging in
    public function __construct()
    {
        $this->middleware('auth');
    }

    //url to be called
    public function index($applicationTypeId)
    {
        if($applicationTypeId==1){
            $applicationType = "New Application <small>Government Permit</small>";
            $snackbar = "Displaying List of New Application for Government Permit";
            $newBtnText = "<i class='fa fa-file-text'></i> New Application";
        }
        elseif($applicationTypeId==2){
            $applicationType = "Renewal <small>Government Permit</small>";
            $snackbar = "Displaying List of Renewal Application for Government Permit";
            $newBtnText = "<i class='fa fa-refresh'></i> Renew Permit";   
        }
        elseif($applicationTypeId==9){
            $applicationType = "Government Recognition - Recognized Applications";
            $snackbar = "Displaying List of Recognized Applications";
            $newBtnText = "";
        }
        elseif($applicationTypeId==3){
            $applicationType = "Applicants for Recognition <small>Government Recognition</small>";
            $snackbar = "Displaying List of Applicants for Government Recognition";
            $newBtnText = "<i class='fa fa-file-text'></i> Apply for Recognition";
        }
        elseif($applicationTypeId==4){
            $applicationType = "Renewal of Recognition <small>Government Recognition</small>";
            $snackbar = "Displaying List of Applicants for Renewal of Government Recognition";
            $newBtnText = "<i class='fa fa-refresh'></i> Renew Recognition";
        }

        return view('newapplication/privateschoolnewapplication')
            ->with('userid', Auth::id())
            ->with('applicationTypeId', $applicationTypeId)
            ->with('applicationType', $applicationType)
            ->with('status', $snackbar)
            ->with('newBtnText', $newBtnText)
            ->with('gradeLevels', Controller::gradeLevels())
            ->with('categoriesshs', Controller::categoriesshs())
            ->with('applicationlists', self::newApplicationLists($applicationTypeId));
    }

    public function draftNewApplication(Request $request){
        $applicationTypeIdfk = $request->input('applicationType');
        $applicationTypeId = $applicationTypeIdfk;
        $userIdfk = Auth::id();
        $gradeLevelIdfkFrom = $request->input('gradeLevelFrom');
        $gradeLevelIdfkTo = $request->input('gradeLevelTo');
        $governmentPRNumber = "";
        $seriesYear = date("Y");
        $schoolYear = $request->input('sy');
        $applicationStatusIdfk = 1; // draft status
        $categoryIdfk = $request->input('category');
        $lastDateUpdated = date("Y-m-d H:i:s");
        $pre="";
        $message = "";
        $message .= "You have successfully drafted an application.<br>";
        if($applicationTypeId==1){
            $pre = "PN"; // Permit New
            $message .= "<br>Type: New Permit";
        }
        else if($applicationTypeId==2){
            $pre = "PR"; // Permit Renewal
            $message .= "<br>Type: Renewal of Permit";
        }
        else if($applicationTypeId==3){
            $pre = "RN"; // Recognition New
            $message .= "<br>Type: Recognition";
        }
        else if($applicationTypeId==4){
            $pre = "RR"; // Recognition Renewal
            $message .= "<br>Type: Renewal of Recognition";
        }
        $controlNumber = $pre . "-". Auth::user()->schoolId . '-' . $schoolYear . '-' . ($gradeLevelIdfkFrom-1) . '-' . ($gradeLevelIdfkTo-1);

        $conn1 = DB::connection('mysql')->getPdo();
        $stmt = $conn1 -> prepare("
                            INSERT INTO `permitrecognitionstatus`(
                                `prStatusId`,
                                `applicationTypeIdfk`,
                                `schoolUserIdfk`,
                                `gradeLevelIdfkFrom`,
                                `gradeLevelIdfkTo`,
                                `governmentPRNumber`,
                                `seriesYear`,
                                `schoolYear`,
                                `applicationStatusIdfk`,
                                `categoryIdfk`,
                                `lastDateUpdated`,
                                `controlNumber`
                            )
                            VALUES(
                                null,
                                :applicationTypeIdfk,
                                :schoolUserIdfk,
                                :gradeLevelIdfkFrom,
                                :gradeLevelIdfkTo,
                                :governmentPRNumber,
                                :seriesYear,
                                :schoolYear,
                                :applicationStatusIdfk,
                                :categoryIdfk,
                                :lastDateUpdated,
                                :controlNumber
                            )
            ");
        $stmt->bindParam(':applicationTypeIdfk', $applicationTypeIdfk);
        $stmt->bindParam('schoolUserIdfk', $userIdfk);
        $stmt->bindParam('gradeLevelIdfkFrom', $gradeLevelIdfkFrom);
        $stmt->bindParam('gradeLevelIdfkTo', $gradeLevelIdfkTo);
        $stmt->bindParam('governmentPRNumber', $governmentPRNumber);
        $stmt->bindParam('seriesYear', $seriesYear);
        $stmt->bindParam('schoolYear', $schoolYear);
        $stmt->bindParam('applicationStatusIdfk', $applicationStatusIdfk);
        $stmt->bindParam('categoryIdfk', $categoryIdfk);
        $stmt->bindParam('lastDateUpdated', $lastDateUpdated);
        $stmt->bindParam('controlNumber', $controlNumber);

        $stmt->execute();

            $stmt = $conn1->prepare("SELECT LAST_INSERT_ID()");
            $stmt->execute();

            $prStatusId = $stmt->fetchColumn();
            $stageIdfk = 1;
            $stageStatusIdfk = 1;
            $updatedBy = $userIdfk;
            
            $remarks = "Draft Application";
            $docReviewFileIdfk = 0;

            $stmt = $conn1 -> prepare("
                                INSERT INTO `stagestimeline`(
                                    `stageTimelineId`,
                                    `prStatusIdfk`,
                                    `stageIdfk`,
                                    `stageStatusIdfk`,
                                    `updatedBy`,
                                    `lastDateUpdated`,
                                    `remarks`,
                                    `docReviewFileIdfk`
                                )
                                VALUES(
                                    null,
                                    :prStatusId,
                                    :stageIdfk,
                                    :stageStatusIdfk,
                                    :updatedBy,
                                    :lastDateUpdated,
                                    :remarks,
                                    :docReviewFileIdfk
                                )
                                ");
            $stmt->bindParam(':prStatusId', $prStatusId);
            $stmt->bindParam('stageIdfk', $stageIdfk);
            $stmt->bindParam('stageStatusIdfk', $stageStatusIdfk);
            $stmt->bindParam('updatedBy', $updatedBy);
            $stmt->bindParam('lastDateUpdated', $lastDateUpdated);
            $stmt->bindParam('remarks', $remarks);
            $stmt->bindParam('docReviewFileIdfk', $docReviewFileIdfk);
            $stmt->execute();

            $status = "Draft New Application";

        $email = Auth::user()->email;

        //send email to school user only
        $greeting = "Hello!";
        $message .= "<br><strong>School ID:</strong> " . Auth::user()->schoolId;
        $message .= "<br><strong>School Name:</strong> " . Auth::user()->name;
        $message .= "<br><strong>School Year:</strong> " . $schoolYear;
        $message .= "<br><strong>Grade Levels Applying for:</strong> From " .  Controller::gradeLevelsDescription($gradeLevelIdfkFrom) . " to " .  Controller::gradeLevelsDescription($gradeLevelIdfkTo);
        $message .= "<br><strong>Date Created: " . $lastDateUpdated;

        $data = array(
            'subject'   =>  "GPRPS - Application Monitoring and Management (Draft Application)",
            'greeting'  =>  $greeting,
            'message'   =>  $message
        );
        Mail::to($email)->send(new SendMail($data));

        return redirect()->route('viewapplication', [$prStatusId, $applicationTypeId, $status]);
    }

    public function editDraftNewApplication(Request $request){
        $applicationTypeId = $request->input('applicationTypeEdit');
        $prStatusId = $request->input('prStatusIdEdit');
        $userIdfk = Auth::id();
        $gradeLevelIdfkFrom = $request->input('gradeLevelFrom');
        $gradeLevelIdfkTo = $request->input('gradeLevelTo');
        $schoolYear = $request->input('sy');
        $categoryIdfk = $request->input('category');
        $lastDateUpdated = date("Y-m-d H:i:s");
        $pre="";
        if($applicationTypeId==1){
            $pre = "PN"; // Permit New
        }
        else if($applicationTypeId==2){
            $pre = "PR"; // Permit Renewal
        }
        else if($applicationTypeId==3){
            $pre = "RN"; // Recognition New
        }
        else if($applicationTypeId==3){
            $pre = "RR"; // Recognition Renewal
        }
        $controlNumber = $pre . "-". Auth::user()->schoolId . '-' . $schoolYear . '-' . ($gradeLevelIdfkFrom-1) . '-' . ($gradeLevelIdfkTo-1);

        $conn1 = DB::connection('mysql')->getPdo();
        $stmt = $conn1 -> prepare("
                                UPDATE
                                    `permitrecognitionstatus`
                                SET
                                    `gradeLevelIdfkFrom` = :gradeLevelIdfkFrom,
                                    `gradeLevelIdfkTo` = :gradeLevelIdfkTo,
                                    `schoolYear` = :schoolYear,
                                    `categoryIdfk` = :categoryIdfk,
                                    `lastDateUpdated` = :lastDateUpdated,
                                    `controlNumber` = :controlNumber
                                WHERE
                                    `prStatusId` = :prStatusId
            ");

        $stmt->bindParam('gradeLevelIdfkFrom', $gradeLevelIdfkFrom);
        $stmt->bindParam('gradeLevelIdfkTo', $gradeLevelIdfkTo);
        $stmt->bindParam('schoolYear', $schoolYear);
        $stmt->bindParam('categoryIdfk', $categoryIdfk);
        $stmt->bindParam('lastDateUpdated', $lastDateUpdated);
        $stmt->bindParam('prStatusId', $prStatusId);
        $stmt->bindParam('controlNumber', $controlNumber);

        $stmt->execute();

            $stageIdfk = 1;
            $stageStatusIdfk = 1;
            $updatedBy = $userIdfk;
            
            $remarks = "Editted Application";
            $docReviewFileIdfk = 0;

            $stmt = $conn1 -> prepare("
                                INSERT INTO `stagestimeline`(
                                    `stageTimelineId`,
                                    `prStatusIdfk`,
                                    `stageIdfk`,
                                    `stageStatusIdfk`,
                                    `updatedBy`,
                                    `lastDateUpdated`,
                                    `remarks`,
                                    `docReviewFileIdfk`
                                )
                                VALUES(
                                    null,
                                    :prStatusId,
                                    :stageIdfk,
                                    :stageStatusIdfk,
                                    :updatedBy,
                                    :lastDateUpdated,
                                    :remarks,
                                    :docReviewFileIdfk
                                )
                                ");
            $stmt->bindParam(':prStatusId', $prStatusId);
            $stmt->bindParam('stageIdfk', $stageIdfk);
            $stmt->bindParam('stageStatusIdfk', $stageStatusIdfk);
            $stmt->bindParam('updatedBy', $updatedBy);
            $stmt->bindParam('lastDateUpdated', $lastDateUpdated);
            $stmt->bindParam('remarks', $remarks);
            $stmt->bindParam('docReviewFileIdfk', $docReviewFileIdfk);
            $stmt->execute();

            $status = "Application has been updated.";
        return redirect()->route('viewapplication', [$prStatusId, $applicationTypeId, $status]);
    }


    public function newApplicationLists($applicationTypeId){
        $conn1 = DB::connection('mysql')->getPdo();
        $schoolUserIdfk = Auth::id();
        $stmt = $conn1 -> prepare("
                                SELECT prStatusId,
                                       applicationTypeIdfk,
                                       schoolUserIdfk,
                                       gradeLevelIdfkFrom,
                                       gradeLevelIdfkTo,
                                       governmentPRNumber,
                                       seriesYear,
                                       schoolYear,
                                       applicationStatusIdfk,
                                       applicationstatus.description AS applicationStatus,
                                       applicationstatus.valueNow,
                                       applicationstatus.progressClass,
                                       categoryIdfk,
                                       permitrecognitionstatus.lastDateUpdated,
                                       categoriesshs.category,
                                       tblFrom.description AS applicationFrom,
                                       tblTo.description AS applicationTo
                                FROM permitrecognitionstatus
                                     LEFT JOIN categoriesshs
                                        ON permitrecognitionstatus.categoryIdfk = categoriesshs.categoryId
                                     LEFT JOIN gradelevels AS tblFrom
                                        ON permitrecognitionstatus.gradeLevelIdfkFrom = tblFrom.gradeLevelId
                                     LEFT JOIN gradelevels AS tblTo
                                        ON permitrecognitionstatus.gradeLevelIdfkTo = tblTo.gradeLevelId
                                     LEFT JOIN applicationstatus
                                        ON permitrecognitionstatus.applicationStatusIdfk =
                                           applicationstatus.applicationStatusId
                                WHERE permitrecognitionstatus.schoolUserIdfk = :schoolUserIdfk
                                AND applicationTypeIdfk = :applicationTypeIdfk
                                AND applicationStatusIdfk <> 9
                            ");
        $stmt->bindParam(":schoolUserIdfk",$schoolUserIdfk);
        $stmt->bindParam(":applicationTypeIdfk",$applicationTypeId);
        $stmt->execute();
        $result=$stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    public function viewapplication($prStatusId, $applicationTypeId, $status){
        session(['prStatusId' => $prStatusId]);
        session(['applicationTypeId' => $applicationTypeId]);
        $appInfo = Controller::applicationInfo($prStatusId);
        return view('newapplication/viewapplication')
            ->with('prStatusId', $prStatusId)
            ->with('applicationTypeId', $applicationTypeId)
            ->with('gradeLevels', Controller::gradeLevels())
            ->with('newApplicationRequirements', Controller::newApplicationRequirements($prStatusId))
            ->with('applicationInfo', Controller::applicationInfo($prStatusId))
            ->with('latestStagesTimeLineInfo', Controller::latestStagesTimeLineInfo($prStatusId))
            ->with('getDivisionName', Controller::getDivisionName($appInfo["schoolsDivisionIdfk"]))
            ->with('categoriesshs', Controller::categoriesshs())
            ->with('stagesTimeLine', Controller::stagesTimeLine($prStatusId))
            ->with('status', $status)
            ->render();
    }


    public function getFixedInfo($prStatusId){
      $applicationInfo = Controller::applicationInfo($prStatusId);
      $fixedInfo = array(
        'schoolYear' => $applicationInfo["schoolYear"],
        'schoolId' => $applicationInfo["schoolId"],
        'schoolName' => $applicationInfo["schoolName"],
        'schoolEmailAddress' => $applicationInfo["schoolEmailAddress"],
        'gradeFrom' => $applicationInfo["gradeLevelIdfkFrom"],
        'gradeTo' => $applicationInfo["gradeLevelIdfkTo"]
      );

      return $fixedInfo;
    }

    public function generateEmail($subject, $greeting, $emailFixedData, $firstParagraph, $lastDateUpdated)
    {
        $message = "";
        $message .= $firstParagraph;
        $message .= "<br><strong>School ID:</strong> " . $emailFixedData["schoolId"];
        $message .= "<br><strong>School Name:</strong> " . $emailFixedData["schoolName"];
        $message .= "<br><strong>School Year:</strong> " . $emailFixedData["schoolYear"];
        $message .= "<br><strong>Grade Levels Applying for:</strong> From " .  Controller::gradeLevelsDescription($emailFixedData["gradeFrom"]) . " to " .  Controller::gradeLevelsDescription($emailFixedData["gradeTo"]);
        $message .= "<br><strong>Date:</strong> " . $lastDateUpdated;

      $generatedEmail = array(
          'subject'   =>  $subject,
          'greeting'  =>  $greeting,
          'message'   =>  $message
      );
      return $generatedEmail;
    }

    public function emailRecipients($getSdoPrivateSchoolFocal, $generatedEmail){
      $status = "";
      $roEmail = "ictu.depedcar@gmail.com";
      Mail::to($roEmail)->send(new SendMail($generatedEmail)); // send to RO (qad.depedcar@gmail.com)
      if(isset($getSdoPrivateSchoolFocal)){
          Mail::to($getSdoPrivateSchoolFocal)->send(new SendMail($generatedEmail)); // send to SDO Focal Person
      }
      else{
          $status .= ". No email sent to SDO";
          //return $status;
      }
    }

    public function emailRecipientSchool($schoolEmailAddress, $generatedEmail){
      Mail::to($schoolEmailAddress)->send(new SendMail($generatedEmail)); // send to school
    }

    public function submitapplication(Request $request){
        $applicationStatusId = 3;
        $prStatusId = $request->input('prStatusId');
        $applicationTypeId = $request->input('applicationType');
        $lastDateUpdated = date("Y-m-d H:i:s");
        $applicationInfo = Controller::applicationInfo($prStatusId);
        $schoolYear = $applicationInfo["schoolYear"];
        $gradeLevelIdfkFrom = $applicationInfo["gradeLevelIdfkFrom"];
        $gradeLevelIdfkTo = $applicationInfo["gradeLevelIdfkTo"];

        $conn1 = DB::connection('mysql')->getPdo();
        $sql = $conn1->prepare("
                        UPDATE
                            `permitrecognitionstatus`
                        SET
                            `applicationStatusIdfk` = :applicationStatusIdfk,
                            `lastDateUpdated` = :lastDateUpdated
                        WHERE
                            `prStatusId` = :prStatusId
                    ");
        $sql->bindParam(':prStatusId', $prStatusId);
        $sql->bindParam(':applicationStatusIdfk', $applicationStatusId);
        $sql->bindParam(':lastDateUpdated', $lastDateUpdated);
        $sql->execute();

            $stageIdfk = 1;
            $stageStatusIdfk = 3;
            $updatedBy = Auth::id();
            
            $remarks = "Submitted Application";
            $docReviewFileIdfk = 0;

            $stmt = $conn1 -> prepare("
                                INSERT INTO `stagestimeline`(
                                    `stageTimelineId`,
                                    `prStatusIdfk`,
                                    `stageIdfk`,
                                    `stageStatusIdfk`,
                                    `updatedBy`,
                                    `lastDateUpdated`,
                                    `remarks`,
                                    `docReviewFileIdfk`
                                )
                                VALUES(
                                    null,
                                    :prStatusId,
                                    :stageIdfk,
                                    :stageStatusIdfk,
                                    :updatedBy,
                                    :lastDateUpdated,
                                    :remarks,
                                    :docReviewFileIdfk
                                )
                                ");
            $stmt->bindParam(':prStatusId', $prStatusId);
            $stmt->bindParam('stageIdfk', $stageIdfk);
            $stmt->bindParam('stageStatusIdfk', $stageStatusIdfk);
            $stmt->bindParam('updatedBy', $updatedBy);
            $stmt->bindParam('lastDateUpdated', $lastDateUpdated);
            $stmt->bindParam('remarks', $remarks);
            $stmt->bindParam('docReviewFileIdfk', $docReviewFileIdfk);
            $stmt->execute();

        $status = "Submitted Application";

        //$email = Auth::user()->email;
        $type="";
        if($applicationTypeId==1){
            $type = "New Application of Permit";
        }
        elseif($applicationTypeId==2){
            $type = "Renewal of Permit";
        }
        elseif($applicationTypeId==3){
            $type = "New Application of Recognition";
        }
        elseif($applicationTypeId==4){
            $type = "Renewal of Recognition";
        }

      //email generation
          $subject = "GPRPS - Application Monitoring and Management (Submitted Application)";
          $greeting = "Hello!";
          $firstParagraph = "You have successfully submitted your application (". $type .").<br>";
          $fixedInfo = self::getFixedInfo($prStatusId);
          $generatedEmail = self::generateEmail($subject, $greeting, $fixedInfo, $firstParagraph, $lastDateUpdated);

        //recipient school
          $email = Auth::user()->email;//email of school
          $schoolEmailAddress = $fixedInfo["schoolEmailAddress"];
          $getSdoPrivateSchoolFocal = Controller::getSdoPrivateSchoolFocal(Auth::user()->schoolsDivisionIdfk);
          self::emailRecipientSchool($schoolEmailAddress, $generatedEmail); 

        // for sdo and ro
          $firstParagraph = "The following private school have submitted an application (". $type .").<br>";
          $fixedInfo = self::getFixedInfo($prStatusId);
          $generatedEmail = self::generateEmail($subject, $greeting, $fixedInfo, $firstParagraph, $lastDateUpdated);

        //recipients sdo and ro
          $getSdoPrivateSchoolFocal = Controller::getSdoPrivateSchoolFocal(Auth::user()->schoolsDivisionIdfk);
          self::emailRecipients($getSdoPrivateSchoolFocal, $generatedEmail);
        

        return redirect()->route('viewapplication', [$prStatusId, $applicationTypeId, $status]);
    }

    public function submitcompletion(Request $request){
        $prStatusId = $request->input('prStatusId');
        $latestStagesTimeLineInfo = Controller::latestStagesTimeLineInfo($prStatusId);
        
        if($latestStagesTimeLineInfo["stageStatusIdfk"]==5){ // from SDO review
            $applicationStatusId = 3;
            $stageIdfk = 1;
            $stageStatusIdfk = 24;
        }
        elseif($latestStagesTimeLineInfo["stageStatusIdfk"]==14){ // from RO review
            $applicationStatusId = 4;
            $stageIdfk = 4;
            $stageStatusIdfk = 13;
        }

        $applicationTypeId = $request->input('applicationType');
        $lastDateUpdated = date("Y-m-d H:i:s");
        $applicationInfo = Controller::applicationInfo($prStatusId);
        
        $schoolYear = $applicationInfo["schoolYear"];
        $gradeLevelIdfkFrom = $applicationInfo["gradeLevelIdfkFrom"];
        $gradeLevelIdfkTo = $applicationInfo["gradeLevelIdfkTo"];



        Controller::updateApplicationStatus($prStatusId, $applicationStatusId, $lastDateUpdated);

            $updatedBy = Auth::id();
            $remarks = "Submitted Completion";
            $docReviewFileIdfk = 0;
            Controller::updateStagesTimeline($prStatusId, $stageIdfk, $stageStatusIdfk, $updatedBy, $lastDateUpdated, $remarks, $docReviewFileIdfk);

        $status = "Submitted Completion";

      //email generation
          $subject = "GPRPS - Application Monitoring and Management (Submitted Completion)";
          $greeting = "Hello!";
          $firstParagraph = "You have successfully submitted a completion of your application.<br>";
          $fixedInfo = self::getFixedInfo($prStatusId);
          $generatedEmail = self::generateEmail($subject, $greeting, $fixedInfo, $firstParagraph, $lastDateUpdated);

        //recipient school
          $email = Auth::user()->email;//email of school
          $schoolEmailAddress = $fixedInfo["schoolEmailAddress"];
          $getSdoPrivateSchoolFocal = Controller::getSdoPrivateSchoolFocal(Auth::user()->schoolsDivisionIdfk);
          self::emailRecipientSchool($schoolEmailAddress, $generatedEmail); 

        // for sdo and ro
          $firstParagraph = "The following private school have submitted completion requirements for an application.<br>";
          $fixedInfo = self::getFixedInfo($prStatusId);
          $generatedEmail = self::generateEmail($subject, $greeting, $fixedInfo, $firstParagraph, $lastDateUpdated);

        //recipients sdo and ro
          $getSdoPrivateSchoolFocal = Controller::getSdoPrivateSchoolFocal(Auth::user()->schoolsDivisionIdfk);
          self::emailRecipients($getSdoPrivateSchoolFocal, $generatedEmail);
        

        return redirect()->route('viewapplication', [$prStatusId, $applicationTypeId, $status]);
    }

    public function cancelapplication(Request $request){
        $applicationStatusId = 2;
        $prStatusId = $request->input('prStatusId');
        $applicationTypeId = $request->input('applicationType');
        $lastDateUpdated = date("Y-m-d H:i:s");
        $applicationInfo = Controller::applicationInfo($prStatusId);
        $schoolYear = $applicationInfo["schoolYear"];
        $gradeLevelIdfkFrom = $applicationInfo["gradeLevelIdfkFrom"];
        $gradeLevelIdfkTo = $applicationInfo["gradeLevelIdfkTo"];

        $conn1 = DB::connection('mysql')->getPdo();
        $sql = $conn1->prepare("
                        UPDATE
                            `permitrecognitionstatus`
                        SET
                            `applicationStatusIdfk` = :applicationStatusIdfk,
                            `lastDateUpdated` = :lastDateUpdated
                        WHERE
                            `prStatusId` = :prStatusId
                    ");
        $sql->bindParam(':prStatusId', $prStatusId);
        $sql->bindParam(':applicationStatusIdfk', $applicationStatusId);
        $sql->bindParam(':lastDateUpdated', $lastDateUpdated);
        $sql->execute();

            $stageIdfk = 1;
            $stageStatusIdfk = 2;
            $updatedBy = Auth::id();
            
            $remarks = "Cancelled Application";
            $docReviewFileIdfk = 0;

            $stmt = $conn1 -> prepare("
                                INSERT INTO `stagestimeline`(
                                    `stageTimelineId`,
                                    `prStatusIdfk`,
                                    `stageIdfk`,
                                    `stageStatusIdfk`,
                                    `updatedBy`,
                                    `lastDateUpdated`,
                                    `remarks`,
                                    `docReviewFileIdfk`
                                )
                                VALUES(
                                    null,
                                    :prStatusId,
                                    :stageIdfk,
                                    :stageStatusIdfk,
                                    :updatedBy,
                                    :lastDateUpdated,
                                    :remarks,
                                    :docReviewFileIdfk
                                )
                                ");
            $stmt->bindParam(':prStatusId', $prStatusId);
            $stmt->bindParam('stageIdfk', $stageIdfk);
            $stmt->bindParam('stageStatusIdfk', $stageStatusIdfk);
            $stmt->bindParam('updatedBy', $updatedBy);
            $stmt->bindParam('lastDateUpdated', $lastDateUpdated);
            $stmt->bindParam('remarks', $remarks);
            $stmt->bindParam('docReviewFileIdfk', $docReviewFileIdfk);
            $stmt->execute();

        $status = "Application has been cancelled";

      //email generation
          $subject = "GPRPS - Application Monitoring and Management (Cancelled Application)";
          $greeting = "Hello!";
          $firstParagraph = "You have successfully Cancelled your application.<br>";
          $fixedInfo = self::getFixedInfo($prStatusId);
          $generatedEmail = self::generateEmail($subject, $greeting, $fixedInfo, $firstParagraph, $lastDateUpdated);

        //recipient school
          $email = Auth::user()->email;//email of school
          $schoolEmailAddress = $fixedInfo["schoolEmailAddress"];
          $getSdoPrivateSchoolFocal = Controller::getSdoPrivateSchoolFocal(Auth::user()->schoolsDivisionIdfk);
          self::emailRecipientSchool($schoolEmailAddress, $generatedEmail); 

        // for sdo and ro
          $firstParagraph = "The following private school have Cancelled an application.<br>";
          $fixedInfo = self::getFixedInfo($prStatusId);
          $generatedEmail = self::generateEmail($subject, $greeting, $fixedInfo, $firstParagraph, $lastDateUpdated);

        //recipients sdo and ro
          $getSdoPrivateSchoolFocal = Controller::getSdoPrivateSchoolFocal(Auth::user()->schoolsDivisionIdfk);
          self::emailRecipients($getSdoPrivateSchoolFocal, $generatedEmail);
        
        return redirect()->route('viewapplication', [$prStatusId, $applicationTypeId, $status]);
    }
}
