<?php

namespace App\Http\Controllers\ManageUsers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Services\PayUService\Exception;
use App\Http\Controllers\PDO;
use App\Mail\SendMail;

class ManageUsers extends Controller
{
	private static $status = "";
	//prevents accessing the page without logging in
    public function __construct()
    {
        $this->middleware('auth');
    }

    //url to be called
    public function index(Request $request)
    {
    	$stat = session()->get('stat');
    	if($stat==''){
    		$stat = "Displaying Users";
    	}
    	return view('manageusers/manageusers')
    		->with('users', self::userLists())
    		->with('usersSchool', self::userListsPerOffice(3))
    		->with('usersSdo', self::userListsPerOffice(2))
    		->with('usersRo', self::userListsPerOffice(1))
    		->with('title', 'Manage Users')
    		->with('status', $stat)
    		->with('schoolsDivisions', Controller::getSchoolsDivisions());
    }

    public function userLists(){
    	$conn1 = DB::connection('mysql')->getPdo();
    	$stmt = $conn1->prepare("
							SELECT
							    `id`,
							    `employeeID`,
							    `firstName`,
							    `middleName`,
							    `lastName`,
							    `schoolId`,
							    `name`,
							    `previousName`,
							    `email`,
							    `email_verified_at`,
							    `password`,
							    `region`,
							    `officeIDfk`,
							    `schoolsDivisionIdfk`,
							    `district`,
							    `legislativeDistrict`,
							    `municipality`,
							    `telNumber`,
							    `mobileNumber`,
							    `faxNo`,
							    `schoolHead`,
							    `designation`,
							    `dateEstablished`,
							    `isActive`,
							    `firstTimeLogin`,
							    `accountType`,
							    `remember_token`,
							    DATE_FORMAT(`created_at`,'%m/%e/%Y') as created_at,
							    `updated_at`,
							    SDO.schoolsDivision
							FROM
							    `users`
                         	LEFT JOIN depedcarprofilesdb.tbl_schoolsdivisionoffices AS SDO
                            	ON users.schoolsDivisionIdfk = SDO.schoolsDivisionID
                            ORDER BY `created_at` DESC

    				");
    	$stmt -> execute();
    	$result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    	return $result;
    }

    public function userListsPerOffice($officeIDfk){
    	if(Auth::user()->officeIDfk==1){
	    	$conn1 = DB::connection('mysql')->getPdo();
	    	$stmt = $conn1->prepare("
								SELECT
								    `id`,
								    `employeeID`,
								    `firstName`,
								    `middleName`,
								    `lastName`,
								    `schoolId`,
								    `name`,
								    `previousName`,
								    `email`,
								    `email_verified_at`,
								    `password`,
								    `region`,
								    `officeIDfk`,
								    `schoolsDivisionIdfk`,
								    `district`,
								    `legislativeDistrict`,
								    `municipality`,
								    `telNumber`,
								    `mobileNumber`,
								    `faxNo`,
								    `schoolHead`,
								    `designation`,
								    `dateEstablished`,
								    `isActive`,
								    `firstTimeLogin`,
								    `accountType`,
								    `remember_token`,
								    DATE_FORMAT(`created_at`,'%m/%e/%Y') as created_at,
								    `updated_at`,
								    SDO.schoolsDivisionID,
								    SDO.schoolsDivision
								FROM
								    `users`
	                         	LEFT JOIN depedcarprofilesdb.tbl_schoolsdivisionoffices AS SDO
	                            	ON users.schoolsDivisionIdfk = SDO.schoolsDivisionID
	                            WHERE users.officeIDfk =:officeIDfk
	                            ORDER BY `created_at` DESC

	    				");
	    	$stmt -> bindParam("officeIDfk", $officeIDfk);
    	}
    	else{
	    	$conn1 = DB::connection('mysql')->getPdo();
	    	$stmt = $conn1->prepare("
								SELECT
								    `id`,
								    `employeeID`,
								    `firstName`,
								    `middleName`,
								    `lastName`,
								    `schoolId`,
								    `name`,
								    `previousName`,
								    `email`,
								    `email_verified_at`,
								    `password`,
								    `region`,
								    `officeIDfk`,
								    `schoolsDivisionIdfk`,
								    `district`,
								    `legislativeDistrict`,
								    `municipality`,
								    `telNumber`,
								    `mobileNumber`,
								    `faxNo`,
								    `schoolHead`,
								    `designation`,
								    `dateEstablished`,
								    `isActive`,
								    `firstTimeLogin`,
								    `accountType`,
								    `remember_token`,
								    DATE_FORMAT(`created_at`,'%m/%e/%Y') as created_at,
								    `updated_at`,
								    SDO.schoolsDivisionID,
								    SDO.schoolsDivision
								FROM
								    `users`
	                         	LEFT JOIN depedcarprofilesdb.tbl_schoolsdivisionoffices AS SDO
	                            	ON users.schoolsDivisionIdfk = SDO.schoolsDivisionID
	                            WHERE users.officeIDfk =:officeIDfk AND SDO.schoolsDivisionID=:schoolsDivisionID
	                            ORDER BY `created_at` DESC

	    				");
	    	$schoolsDivisionID = Auth::user()->schoolsDivisionIdfk;
	    	$stmt -> bindParam("officeIDfk", $officeIDfk);
	    	$stmt -> bindParam("schoolsDivisionID", $schoolsDivisionID);
    	}
    	$stmt -> execute();
    	$result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    	return $result;
    }

    public function registration(Request $request){
    	$stat = session()->get('stat');
    	if($stat==''){
    		$stat = "Displaying Registration Form";
    	}
    	
        return view('manageusers/registration')
    		->with('title', 'Register New User')
    		->with('status', $stat)
    		->with('schoolsDivision', Controller::getDivisionName(Auth::user()->schoolsDivisionIdfk))
    		->with('schoolsDivisions', Controller::getSchoolsDivisions());
    }

    public function registeruser(Request $request){
    	$created_at = date("Y-m-d H:i:s");
    	$officeIDfk = $request->input('officeId');
    	if($officeIDfk==3){
	    	$schoolId =  $request->input('schoolId');
	    	$name =  $request->input('name');
    		$schoolsDivisionIdfk = $request->input('schoolsDivisionId');
	    	$email = $request->input('email');
	    	$passwordRaw = $request->input('password');
	    	$password = Hash::make($request->input('password'));

	        $conn1 = DB::connection('mysql')->getPdo();
	        $sql = $conn1 -> prepare("SELECT * FROM users WHERE email=:email OR schoolId=:schoolId");
	        $sql->bindParam(':email', $email);
	        $sql->bindParam(':schoolId', $schoolId);
	        $sql->execute();
			$result = $sql->fetchAll();
			$count = count($result);
			if($count>0){
				self::$status = "Email or School ID Already Exist";
				return redirect()->route('registration')->with(['stat'=>self::$status])->withInput();
			}
			else{
		        $stmt = $conn1 -> prepare("
									INSERT INTO `users`(
									    `id`,
									    `schoolId`,
									    `name`,
									    `email`,
									    `password`,
									    `officeIDfk`,
									    `schoolsDivisionIdfk`,
									    `created_at`,
									    `updated_at`
									)
									VALUES(
									    null,
									    :schoolId,
									    :name,
									    :email,
									    :password,
									    :officeIDfk,
									    :schoolsDivisionIdfk,
									    :created_at,
									    :updated_at
									)
		        	");
		        $stmt->bindParam(':schoolId', $schoolId);
		        $stmt->bindParam(':name', $name);
		        $stmt->bindParam(':email', $email);
		        $stmt->bindParam(':password', $password);
		        $stmt->bindParam(':officeIDfk', $officeIDfk);
		        $stmt->bindParam(':schoolsDivisionIdfk', $schoolsDivisionIdfk);
		        $stmt->bindParam(':created_at', $created_at);
		        $stmt->bindParam(':updated_at', $created_at);
				$stmt->execute();
				self::$status = "Registration Successful. An Email was sent to the New User.";

		    	//send email to new user
		    	$greeting = "Welcome!";
		    	$message = "";
			    $message .= "An account has been created for your school to access the Government Permit and Recognition Online Application and Monitoring System of DepEd-CAR.<br>";
			    $message .= "<br>School ID: " . $schoolId;
			    $message .= "<br>School Name: " . $name;
			    $message .= "<br>Email Address: " . $email;
			    $message .= "<br>Temporary Password: " . $passwordRaw;

			}
    	}
    	else{
	    	$employeeID =  $request->input('employeeID');
	    	$firstName =  $request->input('firstName');
    		$middleName = $request->input('middleName');
	    	$lastName = $request->input('lastName');
	    	$schoolsDivisionIdfk = $request->input('schoolsDivisionId_ro_sdo');
	    	$email = $request->input('email_ro_sdo');
	    	$passwordRaw = $request->input('password');
	    	$password = Hash::make($request->input('password'));

	        $conn1 = DB::connection('mysql')->getPdo();
	        $sql = $conn1 -> prepare("SELECT * FROM users WHERE email=:email OR employeeID=:employeeID");
	        $sql->bindParam(':email', $email);
	        $sql->bindParam(':employeeID', $employeeID);
	        $sql->execute();
			$result = $sql->fetchAll();
			$count = count($result);
			if($count>0){
				self::$status = "Email or Employee ID Already Exist";
				return redirect()->route('registration')->with(['stat'=>self::$status])->withInput();
			}
			else{
		        $stmt = $conn1 -> prepare("
									INSERT INTO `users`(
									    `id`,
									    `employeeID`,
									    `firstName`,
									    `middleName`,
									    `lastName`,
									    `email`,
									    `password`,
									    `officeIDfk`,
									    `schoolsDivisionIdfk`,
									    `created_at`,
									    `updated_at`
									)
									VALUES(
									    null,
									    :employeeID,
									    :firstName,
									    :middleName,
									    :lastName,
									    :email,
									    :password,
									    :officeIDfk,
									    :schoolsDivisionIdfk,
									    :created_at,
									    :updated_at
									)
		        	");
		        $stmt->bindParam(':employeeID', $employeeID);
		        $stmt->bindParam(':firstName', $firstName);
		        $stmt->bindParam(':middleName', $middleName);
		        $stmt->bindParam(':lastName', $lastName);
		        $stmt->bindParam(':email', $email);
		        $stmt->bindParam(':password', $password);
		        $stmt->bindParam(':officeIDfk', $officeIDfk);
		        $stmt->bindParam(':schoolsDivisionIdfk', $schoolsDivisionIdfk);
		        $stmt->bindParam(':created_at', $created_at);
		        $stmt->bindParam(':updated_at', $created_at);
				$stmt->execute();
				self::$status = "Registration Successful. An Email was sent to the New User.";

		    	//send email to new user
		    	$greeting = "Welcome!";
		    	$message = "";
			    $message .= "An account has been created for you to access the Government Permit and Recognition Online Application and Monitoring System of DepEd-CAR.<br>";
			    $message .= "<br>Employee ID: " . $employeeID;
			    $message .= "<br>Name: " . $firstName . " " . $middleName . " " . $lastName;
			    $message .= "<br>Email Address: " . $email;
			    $message .= "<br>Temporary Password: " . $passwordRaw;


			}
    	}
		
		//send email
        $data = array(
        	'subject'	=>	"GPRPS - User Management (Account Registration)",
            'greeting'	=> 	$greeting,
            'message'	=>	$message
        );
     	Mail::to($email)->send(new SendMail($data));

     	//return to users
		return redirect()->route('manageusers')->with(['stat'=>self::$status])->withInput();		
    }

    public function viewprofile($userId){
    	$stat = session()->get('stat');
    	if($stat==''){
    		$stat = "View Profile";
    	}
    	
        return view('manageusers/viewprofile')
    		->with('title', 'View Profile')
    		->with('status', $stat)
    		->with('getprofile', self::getProfile($userId))
    		->with('schoolsDivisions', Controller::getSchoolsDivisions());
    }

    public function getProfile($userId){
        $conn1 = DB::connection('mysql')->getPdo();
        $stmt = $conn1 -> prepare("SELECT `id`,
									    `employeeID`,
									    `firstName`,
									    `middleName`,
									    `lastName`,
									    `sex`,
									    `birthDate`,
									    `schoolId`,
									    `name`,
									    `previousName`,
									    `email`,
									    `email_verified_at`,
									    `password`,
									    `schoolAddress`,
									    `region`,
									    `officeIDfk`,
									    `schoolsDivisionIdfk`,
									    `district`,
									    `legislativeDistrict`,
									    `municipality`,
									    `telNumber`,
									    `mobileNumber`,
									    `faxNo`,
									    `schoolHead`,
									    `designation`,
									    `dateEstablished`,
									    `isActive`,
									    `firstTimeLogin`,
									    `accountType`,
									    `inspectionCompositionIdfk`,
									    `remember_token`,
									    DATE_FORMAT(`created_at`,'%m/%e/%Y') as created_at,
									    `updated_at`,
									    SDO.schoolsDivisionID,
									    SDO.schoolsDivision,
									    inspectioncomposition.inspectionCompositionId,
									    inspectioncomposition.CM,
									    inspectioncomposition.description
							    FROM users 
							    LEFT JOIN depedcarprofilesdb.tbl_schoolsdivisionoffices AS SDO
                            		ON users.schoolsDivisionIdfk = SDO.schoolsDivisionID
                            	LEFT JOIN inspectioncomposition 
                            		ON users.inspectionCompositionIdfk = inspectioncomposition.inspectionCompositionId
                            	WHERE id = :id");
        $stmt->bindParam(":id", $userId);
        $stmt->execute();
        $result=$stmt->fetch(\PDO::FETCH_ASSOC);
        return $result;
    }

    public function editprofile($userId){
    	$stat = session()->get('stat');
    	if($stat==''){
    		$stat = "Edit User Profile";
    	}

    	$userProfile = self::getProfile($userId);
    	$officeIDfk = $userProfile["officeIDfk"];
    	$inspectionTeamId = 0;
    	if($officeIDfk==1){
    		$inspectionTeamId = 2;
    	}
    	else if($officeIDfk==2){
    		$inspectionTeamId = 1;
    	}

        return view('manageusers/editprofile')
    		->with('title', 'Edit User Profile')
    		->with('status', $stat)
    		->with('getprofile', self::getProfile($userId))
    		->with('schoolsDivisions', Controller::getSchoolsDivisions())
    		->with('inspectionComposition', Controller::getInspectionComposition($inspectionTeamId));
    }

    public function saveschoolprofile(Request $request){
    	$updated_at = date("Y-m-d H:i:s");

		$id =  $request->input('id');
    	$schoolId =  $request->input('schoolId');
    	$name =  $request->input('name');
    	$schoolAddress =  $request->input('schoolAddress');
		$schoolsDivisionIdfk = $request->input('schoolsDivisionId');
    	$email = $request->input('email');
    	$mobileNumber = $request->input('mobileNumber');
    	$schoolHead = $request->input('schoolHead');
    	$designation = $request->input('designation');
    	$dateEstablished = $request->input('dateEstablished');
    	

        $conn1 = DB::connection('mysql')->getPdo();
        $stmt = $conn1 -> prepare("
							UPDATE `users`
							SET `schoolId`=:schoolId,
							    `name`=:name,
							    `schoolAddress`=:schoolAddress,
							    `email`=:email,
							    `mobileNumber`=:mobileNumber,
							    `schoolHead`=:schoolHead,
							    `designation`=:designation,
							    `dateEstablished`=:dateEstablished,
							    `updated_at`=:updated_at
							WHERE `id`=:id
        	");
        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':schoolId', $schoolId);
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':schoolAddress', $schoolAddress);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':mobileNumber', $mobileNumber);
        $stmt->bindParam(':schoolHead', $schoolHead);
        $stmt->bindParam(':designation', $designation);
        $stmt->bindParam(':dateEstablished', $dateEstablished);
        $stmt->bindParam(':updated_at', $updated_at);
		$stmt->execute();
		self::$status = "Edit School Profile Successful";
		return redirect()->route('viewprofile',$id)->with(['stat'=>self::$status])->withInput();
    }

    public function savesdoroprofile(Request $request){
    	$updated_at = date("Y-m-d H:i:s");

		$id =  $request->input('id');
    	$employeeID =  $request->input('employeeID');
    	$firstName =  $request->input('firstName');
    	$middleName = $request->input('middleName');
    	$lastName = $request->input('lastName');
    	$email = $request->input('email');
    	$mobileNumber = $request->input('mobileNumber');
    	$sex = $request->input('sex');
    	$birthDate = $request->input('birthDate');
    	//$designation = $request->input('designation');
    	$inspectionCompositionId = $request->input('inspectionCompositionId');
		$schoolsDivisionIdfk = $request->input('schoolsDivisionId');

        $conn1 = DB::connection('mysql')->getPdo();
        $stmt = $conn1 -> prepare("
							UPDATE `users`
							SET `employeeID`=:employeeID,
							    `firstName`=:firstName,
							    `middleName`=:middleName,
							    `lastName`=:lastName,
							    `email`=:email,
							    `mobileNumber`=:mobileNumber,
							    `sex`=:sex,
							    `birthDate`=:birthDate,
							    `inspectionCompositionIdfk`=:inspectionCompositionId,
							    `schoolsDivisionIdfk`=:schoolsDivisionIdfk,
							    `updated_at`=:updated_at
							WHERE `id`=:id
        	");
        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':employeeID', $employeeID);
        $stmt->bindParam(':firstName', $firstName);
        $stmt->bindParam(':middleName', $middleName);
        $stmt->bindParam(':lastName', $lastName);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':mobileNumber', $mobileNumber);
        $stmt->bindParam(':sex', $sex);
        $stmt->bindParam(':birthDate', $birthDate);
        $stmt->bindParam(':inspectionCompositionId', $inspectionCompositionId);
        $stmt->bindParam(':schoolsDivisionIdfk', $schoolsDivisionIdfk);
        $stmt->bindParam(':updated_at', $updated_at);
		$stmt->execute();
		self::$status = "Edit Profile Successful";
		return redirect()->route('viewprofile',$id)->with(['stat'=>self::$status])->withInput();
    }

    public function changepassword($userId){
    	$stat = session()->get('stat');
    	if($stat==''){
    		$stat = "Change Password";
    	}
    	
        return view('manageusers/changepassword')
    		->with('title', 'Change Password')
    		->with('status', $stat)
    		->with('getprofile', self::getProfile($userId));
    }

    public function savenewpassword(Request $request){
    	$updated_at = date("Y-m-d H:i:s");

		$id =  $request->input('id');
		$passwordRaw = $request->input('password');
    	$password = Hash::make($request->input('password'));	
    	$userProfile = self::getProfile($id);
    	$email = $userProfile["email"];
        $conn1 = DB::connection('mysql')->getPdo();
        $stmt = $conn1 -> prepare("
							UPDATE `users`
							SET `password`=:password,
							    `updated_at`=:updated_at
							WHERE `id`=:id
        	");
        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':password', $password);
        $stmt->bindParam(':updated_at', $updated_at);
		$stmt->execute();
		self::$status = "Change Password Successful";

		//send email
    	$greeting = "Hello!";
    	$message = "";
	    $message .= "You or System Administrator have changed your password successfully. Please don't forget your password.<br>";
	    $message .= "<br>Email: " . $email;
	    $message .= "<br>New Password: " . $passwordRaw;

        $data = array(
        	'subject'	=>	"GPRPS - User Management (Password Change)",
            'greeting'	=> 	$greeting,
            'message'	=>	$message
        );
     	Mail::to($email)->send(new SendMail($data));

		return redirect()->route('viewprofile',$id)->with(['stat'=>self::$status])->withInput();
    }

    public function deactivateuser(Request $request){
    	$userid = $request->input('userid');
    	$userProfile = self::getProfile($userid);
    	$email = $userProfile["email"];
    	$isActive = $request->input('isActive');
    	if($isActive==1){
    		$isActive = 0;
    		self::$status = "User has been deactivated";
			//send email
	    	$greeting = "Hello!";
	    	$message = "";
		    $message .= "System Administrator have deactivated your account. Thank you for using GPRPS.<br>For any concerns, kindly contact your system administrator.";

	        $data = array(
	        	'subject'	=>	"GPRPS - User Management (Account Deactivation)",
	            'greeting'	=> 	$greeting,
	            'message'	=>	$message
	        );
	     	Mail::to($email)->send(new SendMail($data));
    	}
    	else{
    		$isActive = 1;
    		self::$status = "User has been activated";	
    	}

    	$conn1 = DB::connection('mysql')->getPdo();
    	$sql = $conn1 -> prepare("UPDATE users SET `isActive`=:isActive WHERE `id`=:userid");
    	$sql->bindParam(':isActive', $isActive);
    	$sql->bindParam(':userid', $userid);
    	$sql->execute();

    	return redirect()->route('manageusers')->with(['stat'=>self::$status]);
    }
}