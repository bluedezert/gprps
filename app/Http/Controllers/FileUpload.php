<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;
use App\Services\PayUService\Exception;
use App\Http\Controllers\PDO;

class FileUpload extends Controller
{
	//prevents accessing the page without logging in
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function uploadFile(Request $request){
    	//check first if there is already an uploaded file
    	/*$schoolId = $request->input("schoolId");
    	if(is_null($schoolId)){
    		$schoolId = Auth::id();
    	}*/

		$target_dir = public_path() . '/uploads/' .$request->input("divisionName") . '/' . $request->input("prStatusId");
		$basefile = basename($_FILES["fileUpload"]["name"]); // original file name
		//date_default_timezone_set("Asia/Manila");
		$dateTime = date("Y-m-d h-i-s"); // date uploaded
		//$baseFileTmp = Auth::id() . "-" . $request->input("applicationType") . "-" . $request->input("prStatusId") . "-" . $request->input("requirement") . "-" . $dateTime . "." . pathinfo($basefile,PATHINFO_EXTENSION); // does not overwrite existing

		$baseFileTmp = Auth::id() . "-" . $request->input("applicationType") . "-" . $request->input("prStatusId") . "-" . $request->input("mainReqid") . "." . pathinfo($basefile,PATHINFO_EXTENSION); // newfile name
		$target_file = $target_dir . "/" . $baseFileTmp;
		$uploadOk = 1;
		$fileType = pathinfo($target_file,PATHINFO_EXTENSION);
		$fileTmp = $_FILES["fileUpload"]["tmp_name"];

		$fileSize = $_FILES["fileUpload"]["size"];
		$documentTitle = $request->input('documentTitle');

		$applicationTypeId = $request->input("applicationType");
		$prStatusId = $request->input("prStatusId");
		$mainReqid = $request->input('mainReqid');
		$uploadedBy = Auth::id();

		if($fileType != "pdf"){
			$uploadStatus = 'Invalid File Format';
			$uploadOk = 0;
		}

		if($uploadOk == 0){
			$uploadStatus = 'NO file has been uploaded. File to be uploaded must be in .pdf format.';
	    	return redirect()->route('viewapplication', [$request->input("prStatusId"), $request->input("applicationType"), $uploadStatus]);
		}
		else{
			$conn1 = DB::connection('mysql')->getPdo();

			if(is_dir($target_dir)){
				if(file_exists($target_file)){
					unlink($target_file);
					//delete record in fileuploads and permitrecognitiondocreview
					$sql = "SELECT * FROM `fileuploads` 
							WHERE applicationTypeIdfk=:applicationTypeIdfk
								AND prStatusIdfk=:prStatusIdfk
								AND mainReqIdfk=:mainReqIdfk
								AND uploadedBy=:uploadedBy";
					$stmt=$conn1->prepare($sql);
					$stmt->bindParam(":applicationTypeIdfk", $applicationTypeId);
					$stmt->bindParam(":prStatusIdfk", $prStatusId);
					$stmt->bindParam(":mainReqIdfk", $mainReqid);
					$stmt->bindParam(":uploadedBy", $uploadedBy);
					$stmt->execute();
			        $result=$stmt->fetchAll(\PDO::FETCH_ASSOC);
			        foreach($result as $row){
			        	$fileId = $row["fileId"];
						$sql = "DELETE FROM `fileuploads` WHERE fileId=:fileId";
						$stmt=$conn1->prepare($sql);
						$stmt->bindParam(":fileId", $fileId);
						$stmt->execute();

						$sql = "DELETE FROM `permitrecognitiondocreview` WHERE fileIdfk=:fileId";
						$stmt=$conn1->prepare($sql);
						$stmt->bindParam(":fileId", $fileId);
						$stmt->execute();
			        }
				}
				move_uploaded_file($fileTmp, $target_file);
			}
			else{
				mkdir($target_dir);
				if(file_exists($target_file)){
					unlink($target_file);
					//delete record in fileuploads and permitrecognitiondocreview
					$sql = "SELECT * FROM `fileuploads` 
							WHERE applicationTypeIdfk=:applicationTypeIdfk
								AND prStatusIdfk=:prStatusIdfk
								AND mainReqIdfk=:mainReqIdfk
								AND uploadedBy=:uploadedBy";
					$stmt=$conn1->prepare($sql);
					$stmt->bindParam(":applicationTypeIdfk", $applicationTypeId);
					$stmt->bindParam(":prStatusIdfk", $prStatusId);
					$stmt->bindParam(":mainReqIdfk", $mainReqid);
					$stmt->bindParam(":uploadedBy", $uploadedBy);
					$stmt->execute();
			        $result=$stmt->fetchAll(\PDO::FETCH_ASSOC);
			        foreach($result as $row){
			        	$fileId = $row["fileId"];
						$sql = "DELETE FROM `fileuploads` WHERE fileId=:fileId";
						$stmt=$conn1->prepare($sql);
						$stmt->bindParam(":fileId", $fileId);
						$stmt->execute();

						$sql = "DELETE FROM `permitrecognitiondocreview` WHERE fileIdfk=:fileId";
						$stmt=$conn1->prepare($sql);
						$stmt->bindParam(":fileId", $fileId);
						$stmt->execute();
			        }
				}
				move_uploaded_file($fileTmp, $target_file);
			}
			$uploadStatus = 'File has been uploaded';

			$stmt = $conn1->prepare("
								INSERT INTO `fileuploads`(
								    `fileId`,
								    `applicationTypeIdfk`,
								    `prStatusIdfk`,
								    `mainReqIdfk`,
								    `originalFileName`,
								    `documentTitle`,
								    `newFileName`,
								    `fileSize`,
								    `uploadedBy`,
								    `dateUploaded`
								)
								VALUES(
								    null,
								    :applicationTypeIdfk,
								    :prStatusIdfk,
								    :mainReqIdfk,
								    :originalFileName,
								    :documentTitle,
								    :newFileName,
								    :fileSize,
								    :uploadedBy,
								    :dateUploaded
								)
				");
			$stmt->bindParam(":applicationTypeIdfk", $applicationTypeId);
			$stmt->bindParam(":prStatusIdfk", $prStatusId);
			$stmt->bindParam(":mainReqIdfk", $mainReqid);
			$stmt->bindParam(":originalFileName", $basefile);
			$stmt->bindParam(":documentTitle", $documentTitle);
			$stmt->bindParam(":newFileName", $baseFileTmp);
			$stmt->bindParam(":fileSize", $fileSize);
			$stmt->bindParam(":uploadedBy", $uploadedBy);
			$stmt->bindParam(":dateUploaded", $dateTime);
			$stmt->execute();

	            $stmt = $conn1->prepare("SELECT LAST_INSERT_ID()");
	            $stmt->execute();

	            $fileIdfk = $stmt->fetchColumn();

	        	$stmt = $conn1->prepare("
									INSERT INTO `permitrecognitiondocreview`(
									    `prDocReviewId`,
									    `prStatusIdfk`,
									    `mainReqidfk`,
									    `fileUploadStatus`,
									    `remarks`,
									    `fileIdfk`,
									    `updatedBy`,
									    `lastDateUpdated`
									)
									VALUES(
									    null,
									    :prStatusIdfk,
									    :mainReqidfk,
									    :fileUploadStatus,
									    :remarks,
									    :fileIdfk,
									    :updatedBy,
									    :lastDateUpdated
									)
	        		");

				$prStatusId = $request->input("prStatusId");
				$mainReqidfk = $request->input("mainReqid");
				$fileUploadStatus = 1;
				$remarks = "File Uploaded: " . $baseFileTmp;
				$updatedBy = $uploadedBy;
				$lastDateUpdated = $dateTime;

	        	$stmt->bindParam(":prStatusIdfk", $prStatusId);
	        	$stmt->bindParam(":mainReqidfk", $mainReqidfk);
	        	$stmt->bindParam(":fileUploadStatus", $fileUploadStatus);
	        	$stmt->bindParam(":remarks", $remarks);
	        	$stmt->bindParam(":fileIdfk", $fileIdfk);
	        	$stmt->bindParam(":updatedBy", $updatedBy);
	        	$stmt->bindParam(":lastDateUpdated", $lastDateUpdated);
	        	$stmt->execute();

				$stmt1 = $conn1 -> prepare("SELECT * FROM subrequirements1 WHERE mainReqIdfk=:mainReqIdfk AND isActive=1");
		        $stmt1->bindParam(":mainReqIdfk",$mainReqidfk);
		        $stmt1->execute();
		        $result1=$stmt1->fetchAll(\PDO::FETCH_ASSOC);
		        if(count($result1)!=0){
		        	foreach($result1 as $subReq1){
			        	$stmt = $conn1->prepare("
											INSERT INTO `permitrecognitiondocreview`(
											    `prDocReviewId`,
											    `prStatusIdfk`,
											    `mainReqidfk`,
											    `subReq1Idfk`,
											    `fileUploadStatus`,
											    `remarks`,
											    `fileIdfk`,
											    `updatedBy`,
											    `lastDateUpdated`
											)
											VALUES(
											    null,
											    :prStatusIdfk,
											    :mainReqidfk,
											    :subReq1Idfk,
											    :fileUploadStatus,
											    :remarks,
											    :fileIdfk,
											    :updatedBy,
											    :lastDateUpdated
											)
			        		");

			        	$stmt->bindParam(":prStatusIdfk", $prStatusId);
			        	$stmt->bindParam(":mainReqidfk", $mainReqidfk);
			        	$stmt->bindParam(":subReq1Idfk", $subReq1["subReq1Id"]);
			        	$stmt->bindParam(":fileUploadStatus", $fileUploadStatus);
			        	$stmt->bindParam(":remarks", $remarks);
			        	$stmt->bindParam(":fileIdfk", $fileIdfk);
			        	$stmt->bindParam(":updatedBy", $updatedBy);
			        	$stmt->bindParam(":lastDateUpdated", $lastDateUpdated);
			        	$stmt->execute();

						$stmt2 = $conn1 -> prepare("SELECT * FROM subrequirements2 WHERE subReq1Idfk=:subReq1Idfk AND isActive=1");
				        $stmt2->bindParam(":subReq1Idfk",$subReq1["subReq1Id"]);
				        $stmt2->execute();
				        $result2=$stmt2->fetchAll(\PDO::FETCH_ASSOC);
						if(count($result1)!=0){		       
					        foreach($result2 as $subReq2){
					        	$stmt = $conn1->prepare("
													INSERT INTO `permitrecognitiondocreview`(
													    `prDocReviewId`,
													    `prStatusIdfk`,
													    `mainReqidfk`,
													    `subReq1Idfk`,
													    `subReq2Idfk`,
													    `fileUploadStatus`,
													    `remarks`,
													    `fileIdfk`,
													    `updatedBy`,
													    `lastDateUpdated`
													)
													VALUES(
													    null,
													    :prStatusIdfk,
													    :mainReqidfk,
													    :subReq1Idfk,
													    :subReq2Idfk,
													    :fileUploadStatus,
													    :remarks,
													    :fileIdfk,
													    :updatedBy,
													    :lastDateUpdated
													)
					        		");

					        	$stmt->bindParam(":prStatusIdfk", $prStatusId);
					        	$stmt->bindParam(":mainReqidfk", $mainReqidfk);
					        	$stmt->bindParam(":subReq1Idfk", $subReq1["subReq1Id"]);
					        	$stmt->bindParam(":subReq2Idfk", $subReq2["subReq2Id"]);
					        	$stmt->bindParam(":fileUploadStatus", $fileUploadStatus);
					        	$stmt->bindParam(":remarks", $remarks);
					        	$stmt->bindParam(":fileIdfk", $fileIdfk);
					        	$stmt->bindParam(":updatedBy", $updatedBy);
					        	$stmt->bindParam(":lastDateUpdated", $lastDateUpdated);
					        	$stmt->execute();
					        }
					    }
		        	}
		        }

	    	return redirect()->route('viewapplication', [$request->input("prStatusId"), $request->input("applicationType"), $uploadStatus]);
	    }

    }

    public function deleteFile(Request $request){
    	$target_file = public_path() . $request->input("targetFile");
    	$baseFileTmp = $request->input("fileName");
		$applicationTypeId = $request->input("applicationType");
		$prStatusId = $request->input("prStatusId");
		$mainReqid = $request->input('mainReqid');
		$uploadedBy = Auth::id();

    	$conn1 = DB::connection('mysql')->getPdo();
		if(file_exists($target_file)){
			unlink($target_file);
			//delete record in fileuploads 
			$sql = "SELECT * FROM `fileuploads` 
					WHERE applicationTypeIdfk=:applicationTypeIdfk
						AND prStatusIdfk=:prStatusIdfk
						AND mainReqIdfk=:mainReqIdfk
						AND uploadedBy=:uploadedBy";
			$stmt=$conn1->prepare($sql);
			$stmt->bindParam(":applicationTypeIdfk", $applicationTypeId);
			$stmt->bindParam(":prStatusIdfk", $prStatusId);
			$stmt->bindParam(":mainReqIdfk", $mainReqid);
			$stmt->bindParam(":uploadedBy", $uploadedBy);
			$stmt->execute();
	        $result=$stmt->fetchAll(\PDO::FETCH_ASSOC);
	        foreach($result as $row){
	        	$fileId = $row["fileId"];
				$sql = "DELETE FROM `fileuploads` WHERE fileId=:fileId";
				$stmt=$conn1->prepare($sql);
				$stmt->bindParam(":fileId", $fileId);
				$stmt->execute();

				$sql = "DELETE FROM `permitrecognitiondocreview` WHERE fileIdfk=:fileId";
				$stmt=$conn1->prepare($sql);
				$stmt->bindParam(":fileId", $fileId);
				$stmt->execute();
	        }
		}
		else{
			//delete record in fileuploads 
			$sql = "SELECT * FROM `fileuploads` 
					WHERE applicationTypeIdfk=:applicationTypeIdfk
						AND prStatusIdfk=:prStatusIdfk
						AND mainReqIdfk=:mainReqIdfk
						AND uploadedBy=:uploadedBy";
			$stmt=$conn1->prepare($sql);
			$stmt->bindParam(":applicationTypeIdfk", $applicationTypeId);
			$stmt->bindParam(":prStatusIdfk", $prStatusId);
			$stmt->bindParam(":mainReqIdfk", $mainReqid);
			$stmt->bindParam(":uploadedBy", $uploadedBy);
			$stmt->execute();
	        $result=$stmt->fetchAll(\PDO::FETCH_ASSOC);
	        foreach($result as $row){
	        	$fileId = $row["fileId"];
				$sql = "DELETE FROM `fileuploads` WHERE fileId=:fileId";
				$stmt=$conn1->prepare($sql);
				$stmt->bindParam(":fileId", $fileId);
				$stmt->execute();

				$sql = "DELETE FROM `permitrecognitiondocreview` WHERE fileIdfk=:fileId";
				$stmt=$conn1->prepare($sql);
				$stmt->bindParam(":fileId", $fileId);
				$stmt->execute();
	        }
		}
		$uploadStatus = "File has been deleted";
		return redirect()->route('viewapplication', [$request->input("prStatusId"), $request->input("applicationType"), $uploadStatus]);
    }

    public function uploadFileSdoRo(Request $request){
    	//check first if there is already an uploaded file

		$target_dir = public_path() . '/uploads/' .$request->input("divisionName") . '/' . $request->input("prStatusId");
		$basefile = basename($_FILES["fileUpload"]["name"]); // original file name
		//date_default_timezone_set("Asia/Manila");
		$dateTime = date("Y-m-d h-i-s"); // date uploaded
		//$baseFileTmp = Auth::id() . "-" . $request->input("applicationType") . "-" . $request->input("prStatusId") . "-" . $request->input("requirement") . "-" . $dateTime . "." . pathinfo($basefile,PATHINFO_EXTENSION); // does not overwrite existing

		$baseFileTmp = $request->input("schoolId") . "-" . $request->input("applicationType") . "-" . $request->input("prStatusId") . "-" . $request->input("fileFrom") . "." . pathinfo($basefile,PATHINFO_EXTENSION); // newfile name
		$target_file = $target_dir . "/" . $baseFileTmp;
		$uploadOk = 1;
		$fileType = pathinfo($target_file,PATHINFO_EXTENSION);
		$fileTmp = $_FILES["fileUpload"]["tmp_name"];

		$fileSize = $_FILES["fileUpload"]["size"];
		$documentTitle = $request->input('documentTitle');

		$applicationTypeId = $request->input("applicationType");
		$prStatusId = $request->input("prStatusId");
		$mainReqid = $request->input('mainReqid');
		$uploadedBy = Auth::id();

		if($fileType != "pdf"){
			$uploadStatus = 'Invalid File Format';
			$uploadOk = 0;
		}

		if($uploadOk == 0){
			$uploadStatus = 'NO file has been uploaded. File to be uploaded must be in .pdf format.';
	    	return redirect()->route('viewapplication', [$request->input("prStatusId"), $request->input("applicationType"), $uploadStatus]);
		}
		else{
			$conn1 = DB::connection('mysql')->getPdo();

			if(is_dir($target_dir)){
				if(file_exists($target_file)){
					unlink($target_file);
					//delete record in fileuploads and permitrecognitiondocreview
					$sql = "SELECT * FROM `fileuploads` 
							WHERE applicationTypeIdfk=:applicationTypeIdfk
								AND prStatusIdfk=:prStatusIdfk
								AND mainReqIdfk=:mainReqIdfk
								AND uploadedBy=:uploadedBy";
					$stmt=$conn1->prepare($sql);
					$stmt->bindParam(":applicationTypeIdfk", $applicationTypeId);
					$stmt->bindParam(":prStatusIdfk", $prStatusId);
					$stmt->bindParam(":mainReqIdfk", $mainReqid);
					$stmt->bindParam(":uploadedBy", $uploadedBy);
					$stmt->execute();
			        $result=$stmt->fetchAll(\PDO::FETCH_ASSOC);
			        foreach($result as $row){
			        	$fileId = $row["fileId"];
						$sql = "DELETE FROM `fileuploads` WHERE fileId=:fileId";
						$stmt=$conn1->prepare($sql);
						$stmt->bindParam(":fileId", $fileId);
						$stmt->execute();

						$sql = "DELETE FROM `permitrecognitiondocreview` WHERE fileIdfk=:fileId";
						$stmt=$conn1->prepare($sql);
						$stmt->bindParam(":fileId", $fileId);
						$stmt->execute();
			        }
				}
				move_uploaded_file($fileTmp, $target_file);
			}
			else{
				mkdir($target_dir);
				if(file_exists($target_file)){
					unlink($target_file);
					//delete record in fileuploads and permitrecognitiondocreview
					$sql = "SELECT * FROM `fileuploads` 
							WHERE applicationTypeIdfk=:applicationTypeIdfk
								AND prStatusIdfk=:prStatusIdfk
								AND mainReqIdfk=:mainReqIdfk
								AND uploadedBy=:uploadedBy";
					$stmt=$conn1->prepare($sql);
					$stmt->bindParam(":applicationTypeIdfk", $applicationTypeId);
					$stmt->bindParam(":prStatusIdfk", $prStatusId);
					$stmt->bindParam(":mainReqIdfk", $mainReqid);
					$stmt->bindParam(":uploadedBy", $uploadedBy);
					$stmt->execute();
			        $result=$stmt->fetchAll(\PDO::FETCH_ASSOC);
			        foreach($result as $row){
			        	$fileId = $row["fileId"];
						$sql = "DELETE FROM `fileuploads` WHERE fileId=:fileId";
						$stmt=$conn1->prepare($sql);
						$stmt->bindParam(":fileId", $fileId);
						$stmt->execute();

						$sql = "DELETE FROM `permitrecognitiondocreview` WHERE fileIdfk=:fileId";
						$stmt=$conn1->prepare($sql);
						$stmt->bindParam(":fileId", $fileId);
						$stmt->execute();
			        }
				}
				move_uploaded_file($fileTmp, $target_file);
			}
			$uploadStatus = 'File has been uploaded';

			$stmt = $conn1->prepare("
								INSERT INTO `fileuploads`(
								    `fileId`,
								    `applicationTypeIdfk`,
								    `prStatusIdfk`,
								    `mainReqIdfk`,
								    `originalFileName`,
								    `documentTitle`,
								    `newFileName`,
								    `fileSize`,
								    `uploadedBy`,
								    `dateUploaded`
								)
								VALUES(
								    null,
								    :applicationTypeIdfk,
								    :prStatusIdfk,
								    :mainReqIdfk,
								    :originalFileName,
								    :documentTitle,
								    :newFileName,
								    :fileSize,
								    :uploadedBy,
								    :dateUploaded
								)
				");
			$stmt->bindParam(":applicationTypeIdfk", $applicationTypeId);
			$stmt->bindParam(":prStatusIdfk", $prStatusId);
			$stmt->bindParam(":mainReqIdfk", $mainReqid);
			$stmt->bindParam(":originalFileName", $basefile);
			$stmt->bindParam(":documentTitle", $documentTitle);
			$stmt->bindParam(":newFileName", $baseFileTmp);
			$stmt->bindParam(":fileSize", $fileSize);
			$stmt->bindParam(":uploadedBy", $uploadedBy);
			$stmt->bindParam(":dateUploaded", $dateTime);
			$stmt->execute();
			
	    	return redirect()->route('viewapplication', [$request->input("prStatusId"), $request->input("applicationType"), $uploadStatus]);
	    }

    }
}
