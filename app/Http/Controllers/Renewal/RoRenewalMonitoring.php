<?php

namespace App\Http\Controllers\Renewal;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;
use App\Services\PayUService\Exception;
use App\Http\Controllers\PDO;

class RoRenewalMonitoring extends Controller
{
	//prevents accessing the page without logging in
    public function __construct()
    {
        $this->middleware('auth');
    }

    //url to be called
    public function index()
    {
        return view('renewal/rorenewalmonitoring');
    }
}
