<?php

namespace App\Http\Controllers\GenerateCertificates;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ManageUsers\ManageUsers;
use Illuminate\Support\Facades\Config;

use Illuminate\Support\Facades\DB;
use App\Services\PayUService\Exception;
use App\Http\Controllers\PDO;

class GenerateCertificate extends Controller
{
/**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createPermit($prStatusId, $applicationTypeId, $schoolUserId)
    {
        $users = new ManageUsers; 
        return view('generatecertificates/generatecertificate')
            ->with('title', 'Generate Goverment Permit')
            ->with('status', 'Government Permit Form')
            ->with('prStatusId', $prStatusId)
            ->with('applicationTypeId', $applicationTypeId)
            ->with('gradeLevels', Controller::gradeLevels())
            ->with('applicationInfo', Controller::applicationInfo($prStatusId))
            ->with('getDivisionName', Controller::getDivisionName(Controller::getDivisionid()))
            ->with('categoriesshs', Controller::categoriesshs())
            ->with('getprofile', $users->getProfile($schoolUserId))
            ;
    }

    public function createRecognition($prStatusId, $applicationTypeId, $schoolUserId)
    {
        $users = new ManageUsers; 
        return view('generatecertificates/generatecertificate')
            ->with('title', 'Generate Goverment Recognition')
            ->with('status', 'Government Recognition Form')
            ->with('prStatusId', $prStatusId)
            ->with('applicationTypeId', $applicationTypeId)
            ->with('gradeLevels', Controller::gradeLevels())
            ->with('applicationInfo', Controller::applicationInfo($prStatusId))
            ->with('getDivisionName', Controller::getDivisionName(Controller::getDivisionid()))
            ->with('categoriesshs', Controller::categoriesshs())
            ->with('getprofile', $users->getProfile($schoolUserId))
            ;
    }

	public function generatepermit(Request $request)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection(array('marginLeft' => 1440, 'marginRight' => 1440, 'marginTop' => 1440, 'marginBottom' => 1440, 'pageSizeW' => \PhpOffice\PhpWord\Shared\Converter::inchToTwip(8.5),'pageSizeH' => \PhpOffice\PhpWord\Shared\Converter::inchToTwip(13)));
        
        //----------------------------paragraph styles-----------------------------
        $phpWord->addFontStyle('textStyle', array(
        	'name'=>'Book Antiqua',
        	'size' => 12)
    	);
        $phpWord->addFontStyle('textStyleBold', array(
        	'name'=>'Book Antiqua',
        	'size' => 12,
        	'bold' => true)
    	);
        $phpWord->addFontStyle('textStyleBoldUnderlined', array(
        	'name'=>'Book Antiqua',
        	'size' => 12,
        	'bold' => true,
        	'underline' => 'single'
        	)
    	);
        $phpWord->addFontStyle('textStyleCenturyGothic', array(
            'name'=>'Century Gothic',
            'size' => 16)
        );

		$phpWord->addParagraphStyle('textPStyle', array(
			'align'=>'both', 
			'spaceAfter' => 0,
			'spaceBefore' => 0,
			'spacing' => 120,
			'lineHeight' => 1,
			'indentation' => array(
				'firstLine' => 700
				)
			)
		);
		$phpWord->addParagraphStyle('textPStyle2', array(
			'align'=>'left', 
			'spaceAfter' => 0,
			'spaceBefore' => 0
			)
		);
		$phpWord->addParagraphStyle('textPStyleCenter', array(
			'align'=>'center', 
			'spaceAfter' => 0,
			'spaceBefore' => 0
			)
		);
		//----------------------------style for title-------------------------------
        $phpWord->addFontStyle('titleStyle', array(
        	'name'=>'Book Antiqua',
        	'size' => 14,
        	'bold' => true,
        	'spaceAfter'=>10,
			'spaceBefore'=>10
        	)
    	);
		$phpWord->addParagraphStyle('titlePStyle', array(
			'align'=>'left', 
			'spaceAfter' => 0,
			'spaceBefore' => 0
			)
		);

		//----------------------------style for impact text-------------------------
        $phpWord->addFontStyle('impactStyle', array(
        	'name'=>'Impact',
        	'size' => 26
        	)
    	);
        $phpWord->addFontStyle('impactStyle2', array(
        	'name'=>'Impact',
        	'size' => 22
        	)
    	);
		$phpWord->addParagraphStyle('impactPStyle', array(
			'align'=>'center',
			'spaceAfter' => 0,
			'spaceBefore' => 0,
			'spacing' => 120,
			'lineHeight' => 1
			)
		);

        //-------------------------- HEADER ----------------------
        $header = $section->addHeader();
        $header -> addImage("./img/header.jpg", array(
	        'width'            => round(\PhpOffice\PhpWord\Shared\Converter::cmToPixel(19)),
	        'height'           => round(\PhpOffice\PhpWord\Shared\Converter::cmToPixel(3)),
	        'positioning'      => \PhpOffice\PhpWord\Style\Image::POSITION_ABSOLUTE,
	        'posVertical'    => \PhpOffice\PhpWord\Style\Image::POSITION_VERTICAL_TOP,
	        'posHorizontalRel' => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_PAGE,
	        'posVerticalRel'   => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_PAGE,
	        'marginLeft'       => round(\PhpOffice\PhpWord\Shared\Converter::cmToPixel(0)),
	        'marginTop'        => round(\PhpOffice\PhpWord\Shared\Converter::cmToPixel(0)),
    	));

        //-------------------------- FOOTER ----------------------
		$footer = $section->addFooter();
        $footer -> addImage("./img/footer.jpg", array(
	        'width'            => round(\PhpOffice\PhpWord\Shared\Converter::cmToPixel(19)),
	        'height'           => round(\PhpOffice\PhpWord\Shared\Converter::cmToPixel(1.5)),
	        'positioning'      => \PhpOffice\PhpWord\Style\Image::POSITION_ABSOLUTE,
	        'posVertical'    => \PhpOffice\PhpWord\Style\Image::POSITION_VERTICAL_BOTTOM,
	        'posHorizontalRel' => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_PAGE,
	        'posVerticalRel'   => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_PAGE,
	        'marginLeft'       => round(\PhpOffice\PhpWord\Shared\Converter::cmToPixel(0)),
    	));

        //-------------------------- CONTENT ----------------------
        $section->addTextBreak(1, null, 'titlePStyle');
        $section->addTextBreak(1, null, 'titlePStyle');
        $section->addTextBreak(1, null, 'titlePStyle');
        $section->addTextBreak(1, null, 'titlePStyle');
        $text = "GOVERMENT PERMIT (DepED-CAR)";
        $section->addText($text, 'titleStyle', 'titlePStyle');
        $text = strtoupper($request->input('certNumber'));
        $section->addText($text, 'titleStyle', 'titlePStyle');

        $section->addTextBreak(1, null, 'titlePStyle');
        $text = "Pursuant to the provisions of Republic Act No. 9155 (Governance of Basic Education Act of 2001) and Republic Act No. 10533 (Enhanced Basic Education Act of 2012), the….";
        $section->addText($text, 'textStyle', 'textPStyle');

        $section->addTextBreak(1, null, 'titlePStyle');
        $text = strtoupper($request->input('name'));
        $section->addText($text, 'impactStyle', 'impactPStyle');

        $text = strtoupper($request->input('schoolId'));
        $section->addText($text, 'textStyleCenturyGothic', 'impactPStyle');

        $text = ucfirst($request->input('address')) . ', ' . ucfirst($request->input('schoolsDivision'));;
        $section->addText($text, 'impactStyle2', 'impactPStyle');

        $section->addTextBreak(1, null, 'titlePStyle');
        $text = "is hereby granted authority to operate…";
        $section->addText($text, 'textStyle', 'textPStyle2');

        $section->addTextBreak(1, null, 'titlePStyle');
        $text = strtoupper($request->input('applicationFrom') . ' TO ' . $request->input('applicationTo'));
        $section->addText($text, 'impactStyle', 'impactPStyle');

        $section->addTextBreak(1, null, 'titlePStyle');
        $text = "for SY " . Config::get('constants.gprpsconst.application_school_year') . " only.";
        $section->addText($text, 'textStyle', 'textPStyle2');

        $section->addTextBreak(1, null, 'titlePStyle');
        $text = "This permit is subject to revocation anytime the school fails to maintain the standards required for this course and to faithfully follow the laws, policies, rules and regulations, and private school course. The school authorities are to inform the Department of Education of any plan or action regarding closure or phasing out of the course or any change(s) in the requirements. Furthermore, this permit does not extend to any branch of the school.";
		$section->addText($text, 'textStyle', 'textPStyle');

		$section->addTextBreak(1, null, 'titlePStyle');
        $text = "Given at DepED-CAR Regional Office, Wangal, La Trinidad, Benguet, Philippines this 8th day of September 2018.";
        $section->addText($text, 'textStyle', 'textPStyle');

        //---------------------------- SIGNATORIES ------------------------------
        $section->addTextBreak(1, null, 'titlePStyle');
		$tableDirector = 'Fancy Table';
		$tableDirectorStyle = array(
			'border' => 0, 
			'cellMargin' => 0, 
			'alignment' => \PhpOffice\PhpWord\SimpleType\JcTable::CENTER, 
			'cellSpacing' => 0,
			'width'=> 10000
		);

		$phpWord->addTableStyle($tableDirector, $tableDirectorStyle);
		$table = $section->addTable($tableDirector);

		$table->addRow(100);
        $text = "";
		$table->addCell(5900)->addText($text, 'textStyleBold', 'textPStyleCenter');
        $text = "For the Secretary of Education:";
		$table->addCell(4100)->addText($text, 'textStyle', 'textPStyleCenter');

		$table->addRow(100);
        $text = "";
		$table->addCell(5900)->addText($text, 'textStyleBold', 'textPStyleCenter');
        $text = "";
		$table->addCell(4100)->addText($text, 'textStyle', 'textPStyleCenter');

        $table->addRow(100);
        $text = "";
        $table->addCell(5900)->addText($text, 'textStyleBold', 'textPStyleCenter');
        $text = "";
        $table->addCell(4100)->addText($text, 'textStyle', 'textPStyleCenter');

		$table->addRow(100);
        $text = "";
		$table->addCell(5900)->addText($text, 'textStyleBold', 'textPStyleCenter');
        $text = strtoupper($request->input('regionalDirector'));
		$table->addCell(4100)->addText($text, 'textStyleBold', 'textPStyleCenter');

		$table->addRow(100);
        $text = "";
		$table->addCell(5900)->addText($text, 'textStyle', 'textPStyleCenter');
        $text = $request->input('regionalDirectorPosition');
		$table->addCell(4100)->addText($text, 'textStyle', 'textPStyleCenter');

		$table->addRow(100);
        $text = "";
		$table->addCell(5900)->addText($text, 'textStyle', 'textPStyleCenter');
        $text = "Office of the Regional Director";
		$table->addCell(4100)->addText($text, 'textStyle', 'textPStyleCenter');

        $section->addTextBreak(1, null, 'titlePStyle');
        $text = "(Not valid w/o seal or with erasures or alterations)";
        $section->addText($text, 'textStyle', 'textPStyle2');

		$section->addTextBreak(1, null, 'titlePStyle');
        $text = "PERMIT TASK FORCE";
        $section->addText($text, 'textStyleBoldUnderlined', 'textPStyleCenter');

		$section->addTextBreak(1, null, 'titlePStyle');
		$section->addTextBreak(1, null, 'titlePStyle');

		$tableTaskForce = 'Fancy Table';
		$tableTaskForceStyle = array(
			'border' => 0, 
			'cellMargin' => 0, 
			'alignment' => \PhpOffice\PhpWord\SimpleType\JcTable::CENTER, 
			'cellSpacing' => 0,
			'width'=> 10000
		);

		$phpWord->addTableStyle($tableTaskForce, $tableTaskForceStyle);
		$table = $section->addTable($tableTaskForce);
		$table->addRow(100);
        $text = strtoupper($request->input('taskForceMember1'));
		$table->addCell(5000)->addText($text, 'textStyleBold', 'textPStyleCenter');
        $text = strtoupper($request->input('taskForceMember2'));
		$table->addCell(5000)->addText($text, 'textStyleBold', 'textPStyleCenter');

		$table->addRow(100);
        $text = $request->input('taskForceMember1Position');
		$table->addCell(5000)->addText($text, 'textStyle', 'textPStyleCenter');
        $text = $request->input('taskForceMember2Position');;
		$table->addCell(5000)->addText($text, 'textStyle', 'textPStyleCenter');

		$table->addRow(100);
        $text = "Member";
		$table->addCell(5000)->addText($text, 'textStyle', 'textPStyleCenter');
        $text = "Member";
		$table->addCell(5000)->addText($text, 'textStyle', 'textPStyleCenter');

		$section->addTextBreak(1, null, 'titlePStyle');
        $text = strtoupper($request->input('taskForceChair'));
        $section->addText($text, 'textStyleBold', 'textPStyleCenter');
        $text = $request->input('taskForceChairPosition');
        $section->addText($text, 'textStyle', 'textPStyleCenter');
        $text = "Chairman";
        $section->addText($text, 'textStyle', 'textPStyleCenter');
	

        $dirname = uniqid('', true);
        $dir = \PhpOffice\PhpWord\Settings::getTempDir() . '/' . $dirname;
        echo $dir;
        mkdir($dir);
        \PhpOffice\PhpWord\Settings::setTempDir($dir);

        $obj_writer = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $obj_writer->save('Government Permit Certificate.docx');


        $prStatusId = $request->input('prStatusId');

          //update generated certificate status
          $applicationStatusId = 5;
          $prStatusId = $request->input('prStatusId');
          $lastDateUpdated = date("Y-m-d H:i:s");
          Controller::updateApplicationStatus($prStatusId, $applicationStatusId, $lastDateUpdated);

          //update Stages Timeline
              $stageIdfk = 5;
              $stageStatusIdfk = 25;
              $updatedBy = Auth::id();
              $remarks = "Generated Certificate";
              $docReviewFileIdfk = 0;
              Controller::updateStagesTimeline($prStatusId, $stageIdfk, $stageStatusIdfk, $updatedBy, $lastDateUpdated, $remarks, $docReviewFileIdfk);


            $regionalDirectorName = $request->input('regionalDirectorPosition');
            $regionalDirectorPosition = $request->input('regionalDirectorPosition');
            $taskForceMember1Name = $request->input('taskForceMember1');
            $taskForceMember1Position = $request->input('taskForceMember1Position');
            $taskForceMember2Name = $request->input('taskForceMember2');
            $taskForceMember2Position = $request->input('taskForceMember2Position');
            $taskForceChairName = $request->input('taskForceChair');
            $taskForceChairPosition = $request->input('taskForceChairPosition');
            $governmentPRNumberfk = $request->input('certNumber');

            $conn1 = DB::connection('mysql')->getPdo();
            $sql="UPDATE
                `generatedcertificates`
            SET
                `certGenerated` = 1,
                `regionalDirectorName` =:regionalDirectorName,
                `regionalDirectorPosition` = :regionalDirectorPosition,
                `taskForceMember1Name` = :taskForceMember1Name,
                `taskForceMember1Position` = :taskForceMember1Position,
                `taskForceMember2Name` = :taskForceMember2Name,
                `taskForceMember2Position` = :taskForceMember2Position,
                `taskForceChairName` = :taskForceChairName,
                `taskForceChairPosition` = :taskForceChairPosition
            WHERE
                `governmentPRNumberfk`=:governmentPRNumberfk";
              $stmt=$conn1->prepare($sql);
              $stmt->bindParam(":regionalDirectorName", $regionalDirectorName);
              $stmt->bindParam(":regionalDirectorPosition", $regionalDirectorPosition);
              $stmt->bindParam(":taskForceMember1Name", $taskForceMember1Name);
              $stmt->bindParam(":taskForceMember1Position", $taskForceMember1Position);
              $stmt->bindParam(":taskForceMember2Name", $taskForceMember2Name);
              $stmt->bindParam(":taskForceMember2Position", $taskForceMember2Position);
              $stmt->bindParam(":taskForceChairName", $taskForceChairName);
              $stmt->bindParam(":taskForceChairPosition", $taskForceChairPosition);
              $stmt->bindParam(":governmentPRNumberfk", $governmentPRNumberfk);
              $stmt->execute();


        return response()->download(public_path('Government Permit Certificate.docx'));

        rmdir($dir);

//        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
//        $objWriter->save('Appdividend.docx');
//        return response()->download(public_path('Appdividend.docx'));


          //return to view application page
          //$applicationTypeId = $request->input('applicationType');
          //$status = "Certificate Generated.";
          //return redirect()->route('viewapplication', [$prStatusId, $applicationTypeId, $status]);
    }

    public function generateRecognition(Request $request)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection(array('marginLeft' => 1440, 'marginRight' => 1440, 'marginTop' => 1440, 'marginBottom' => 1440, 'pageSizeW' => \PhpOffice\PhpWord\Shared\Converter::inchToTwip(8.5),'pageSizeH' => \PhpOffice\PhpWord\Shared\Converter::inchToTwip(13)));
        
        //----------------------------paragraph styles-----------------------------
        $phpWord->addFontStyle('textStyle', array(
            'name'=>'Book Antiqua',
            'size' => 12)
        );
        $phpWord->addFontStyle('textStyleBold', array(
            'name'=>'Book Antiqua',
            'size' => 12,
            'bold' => true)
        );
        $phpWord->addFontStyle('textStyleBoldUnderlined', array(
            'name'=>'Book Antiqua',
            'size' => 12,
            'bold' => true,
            'underline' => 'single'
            )
        );
        $phpWord->addFontStyle('textStyleCenturyGothic', array(
            'name'=>'Century Gothic',
            'size' => 16)
        );

        $phpWord->addParagraphStyle('textPStyle', array(
            'align'=>'both', 
            'spaceAfter' => 0,
            'spaceBefore' => 0,
            'spacing' => 120,
            'lineHeight' => 1,
            'indentation' => array(
                'firstLine' => 700
                )
            )
        );
        $phpWord->addParagraphStyle('textPStyle2', array(
            'align'=>'left', 
            'spaceAfter' => 0,
            'spaceBefore' => 0
            )
        );
        $phpWord->addParagraphStyle('textPStyleCenter', array(
            'align'=>'center', 
            'spaceAfter' => 0,
            'spaceBefore' => 0
            )
        );
        //----------------------------style for title-------------------------------
        $phpWord->addFontStyle('titleStyle', array(
            'name'=>'Book Antiqua',
            'size' => 14,
            'bold' => true,
            'spaceAfter'=>10,
            'spaceBefore'=>10
            )
        );
        $phpWord->addParagraphStyle('titlePStyle', array(
            'align'=>'left', 
            'spaceAfter' => 0,
            'spaceBefore' => 0
            )
        );

        //----------------------------style for impact text-------------------------
        $phpWord->addFontStyle('impactStyle', array(
            'name'=>'Impact',
            'size' => 26
            )
        );
        $phpWord->addFontStyle('impactStyle2', array(
            'name'=>'Impact',
            'size' => 16
            )
        );
        $phpWord->addParagraphStyle('impactPStyle', array(
            'align'=>'center',
            'spaceAfter' => 0,
            'spaceBefore' => 0,
            'spacing' => 120,
            'lineHeight' => 1
            )
        );

        //-------------------------- HEADER ----------------------
        $header = $section->addHeader();
        $header -> addImage("./img/header.jpg", array(
            'width'            => round(\PhpOffice\PhpWord\Shared\Converter::cmToPixel(19)),
            'height'           => round(\PhpOffice\PhpWord\Shared\Converter::cmToPixel(3)),
            'positioning'      => \PhpOffice\PhpWord\Style\Image::POSITION_ABSOLUTE,
            'posVertical'    => \PhpOffice\PhpWord\Style\Image::POSITION_VERTICAL_TOP,
            'posHorizontalRel' => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_PAGE,
            'posVerticalRel'   => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_PAGE,
            'marginLeft'       => round(\PhpOffice\PhpWord\Shared\Converter::cmToPixel(0)),
            'marginTop'        => round(\PhpOffice\PhpWord\Shared\Converter::cmToPixel(0)),
        ));

        //-------------------------- FOOTER ----------------------
        $footer = $section->addFooter();
        $footer -> addImage("./img/footer.jpg", array(
            'width'            => round(\PhpOffice\PhpWord\Shared\Converter::cmToPixel(19)),
            'height'           => round(\PhpOffice\PhpWord\Shared\Converter::cmToPixel(1.5)),
            'positioning'      => \PhpOffice\PhpWord\Style\Image::POSITION_ABSOLUTE,
            'posVertical'    => \PhpOffice\PhpWord\Style\Image::POSITION_VERTICAL_BOTTOM,
            'posHorizontalRel' => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_PAGE,
            'posVerticalRel'   => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_PAGE,
            'marginLeft'       => round(\PhpOffice\PhpWord\Shared\Converter::cmToPixel(0)),
        ));

        //-------------------------- CONTENT ----------------------
        $section->addTextBreak(1, null, 'titlePStyle');
        $section->addTextBreak(1, null, 'titlePStyle');
        $section->addTextBreak(1, null, 'titlePStyle');
        $section->addTextBreak(1, null, 'titlePStyle');
        $text = "GOVERMENT PERMIT (DepED-CAR)";
        $section->addText($text, 'titleStyle', 'titlePStyle');
        $text = "QAD-R-Ap-001, s. 2019";
        $section->addText($text, 'titleStyle', 'titlePStyle');

        $section->addTextBreak(1, null, 'titlePStyle');
        $text = "By virtue of the authority in me vested by Republic Act No. 9155 (Governance of Basic Education Act of 2001) and Republic Act No. 10533 (Enhanced Basic Education Act of 2012), I, the Secretary, Department of Education, do hereby grant Government Recognition to…";
        $section->addText($text, 'textStyle', 'textPStyle');

        $section->addTextBreak(1, null, 'titlePStyle');
        $text = strtoupper($request->input('name'));
        $section->addText($text, 'impactStyle', 'impactPStyle');

        $text = strtoupper($request->input('schoolId'));
        $section->addText($text, 'textStyleCenturyGothic', 'impactPStyle');

        $text = ucfirst($request->input('address')) . ', ' . ucfirst($request->input('schoolsDivision'));;
        $section->addText($text, 'impactStyle2', 'impactPStyle');

        $section->addTextBreak(1, null, 'titlePStyle');
        $textrun = $section->addTextRun();
        $text = "for ";
        $textrun->addText($text, 'textStyle', 'textPStyle2');

        $text = strtoupper($request->input('applicationFrom') . ' TO ' . $request->input('applicationTo'));
        $textrun->addText($text, 'impactStyle2', 'impactPStyle');

        $text = " effective ";
        $textrun->addText($text, 'textStyle', 'textPStyle2');

        $text = " SY " . Config::get('constants.gprpsconst.application_school_year') . ".";
        $textrun->addText($text, 'impactStyle2', 'textPStyle2');

        $section->addTextBreak(1, null, 'titlePStyle');
        $text = "In case of failure of school to observe and maintain any of the required standards of the Department of Education affecting the course herein approved, the authority hereby granted shall be revoked and the records of the students who attended these courses shall be taken over and kept in the files of the Secretary of Education. Also, the guarantee bond of the school shall be forfeited in accordance with the provisions of Section 8 of Act No. 2706.";
        $section->addText($text, 'textStyle', 'textPStyle');

        $section->addTextBreak(1, null, 'titlePStyle');
        $text = "It is also stipulated that the Government Recognition hereby granted does not extend to any branch of the said school nor to any other course, grade or curriculum year.";
        $section->addText($text, 'textStyle', 'textPStyle');

        $section->addTextBreak(1, null, 'titlePStyle');
        $text = "Issued at DepED-CAR Regional Office, Wangal, La Trinidad, Benguet, Philippines this 22nd day of February 2019.";
        $section->addText($text, 'textStyle', 'textPStyle');

        //---------------------------- SIGNATORIES ------------------------------
        $section->addTextBreak(1, null, 'titlePStyle');
        $tableDirector = 'Fancy Table';
        $tableDirectorStyle = array(
            'border' => 0, 
            'cellMargin' => 0, 
            'alignment' => \PhpOffice\PhpWord\SimpleType\JcTable::CENTER, 
            'cellSpacing' => 0,
            'width'=> 10000
        );

        $phpWord->addTableStyle($tableDirector, $tableDirectorStyle);
        $table = $section->addTable($tableDirector);

        $table->addRow(100);
        $text = "";
        $table->addCell(5900)->addText($text, 'textStyleBold', 'textPStyleCenter');
        $text = "For the Secretary of Education:";
        $table->addCell(4100)->addText($text, 'textStyle', 'textPStyleCenter');

        $table->addRow(100);
        $text = "";
        $table->addCell(5900)->addText($text, 'textStyleBold', 'textPStyleCenter');
        $text = "";
        $table->addCell(4100)->addText($text, 'textStyle', 'textPStyleCenter');

        $table->addRow(100);
        $text = "";
        $table->addCell(5900)->addText($text, 'textStyleBold', 'textPStyleCenter');
        $text = "";
        $table->addCell(4100)->addText($text, 'textStyle', 'textPStyleCenter');

        $table->addRow(100);
        $text = "";
        $table->addCell(5900)->addText($text, 'textStyleBold', 'textPStyleCenter');
        $text = strtoupper($request->input('regionalDirector'));
        $table->addCell(4100)->addText($text, 'textStyleBold', 'textPStyleCenter');

        $table->addRow(100);
        $text = "";
        $table->addCell(5900)->addText($text, 'textStyle', 'textPStyleCenter');
        $text = $request->input('regionalDirectorPosition');
        $table->addCell(4100)->addText($text, 'textStyle', 'textPStyleCenter');

        $table->addRow(100);
        $text = "";
        $table->addCell(5900)->addText($text, 'textStyle', 'textPStyleCenter');
        $text = "Office of the Regional Director";
        $table->addCell(4100)->addText($text, 'textStyle', 'textPStyleCenter');

        $section->addTextBreak(1, null, 'titlePStyle');
        $text = "(Not valid w/o seal or with erasures or alterations)";
        $section->addText($text, 'textStyle', 'textPStyle2');

        $section->addTextBreak(1, null, 'titlePStyle');
        $text = "PERMIT TASK FORCE";
        $section->addText($text, 'textStyleBoldUnderlined', 'textPStyleCenter');

        $section->addTextBreak(1, null, 'titlePStyle');
        $section->addTextBreak(1, null, 'titlePStyle');

        $tableTaskForce = 'Fancy Table';
        $tableTaskForceStyle = array(
            'border' => 0, 
            'cellMargin' => 0, 
            'alignment' => \PhpOffice\PhpWord\SimpleType\JcTable::CENTER, 
            'cellSpacing' => 0,
            'width'=> 10000
        );

        $phpWord->addTableStyle($tableTaskForce, $tableTaskForceStyle);
        $table = $section->addTable($tableTaskForce);
        $table->addRow(100);
        $text = strtoupper($request->input('taskForceMember1'));
        $table->addCell(5000)->addText($text, 'textStyleBold', 'textPStyleCenter');
        $text = strtoupper($request->input('taskForceMember2'));
        $table->addCell(5000)->addText($text, 'textStyleBold', 'textPStyleCenter');

        $table->addRow(100);
        $text = $request->input('taskForceMember1Position');
        $table->addCell(5000)->addText($text, 'textStyle', 'textPStyleCenter');
        $text = $request->input('taskForceMember2Position');;
        $table->addCell(5000)->addText($text, 'textStyle', 'textPStyleCenter');

        $table->addRow(100);
        $text = "Member";
        $table->addCell(5000)->addText($text, 'textStyle', 'textPStyleCenter');
        $text = "Member";
        $table->addCell(5000)->addText($text, 'textStyle', 'textPStyleCenter');

        $section->addTextBreak(1, null, 'titlePStyle');
        $text = strtoupper($request->input('taskForceChair'));
        $section->addText($text, 'textStyleBold', 'textPStyleCenter');
        $text = $request->input('taskForceChairPosition');
        $section->addText($text, 'textStyle', 'textPStyleCenter');
        $text = "Chairman";
        $section->addText($text, 'textStyle', 'textPStyleCenter');
    

        $dirname = uniqid('', true);
        $dir = \PhpOffice\PhpWord\Settings::getTempDir() . '/' . $dirname;
        echo $dir;
        mkdir($dir);
        \PhpOffice\PhpWord\Settings::setTempDir($dir);

        $obj_writer = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $obj_writer->save('Government Recognition Certificate.docx');
        return response()->download(public_path('Government Recognition Certificate.docx'));

        rmdir($dir);

//        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
//        $objWriter->save('Appdividend.docx');
//        return response()->download(public_path('Appdividend.docx'));
    }
}
