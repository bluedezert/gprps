<?php

return [

    'gprpsconst'=>[
        'application_school_year'=>'2020 - 2021',
        'application_regional_director'=>'MAY B. ECLAR, Ph.D., CESO V',
        'application_regional_director_pos'=>'Regional Director',
        'application_taskforce_chair'=>'AIDA L. PAYANG, Ed.D.',
        'application_taskforce_chair_pos'=>'Chief, Quality Assurance Division',
        'qad_email'=>'ictu.depedcar@gmail.com',
    ]

];