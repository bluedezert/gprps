@extends('layouts.app')

@section('content')

@include('layouts.sidebar')

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            {{ $title }}<small> [Cert Number: <?php echo $applicationInfo["governmentPRNumber"]; ?>]</small>
        </h1>
        <!-- You can dynamically generate breadcrumbs here -->
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> GPRPS</a></li>
                <li class="active"> {{ $title }}</li>
        </ol>
    </section>
    <section class="content container-fluid">
    @if($applicationTypeId==1 || $applicationTypeId==2)
      <form method="POST" action="{{ route('generatepermit') }}">
    @elseif($applicationTypeId==3 || $applicationTypeId==4)
      <form method="POST" action="{{ route('generateRecognition') }}">
    @endif
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="box box-private-schools">
                    <div class="box-header with-border">
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                              <p>
                                <a href="#">
                                  <button type="submit" class="btn bg-navy btn-flat btn-on-assessment" data-tooltip="tooltip" title="Generate"><span class="fa fa-print"></span> Generate Certificate</button>
                                </a>
                                <a href="{{ route('viewapplication',[$prStatusId, $applicationTypeId, 'View Application'])}}"><button type="button" class="btn bg-default btn-flat btn-close-new-application"><span class="fa fa-close"></span> Close</button></a>
                              </p>
                            </div>
                            <!-- /.col -->
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="col-xs-6">
                          <div class="box">
                            <div class="box-header">
                              <h3 class="box-title">Application Information</h3>
                              <em><br>All Fields are required</em>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                              <div class="" style="padding:0px;">
                                  <img class="profile-user-img img-responsive img-circle" src="{{ asset("/bower_components/admin-lte/dist/img/Private-School-icon.jpg") }}" alt="User profile picture">
                              </div>    
                              <p><label><i class="fa fa-university"></i> <strong>School Name</strong></label>
                                  <input id="id" type="hidden" name="certNumber" value="{{ $applicationInfo['governmentPRNumber'] }}" required>
                                  <input id="id" type="hidden" name="prStatusId" value="{{ $applicationInfo['prStatusId'] }}" required>
                                  <input id="id" type="hidden" name="applicationType" value="{{ $applicationInfo['applicationTypeIdfk'] }}" required>
                                  applicationType
                                  <input id="id" type="hidden" name="id" value="{{ $getprofile['id'] }}" required>
                                  <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $getprofile['name'] }}" required></p>
                              <p>
                                  <label><i class="fa fa-flag"></i> <strong>School Id</strong></label>
                                  <input id="schoolId" type="text" class="form-control{{ $errors->has('schoolId') ? ' is-invalid' : '' }}" name="schoolId" value="{{ $getprofile['schoolId'] }}" required readonly="true">
                              </p>
                              <p>
                                  <label><i class="fa fa-map-marker"></i> <strong>Address (Exept Province Name)</strong></label>
                                  <input id="address" type="text" class="form-control" name="address" value="{{ $getprofile['schoolAddress'] }}" placeholder="Address" required>
                              </p> 
                              <p>
                                  <label><i class="fa fa-map-marker"></i> <strong>Schools Division</strong></label>
                                  <input id="schoolsDivision" type="text" class="form-control{{ $errors->has('schoolsDivisionId') ? ' is-invalid' : '' }}" name="schoolsDivision" value="{{ $getprofile['schoolsDivision'] }}" required readonly="true">
                              </p>                               
                            </div>
                            <div class="box-body"> 
                                <p>
                                  <label>APPLYING FOR</strong></label>
                                </p>
                                <p>
                                  <label>School Year</strong></label>
                                    <input id="schoolYear" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="schoolYear" value="{{ Config::get('constants.gprpsconst.application_school_year') }}" required>
                                </p>
                                <p>
                                  <label>Grade Level From</strong></label>
                                    <input id="id" type="hidden" name="id" value="{{ $getprofile['id'] }}" required>
                                    <input id="applicationFrom" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="applicationFrom" value="{{ $applicationInfo['applicationFrom'] }}" required>
                                </p>
                                <p>
                                  <label>Grade Level To</strong></label>
                                    <input id="id" type="hidden" name="id" value="{{ $getprofile['id'] }}" required>
                                    <input id="applicationTo" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="applicationTo" value="{{ $applicationInfo['applicationTo'] }}" required>
                                </p>                             
                            </div>
                            <!-- /.box-body -->
                          </div>
                          <!-- /.box -->
                        </div>
                        <div class="col-xs-6">
                          <div class="box">
                            <div class="box-header">
                              <h3 class="box-title">Permit Task Force Information</h3>
                              <em><br>All Fields are required</em>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                    <div class="" style="padding:0px;">
                                        <img class="profile-user-img img-responsive img-circle" src="{{ asset('/bower_components/admin-lte/dist/img/users.png') }}" alt="User profile picture">
                                    </div>     
                                    <p><label><i class="fa fa-user"></i> <strong> Regional Director</strong></label>
                                        <input id="regionalDirector" type="text" class="form-control" name="regionalDirector" placeholder="Name" value="{{ Config::get('constants.gprpsconst.application_regional_director') }}" required>
                                    </p>
                                    <p>
                                        <input id="regionalDirectorPosition" type="text" class="form-control" name="regionalDirectorPosition" placeholder="Designation" value="{{ Config::get('constants.gprpsconst.application_regional_director_pos') }}" required>
                                    </p>
                                    <p><label><i class="fa fa-university"></i> <strong> Permit Task Force Chair</strong></label>
                                        <input id="taskForceChair" type="text" class="form-control" name="taskForceChair" placeholder="Name" value="{{ Config::get('constants.gprpsconst.application_taskforce_chair') }}" required>
                                    </p>
                                    <p>
                                        <input id="taskForceChairPosition" type="text" class="form-control" name="taskForceChairPosition" placeholder="Designation" value="{{ Config::get('constants.gprpsconst.application_taskforce_chair_pos') }}" required>
                                    </p>
                                    <p><label><i class="fa fa-university"></i> <strong> Permit Task Force Memeber 1</strong></label>
                                        <input id="taskForceMember1" type="text" class="form-control" name="taskForceMember1" placeholder="Name" value="" required>
                                    </p>
                                    <p>
                                        <input id="taskForceMember1Position" type="text" class="form-control" name="taskForceMember1Position" placeholder="Designation"  value="" required>
                                    </p>
                                    <p><label><i class="fa fa-university"></i> <strong> Permit Task Force Member 2</strong></label>
                                        <input id="taskForceMember2" type="text" class="form-control" name="taskForceMember2" placeholder="Name" value="" required>
                                    </p>
                                    <p>
                                        <input id="taskForceMember2Position" type="text" class="form-control" name="taskForceMember2Position" placeholder="Designation"  value="" required>
                                    </p>
                            </div>
                            <!-- /.box-body -->
                          </div>
                          <!-- /.box -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </form>
    </section>
</div>
<div id="snackbar">{{ $status }}</div>
@include('layouts.footer')
@include('layouts.rightsidebar')
@endsection
