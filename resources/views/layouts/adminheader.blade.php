    <header class="main-header">
        <!-- Logo -->
        <a href="" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>GPRPS</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>GPRPS</b></span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu pull-left">
                <ul class="nav navbar-nav">
                    <li class="pull-left">
                        <span class="logo-lg">
                            <a class="navbar-brand" href="{{ url('/') }}">
                                <h4 style="margin-top: 5px; color: white;"><span style="font-size: small; font-weight: lighter;">Online Application System for</span> Government Permit and Recognition for Private Schools </h4>
                            </a>
                        </span>
                    </li>
                </ul>
            </div>
            <div class="navbar-custom-menu">
                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <!-- Authentication Links -->
                    @guest
                        <li>
                            <a class="nav-link" href="{{ route('adminlogin') }}">{{ __('AdminLogin') }}</a>
                        </li>

                        @else
                        @if (Route::has('register'))
                            <!--<li>
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>-->
                        @endif
                          <li class="dropdown user user-menu">
                            <!-- Menu Toggle Button -->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <!-- The user image in the navbar-->
                                <img src="{{ asset("/bower_components/admin-lte/dist/img/Private-School-icon.jpg") }}" class="user-image" alt="User Image"/>
                                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                <span class="hidden-xs">{{ substr(Auth::user()->name,0,20) }}...</span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- The user image in the menu -->
                                <li class="user-header">
                                    <img src="{{ asset("/bower_components/admin-lte/dist/img/Private-School-icon.jpg") }}" class="img-circle" alt="User Image" />
                                    <p>
                                        {{ Auth::user()->name }}
                                        <small>Member since 2019</small>
                                    </p>
                                </li>
                              <!-- Menu Footer-->
                              <li class="user-footer">
                                <div class="pull-left">
                                  <a href="#" class="btn btn-default btn-flat"><i class="fa fa-university"></i>Profile</a>
                                </div>
                                <div class="pull-right">
                                    <a class="btn btn-default btn-flat" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i>
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                              </li>
                            </ul>
                          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
                    @endguest
                </ul>
            </div>
        </nav>
    </header>