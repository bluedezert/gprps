<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        
        <link rel="shortcut icon" href="{{ asset('new logo seal.png') }}" >
    <!-- Styles 
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">-->
    <!-- Bootstrap 3.3.2 -->
    <link href="{{ asset("/bower_components/bootstrap/dist/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="{{ asset("/bower_components/font-awesome/css/font-awesome.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- DataTables -->
    <link href="{{ asset("/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}" rel="stylesheet" type="text/css">
    <!-- Theme style -->
    <link href="{{ asset("/bower_components/admin-lte/dist/css/AdminLTE.css")}}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="{{ asset("/bower_components/admin-lte/dist/css/skins/skin-purple.min.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/bower_components/bootstrap-toggle-master/css/bootstrap-toggle.min.css")}}" rel="stylesheet" type="text/css" />

    <!-- Custom Styles-->
    <link href="{{ asset("/css/custom/private-schools.css")}}" rel="stylesheet" type="text/css" />
</head>
<body class="hold-transition skin-purple sidebar-mini">
    <div class="" id="app" style="background-color: #ecf0f5;">
        @include('layouts.header')
        @yield('content')
    </div>

    <!-- jQuery 2.1.3 --><script src="{{ asset ("/js/app.js") }}" type="text/javascript"></script>
    <script src="{{ asset ("/bower_components/jquery/dist/jquery.min.js") }}"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{ asset ("/bower_components/bootstrap/dist/js/bootstrap.min.js") }}" type="text/javascript"></script>
<!-- DataTables -->
    <script src="{{ asset ("/bower_components/datatables.net/js/jquery.dataTables.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset ("/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset ("/bower_components/admin-lte/dist/js/adminlte.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset ("/bower_components/bootstrap-toggle-master/js/bootstrap-toggle.min.js") }}" type="text/javascript"></script>
    <!-- <script src="{{ asset ("/js/app.js") }}" type="text/javascript"></script>
    Custom JS -->
    <script src="{{ asset ("/js/global/sidebar-menu.js") }}" type="text/javascript"></script>
    <script src="{{ asset ("/js/newapplication/newapplication.js") }}" type="text/javascript"></script>
    <script src="{{ asset ("/js/global/manageusers.js") }}" type="text/javascript"></script>
    <script src="{{ asset ("/js/global/managerequirements.js") }}" type="text/javascript"></script>
</body>

</html>