  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-users"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      
      <!-- Home tab content -->
      <div class="tab-pane active" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Manage Users</h3>
        @if(Auth::user()->officeIDfk<3)
        <ul class="control-sidebar-menu">
          <li>
            <a href="{{ route('manageusers') }}">
              <i class="menu-icon fa fa-users bg-red"></i>
              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Users</h4>
                <p>Shows all users</p>
              </div>
            </a>
          </li>
          @if(Auth::user()->officeIDfk==1 || (Auth::user()->officeIDfk==2 && Auth::user()->inspectionCompositionIdfk==2))
          <li>
            <a href="{{ route('registration') }}">
              <i class="menu-icon fa fa-user bg-red"></i>
              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Register</h4>
                <p>Register New Account</p>
              </div>
            </a>
          </li>
          @endif
        </ul>
        @else
        <ul class="control-sidebar-menu">
          <li>
            <a href="#">
              <i class="menu-icon fa fa-contact bg-red"></i>
              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Contact Regional Office</h4>
                <p>Quality Assurance Division<br>074-1234-456</p>
              </div>
            </a>
          </li>
        </ul>
        @endif
        <!-- /.control-sidebar-menu -->

      </div>
      
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">Audit Trail</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Audit Trails
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              This part is under construction
            </p>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->