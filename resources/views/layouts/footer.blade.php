<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

    </div>
    <!-- Default to the left -->
    <strong>Copyright © 2019 <a href="http://depedcar.ph" target="_blank">DepEd Cordillera</a>.</strong> All rights reserved.
</footer>