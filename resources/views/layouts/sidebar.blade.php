<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset("/bower_components/admin-lte/dist/img/Private-School-icon.jpg") }}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>@if(Auth::user()->name==null)
                    {{ substr(Auth::user()->firstName,0,15) }}...
                    @else
                    {{ substr(Auth::user()->name,0,15) }}...
                    @endif
                </p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a><br>
                
            </div>
        </div>

        <ul class="sidebar-menu" data-widget="tree">
            <li>
                <a href="{{ route('viewprofile', Auth::user()->id) }}"><i class="fa fa-user"></i><span>Profile</span></a>
            </li>
            <li>
                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i><span>Sign out</span></a>
            </li>
        </ul>

        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
                    <span class="input-group-btn">
                        <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                    </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN MENU</li>
            <!-- Optionally, you can add icons to the links -->

            @if(Auth::user()->officeIDfk==3)<!--menu for school-->
                <li class="lmenu">
                    <a href="{{ route('home') }}"><i class="fa fa-dashboard"></i><span>Dashboard</span></a>
                </li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-folder"></i><span>Permit</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li class="lmenu"><a href="{{ route('privateschoolnewapplication', 1) }}"><i class="fa fa-file-text"></i><span>New Application</span></a></li>
                        <li class="lmenu"><a href="{{ route('privateschoolnewapplication', 2) }}"><i class="fa fa-refresh"></i><span>Renewal of Permit</span></a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-registered"></i><span>Recognition</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li class="lmenu"><a href="{{ route('privateschoolnewapplication', 9) }}"><i class="fa fa-registered"></i><span>Recognition</span></a></li>
                        <li class="lmenu"><a href="{{ route('privateschoolnewapplication', 3) }}"><i class="fa fa-file-text"></i><span>Apply For Recognition</span></a></li>
                    </ul>
                </li>

                <li class="treeview">
                    <a href="#"><i class="fa fa-bar-chart"></i><span>Report</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li class="lmenu"><a href="#"><i class="fa fa-dot-circle-o"></i>Some Reports</a></li>
                        <li class="lmenu"><a href="#"><i class="fa fa-circle-o"></i>Some Reports</a></li>
                    </ul>
                </li>
            @elseif(Auth::user()->officeIDfk==2)<!--menu for SDO-->
                <li class="lmenu">
                    <a href="{{ route('home') }}"><i class="fa fa-dashboard"></i><span>Dashboard</span></a>
                </li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-folder"></i><span>Permit</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li class="lmenu"><a href="{{ route('sdoapplicationmonitoring', 1) }}"><i class="fa fa-file-text"></i><span>New Application</span></a></li>
                        <li class="lmenu"><a href="{{ route('sdoapplicationmonitoring', 2) }}"><i class="fa fa-refresh"></i><span>Renewal of Permit</span></a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-registered"></i><span>Recognition</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li class="lmenu"><a href="{{ route('roapplicationmonitoring', 9) }}"><i class="fa fa-registered"></i><span>Recognized Schools</span></a></li>
                        <li class="lmenu"><a href="{{ route('sdoapplicationmonitoring', 3) }}"><i class="fa fa-file-text"></i><span>Application For Recognition</span></a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-bar-chart"></i><span>Report</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li class="lmenu"><a href="#"><i class="fa fa-dot-circle-o"></i>Some Reports</a></li>
                        <li class="lmenu"><a href="#"><i class="fa fa-circle-o"></i>Some Reports</a></li>
                    </ul>
                </li>
            @elseif(Auth::user()->officeIDfk==1)<!--menu for RO-->
                <li class="lmenu">
                    <a href="{{ route('home') }}"><i class="fa fa-dashboard"></i><span>Dashboard</span></a>
                </li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-folder"></i><span>Permit</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li class="lmenu"><a href="{{ route('roapplicationmonitoring', 1) }}"><i class="fa fa-file-text"></i><span>New Application</span></a></li>
                        <li class="lmenu"><a href="{{ route('roapplicationmonitoring', 2) }}"><i class="fa fa-refresh"></i><span>Renewal of Permit</span></a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-registered"></i><span>Recognition</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li class="lmenu"><a href="{{ route('roapplicationmonitoring', 9) }}"><i class="fa fa-registered"></i><span>Recognized Schools</span></a></li>
                        <li class="lmenu"><a href="{{ route('roapplicationmonitoring', 3) }}"><i class="fa fa-file-text"></i><span>Application For Recognition</span></a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-book"></i><span>Manage Requirements</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li class="treeview lmenu"><a href="#"><i class="fa fa-file-text"></i><span>Permit/Recognition<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Requirements</span><i class="fa fa-angle-left pull-right"></i></a>
                            <ul class="treeview-menu">
                                <li class="lmenu"><a href="{{ route('requirements', [1, 3, 'Requirements Displayed']) }}"><i class="fa fa-circle-o"></i>K to 10</a></li>
                                <li class="lmenu"><a href="{{ route('requirements', [1, 4, 'Requirements Displayed']) }}"><i class="fa fa-circle-o"></i>SHS</a></li>
                            </ul>
                        </li>
                        <li class="treeview lmenu"><a href="#"><i class="fa fa-refresh"></i><span>Renewal Requirements</span><i class="fa fa-angle-left pull-right"></i></a>
                            <ul class="treeview-menu">
                                <li class="lmenu"><a href="{{ route('requirements', [2, 3, 'Requirements Displayed']) }}"><i class="fa fa-circle-o"></i>K to 10</a></li>
                                <li class="lmenu"><a href="{{ route('requirements', [2, 4, 'Requirements Displayed']) }}"><i class="fa fa-circle-o"></i>SHS</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-bar-chart"></i><span>Report</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li class="lmenu"><a href="#"><i class="fa fa-file-text"></i>Generate Certificates</a></li>
                        <li class="lmenu"><a href="#"><i class="fa fa-circle-o"></i>Some Reports</a></li>
                    </ul>
                </li>
            @endif

        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>