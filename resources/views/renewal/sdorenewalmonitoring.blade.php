@extends('layouts.app')

@section('content')

@include('layouts.sidebar')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Renewal Monitoring (SDO)
        </h1>
        <!-- You can dynamically generate breadcrumbs here -->
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> GPRPS</a></li>
            <li class="active"> Renewal</li>
        </ol>
    </section>
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Status</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        Some contents
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@include('layouts.footer')
@include('layouts.rightsidebar')
@endsection