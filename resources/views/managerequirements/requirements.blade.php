@extends('layouts.app')

@section('content')

@include('layouts.sidebar')
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        {{ $applicationType }} <small>Mange Requirements</small>
      </h1>
        <!-- You can dynamically generate breadcrumbs here -->
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> GPRPS</a></li>
                <li class="active"> {{ $applicationType }}</li>
        </ol>
    </section>
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-private-schools">
                    <div class="box-header with-border">

                          <div class="row">
                            <div class="col-md-12">
                                <span data-toggle="modal" data-target="#modal-add-main-requirement">
                                    <button class="btn btn-s btn-flat btn-primary TT" data-toggle="tooltip" data-placement="left" title="Add New Requirement"><span class="fa fa-plus"></span> Add New Requirement </button>
                                </span>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                </div>
                              </div>
                            </div>
                    </div>
                    <div class="box-body">
                        <div class="col-xs-12">
                          <div class="box">
                            <div class="box-header">
                              <h3 class="box-title">List of Requirements <em style="font-size: small;">(Shows requirements for the current SY)</em>
                                    <!--<select name="requirementslistsSY" applicationTypeId="{{ $applicationTypeId }}" levelId="{{ $levelId }}" id="requirementslistsSY">
                                        <option value="2019 - 2020">2019 - 2020</option>
                                        <option value="2020 - 2021" selected>2020 - 2021</option>
                                    </select>-->
                              </h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body table-responsive no-padding">
                                <table class="table table-hover table-responsive table-bordered table-condensed">
                                        {!! $requirementslists !!} <!-- used to render text as HTML -->
                                </table>
                            </div>
                            <!-- /.box-body -->
                          </div>
                          <!-- /.box -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- Add Main Requirements Modal -->
        <div class="modal fade" id="modal-add-main-requirement">
          <div class="modal-dialog">
            <div class="modal-content">
              <form method="POST" action="{{ route('addmainrequirement') }}">
                {{ csrf_field() }}
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Requirement</h4>
              </div>
              <div class="modal-body">
                <input type="hidden" value="{{ $applicationTypeId }}" name="applicationTypeId">
                <input type="hidden" value="{{ $levelId }}" name="levelId">
                <input type="hidden" value="{{ $schoolYear }}" name="schoolYear">
                <h5><strong>*Description</strong></h5>
                <textarea placeholder="Enter Description" name="description" id="maindescription_add" class="form-control" required></textarea>
                <div class="col-md-12"><br></div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary bg-purple" id="draft-new-application">Save</button>
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

<!-- Edit Main Requirements Modal -->
        <div class="modal fade" id="modal-edit-main-requirement">
          <div class="modal-dialog">
            <div class="modal-content">
              <form method="POST" action="{{ route('updaterequirement') }}">
                {{ csrf_field() }}
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Update Requirement</h4>
              </div>
              <div class="modal-body">
                <input type="hidden" value="{{ $applicationTypeId }}" name="applicationTypeId">
                <input type="hidden" value="{{ $levelId }}" name="levelId">
                <input type="hidden" value="{{ $schoolYear }}" name="schoolYear">
                <input type="hidden" name="mainReqId" id="mainReqId">
                <input type="hidden" name="subReq1Id" id="subReq1Id" value="0">
                <input type="hidden" name="subReq2Id" id="subReq2Id" value="0">
                <input type="hidden" name="type" id="type" value="main">
                <h5><strong>Toggle Requirement</strong></h5>
                <input type="checkbox" name="isActive" id="maindescriptionToggle" data-toggle="toggle">
                <h5><strong>*Description</strong></h5>
                <textarea placeholder="Enter Description" name="description" id="maindescription" class="form-control" required></textarea>
                <div class="col-md-12">
                    <h5><strong>Add Sub Requirement</strong><em>(Optional)</em></h5>
                    <textarea placeholder="Enter Sub Requirement" name="subdescription" class="form-control"></textarea>
                </div>
                <div class="col-md-12"><br></div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary bg-purple" id="draft-new-application">Save</button>
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

<!-- Edit Main Requirements Modal -->
        <div class="modal fade" id="modal-edit-sub1-requirement">
          <div class="modal-dialog">
            <div class="modal-content">
              <form method="POST" action="{{ route('updaterequirement') }}">
                {{ csrf_field() }}
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Update Requirement</h4>
              </div>
              <div class="modal-body">
                <input type="hidden" value="{{ $applicationTypeId }}" name="applicationTypeId">
                <input type="hidden" value="{{ $levelId }}" name="levelId">
                <input type="hidden" value="{{ $schoolYear }}" name="schoolYear">
                <input type="hidden" name="mainReqId" id="mainReqId_sub1">
                <input type="hidden" name="subReq1Id" id="subReq1Id_sub1">
                <input type="hidden" name="subReq2Id" id="subReq2Id_sub1" value="0">
                <input type="hidden" name="type" id="type" value="sub1">
                <h5><strong>Toggle Requirement</strong></h5>
                <input type="checkbox" name="isActive" id="sub1descriptionToggle" data-toggle="toggle">
                <h5><strong>*Description</strong></h5>
                <textarea placeholder="Enter Description" name="description" id="sub1description" class="form-control" required></textarea>
                <div class="col-md-12">
                    <h5><strong>Add Sub Requirement</strong><em>(Optional)</em></h5>
                    <textarea placeholder="Enter Sub Requirement" name="subdescription" class="form-control"></textarea>
                </div>
                <div class="col-md-12"><br></div>                
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary bg-purple" id="draft-new-application">Save</button>
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

<!-- Edit Main Requirements Modal -->
        <div class="modal fade" id="modal-edit-sub2-requirement">
          <div class="modal-dialog">
            <div class="modal-content">
              <form method="POST" action="{{ route('updaterequirement') }}">
                {{ csrf_field() }}
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Update 2 Requirement</h4>
              </div>
              <div class="modal-body">
                <input type="hidden" value="{{ $applicationTypeId }}" name="applicationTypeId">
                <input type="hidden" value="{{ $levelId }}" name="levelId">
                <input type="hidden" value="{{ $schoolYear }}" name="schoolYear">
                <input type="hidden" name="mainReqId" id="mainReqId_sub2">
                <input type="hidden" name="subReq1Id" id="subReq1Id_sub2">
                <input type="hidden" name="subReq2Id" id="subReq2Id_sub2">
                <input type="hidden" name="type" id="type" value="sub2">
                <h5><strong>Toggle Requirement</strong></h5>
                <input type="checkbox" name="isActive" id="sub2descriptionToggle" data-toggle="toggle">
                <h5><strong>*Description</strong></h5>
                <textarea placeholder="Enter Description" name="description" id="sub2description" class="form-control" required></textarea>
                <div class="col-md-12"><br></div>                   
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary bg-purple" id="draft-new-application">Save</button>
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
<div id="snackbar">{{ $status }}</div>
@include('layouts.footer')
@include('layouts.rightsidebar')
@endsection