@extends('layouts.app')

@section('content')

@include('layouts.sidebar')

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            {{ $title }}<small> User Management</small>
        </h1>
        <!-- You can dynamically generate breadcrumbs here -->
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> GPRPS</a></li>
                <li class="active"> {{ $title }}</li>
        </ol>
    </section>
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-private-schools">
                    <div class="box-header with-border">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                </div>
                              <p>
                                <a href="{{ route('editprofile', $getprofile['id']) }}"><button type="button" class="btn bg-default btn-flat btn-close-registration"><span class="fa fa-close"></span> Close</button></a>
                              </p>
                            </div>
                            <!-- /.col -->
                        </div>
                    </div>
                    <div class="box-body">
                        <form method="POST" id="changePasswordForm" action="{{ route('savenewpassword') }}" onsubmit="return validatePassword();">
                            @csrf
                            <div class="col-xs-4">
                              <div class="box">
                                <div class="box-body">
                                    <div class="" style="padding:0px;">
                                        <img class="profile-user-img img-responsive img-circle" src="{{ asset("/img/user.jpg") }}" alt="User profile picture">
                                    </div>
                                    <p><label><i class="fa fa-key"></i> <strong>Create New Password</strong></label>
                                        <input id="idsdoro" type="hidden" name="id" value="{{ $getprofile['id'] }}" required>
                                        <input id="password" type="password" class="form-control" name="password" required> 
                                    </p>
                                    <p><label><i class="fa fa-key"></i> <strong>Confirm Password</strong></label>
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                    </p>
                                    <div class="text-center" style="padding-top:10px;">
                                        <a href="#">
                                            <button type="submit" class="btn btn-info btn-flat btn-view-user">
                                                <span class="fa fa-save"></span> Change Password
                                            </button>
                                        </a>
                                        <a href="{{ route('viewprofile', $getprofile['id']) }}">
                                            <button type="button" class="btn btn-default btn-flat btn-view-user">
                                                <span class="fa fa-close"></span> Cancel
                                            </button>
                                        </a>               
                                    </div>
                                </div>
                                <!-- /.box-body -->
                              </div>
                              <!-- /.box -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div id="snackbar">{{ $status }}</div>
@include('layouts.footer')
@include('layouts.rightsidebar')
@endsection
