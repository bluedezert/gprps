@extends('layouts.app')

@section('content')

@include('layouts.sidebar')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            {{ $title }}<small> User Management</small>
        </h1>
        <!-- You can dynamically generate breadcrumbs here -->
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> GPRPS</a></li>
                <li class="active"> {{ $title }}</li>
        </ol>
    </section>
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-private-schools">
                    <div class="box-header with-border">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                </div>
                                <p>
                                    @if(Auth::user()->officeIDfk==1 || (Auth::user()->officeIDfk==2 && Auth::user()->inspectionCompositionIdfk==2))
                                    <a href="{{ route('registration') }}" class="btn bg-purple btn-flat btn-new-user"><span class="fa fa-plus"></span> Register New Account</a>
                                    @endif
                                    <a href="{{ route('home') }}"><button type="button" class="btn bg-default btn-flat btn-close-registration"><span class="fa fa-close"></span> Close</button></a>
                                </p>
                            </div>
                            <!-- /.col -->
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="col-xs-12">
                          <div class="box">
                            <div class="box-header">
                              <h3 class="box-title">List of Users</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body table-responsive no-padding">
                                <div>
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-pills" role="tablist">
                                        <li role="presentation" class="active" style="background-color: #f1f1f1;"><a href="#private-school-users" aria-controls="private-school-users" role="tab" data-toggle="tab">Private Schools</a></li>
                                        @if(Auth::user()->officeIDfk==1 || (Auth::user()->officeIDfk==2 && Auth::user()->inspectionCompositionIdfk==2))
                                        <li role="presentation" style="background-color: #f1f1f1;"><a href="#sdo-users" aria-controls="sdo-users" role="tab" data-toggle="tab">SDO Users</a></li>
                                        @endif
                                        @if(Auth::user()->officeIDfk==1)
                                        <li role="presentation" style="background-color: #f1f1f1;"><a href="#ro-users" aria-controls="ro-users" role="tab" data-toggle="tab">RO Users</a></li>
                                        @endif
                                    </ul>
                                    <br>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane table-responsive active" id="private-school-users">
                                            <h2>Private School Accounts</h2>
                                            <table class="table table-hover table-responsive table-bordered table-condensed" id="usersTable">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 150px;">Division</th>
                                                        <th style="width: 100px;">School ID</th>
                                                        <th>School Name</th>
                                                        <th style="width: 200px;">Email</th>
                                                        <th style="width: 200px;">Date Created</th>
                                                        <th style="width: 175px;"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($usersSchool as $user)
                                                        <tr @if($user['isActive']==0) style="background-color: lightgray;" @endif>
                                                            <td>@if($user['schoolsDivision']==''){{ 'Regional Office' }}@else{{ $user['schoolsDivision'] }}@endif</td>
                                                            <td>@if($user['schoolId']==''){{ 'N/A' }}@else{{ $user['schoolId'] }}@endif</td>
                                                            <td>{{ $user['name'] }}</td>
                                                            <td>{{ $user['email'] }}</td>
                                                            <td>{{ $user['created_at'] }}</td>
                                                            <td>
                                                                <a href="{{ route('viewprofile', $user['id']) }}">
                                                                    <button type="button" class="btn btn-xs btn-info btn-flat btn-view-user">
                                                                        <span class="fa fa-eye"></span> View
                                                                    </button>
                                                                </a>
                                                                <a href="{{ route('editprofile', $user['id']) }}">
                                                                    <button type="button" class="btn btn-xs bg-olive btn-flat btn-edit-user">
                                                                        <span class="fa fa-edit"></span> Edit
                                                                    </button>
                                                                </a>
                                                                @if($user['isActive']==1)
                                                                <a href="#" data-toggle="modal" data-target="#modal-deactivate-user">
                                                                    <button type="button" class="btn btn-xs btn-danger btn-flat btn-deactivate-user" id="{{ $user['id'] }}" isActive="{{ $user['isActive'] }}">
                                                                        <span class="fa fa-ban"></span> Deactivate
                                                                    </button>
                                                                </a>
                                                                @else
                                                                <a href="#" data-toggle="modal" data-target="#modal-deactivate-user">
                                                                    <button type="button" class="btn btn-xs btn-warning btn-flat btn-deactivate-user" id="{{ $user['id'] }}" isActive="{{ $user['isActive'] }}">
                                                                        <span class="fa fa-check"></span> Activate
                                                                    </button>
                                                                </a>
                                                                @endif                                            
                                                            </td>
                                                        </tr>

                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div role="tabpanel" class="tab-pane table-responsive" id="sdo-users">
                                            <h2>SDO Accounts</h2>
                                            <table class="table table-hover table-responsive table-bordered table-condensed" id="usersTableSDO">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 150px;">Division</th>
                                                        <th style="width: 100px;">Employee ID</th>
                                                        <th>Name</th>
                                                        <th style="width: 200px;">Email</th>
                                                        <th style="width: 200px;">Date Created</th>
                                                        <th style="width: 175px;"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($usersSdo as $user)
                                                        <tr @if($user['isActive']==0) style="background-color: lightgray;" @endif>
                                                            <td>@if($user['schoolsDivision']==''){{ 'Regional Office' }}@else{{ $user['schoolsDivision'] }}@endif</td>
                                                            <td>@if($user['employeeID']==''){{ 'N/A' }}@else{{ $user['employeeID'] }}@endif</td>
                                                            <td>{{ $user['firstName'] . ' ' . $user['lastName'] }}</td>
                                                            <td>{{ $user['email'] }}</td>
                                                            <td>{{ $user['created_at'] }}</td>
                                                            <td>
                                                                <a href="{{ route('viewprofile', $user['id']) }}">
                                                                    <button type="button" class="btn btn-xs btn-info btn-flat btn-view-user">
                                                                        <span class="fa fa-eye"></span> View
                                                                    </button>
                                                                </a>  
                                                                <a href="{{ route('editprofile', $user['id']) }}">
                                                                    <button type="button" class="btn btn-xs bg-olive btn-flat btn-edit-user">
                                                                        <span class="fa fa-edit"></span> Edit
                                                                    </button>
                                                                </a>
                                                                @if($user['isActive']==1)
                                                                <a href="#" data-toggle="modal" data-target="#modal-deactivate-user">
                                                                    <button type="button" class="btn btn-xs btn-danger btn-flat btn-deactivate-user" id="{{ $user['id'] }}" isActive="{{ $user['isActive'] }}">
                                                                        <span class="fa fa-ban"></span> Deactivate
                                                                    </button>
                                                                </a>
                                                                @else
                                                                <a href="#" data-toggle="modal" data-target="#modal-deactivate-user">
                                                                    <button type="button" class="btn btn-xs btn-warning btn-flat btn-deactivate-user" id="{{ $user['id'] }}" isActive="{{ $user['isActive'] }}">
                                                                        <span class="fa fa-check"></span> Activate
                                                                    </button>
                                                                </a>
                                                                @endif                                              
                                                            </td>
                                                        </tr>

                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div role="tabpanel" class="tab-pane table-responsive" id="ro-users">
                                            <h2>Regional Office Accounts</h2>
                                            <table class="table table-hover table-responsive table-bordered table-condensed" id="usersTableRO">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 150px;">Division</th>
                                                        <th style="width: 100px;">Employee ID</th>
                                                        <th>Name</th>
                                                        <th style="width: 200px;">Email</th>
                                                        <th style="width: 200px;">Date Created</th>
                                                        <th style="width: 175px;"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($usersRo as $user)
                                                        <tr @if($user['isActive']==0) style="background-color: lightgray;" @endif>
                                                            <td>@if($user['schoolsDivision']==''){{ 'Regional Office' }}@else{{ $user['schoolsDivision'] }}@endif</td>
                                                            <td>@if($user['employeeID']==''){{ 'N/A' }}@else{{ $user['employeeID'] }}@endif</td>
                                                            <td>{{ $user['firstName'] . ' ' . $user['lastName'] }}</td>
                                                            <td>{{ $user['email'] }}</td>
                                                            <td>{{ $user['created_at'] }}</td>
                                                            <td>
                                                                <a href="{{ route('viewprofile', $user['id']) }}">
                                                                    <button type="button" class="btn btn-xs btn-info btn-flat btn-view-user">
                                                                        <span class="fa fa-eye"></span> View
                                                                    </button>
                                                                </a>
                                                                <a href="{{ route('editprofile', $user['id']) }}">
                                                                    <button type="button" class="btn btn-xs bg-olive btn-flat btn-edit-user">
                                                                        <span class="fa fa-edit"></span> Edit
                                                                    </button>
                                                                </a>
                                                                @if($user['isActive']==1)
                                                                <a href="#" data-toggle="modal" data-target="#modal-deactivate-user">
                                                                    <button type="button" class="btn btn-xs btn-danger btn-flat btn-deactivate-user" id="{{ $user['id'] }}" isActive="{{ $user['isActive'] }}">
                                                                        <span class="fa fa-ban"></span> Deactivate
                                                                    </button>
                                                                </a>
                                                                @else
                                                                <a href="#" data-toggle="modal" data-target="#modal-deactivate-user">
                                                                    <button type="button" class="btn btn-xs btn-warning btn-flat btn-deactivate-user" id="{{ $user['id'] }}" isActive="{{ $user['isActive'] }}">
                                                                        <span class="fa fa-check"></span> Activate
                                                                    </button>
                                                                </a>
                                                                @endif                                             
                                                            </td>
                                                        </tr>

                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                          </div>
                          <!-- /.box -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div id="snackbar">{{ $status }}</div>

<!-- Modal for adding new user -->
        <div class="modal fade" id="modal-deactivate-user">
          <div class="modal-dialog">
            <div class="modal-content">
              <form method="POST" action="{{ route('deactivateuser') }}">
                {{ csrf_field() }}
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Deactivate User</h4>
                  </div>
                  <div class="modal-body">
                    <input type="hidden" name="userid" id="userid">
                    <input type="hidden" name="isActive" id="isActive">
                    <div class="form-group row">
                        <label for="name" class="col-md-12 col-form-label text-md-right">Are you sure you want to deactivate the following user?</label>
                    </div>
                    <div class="form-group row mb-1">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-md btn-danger btn-flat btn-isactive">
                                <span class="fa fa-ban"></span> Deactivate
                            </button>
                        </div>
                    </div>
                </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
@include('layouts.footer')
@include('layouts.rightsidebar')
@endsection