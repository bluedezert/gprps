@extends('layouts.app')

@section('content')

@include('layouts.sidebar')

<div class="content-wrapper">
    <section class="content-header">
        <h1>{{ $title }}<small> User Management</small>
        </h1>
        <!-- You can dynamically generate breadcrumbs here -->
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> GPRPS</a></li>
                <li class="active"> {{ $title }}</li>
        </ol>
    </section>
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-private-schools">
                    <div class="box-header with-border">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                </div>
                              <p>
                                @if(Auth::user()->officeIDfk<3)
                                <a href="{{ route('manageusers') }}"><button type="button" class="btn bg-default btn-flat btn-close-registration"><span class="fa fa-close"></span> Close</button></a>
                                @endif
                              </p>
                            </div>
                            <!-- /.col -->
                        </div>
                    </div>
                @if($getprofile['officeIDfk']<3)
                    <div class="box-body">
                        <form method="POST" id="editSdoRoForm" action="{{ route('savesdoroprofile', $getprofile['id']) }}">
                            @csrf
                            <div class="col-xs-4">
                              <div class="box">
                                <div class="box-body">
                                    <div class="" style="padding:0px;">
                                        <img class="profile-user-img img-responsive img-circle" src="{{ asset("/img/user.jpg") }}" alt="User profile picture">
                                    </div>
                                    <p><label><i class="fa fa-user"></i> <strong>Employee ID</strong></label>
                                        <input id="idsdoro" type="hidden" name="id" value="{{ $getprofile['id'] }}" required>
                                        <input id="employeeID" type="text" class="form-control{{ $errors->has('employeeID') ? ' is-invalid' : '' }}" name="employeeID" value="{{ $getprofile['employeeID'] }}" required readonly="true"> 
                                    </p>
                                    <p><label><i class="fa fa-user"></i> <strong>First Name</strong></label>
                                        <input id="firstName" type="text" class="form-control{{ $errors->has('firstName') ? ' is-invalid' : '' }}" name="firstName" value="{{ $getprofile['firstName'] }}" required> 
                                    </p>
                                    <p><label><i class="fa fa-user"></i> <strong>Middle Name</strong></label>
                                        <input id="middleName" type="text" class="form-control{{ $errors->has('middleName') ? ' is-invalid' : '' }}" name="middleName" value="{{ $getprofile['middleName'] }}" required> 
                                    </p>
                                    <p><label><i class="fa fa-user"></i> <strong>Last Name</strong></label>
                                        <input id="lastName" type="text" class="form-control{{ $errors->has('lastName') ? ' is-invalid' : '' }}" name="lastName" value="{{ $getprofile['lastName'] }}" required> 
                                    </p>
                                    <div class="text-center" style="padding-top:10px;">
                                        <a href="#">
                                            <button type="submit" class="btn btn-info btn-flat btn-view-user">
                                                <span class="fa fa-save"></span> Save Profile
                                            </button>
                                        </a>
                                        <a href="{{ route('changepassword', $getprofile['id']) }}">
                                            <button type="button" class="btn btn-primary btn-flat btn-view-user">
                                                <span class="fa fa-lock"></span> Change Password
                                            </button>
                                        </a>               
                                    </div>
                                </div>
                                <!-- /.box-body -->
                              </div>
                              <!-- /.box -->
                            </div>
                            <div class="col-xs-8">
                              <div class="box">
                                <div class="box-header">
                                  <h3 class="box-title"><span class="text-uppercase">{{ $getprofile['firstName'] }}</span>'s Profile</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="col-md-4" style="padding:0px;">
                                        <div class="box-body">
                                            <strong><i class="fa fa-envelope"></i> Email Address</strong>
                                            <p><input id="emailsdoro" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $getprofile['email'] }}" required></p>
                                            <strong><i class="fa fa-mobile"></i> Mobile Number</strong>
                                            <p><input id="mobileNumbersdoro" type="text" class="form-control{{ $errors->has('mobileNumber') ? ' is-invalid' : '' }}" name="mobileNumber" value="{{ $getprofile['mobileNumber'] }}" required></p>
                                        </div>
                                    </div>
                                    <div class="col-md-4" style="padding:0px;">
                                        <div class="box-body">
                                            <strong><i class="fa fa-venus"></i> Sex</strong>
                                            <p>
                                                <select required class="form-control" name="sex" id="sex">
                                                    <option value="Male" {{ $getprofile['sex']=="Male" ? ' selected' : '' }}>Male</option>
                                                    <option value="Female" {{ $getprofile['sex']=="Female" ? ' selected' : '' }}>Female</option>
                                                </select>
                                            </p>
                                            <strong><i class="fa fa-calendar-o"></i> Date of Birth</strong>
                                            <p><input id="birthDate" type="date" class="form-control{{ $errors->has('birthDate') ? ' is-invalid' : '' }}" name="birthDate" value="{{ $getprofile['birthDate'] }}" required></p>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                              </div>
                              <!-- /.box -->
                            </div>
                            <div class="col-xs-8">
                              <div class="box">
                                <div class="box-header">
                                    @if($getprofile['officeIDfk']==1)
                                    <h3 class="box-title"><span>Regional Validation Team Designation</h3>
                                    @elseif($getprofile['officeIDfk']==2)
                                    <h3 class="box-title"><span>Division Assessment and Inspection Team Designation</h3>
                                    @endif
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="col-md-4" style="padding:0px;">
                                        <div class="box-body">
                                            
                                                <!--<input id="designationsdoro" type="text" class="form-control{{ $errors->has('designation') ? ' is-invalid' : '' }}" name="designation" value="{{ $getprofile['designation'] }}" required> -->
                                                @if(Auth::user()->officeIDfk==1 || (Auth::user()->officeIDfk==2 && Auth::user()->inspectionCompositionIdfk==2))
                                                <p><label><i class="fa fa-user"></i> <strong>Designation</strong></label>
                                                    <select required class="form-control" name="inspectionCompositionId" id="inspectionCompositionId" 
                                                    {{ (Auth::user()->officeIDfk==1 || (Auth::user()->officeIDfk==2 && Auth::user()->inspectionCompositionIdfk==2)) ? '' : ' disabled="true"' }}>
                                                        <option value=""> </option>
                                                    @foreach($inspectionComposition as $inspectionCompo)
                                                        <option value="{{ $inspectionCompo['inspectionCompositionId'] }}" {{ $getprofile['inspectionCompositionIdfk']==$inspectionCompo['inspectionCompositionId'] ? ' selected' : '' }}>
                                                        {{ $inspectionCompo['CM']=='C' ? 'Chair'. ' - ' . $inspectionCompo['description'] : 'Member' . ' - ' . $inspectionCompo['description'] }}</option>
                                                    @endforeach
                                                    </select>
                                                </p>
                                                @elseif(Auth::user()->officeIDfk==2)
                                                <p><label><i class="fa fa-user"></i> <strong>Designation</strong></label>
                                                    <input id="inspectionCompositionId" type="hidden" name="inspectionCompositionId" value="{{ $getprofile['inspectionCompositionId'] }}" required>
                                                    <input id="inspectionCompositionIdsdoro" type="text" class="form-control" name="inspectionCompositionIdsdoro" value="{{ $getprofile['CM']=='C' ? 'Chair'. ' - ' . $getprofile['description'] : 'Member' . ' - ' . $getprofile['description'] }}" required disabled="true">
                                                </p>
                                                @endif

                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-4" style="padding:0px;">
                                        <div class="box-body">
                                            @if(Auth::user()->officeIDfk==2)
                                            <p>
                                                <label><i class="fa fa-building-o"></i> <strong>Schools Division</strong></label>
                                                <input id="schoolsDivisionIdsdoro" type="hidden" class="form-control{{ $errors->has('schoolsDivisionId') ? ' is-invalid' : '' }}" name="schoolsDivisionId" value="{{ $getprofile['schoolsDivisionID'] }}" required>
                                                <input id="schoolsDivisionsdoro" type="text" class="form-control{{ $errors->has('schoolsDivision') ? ' is-invalid' : '' }}" name="schoolsDivision" value="{{ $getprofile['schoolsDivision'] }}" required disabled="true">
                                            </p>
                                            @endif
                                            @if(Auth::user()->officeIDfk==1)
                                            <p>
                                                <label><i class="fa fa-building-o"></i> <strong>Schools Division</strong></label>
                                                <select required class="form-control" name="schoolsDivisionId" id="schoolsDivisionId">
                                                    <option value=""> </option>
                                                @foreach($schoolsDivisions as $schoolsDivision)
                                                    <option value="{{ $schoolsDivision['schoolsDivisionID'] }}" {{ $getprofile['schoolsDivisionID']==$schoolsDivision['schoolsDivisionID'] ? ' selected' : '' }}>{{ $schoolsDivision['schoolsDivision'] }}</option>
                                                @endforeach
                                                    <option value="0" {{ $getprofile['schoolsDivisionIdfk']==0 ? ' selected' : '' }}>Regional Office</option>
                                                </select>
                                            </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                              </div>
                              <!-- /.box -->
                            </div>
                        </form>
                    </div>
                @endif

                @if($getprofile['officeIDfk']==3)
                    <div class="box-body">
                        <form method="POST" id="editSchoolProfileForm" action="{{ route('saveschoolprofile') }}">
                            @csrf
                            <div class="col-xs-4">
                              <div class="box">
                                <div class="box-body">
                                    <div class="" style="padding:0px;">
                                        <img class="profile-user-img img-responsive img-circle" src="{{ asset("/bower_components/admin-lte/dist/img/Private-School-icon.jpg") }}" alt="User profile picture">
                                    </div>    
                                    <p><label><i class="fa fa-university"></i> <strong>School Name</strong></label>
                                        <input id="id" type="hidden" name="id" value="{{ $getprofile['id'] }}" required>
                                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $getprofile['name'] }}" required></p>
                                    <p>
                                        <label><i class="fa fa-flag"></i> <strong>School Id</strong></label>
                                        <input id="schoolId" type="text" class="form-control{{ $errors->has('schoolId') ? ' is-invalid' : '' }}" name="schoolId" value="{{ $getprofile['schoolId'] }}" required>
                                    </p>
                                    <p>
                                        <label><i class="fa fa-map-marker"></i> <strong>Address (Exept Province Name)</strong></label>
                                        <input id="schoolAddress" type="text" class="form-control" name="schoolAddress" value="{{ $getprofile['schoolAddress'] }}" placeholder="Address" required>
                                    </p> 
                                    <p>
                                        <label><i class="fa fa-building-o"></i> <strong>Schools Division</strong></label>
                                        <input id="schoolsDivisionId" type="text" class="form-control{{ $errors->has('schoolsDivisionId') ? ' is-invalid' : '' }}" name="schoolsDivisionId" value="{{ $getprofile['schoolsDivision'] }}" required readonly="true">
                                    </p>
                                    <div class="text-center" style="padding-top:10px;">
                                        <a href="#">
                                            <button type="submit" class="btn btn-info btn-flat btn-view-user">
                                                <span class="fa fa-save"></span> Save Profile
                                            </button>
                                        </a>
                                        <a href="{{ route('changepassword', $getprofile['id']) }}">
                                            <button type="button" class="btn btn-primary btn-flat btn-view-user">
                                                <span class="fa fa-lock"></span> Change Password
                                            </button>
                                        </a>                
                                    </div>
                                </div>
                                <!-- /.box-body -->
                              </div>
                              <!-- /.box -->
                            </div>
                            <div class="col-xs-8">
                              <div class="box">
                                <div class="box-header">
                                  <h3 class="box-title"><span class="text-uppercase">{{ $getprofile['name'] }}</span>'s Profile</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="col-md-4" style="padding:0px;">
                                        <div class="box-body">
                                            <strong><i class="fa fa-envelope"></i> Email Address</strong>
                                            <p><input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $getprofile['email'] }}" required></p>

                                            <strong><i class="fa fa-mobile"></i> Mobile Number</strong>
                                            <p><input id="mobileNumber" type="text" class="form-control{{ $errors->has('mobileNumber') ? ' is-invalid' : '' }}" name="mobileNumber" value="{{ $getprofile['mobileNumber'] }}" required></p>
                                        </div>
                                    </div>
                                    <div class="col-md-4" style="padding:0px;">
                                        <div class="box-body">
                                            <strong><i class="fa fa-user"></i> School Head</strong>
                                            <p><input id="schoolHead" type="text" class="form-control{{ $errors->has('schoolHead') ? ' is-invalid' : '' }}" name="schoolHead" value="{{ $getprofile['schoolHead'] }}" required></p>

                                            <strong><i class="fa fa-calendar-o"></i> Date Established</strong>
                                            <p><input id="dateEstablished" type="date" class="form-control{{ $errors->has('dateEstablished') ? ' is-invalid' : '' }}" name="dateEstablished" value="{{ $getprofile['dateEstablished'] }}" required></p>
                                        </div>
                                    </div>
                                    <div class="col-md-4" style="padding:0px;">
                                        <div class="box-body">
                                            <strong><i class="fa fa-user"></i> Designation</strong>
                                            <p><input id="designation" type="text" class="form-control{{ $errors->has('designation') ? ' is-invalid' : '' }}" name="designation" value="{{ $getprofile['designation'] }}" required></p>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                              </div>
                              <!-- /.box -->
                            </div>
                        </form>
                    </div>
                @endif

                </div>
            </div>
        </div>
    </section>
</div>
<div id="snackbar">{{ $status }}</div>
@include('layouts.footer')
@include('layouts.rightsidebar')
@endsection
