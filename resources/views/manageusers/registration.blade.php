@extends('layouts.app')

@section('content')

@include('layouts.sidebar')

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            {{ $title }}<small> User Management</small>
        </h1>
        <!-- You can dynamically generate breadcrumbs here -->
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> GPRPS</a></li>
                <li class="active"> {{ $title }}</li>
        </ol>
    </section>
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-private-schools">
                    <div class="box-header with-border">

                        <h2></h2>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                              <p>
                                <a href="{{ route('manageusers') }}"><button type="button" class="btn bg-default btn-flat btn-close-registration"><span class="fa fa-close"></span> Close</button></a>
                              </p>
                            </div>
                            <!-- /.col -->
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="col-xs-9">
                          <div class="box">
                            <div class="box-header">
                              <h3 class="box-title">Registration</h3>
                              <em><br>All Fields are required</em>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <form method="POST" id="registrationForm" action="{{ route('registeruser') }}" onsubmit="return validateRegistration();">
                                    @csrf

                                    <div class="form-group row">
                                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Office') }}</label>

                                        <div class="col-md-6">
                                            <select id="officeId" required name="officeId" class="form-control">
                                                <option value="0" {{ old('officeId')==0 ? ' selected' : '' }}>Select an Office</option>
                                                @if(Auth::user()->officeIDfk==1)
                                                <option value="1" {{ old('officeId')==1 ? ' selected' : '' }}>Regional Office</option>
                                                @endif
                                                <option value="2" {{ old('officeId')==2 ? ' selected' : '' }}>SDO</option>
                                                <option value="3" {{ old('officeId')==3 ? ' selected' : '' }}>Private School</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row for-school">
                                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('School ID') }}</label>
                                        <div class="col-md-6">
                                            <input id="schoolId" type="text" class="form-control{{ $errors->has('schoolId') ? ' is-invalid' : '' }}" name="schoolId" value="{{ old('schoolId') }}" required>

                                            @if ($errors->has('schoolId'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('schoolId') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <br>
                                        <br>
                                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('School Name') }}</label>
                                        <div class="col-md-6">
                                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required>

                                            @if ($errors->has('name'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <br>
                                        <br>
                                        <label for="schoolsDivisionId" class="col-md-4 col-form-label text-md-right">{{ __('Division') }}</label>
                                        <div class="col-md-6">
                                            @if(Auth::user()->officeIDfk==2)
                                            <p>
                                                <input id="schoolsDivisionId" type="hidden" class="form-control{{ $errors->has('schoolsDivisionId') ? ' is-invalid' : '' }}" name="schoolsDivisionId" value="{{ Auth::user()->schoolsDivisionIdfk }}" required>
                                                <input id="schoolsDivision" type="text" class="form-control{{ $errors->has('schoolsDivision') ? ' is-invalid' : '' }}" name="schoolsDivision" value="{{ $schoolsDivision }}" required readonly="true">
                                            </p>
                                            @endif
                                            @if(Auth::user()->officeIDfk==1)
                                            <p>
                                                <select required class="form-control" name="schoolsDivisionId" id="schoolsDivisionId">
                                                    <option value=""> </option>
                                                @foreach($schoolsDivisions as $schoolsDivision)
                                                    <option value="{{ $schoolsDivision['schoolsDivisionID'] }}" {{ old('schoolsDivisionId')==$schoolsDivision['schoolsDivisionID'] ? ' selected' : '' }}>{{ $schoolsDivision['schoolsDivision'] }}</option>
                                                @endforeach
                                                    <option value="0">Regional Office</option>
                                                </select>
                                            </p>
                                            @endif
                                        </div>
                                        <br>
                                        <br>
                                        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('EMail Address - (Prefferably GMail)') }}</label>
                                        <div class="col-md-6">
                                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row for-ro-sdo">
                                        <label for="employeeID" class="col-md-4 col-form-label text-md-right">{{ __('Employee ID') }}</label>
                                        <div class="col-md-6">
                                            <input id="employeeID" type="text" class="form-control{{ $errors->has('employeeID') ? ' is-invalid' : '' }}" name="employeeID" value="{{ old('employeeID') }}" required>

                                            @if ($errors->has('employeeID'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('employeeID') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <br>
                                        <br>
                                        <label for="firstName" class="col-md-4 col-form-label text-md-right">{{ __('First Name') }}</label>
                                        <div class="col-md-6">
                                            <input id="firstName" type="text" class="form-control{{ $errors->has('firstName') ? ' is-invalid' : '' }}" name="firstName" value="{{ old('firstName') }}" required>

                                            @if ($errors->has('firstName'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('firstName') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <br>
                                        <br>
                                        <label for="middleName" class="col-md-4 col-form-label text-md-right">{{ __('Middle Name') }}</label>
                                        <div class="col-md-6">
                                            <input id="middleName" type="text" class="form-control{{ $errors->has('middleName') ? ' is-invalid' : '' }}" name="middleName" value="{{ old('middleName') }}" required>

                                            @if ($errors->has('middleName'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('middleName') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <br>
                                        <br>
                                        <label for="lastName" class="col-md-4 col-form-label text-md-right">{{ __('Last Name') }}</label>
                                        <div class="col-md-6">
                                            <input id="lastName" type="text" class="form-control{{ $errors->has('lastName') ? ' is-invalid' : '' }}" name="lastName" value="{{ old('lastName') }}" required>

                                            @if ($errors->has('lastName'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('lastName') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <br>
                                        <br>
                                        <label for="schoolsDivisionId_ro_sdo" class="col-md-4 col-form-label text-md-right">{{ __('Division') }}</label>
                                        <div class="col-md-6">
                                            @if(Auth::user()->officeIDfk==2)
                                            <p>
                                                <input id="schoolsDivisionId_ro_sdo" type="hidden" class="form-control{{ $errors->has('schoolsDivisionId') ? ' is-invalid' : '' }}" name="schoolsDivisionId_ro_sdo" value="{{ Auth::user()->schoolsDivisionIdfk }}" required>
                                                <input id="schoolsDivisionsdoro" type="text" class="form-control{{ $errors->has('schoolsDivision') ? ' is-invalid' : '' }}" name="schoolsDivision" value="{{ $schoolsDivision }}" required readonly="true">
                                            </p>
                                            @endif
                                            @if(Auth::user()->officeIDfk==1)
                                            <p>
                                                <select required class="form-control" name="schoolsDivisionId_ro_sdo" id="schoolsDivisionId_ro_sdo">
                                                    <option value=""> </option>
                                                @foreach($schoolsDivisions as $schoolsDivision)
                                                    <option value="{{ $schoolsDivision['schoolsDivisionID'] }}" {{ old('schoolsDivisionId_ro_sdo')==$schoolsDivision['schoolsDivisionID'] ? ' selected' : '' }}>{{ $schoolsDivision['schoolsDivision'] }}</option>
                                                @endforeach
                                                    <option value="0">Regional Office</option>
                                                </select>
                                            </p>
                                            @endif

                                        </div>
                                        <br>
                                        <br>
                                        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('DepEd Email Address') }}</label>

                                        <div class="col-md-6">
                                            <input id="email_ro_sdo" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email_ro_sdo" value="{{ old('email_ro_sdo') }}" required>

                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row passwords">
                                        <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                                        <div class="col-md-6">
                                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                            @if ($errors->has('password'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <br>
                                        <br>
                                        <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                        <div class="col-md-6">
                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                        </div>
                                    </div>


                                    <div class="form-group row mb-0">
                                        <div class="col-md-6 offset-md-4">
                                            <button type="submit" class="btn btn-primary">
                                                {{ __('Register') }}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.box-body -->
                          </div>
                          <!-- /.box -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div id="snackbar">{{ $status }}</div>
@include('layouts.footer')
@include('layouts.rightsidebar')
@endsection
