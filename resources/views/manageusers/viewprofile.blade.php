@extends('layouts.app')

@section('content')

@include('layouts.sidebar')

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            {{ $title }}<small> User Management</small>
        </h1>
        <!-- You can dynamically generate breadcrumbs here -->
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> GPRPS</a></li>
                <li class="active"> {{ $title }}</li>
        </ol>
    </section>
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-private-schools">
                    <div class="box-header with-border">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                </div>
                              <p>
                                @if(Auth::user()->officeIDfk<3)
                                <a href="{{ route('manageusers') }}"><button type="button" class="btn bg-default btn-flat btn-close-registration"><span class="fa fa-close"></span> Close</button></a>
                                @endif
                              </p>
                            </div>
                            <!-- /.col -->
                        </div>
                    </div>
                @if($getprofile['officeIDfk']<3)
                    <div class="box-body">
                        <div class="col-xs-4">
                          <div class="box">
                            <div class="box-body">
                                <div class="" style="padding:0px;">
                                    <img class="profile-user-img img-responsive img-circle" src="{{ asset("/img/user.jpg") }}" alt="User profile picture">
                                </div>    
                                <h3 class="text-uppercase text-center">{{ $getprofile['firstName'] }} {{ $getprofile['middleName'] }} {{ $getprofile['lastName'] }}</h3>
                                <p class="text-muted text-center"><span style="font-size: x-large;">{{ $getprofile['employeeID'] }}</span><br>{{ $getprofile['designation'] }}<br><b>Schools Division Office of {{ $getprofile['schoolsDivision'] }}</b></p>
                                <div class="text-center" style="padding-top:10px;">
                                    <a href="{{ route('editprofile', $getprofile['id']) }}">
                                        <i class="fa fa-edit"></i> Edit Profile
                                    </a> &nbsp; &nbsp;
                                    <a href="{{ route('changepassword', $getprofile['id']) }}">
                                        <i class="fa fa-key"></i> Change Password
                                    </a>                 
                                </div>
                            </div>
                            <!-- /.box-body -->
                          </div>
                          <!-- /.box -->
                        </div>
                        <div class="col-xs-8">
                          <div class="box">
                            <div class="box-header">
                              <h3 class="box-title"><span class="text-uppercase">{{ $getprofile['firstName'] }}</span>'s Profile</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="col-md-4" style="padding:0px;">
                                    <div class="box-body">
                                        <strong><i class="fa fa-envelope"></i> Email Address</strong>
                                        <p class="text-muted">{{ $getprofile['email'] }}</p>
                                        <strong><i class="fa fa-mobile"></i> Mobile Number</strong>
                                        <p class="text-muted">{{ $getprofile['mobileNumber'] }}</p>
                                    </div>
                                </div>
                                <div class="col-md-4" style="padding:0px;">
                                    <div class="box-body">
                                        <strong><i class="fa fa-venus"></i> Sex</strong>
                                        <p class="text-muted">{{ $getprofile['sex'] }}</p>
                                        <strong><i class="fa fa-calendar-o"></i> Date of Birth</strong>
                                        <p class="text-muted">{{ $getprofile['birthDate'] }}</p>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                          </div>
                          <!-- /.box -->
                        </div>
                        <div class="col-xs-8">
                          <div class="box">
                            <div class="box-header">
                                @if($getprofile['officeIDfk']==1)
                                <h3 class="box-title"><span>Regional Validation Team Designation</h3>
                                @elseif($getprofile['officeIDfk']==2)
                                <h3 class="box-title"><span>Division Assessment and Inspection Team Designation</h3>
                                @endif
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="col-md-4" style="padding:0px;">
                                    <div class="box-body">
                                        <strong><i class="fa fa-envelope"></i> Designation</strong>
                                        <p class="text-muted">{{ $getprofile['CM']=='C' ? 'Chair'. ' - ' . $getprofile['description'] : 'Member' . ' - ' . $getprofile['description'] }}</p>
                                    </div>
                                </div>
                                <div class="col-md-4" style="padding:0px;">
                                    <div class="box-body">
                                        <strong><i class="fa fa-venus"></i> Division</strong>
                                        <p class="text-muted">{{ $getprofile['schoolsDivision'] }}</p>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                          </div>
                          <!-- /.box -->
                        </div>
                    </div>
                @endif

                @if($getprofile['officeIDfk']==3)
                    <div class="box-body">
                        <div class="col-xs-4">
                          <div class="box">
                            <div class="box-body">
                                <div class="" style="padding:0px;">
                                    <img class="profile-user-img img-responsive img-circle" src="{{ asset("/bower_components/admin-lte/dist/img/Private-School-icon.jpg") }}" alt="User profile picture">
                                </div>    
                                <h3 class="text-uppercase text-center">{{ $getprofile['name'] }}</h3>
                                <p class="text-muted text-center"><span style="font-size: x-large;">{{ $getprofile['schoolId'] }}</span><br>Private School<br><b>Schools Division Office of {{ $getprofile['schoolsDivision'] }}</b></p>
                                <div class="text-center" style="padding-top:10px;">
                                    <a href="{{ route('editprofile', $getprofile['id']) }}">
                                        <i class="fa fa-edit"></i> Edit School Profile
                                    </a> &nbsp; &nbsp;
                                    <a href="{{ route('changepassword', $getprofile['id']) }}">
                                        <i class="fa fa-key"></i> Change Password
                                    </a>                 
                                </div>
                            </div>
                            <!-- /.box-body -->
                          </div>
                          <!-- /.box -->
                        </div>
                        <div class="col-xs-8">
                          <div class="box">
                            <div class="box-header">
                              <h3 class="box-title"><span class="text-uppercase">{{ $getprofile['name'] }}</span>'s Profile</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="col-md-4" style="padding:0px;">
                                    <div class="box-body">
                                        <strong><i class="fa fa-envelope"></i> Email Address</strong>
                                        <p class="text-muted">{{ $getprofile['email'] }}</p>
                                        <strong><i class="fa fa-mobile"></i> Mobile Number</strong>
                                        <p class="text-muted">{{ $getprofile['mobileNumber'] }}</p>
                                    </div>
                                </div>
                                <div class="col-md-4" style="padding:0px;">
                                    <div class="box-body">
                                        <strong><i class="fa fa-user"></i> School Head</strong>
                                        <p class="text-muted">{{ $getprofile['schoolHead'] }} - {{ $getprofile['designation'] }}</p>
                                        <strong><i class="fa fa-calendar-o"></i> Date Established</strong>
                                        <p class="text-muted">{{ $getprofile['dateEstablished'] }}</p>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                          </div>
                          <!-- /.box -->
                        </div>
                    </div>
                @endif

                </div>
            </div>
        </div>
    </section>
</div>
<div id="snackbar">{{ $status }}</div>
@include('layouts.footer')
@include('layouts.rightsidebar')
@endsection
