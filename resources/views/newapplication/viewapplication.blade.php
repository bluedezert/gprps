@extends('layouts.app')

@section('content')

@include('layouts.sidebar')
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        @if($applicationInfo["applicationTypeIdfk"]==1)
          New Application of Permit
        @elseif($applicationInfo["applicationTypeIdfk"]==2)
          Permit Renewal
        @elseif($applicationInfo["applicationTypeIdfk"]==3)
          Application of Recognition
        @endif
        <small>[Cert Number: <?php echo $applicationInfo["governmentPRNumber"]; ?>]</small>
      </h1>
        <!-- You can dynamically generate breadcrumbs here -->
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> GPRPS</a></li>
            <li class="active">         
        @if($applicationInfo["applicationTypeIdfk"]==1)
          New Application of Permit
        @elseif($applicationInfo["applicationTypeIdfk"]==2)
          Permit Renewal
        @elseif($applicationInfo["applicationTypeIdfk"]==3)
          Application of Recognition
        @endif</li>
        </ol>
    </section>
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-private-schools">
                    <div class="box-header with-border">
                        <h2></h2>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <form>
                            <div class="row">
                                <div class="col-md-12">
                                  <p>









<!------- Buttons for School---------------------------------------------------------------->
                                    @if($applicationInfo['applicationStatusIdfk']==1 && Auth::user()->officeIDfk==3)
                                      <!--<a href="{{ route('submitapplication', [$prStatusId, $applicationTypeId]) }}">-->
                                      <button type="button" class="btn bg-purple btn-flat btn-submit-application TT" data-toggle="modal" data-target="#modal-confirm-submit-application" prStatusId="{{ $prStatusId }}" applicationTypeId="{{ $applicationTypeId }}" data-toggle="tooltip" data-placement="top" title="ALL requirements must have an uploaded file."><span class="fa fa-send-o"></span> Submit Application</button>
                                      <!--</a>-->

                                      <button type="button" class="btn bg-default btn-flat btn-edit-new-application" data-toggle="modal" data-target="#modal-edit-new-application"><span class="fa fa-edit"></span> Edit Application</button>
                                    @endif

                                    @if($applicationInfo['applicationStatusIdfk']==8 && Auth::user()->officeIDfk==3)
                                      <!--<a href="{{ route('submitapplication', [$prStatusId, $applicationTypeId]) }}">-->
                                      <button type="button" class="btn bg-purple btn-flat btn-submit-application TT" data-toggle="modal" data-target="#modal-confirm-submit-completion" prStatusId="{{ $prStatusId }}" applicationTypeId="{{ $applicationTypeId }}" data-toggle="tooltip" data-placement="top" title="ALL requirements must have an uploaded file."><span class="fa fa-send-o"></span> Submit Completion</button>
                                      <!--</a>-->
                                    @endif

                                    @if(($applicationInfo['applicationStatusIdfk']==3 || $applicationInfo['applicationStatusIdfk']==4) && Auth::user()->officeIDfk==3)
                                      <button type="button" class="btn btn-danger btn-flat btn-cancel-new-application" data-toggle="modal" data-target="#modal-cancel-new-application"><span class="fa fa-trash"></span> Cancel Application</button>
                                    @endif

<!------- End Buttons for School---------------------------------------------------------------->










<!------- Buttons for SDO---------------------------------------------------------------->
                                    @if($applicationInfo['applicationStatusIdfk']==3 && Auth::user()->officeIDfk==2 && ($latestStagesTimeLineInfo["stageStatusIdfk"]==3 || $latestStagesTimeLineInfo["stageStatusIdfk"]==24))
                                      <button type="button" class="btn bg-orange btn-flat btn-on-assessment" data-toggle="modal" data-target="#modal-on-assessment"><span class="fa fa-tasks"></span> Assess Application Document</button>
                                    @endif

                                    @if($applicationInfo['applicationStatusIdfk']==4 && Auth::user()->officeIDfk==2 && ($latestStagesTimeLineInfo["stageStatusIdfk"]==4 || $latestStagesTimeLineInfo["stageStatusIdfk"]==5))
                                      <button type="button" class="btn bg-navy btn-flat btn-on-assessment btn-notify-for-compliance-sdo-assessment" data-toggle="modal" data-target="#modal-notify-for-compliance-sdo-assessment" data-tooltip="tooltip" title="Notify for Compliance"><span class="fa fa-bell"></span> Notify For Compliance</button>

                                      <button type="button" class="btn bg-olive btn-flat btn-on-assessment btn-with-complete-docu TT" data-toggle="modal" data-target="#modal-complete-documents" data-tooltip="tooltip" title="Tag Application for Completeness of documents submitted"><span class="fa fa-check"></span> With Complete Documents</button>
                                    @endif
                                    @if($applicationInfo['applicationStatusIdfk']==4 && Auth::user()->officeIDfk==2 && ($latestStagesTimeLineInfo["stageStatusIdfk"]==6))
                                      <button type="button" class="btn bg-navy btn-flat btn-on-assessment btn-notify-for-compliance-sdo-assessment" data-toggle="modal" data-target="#modal-notify-for-compliance-sdo-assessment" data-tooltip="tooltip" title="Notify for Compliance"><span class="fa fa-bell"></span> Notify For Compliance</button>
                                      
                                      <button type="button" class="btn bg-orange btn-flat btn-for-inspection" data-toggle="modal" data-target="#modal-for-inspection-sdo" data-tooltip="tooltip" title="For Ocular Inspection by SDO"><span class="fa fa-search"></span> For SDO Ocular Visit</button>
                                    @endif

                                    @if($applicationInfo['applicationStatusIdfk']==4 && Auth::user()->officeIDfk==2 && ($latestStagesTimeLineInfo["stageStatusIdfk"]==7 || $latestStagesTimeLineInfo["stageStatusIdfk"]==11))
                                      <button type="button" class="btn bg-navy btn-flat btn-on-assessment btn-notify-for-compliance-sdo-visit" data-toggle="modal" data-target="#modal-notify-for-compliance-sdo-visit" data-tooltip="tooltip" title="Notify for Compliance"><span class="fa fa-bell"></span> Notify For Compliance</button>

                                      <button type="button" class="btn bg-orange btn-flat btn-on-assessment btn-back-to-assessment" data-toggle="modal" data-target="#modal-on-assessment"><span class="fa fa-arrow-left"></span> Back to Assessment</button>

                                      <button type="button" class="btn btn-flat btn-on-assessment btn-for-reinspection bg-blue" data-toggle="modal" data-target="#modal-for-re-inspection-sdo" data-tooltip="tooltip" title="For SDO Ocular Revisit"><span class="fa fa-search"></span> For SDO Ocular Revisit</button>

                                      <button type="button" class="btn bg-olive btn-flat btn-on-assessment btn-compliant-for-endorsement" data-toggle="modal" data-target="#modal-compliant-for-endorsement" data-tooltip="tooltip" title="Compliant for Endorsement to Regional Office"><span class="fa fa-check fa-eye"></span> Compliant for Endorsement and for RO Review</button>
                                      <!--<button type="button" class="btn bg-navy btn-flat btn-on-assessment" data-toggle="modal" data-target="#modal-for-review" data-tooltip="tooltip" title="Regional Office For Review"><span class="fa fa-eye"></span> For Review</button>-->
                                    @endif
<!------- End Buttons for SDO---------------------------------------------------------------->












<!------- Buttons for RO---------------------------------------------------------------->
                                    @if($applicationInfo['applicationStatusIdfk']==4 && Auth::user()->officeIDfk==1 && $latestStagesTimeLineInfo["stageStatusIdfk"]==12)
                                      <button type="button" class="btn bg-navy btn-flat btn-on-assessment" data-toggle="modal" data-target="#modal-ro-review-application"><span class="fa fa-eye"></span> Review Application</button>
                                    @endif

                                    @if($applicationInfo['applicationStatusIdfk']==4 && Auth::user()->officeIDfk==1 && ($latestStagesTimeLineInfo["stageStatusIdfk"]==13 || $latestStagesTimeLineInfo["stageStatusIdfk"]==14 || $latestStagesTimeLineInfo["stageStatusIdfk"]==15))
                                      <button type="button" class="btn bg-navy btn-flat btn-on-assessment btn-notify-for-compliance-ro-review bg-aqua" data-toggle="modal" data-target="#modal-notify-for-compliance-ro-review" data-tooltip="tooltip" title="Notify for compliance"><span class="fa fa-bell"></span> Notify For Compliance</button>

                                      @if($latestStagesTimeLineInfo["stageStatusIdfk"]==15)
                                      <button type="button" class="btn bg-orange btn-flat btn-on-assessment btn-back-for-ro-review" data-toggle="modal" data-target="#modal-ro-review-application" data-tooltip="tooltip" title="Back to Regional Office reviwe"><span class="fa fa-search"></span> Back to RO Review</button>
                                      @else
                                      <button type="button" class="btn bg-orange btn-flat btn-on-assessment btn-for-inspection-ro" data-toggle="modal" data-target="#modal-for-inspection-ro" data-tooltip="tooltip" title="For Ocular Visit by the Regional Office"><span class="fa fa-search"></span> For RO Ocular Visit</button>
                                      @endif

                                      @if($applicationTypeId==1 || $applicationTypeId==2)
                                        <button type="button" class="btn bg-olive btn-flat btn-on-assessment btn-for-compliant-permit-ro" data-toggle="modal" data-target="#modal-compliant-issuance-permit" data-tooltip="tooltip" title="Compliant for Issuance of Permit to Operate"><span class="fa fa-check"></span> Compliant</button>
                                      @endif

                                      @if($applicationTypeId==3 || $applicationTypeId==4)
                                        <button type="button" class="btn bg-olive btn-flat btn-on-assessment btn-for-compliant-recognition-ro" data-toggle="modal" data-target="#modal-compliant-issuance-recognition" data-tooltip="tooltip" title="Compliant for Issuance of Recognition"><span class="fa fa-check"></span> Compliant</button>
                                      @endif

                                      <button type="button" class="btn btn-danger btn-flat btn-on-assessment btn-for-disapprove-ro" data-toggle="modal" data-target="#modal-disapprove" data-tooltip="tooltip" title="For Application Disapproval"><span class="fa fa-ban"></span> Disapprove</button>
                                    @endif

                                    @if($applicationInfo['applicationStatusIdfk']==5 && Auth::user()->officeIDfk==1 && $latestStagesTimeLineInfo["stageStatusIdfk"]==25)
                                      <button type="button" class="btn bg-navy btn-flat btn-on-assessment bg-aqua btn-for-payment-issuance-ro" data-toggle="modal" data-target="#modal-paymnet-and-issuance" data-tooltip="tooltip" title="For Pick-up"><span class="fa fa-money"></span> For Pick-up</button>
                                    @endif

                                    @if($applicationInfo['applicationStatusIdfk']==5 && Auth::user()->officeIDfk==1 && ($latestStagesTimeLineInfo["stageStatusIdfk"]==18))
                                      <button type="button" class="btn bg-navy btn-flat btn-on-assessment bg-aqua btn-for-payment-issuance-ro-ro" data-toggle="modal" data-target="#modal-payment" data-tooltip="tooltip" title="For Application Payment"><span class="fa fa-money"></span> Payment</button>
                                    @endif

                                    @if($applicationInfo['applicationStatusIdfk']==5 && Auth::user()->officeIDfk==1 && ($latestStagesTimeLineInfo["stageStatusIdfk"]==19))

                                      @if($applicationTypeId==1 || $applicationTypeId==2)
                                        <button type="button" class="btn bg-olive btn-flat btn-on-assessment btn-for-issue-permit-ro" data-toggle="modal" data-target="#modal-issuance-permit" data-tooltip="tooltip" title="Issue Government Permit"><span class="fa fa-certificate"></span> Issue Government Permit</button>
                                      @endif

                                      @if($applicationTypeId==3 || $applicationTypeId==5)
                                        <button type="button" class="btn bg-olive btn-flat btn-on-assessment btn-for-issue-recognition-ro" data-toggle="modal" data-target="#modal-issuance-recognition" data-tooltip="tooltip" title="Issue Government Recognition"><span class="fa fa-certificate"></span> Issue Government Recognition</button>
                                      @endif

                                    @endif


                                    @if($applicationInfo['applicationStatusIdfk']==5 && Auth::user()->officeIDfk==1 && ($latestStagesTimeLineInfo["stageStatusIdfk"]==20 || $latestStagesTimeLineInfo["stageStatusIdfk"]==21))

                                      @if($applicationTypeId==1 || $applicationTypeId==2)
                                        <button type="button" class="btn bg-olive btn-flat btn-on-assessment"><span class="fa fa-check"></span> Approved</button>
                                      @endif

                                      @if($applicationTypeId==3 || $applicationTypeId==5)
                                        <button type="button" class="btn bg-olive btn-flat btn-on-assessment btn-for-issue-recognition-ro" data-toggle="modal" data-target="#modal-issuance-recognition" data-tooltip="tooltip" title="Issue Government Recognition"><span class="fa fa-certificate"></span> Issue Government Recognition</button>
                                      @endif

                                    @endif

                                    @if($applicationInfo['applicationStatusIdfk']==5 && Auth::user()->officeIDfk==1 && ($latestStagesTimeLineInfo["stageStatusIdfk"]==16 || $latestStagesTimeLineInfo["stageStatusIdfk"]==17 || $latestStagesTimeLineInfo["stageStatusIdfk"]==22))
                                      @if($applicationTypeId==1 || $applicationTypeId==2)
                                        <a href="{{ route('createPermit', [$prStatusId, $applicationTypeId, $applicationInfo['schoolUserIdfk']]) }}">
                                          <button type="button" class="btn bg-purple btn-flat btn-on-assessment" data-tooltip="tooltip" title="Print Government Permit Certification"><span class="fa fa-print"></span> Generate Government Permit</button>
                                        </a>
                                      @endif

                                      @if($applicationTypeId==3 || $applicationTypeId==4)
                                        <a href="{{ route('createRecognition', [$prStatusId, $applicationTypeId, $applicationInfo['schoolUserIdfk']]) }}">
                                          <button type="button" class="btn bg-purple btn-flat btn-on-assessment" data-toggle="modal" data-target="#modal-print-recognition" data-tooltip="tooltip" title="Print Government Recognition Certification"><span class="fa fa-print"></span> Generate Government Recognition</button>
                                        </a>
                                      @endif
                                    @endif
<!------- End Buttons for RO---------------------------------------------------------------->

<!-------------------------------------- Close Buttons ------------------------------------>
@if(Auth::user()->officeIDfk==3)
<a href="{{ route('privateschoolnewapplication', [$applicationTypeId]) }}"><button type="button" class="btn bg-default btn-flat btn-close-new-application"><span class="fa fa-close"></span> Close</button></a>
@elseif(Auth::user()->officeIDfk==2)
<a href="{{ route('sdoapplicationmonitoring', [$applicationTypeId]) }}"><button type="button" class="btn bg-default btn-flat btn-close-new-application"><span class="fa fa-close"></span> Close</button></a>
@elseif(Auth::user()->officeIDfk==1)
<a href="{{ route('roapplicationmonitoring', [$applicationTypeId]) }}"><button type="button" class="btn bg-default btn-flat btn-close-new-application"><span class="fa fa-close"></span> Close</button></a>
@endif

<!------------------------------ Notification Labels  -->

                                    @if($applicationInfo['applicationStatusIdfk']==4 && (Auth::user()->officeIDfk==3 || Auth::user()->officeIDfk==1) && $latestStagesTimeLineInfo["stageStatusIdfk"]>=4 && $latestStagesTimeLineInfo["stageStatusIdfk"]<=6)
                                      <label class="label label-warning"><span class="fa fa-hourglass"></span> Application is under assessment and evaluation in the Division Office...</label>
                                    @endif

                                    @if($applicationInfo['applicationStatusIdfk']==4 && (Auth::user()->officeIDfk==3 || Auth::user()->officeIDfk==1) && $latestStagesTimeLineInfo["stageStatusIdfk"]>=7 && $latestStagesTimeLineInfo["stageStatusIdfk"]<=11)
                                      <label class="label label-warning"><span class="fa fa-hourglass"></span> Application is under ocular inspection by the Division Office...</label>
                                    @endif

                                    @if($applicationInfo['applicationStatusIdfk']==4 && (Auth::user()->officeIDfk==2 || Auth::user()->officeIDfk==3) && $latestStagesTimeLineInfo["stageStatusIdfk"]==12)
                                      <label class="label label-warning"><span class="fa fa-hourglass"></span> Waiting for Regional Office to Review Application...</label>
                                    @endif

                                    @if($applicationInfo['applicationStatusIdfk']==4 && (Auth::user()->officeIDfk==2 || Auth::user()->officeIDfk==3) && ($latestStagesTimeLineInfo["stageStatusIdfk"]>12 && $latestStagesTimeLineInfo["stageStatusIdfk"]<=15))
                                      <label class="label label-warning"><span class="fa fa-hourglass"></span> Application has been endorsed to the Regional Office...</label>
                                    @endif

                                    @if($applicationInfo['applicationStatusIdfk']==8 && (Auth::user()->officeIDfk==1 || Auth::user()->officeIDfk==2) && ($latestStagesTimeLineInfo["stageStatusIdfk"]==5 || $latestStagesTimeLineInfo["stageStatusIdfk"]==14))
                                      <label class="label label-warning"><span class="fa fa-hourglass"></span> Application is on compliance...</label>
                                    @endif

                                    @if($applicationInfo['applicationStatusIdfk']==3 && Auth::user()->officeIDfk==1 && ($latestStagesTimeLineInfo["stageStatusIdfk"]==3 || $latestStagesTimeLineInfo["stageStatusIdfk"]==24))
                                      <label class="label label-warning"><span class="fa fa-hourglass"></span> Waiting for Division to assess application document...</label>
                                    @endif

                                    @if($applicationInfo['applicationStatusIdfk']==4 && (Auth::user()->officeIDfk==2 || Auth::user()->officeIDfk==3) && ($latestStagesTimeLineInfo["stageStatusIdfk"]==13 || $latestStagesTimeLineInfo["stageStatusIdfk"]==14 || $latestStagesTimeLineInfo["stageStatusIdfk"]==15))
                                      <label class="label label-warning"><span class="fa fa-hourglass"></span> Application is under Regional Office Review...</label>
                                    @endif

                                    @if($applicationInfo['applicationStatusIdfk']==5 && (Auth::user()->officeIDfk==2 || Auth::user()->officeIDfk==3) && ($latestStagesTimeLineInfo["stageStatusIdfk"]==16 || $latestStagesTimeLineInfo["stageStatusIdfk"]==17 || $latestStagesTimeLineInfo["stageStatusIdfk"]==25))
                                      <label class="label label-warning"><span class="fa fa-hourglass"></span> Application is Compliant and Waiting for Pick-up...</label>
                                    @endif


                                    @if($applicationInfo['applicationStatusIdfk']==5 && (Auth::user()->officeIDfk==2 || Auth::user()->officeIDfk==3) && ($latestStagesTimeLineInfo["stageStatusIdfk"]==18))
                                      <label class="label label-warning"><span class="fa fa-hourglass"></span> For Payment...</label>
                                    @endif

                                    @if($applicationInfo['applicationStatusIdfk']==5 && (Auth::user()->officeIDfk==2 || Auth::user()->officeIDfk==3) && ($latestStagesTimeLineInfo["stageStatusIdfk"]==19))
                                      <label class="label label-warning"><span class="fa fa-hourglass"></span> For Issuance of Permit or Recognition...</label>
                                    @endif

                                    @if($applicationInfo['applicationStatusIdfk']==5 && (Auth::user()->officeIDfk==2 || Auth::user()->officeIDfk==3) && ($latestStagesTimeLineInfo["stageStatusIdfk"]==19))
                                      <label class="label label-primary"><span class="fa fa-money"></span> Paid</label>
                                    @endif

                                    @if($applicationInfo['applicationStatusIdfk']==5 && (Auth::user()->officeIDfk==2 || Auth::user()->officeIDfk==3) && ($latestStagesTimeLineInfo["stageStatusIdfk"]==20 || $latestStagesTimeLineInfo["stageStatusIdfk"]==21))
                                      <label class="label label-success"><span class="fa fa-check"></span> Approved</label>
                                    @endif

                                    <button type="button" class="btn bg-purple btn-flat btn-timeline-new-application pull-right" data-toggle="modal" data-target="#modal-timeline-new-application"><span class="fa fa-history"></span> View Timeline</button>

                                  </p>
                                </div>
                                <!-- /.col -->
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="col-md-2">Status</label>
                                    <div class="col-sm-6" style="padding-left:0px">
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-striped {{ $applicationInfo['progressClass'] }}" role="progressbar" aria-valuenow="{{ $applicationInfo['valueNow'] }}" aria-valuemin="0" aria-valuemax="100" style="max-width: {{ $applicationInfo['valueNow'] }}%"><strong>{{ $applicationInfo["applicationStatus"]}}</strong>
                                            </div>
                                        </div>                      
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <label class="col-md-4">Name of School:</label>
                                        <div class="col-md-8">{{ $applicationInfo["schoolName"] }}</div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <label class="col-md-4">Address of School:</label>
                                        <div class="col-md-8">{{ $applicationInfo["schoolAddress"] }} , {{ $getDivisionName }}</div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <label class="col-md-4">Course (Grade Level Applying for?):</label>
                                        <div class="col-md-8">{{ $applicationInfo["applicationFrom"] }} To {{ $applicationInfo["applicationTo"] }} </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <label class="col-md-4">Principal/School Head:</label>
                                        <div class="col-md-8">{{ $applicationInfo["schoolHead"] }} </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <label class="col-md-4">Contact Number:</label>
                                        <div class="col-md-8">{{ $applicationInfo["telNumber"] }} / {{ $applicationInfo["mobileNumber"] }}</div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <label class="col-md-4">School Year:</label>
                                        <div class="col-md-8">{{ $applicationInfo["schoolYear"] }}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                              <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Application Document Assessment and Review</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body table-responsive no-padding">
                                    <table class="table table-hover table-responsive table-bordered table-condensed">
                                            {!! $newApplicationRequirements !!} <!-- used to render text as HTML -->
                                    </table>
                                </div>
                                <!-- /.box-body -->
                              </div>
                              <!-- /.box -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- Modal for cancellation of application -->
        <div class="modal fade" id="modal-cancel-new-application">
          <div class="modal-dialog">
            <div class="modal-content">
              <form method="POST" action="{{ route('cancelapplication') }}">
                {{ csrf_field() }}
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Cancel New Application</h4>
              </div>
              <div class="modal-body">
                <input type="hidden" name="prStatusId" id="prStatusId" value="{{ $prStatusId }}">
                <input type="hidden" name="applicationType" id="applicationType" value="{{ $applicationTypeId }}">
                <h4>Are you sure you want to cancel the application? This action cannot be undone.</h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary bg-purple" id="cancel-new-application">Yes</button>
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

<!-- Modal for cancellation of application -->
        <div class="modal fade" id="modal-confirm-submit-application">
          <div class="modal-dialog">
            <form method="POST" action="{{ route('submitapplication') }}">
              {{ csrf_field() }}
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Submit Application</h4>
              </div>
              <div class="modal-body">
                <input type="hidden" name="prStatusId" id="prStatusId" value="{{ $prStatusId }}">
                <input type="hidden" name="applicationType" id="applicationType" value="{{ $applicationTypeId }}">
                <h4 class="submitNotification"></h4>
                <!--<h4>Are you sure you want to submit this application? You won't be able to update your documents once submitted.</h4>-->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left submit-completion-no" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary bg-purple submit-completion" id="cancel-new-application">Yes</button>
              </div>
            </div>
          </form>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

<!-- Modal for cancellation of application -->
        <div class="modal fade" id="modal-confirm-submit-completion">
          <div class="modal-dialog">
            <form method="POST" action="{{ route('submitcompletion') }}">
              {{ csrf_field() }}
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Submit Completion</h4>
              </div>
              <div class="modal-body">
                <input type="hidden" name="prStatusId" id="prStatusId" value="{{ $prStatusId }}">
                <input type="hidden" name="applicationType" id="applicationType" value="{{ $applicationTypeId }}">

                <h4 class="submitNotification"></h4>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left submit-completion-no" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary bg-purple submit-completion" id="submit-completion">Yes</button>
              </div>
            </div>
          </form>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->


<!-- Modal for uploading -->
        <div class="modal fade" id="modal-upload-requirement-for-new-application">
          <div class="modal-dialog">
            <div class="modal-content">
              <form method="post" action="{{ route('uploadfile') }}" enctype="multipart/form-data" onsubmit="return validateForm();">
                {{ csrf_field() }}
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">File Upload</h4>
              </div>
              <div class="modal-body">
                <h4>Upload necessary file for the ff. requirement.<br> File must be in .pdf file format.</h4>
                <h4>Existing uploaded file under this requirement will be <em><strong>overwritten.</strong></em></h4>
                <br>
                <h4><i class="fa fa-upload"></i>Upload File: </h4>
                <input type="file" class="form-control" name="fileUpload" id="fileUpload" required="required">
                <br>
                <label for="documentTitle">Document Title*</label>
                <input type="text" class="form-control" name="documentTitle_tmp" id="documentTitle_tmp" value="" required="required" disabled="disabled">
                <input type="hidden" class="form-control" name="documentTitle" id="documentTitle" value="" required="required">
                <input type="hidden" name="prStatusId" id="prStatusId" value="{{ $prStatusId }}">
                <input type="hidden" name="applicationType" id="applicationType" value="{{ $applicationTypeId }}">
                <input type="hidden" name="mainReqid" id="mainReqid" value="">
                <input type="hidden" name="fileFrom" value="School">
                <input type="hidden" name="updatedBy" id="updatedBy" value="{{ Auth::user()->id }}">
                <input type="hidden" name="requirement" id="requirement" value="">
                <input type="hidden" name="divisionName" id="divisionName" value="{{ $getDivisionName }}">
                <input type="hidden" name="schoolId" id="schoolId" value="{{ Auth::user()->schoolId }}">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary bg-purple" id="upload-requirement">Yes</button>
                
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

<!-- Modal for delete uploaded file -->
        <div class="modal fade" id="modal-delete-requirement-for-new-application">
          <div class="modal-dialog">
            <div class="modal-content">
              <form method="post" action="{{ route('deletefile') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Delete Upload File</h4>
              </div>
              <div class="modal-body">
                <h4>You are about to delete the following file.<br>Are you sure you want to continue?</h4>
                <h4><em><strong>You cannot undo this process.</strong></em></h4>
                <br>
                <h4><i class="fa fa-upload"></i>File to be deleted: </h4>
                <label id="fileToBeDeleted">[file name]</label>
                <input type="hidden" name="fileName" id="fileName" value="">
                <input type="hidden" name="targetFile" id="targetFile" value="">
                <input type="hidden" name="prStatusId" id="prStatusId_del" value="{{ $prStatusId }}">
                <input type="hidden" name="applicationType" id="applicationTypeId_del" value="{{ $applicationTypeId }}">
                <input type="hidden" name="mainReqid" id="mainReqid_del">
                <input type="hidden" name="updatedBy" id="updatedBy_del" value="{{ Auth::user()->id }}">             
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary bg-purple" id="delete-requirement">Yes</button>
                
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

<!-- Modal for timeline -->
        <div class="modal fade" id="modal-timeline-new-application">
          <div class="modal-dialog" style="width: 85%;">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Application Timeline <span class="fa fa-history"></span></h4>
              </div>
              <div class="modal-body">
                  <!-- Content Wrapper. Contains page content -->
                  <div>
                    <!-- Content Header (Page header) -->
                    <section class="content-header">
                      <h1>
                        Timeline
                        <small>Permit for New Application</small>
                      </h1>
                    </section>

                    <!-- Main content -->
                    <section class="content">
                      <!-- row -->
                      <div class="row">
                        <div class="col-md-12">
                          <!-- The time line -->
                          <ul class="timeline">
                                {!! $stagesTimeLine !!}
                          </ul>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->
                    </section>
                    <!-- /.content -->
                  </div>
                  <!-- /.content-wrapper -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left closeTimeLine" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->


<!-- New Application Modal -->
        <div class="modal fade" id="modal-edit-new-application">
          <div class="modal-dialog">
            <div class="modal-content">
              <form method="POST" action="{{ route('editdraftnewapplication') }}" onsubmit="return onFormNextSubmit();">
                {{ csrf_field() }}
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">New Application</h4>
              </div>
              <div class="modal-body">
                <input type="hidden" value="{{ $prStatusId }}" name="prStatusIdEdit" id="prStatusIdEdit">
                <input type="hidden" value="{{ $applicationTypeId }}" name="applicationTypeEdit" id="applicationTypeEdit">
                <h4>Applying for:</h4>
                <div class="row">
                    <div class="col-md-6 form-group">
                        <label>School Year: </label>
                        <select class="form-control" name="sy" id="sy">
                            <option value="" selected disabled>Select School Year</option>
                            <option value="2019 - 2020"  @if($applicationInfo['schoolYear']=='2019 - 2020') selected="selected" @endif>2019 - 2020</option>
                            <option value="2020 - 2021"  @if($applicationInfo['schoolYear']=='2020 - 2021') selected="selected" @endif>2020 - 2021</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        <label>From</label>
                        <select class="form-control" name="gradeLevelFrom" id="gradeLevelFrom">
                            <option value="" selected disabled>Select Grade From</option>
                            @foreach($gradeLevels as $gradeLevel)
                                <option value="{{ $gradeLevel['gradeLevelId'] }}" @if($applicationInfo['gradeLevelIdfkFrom']==$gradeLevel['gradeLevelId']) selected="selected" @endif>{{ $gradeLevel['description'] }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-6 form-group">
                        <label>To</label>
                        <select class="form-control" name="gradeLevelTo" id="gradeLevelTo">
                            <option value="" selected disabled>Select Grade To</option>
                            @foreach($gradeLevels as $gradeLevel)
                                <option value="{{ $gradeLevel['gradeLevelId'] }}" @if($applicationInfo['gradeLevelIdfkTo']==$gradeLevel['gradeLevelId']) selected="selected" @endif>{{ $gradeLevel['description'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="row categorydiv">
                    <div class="col-md-6 form-group">
                        <label>For Senior High Application</label>
                        <select class="form-control" name="category" id="category">
                                <option value="0" selected> </option>
                            @foreach($categoriesshs as $categoryshs)
                                <option value="{{ $categoryshs['categoryId'] }}" @if($applicationInfo['categoryIdfk']==$categoryshs['categoryId']) selected="selected" @endif>{{ $categoryshs['category'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary bg-purple" id="edit-draft-application">Next</button>
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->














<!--------------------- start SDO Modals ------------------------------------------------------------>
<!-- Assess document modal -->
        <div class="modal fade" id="modal-on-assessment">
          <div class="modal-dialog">
            <div class="modal-content">
              <form method="POST" action="{{ route('assessapplication') }}">
                {{ csrf_field() }}
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Assess Application Document</h4>
              </div>
              <div class="modal-body">
                <input type="hidden" name="prStatusId" id="prStatusId" value="{{ $prStatusId }}">
                <input type="hidden" name="applicationType" id="applicationType" value="{{ $applicationTypeId }}">
                <h4 class="back-to-assessment">Start assessing application document.</h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary bg-purple" id="assess-application">Yes</button>
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

<!-- Modal for compliance of application assessment -->
        <div class="modal fade" id="modal-notify-for-compliance-sdo-assessment">
          <div class="modal-dialog">
            <div class="modal-content">
              <form method="POST" action="{{ route('notifyforcpmpliancesdo') }}">
                {{ csrf_field() }}
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Notify for Compliance on Assessment</h4>
              </div>
              <div class="modal-body">
                <input type="hidden" name="prStatusId" id="prStatusId" value="{{ $prStatusId }}">
                <input type="hidden" name="applicationType" id="applicationType" value="{{ $applicationTypeId }}">
                <h4 class="for-compliance-assessment"></h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left for-compliance-assessment-btn-no" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary bg-purple for-compliance-assessment-btn-yes" id="cancel-notify-for-compliance-sdo">Notify</button>
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

<!-- Modal for compliance of application visit -->
        <div class="modal fade" id="modal-notify-for-compliance-sdo-visit">
          <div class="modal-dialog">
            <div class="modal-content">
              <form method="POST" action="{{ route('notifyforcpmpliancesdo') }}">
                {{ csrf_field() }}
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Notify for Compliance on Ocular Visit</h4>
              </div>
              <div class="modal-body">
                <input type="hidden" name="prStatusId" id="prStatusId" value="{{ $prStatusId }}">
                <input type="hidden" name="applicationType" id="applicationType" value="{{ $applicationTypeId }}">
                <h4 class="for-compliance-visit"></h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left for-compliance-visit-btn-no" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary bg-purple for-compliance-visit-btn-yes" id="cancel-notify-for-compliance-sdo">Notify</button>
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

<!-- Modal for complete documents -->
        <div class="modal fade" id="modal-complete-documents">
          <div class="modal-dialog">
            <div class="modal-content">
              <form method="POST" action="{{ route('completedocuments') }}">
                {{ csrf_field() }}
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Application with Complete Documents</h4>
              </div>
              <div class="modal-body">
                <input type="hidden" name="prStatusId" id="prStatusId" value="{{ $prStatusId }}">
                <input type="hidden" name="applicationType" id="applicationType" value="{{ $applicationTypeId }}">
                <h4 class="with-complete-docu-notification">Tag application for completeness of document submitted during assessment and evaluation by SDO.</h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left with-complete-docu-no" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary bg-purple with-complete-docu" id="cancel-notify-for-compliance-sdo">Yes</button>
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

<!-- Modal for inspection -->
        <div class="modal fade" id="modal-for-inspection-sdo">
          <div class="modal-dialog">
            <div class="modal-content">
              <form method="POST" action="{{ route('forinspectionsdo') }}">
                {{ csrf_field() }}
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Application For Ocular Visit by SDO</h4>
              </div>
              <div class="modal-body">
                <input type="hidden" name="prStatusId" id="prStatusId" value="{{ $prStatusId }}">
                <input type="hidden" name="applicationType" id="applicationType" value="{{ $applicationTypeId }}">
                <h4 class="submitForInspection"></h4>
                <!--<h4>For Inspection by SDO. Set proposed schedule for inspection.. other Details blah blah blah</h4>-->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left for-inspection-btn-no" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary bg-purple for-inspection-btn-yes" id="cancel-notify-for-compliance-sdo">Yes</button>
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

<!-- Modal for inspection -->
        <div class="modal fade" id="modal-for-re-inspection-sdo">
          <div class="modal-dialog">
            <div class="modal-content">
              <form method="POST" action="{{ route('forreinspectionsdo') }}">
                {{ csrf_field() }}
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Application For Another Ocular Visit by SDO</h4>
              </div>
              <div class="modal-body">
                <input type="hidden" name="prStatusId" id="prStatusId" value="{{ $prStatusId }}">
                <input type="hidden" name="applicationType" id="applicationType" value="{{ $applicationTypeId }}">
                <h4 class="for-reinspection"></h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left for-reinspection-btn-no" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary bg-purple for-reinspection-btn-yes" id="cancel-notify-for-compliance-sdo">Yes</button>
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

<!-- Modal for compliant for endorsement -->
        <div class="modal fade" id="modal-compliant-for-endorsement">
          <div class="modal-dialog">
            <div class="modal-content">
              <form method="POST" action="{{ route('compliantforendorsement') }}">
                {{ csrf_field() }}
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Application For Endorsement and for RO review</h4>
              </div>
              <div class="modal-body">
                <input type="hidden" name="prStatusId" id="prStatusId" value="{{ $prStatusId }}">
                <input type="hidden" name="applicationType" id="applicationType" value="{{ $applicationTypeId }}">
                <h4 class="for-endorsement-review">Application is tagged to be compliant for endorsement to the Regional Office for review.</h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left for-endorsement-btn-no" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary bg-purple for-endorsement-btn-yes" id="cancel-notify-for-compliance-sdo">Yes</button>
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

<!-- Modal for compliant for endorsement -->
        <div class="modal fade" id="modal-for-review">
          <div class="modal-dialog">
            <div class="modal-content">
              <form method="POST" action="{{ route('applicationforreview') }}">
                {{ csrf_field() }}
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Application For Endorsement</h4>
              </div>
              <div class="modal-body">
                <input type="hidden" name="prStatusId" id="prStatusId" value="{{ $prStatusId }}">
                <input type="hidden" name="applicationType" id="applicationType" value="{{ $applicationTypeId }}">
                <h4>Application is to be endorsed to the Regional Office for review.</h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary bg-purple" id="cancel-notify-for-compliance-sdo">Yes</button>
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
<!--------------------- start RO Modals ------------------------------------------------------------>
<!-- Review document modal -->
        <div class="modal fade" id="modal-ro-review-application">
          <div class="modal-dialog">
            <div class="modal-content">
              <form method="POST" action="{{ route('roreviewapplication') }}">
                {{ csrf_field() }}
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Review Application Document</h4>
              </div>
              <div class="modal-body">
                <input type="hidden" name="prStatusId" id="prStatusId" value="{{ $prStatusId }}">
                <input type="hidden" name="applicationType" id="applicationType" value="{{ $applicationTypeId }}">
                <h4 class="ro-review-application">Start reviewing application document.</h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary bg-purple" id="assess-application">Yes</button>
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->














<!-- RO Notify for completion modal -->
        <div class="modal fade" id="modal-notify-for-compliance-ro-review">
          <div class="modal-dialog">
            <div class="modal-content">
              <form method="POST" action="{{ route('notifyforcpmpliancero') }}">
                {{ csrf_field() }}
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Notify for Compliance</h4>
              </div>
              <div class="modal-body">
                <input type="hidden" name="prStatusId" id="prStatusId" value="{{ $prStatusId }}">
                <input type="hidden" name="applicationType" id="applicationType" value="{{ $applicationTypeId }}">
                <h4 class="for-compliance-ro-review"></h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left for-compliance-ro-review-btn-no" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary bg-purple for-compliance-ro-review-btn-yes" id="cancel-notify-for-compliance-sdo">Notify</button>
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

<!-- RO Notify for completion modal -->
        <div class="modal fade" id="modal-notify-for-compliance-ro-visit">
          <div class="modal-dialog">
            <div class="modal-content">
              <form method="POST" action="{{ route('notifyforcpmpliancero') }}">
                {{ csrf_field() }}
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Notify for Compliance</h4>
              </div>
              <div class="modal-body">
                <input type="hidden" name="prStatusId" id="prStatusId" value="{{ $prStatusId }}">
                <input type="hidden" name="applicationType" id="applicationType" value="{{ $applicationTypeId }}">
                <h4 class="for-compliance-ro-visit"></h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left for-compliance-ro-visit-btn-no" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary bg-purple for-compliance-ro-visit-btn-yes" id="cancel-notify-for-compliance-sdo">Notify</button>
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

<!-- RO for inspection modal -->
        <div class="modal fade" id="modal-for-inspection-ro">
          <div class="modal-dialog">
            <div class="modal-content">
              <form method="POST" action="{{ route('forinspectionro') }}">
                {{ csrf_field() }}
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">For RO Ocular Visit</h4>
              </div>
              <div class="modal-body">
                <input type="hidden" name="prStatusId" id="prStatusId" value="{{ $prStatusId }}">
                <input type="hidden" name="applicationType" id="applicationType" value="{{ $applicationTypeId }}">
                <h4 class="for-inspection-ro"></h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left for-inspection-ro-btn-no" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary bg-purple for-inspection-ro-btn-yes" id="for-apinspection-ro">Yes</button>
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

<!-- RO for compliant permit modal -->
        <div class="modal fade" id="modal-compliant-issuance-permit">
          <div class="modal-dialog">
            <div class="modal-content">
              <form method="POST" action="{{ route('compliantfornewissuance') }}">
                {{ csrf_field() }}
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Complaint for Issuance of Permit</h4>
              </div>
              <div class="modal-body">
                <input type="hidden" name="prStatusId" id="prStatusId" value="{{ $prStatusId }}">
                <input type="hidden" name="applicationType" id="applicationType" value="{{ $applicationTypeId }}">
                <h4 class="compliant-issuance-permit"></h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left for-compliant-permit-ro-btn-no" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary bg-purple for-compliant-permit-ro-btn-yes" id="for-apinspection-ro">Yes</button>
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

<!-- RO for compliant recognition modal -->
        <div class="modal fade" id="modal-compliant-issuance-recognition">
          <div class="modal-dialog">
            <div class="modal-content">
              <form method="POST" action="{{ route('compliantforrecognitionissuance') }}">
                {{ csrf_field() }}
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Complaint for Issuance of Recognition</h4>
              </div>
              <div class="modal-body">
                <input type="hidden" name="prStatusId" id="prStatusId" value="{{ $prStatusId }}">
                <input type="hidden" name="applicationType" id="applicationType" value="{{ $applicationTypeId }}">
                <h4 class="compliant-issuance-recognition"></h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left for-compliant-recognition-ro-btn-no" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary bg-purple for-compliant-recognition-ro-btn-yes" id="for-apinspection-ro">Yes</button>
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

<!-- RO for compliant recognition modal -->
        <div class="modal fade" id="modal-paymnet-and-issuance">
          <div class="modal-dialog">
            <div class="modal-content">
              <form method="POST" action="{{ route('forpaymentandissunce') }}">
                {{ csrf_field() }}
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">For Payment and Issuance</h4>
              </div>
              <div class="modal-body">
                <input type="hidden" name="prStatusId" id="prStatusId" value="{{ $prStatusId }}">
                <input type="hidden" name="applicationType" id="applicationType" value="{{ $applicationTypeId }}">
                <h4>Application is compliant and for payment and issuance of certification.</h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary bg-purple" id="for-apinspection-ro">Proceed</button>
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

<!-- RO for compliant recognition modal -->
        <div class="modal fade" id="modal-payment">
          <div class="modal-dialog">
            <div class="modal-content">
              <form method="POST" action="{{ route('payment') }}">
                {{ csrf_field() }}
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Payment</h4>
              </div>
              <div class="modal-body">
                <input type="hidden" name="prStatusId" id="prStatusId" value="{{ $prStatusId }}">
                <input type="hidden" name="applicationType" id="applicationType" value="{{ $applicationTypeId }}">
                <h4>Payment Requirements and Details.</h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary bg-purple" id="for-apinspection-ro">Proceed</button>
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

<!-- RO for compliant recognition modal -->
        <div class="modal fade" id="modal-issuance-permit">
          <div class="modal-dialog">
            <div class="modal-content">
              <form method="POST" action="{{ route('issuedpermit') }}">
                {{ csrf_field() }}
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Issuance of Permit</h4>
              </div>
              <div class="modal-body">
                <input type="hidden" name="prStatusId" id="prStatusId" value="{{ $prStatusId }}">
                <input type="hidden" name="applicationType" id="applicationType" value="{{ $applicationTypeId }}">
                <h4>Issuance of Permit details.</h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary bg-purple" id="for-apinspection-ro">Proceed</button>
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

<!-- RO for compliant recognition modal -->
        <div class="modal fade" id="modal-issuance-recognition">
          <div class="modal-dialog">
            <div class="modal-content">
              <form method="POST" action="{{ route('issuedrecognition') }}">
                {{ csrf_field() }}
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Issuance of Recognition</h4>
              </div>
              <div class="modal-body">
                <input type="hidden" name="prStatusId" id="prStatusId" value="{{ $prStatusId }}">
                <input type="hidden" name="applicationType" id="applicationType" value="{{ $applicationTypeId }}">
                <h4>Issuance of Recognition details.</h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary bg-purple" id="for-apinspection-ro">Proceed</button>
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

<!-- RO for compliant recognition modal -->
        <div class="modal fade" id="modal-disapprove">
          <div class="modal-dialog">
            <div class="modal-content">
              <form method="POST" action="{{ route('disapproveapplication') }}">
                {{ csrf_field() }}
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Application Disapproval</h4>
              </div>
              <div class="modal-body">
                <input type="hidden" name="prStatusId" id="prStatusId" value="{{ $prStatusId }}">
                <input type="hidden" name="applicationType" id="applicationType" value="{{ $applicationTypeId }}">
                <h4>Do you really want to disapprove the following appliction? This operation cannot be undone.</h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary bg-purple" id="for-apinspection-ro">Yes</button>
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

<!-- RO for compliant recognition modal -->
        <div class="modal fade" id="modal-print-recognition">
          <div class="modal-dialog">
            <div class="modal-content">
              <form method="POST" action="{{ route('issuedrecognition') }}">
                {{ csrf_field() }}
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Print Government Recognition</h4>
                </div>
                <div class="modal-body">
                  <input type="hidden" name="prStatusId" id="prStatusId" value="{{ $prStatusId }}">
                  <input type="hidden" name="applicationType" id="applicationType" value="{{ $applicationTypeId }}">
                  <h4>Print government recognition certificate.</h4>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                  <button type="submit" class="btn btn-primary bg-purple" id="">Print</button>
                </div>
              </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

<!-- RO for compliant recognition modal -->
        <div class="modal fade" id="modal-print-permit">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Print Government Permit</h4>
              </div>
              <div class="modal-body">
                <input type="hidden" name="prStatusId" id="prStatusId" value="{{ $prStatusId }}">
                <input type="hidden" name="applicationType" id="applicationType" value="{{ $applicationTypeId }}">
                <h4>Print government permit certificate.</h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary bg-purple" id="">Print</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
     <div id="snackbar">{{ $status }}</div>

<!-- RO for compliant recognition modal -->
        <div class="modal fade" id="modal-remarks">
          <div class="modal-dialog">
            <div class="modal-content">
              <form method="POST" action="{{ route('saveRemarks') }}">
                {{ csrf_field() }}
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Remarks</h4>
                </div>
                <div class="modal-body">
                  <input type="hidden" name="prStatusId" id="prStatusId" value="{{ $prStatusId }}">
                  <input type="hidden" name="applicationType" id="applicationType" value="{{ $applicationTypeId }}">
                  <input type="hidden" name="remarksId" id="remarksId">
                  <input type="hidden" name="statusType" id="statusType">
                  <label>Update remarks for this requirement then click <span style="color: #605ca8;">Save.</span></label>
                  <textarea placeholder="Enter Remarks" name="reqRemarks" id="reqRemarks" class="form-control"></textarea>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                  <button type="submit" class="btn btn-primary bg-purple" id="">Save</button>
                </div>
              </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->






<!-- Modal for uploading -->
        <div class="modal fade" id="modal-upload-sdo-assessment">
          <div class="modal-dialog">
            <div class="modal-content">
              <form method="post" action="{{ route('uploadfilesdoro') }}" enctype="multipart/form-data" onsubmit="return validateFormSdoRo();">
                {{ csrf_field() }}
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">File Upload</h4>
              </div>
              <div class="modal-body">
                <h4>Upload filled-up and signed application assessment.<br> File must be in .pdf file format.</h4>
                <h4>Existing uploaded file under this requirement will be <em><strong>overwritten.</strong></em></h4>
                <br>
                <h4><i class="fa fa-upload"></i>Upload File: </h4>
                <input type="file" class="form-control fileUploadSdoRo" name="fileUpload" required="required">
                <br>
                <label for="documentTitle">Document Title*</label>
                <input type="text" class="form-control" name="documentTitle_tmp" value="SDO Assessment" required="required" disabled="disabled">
                <input type="hidden" class="form-control" name="documentTitle" value="SDO Assessment" required="required">
                <input type="hidden" name="prStatusId" value="{{ $prStatusId }}">
                <input type="hidden" name="applicationType" value="{{ $applicationTypeId }}">
                <input type="hidden" name="mainReqid" value="0">
                <input type="hidden" name="fileFrom" value="SDO Assessment">
                <input type="hidden" name="updatedBy" value="{{ Auth::user()->id }}">
                <input type="hidden" name="divisionName" value="{{ $getDivisionName }}">
                <input type="hidden" name="schoolId" value="{{ $applicationInfo['schoolUserIdfk'] }}">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary bg-purple" id="upload-assessment">Yes</button>
                
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->      

<!-- Modal for uploading -->
        <div class="modal fade" id="modal-upload-sdo-ocular-visit">
          <div class="modal-dialog">
            <div class="modal-content">
              <form method="post" action="{{ route('uploadfilesdoro') }}" enctype="multipart/form-data" onsubmit="return validateFormSdoOcular();">
                {{ csrf_field() }}
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">File Upload</h4>
              </div>
              <div class="modal-body">
                <h4>Upload filled-up and signed sdo ocular visit report.<br> File must be in .pdf file format.</h4>
                <h4>Existing uploaded file under this requirement will be <em><strong>overwritten.</strong></em></h4>
                <br>
                <h4><i class="fa fa-upload"></i>Upload File: </h4>
                <input type="file" class="form-control fileUploadSdoOcular" name="fileUpload" required="required">
                <br>
                <label for="documentTitle">Document Title*</label>
                <input type="text" class="form-control" name="documentTitle_tmp" value="SDO Ocular Visit" required="required" disabled="disabled">
                <input type="hidden" class="form-control" name="documentTitle" value="SDO Ocular Visit" required="required">
                <input type="hidden" name="prStatusId" value="{{ $prStatusId }}">
                <input type="hidden" name="applicationType" value="{{ $applicationTypeId }}">
                <input type="hidden" name="mainReqid" value="0">
                <input type="hidden" name="fileFrom" value="SDO Ocular Visit">
                <input type="hidden" name="updatedBy" value="{{ Auth::user()->id }}">
                <input type="hidden" name="divisionName" value="{{ $getDivisionName }}">
                <input type="hidden" name="schoolId" value="{{ $applicationInfo['schoolUserIdfk'] }}">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary bg-purple" id="upload-sdo-ocular-visit">Yes</button>
                
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->        


<!-- Modal for uploading -->
        <div class="modal fade" id="modal-upload-ro-review">
          <div class="modal-dialog">
            <div class="modal-content">
              <form method="post" action="{{ route('uploadfilesdoro') }}" enctype="multipart/form-data" onsubmit="return validateFormRoReview();">
                {{ csrf_field() }}
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">File Upload</h4>
              </div>
              <div class="modal-body">
                <h4>Upload filled-up and signed RO Review report.<br> File must be in .pdf file format.</h4>
                <h4>Existing uploaded file under this requirement will be <em><strong>overwritten.</strong></em></h4>
                <br>
                <h4><i class="fa fa-upload"></i>Upload File: </h4>
                <input type="file" class="form-control fileUploadRoReview" name="fileUpload" required="required">
                <br>
                <label for="documentTitle">Document Title*</label>
                <input type="text" class="form-control" name="documentTitle_tmp" value="RO Review" required="required" disabled="disabled">
                <input type="hidden" class="form-control" name="documentTitle" value="RO Review" required="required">
                <input type="hidden" name="prStatusId" value="{{ $prStatusId }}">
                <input type="hidden" name="applicationType" value="{{ $applicationTypeId }}">
                <input type="hidden" name="mainReqid" value="0">
                <input type="hidden" name="fileFrom" value="RO Review">
                <input type="hidden" name="updatedBy" value="{{ Auth::user()->id }}">
                <input type="hidden" name="divisionName" value="{{ $getDivisionName }}">
                <input type="hidden" name="schoolId" value="{{ $applicationInfo['schoolUserIdfk'] }}">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary bg-purple" id="upload-ro-review">Yes</button>
                
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->      


<!-- Modal for uploading -->
        <div class="modal fade" id="modal-upload-ro-ocular-visit">
          <div class="modal-dialog">
            <div class="modal-content">
              <form method="post" action="{{ route('uploadfilesdoro') }}" enctype="multipart/form-data" onsubmit="return validateFormRoOcular();">
                {{ csrf_field() }}
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">File Upload</h4>
              </div>
              <div class="modal-body">
                <h4>Upload filled-up and signed RO Ocular Visit report.<br> File must be in .pdf file format.</h4>
                <h4>Existing uploaded file under this requirement will be <em><strong>overwritten.</strong></em></h4>
                <br>
                <h4><i class="fa fa-upload"></i>Upload File: </h4>
                <input type="file" class="form-control fileUploadRoOcular" name="fileUpload" required="required">
                <br>
                <label for="documentTitle">Document Title*</label>
                <input type="text" class="form-control" name="documentTitle_tmp" value="RO Ocular Visit" required="required" disabled="disabled">
                <input type="hidden" class="form-control" name="documentTitle" value="RO Ocular Visit" required="required">
                <input type="hidden" name="prStatusId" value="{{ $prStatusId }}">
                <input type="hidden" name="applicationType" value="{{ $applicationTypeId }}">
                <input type="hidden" name="mainReqid" value="0">
                <input type="hidden" name="fileFrom" value="RO Ocular Visit">
                <input type="hidden" name="updatedBy" value="{{ Auth::user()->id }}">
                <input type="hidden" name="divisionName" value="{{ $getDivisionName }}">
                <input type="hidden" name="schoolId" value="{{ $applicationInfo['schoolUserIdfk'] }}">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary bg-purple" id="upload-ro-ocular-visit">Yes</button>
                
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->   
     <div id="snackbar">Some </div>
@include('layouts.footer')
@include('layouts.rightsidebar')
@endsection
