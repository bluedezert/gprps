@extends('layouts.app')

@section('content')

@include('layouts.sidebar')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            &nbsp;
        </h1>
        <!-- You can dynamically generate breadcrumbs here -->
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> GPRPS</a></li>
            <li class="active"> New Application</li>
        </ol>
    </section>
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-private-schools">
                    <div class="box-header with-border">
                        <h2>New Application</h2>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <form>
                            <div class="row">
                                <div class="col-md-12">
                                  <p>
                                    <button type="button" class="btn bg-purple btn-flat btn-new-application" data-toggle="modal" data-target="#modal-set-new-application" data-toggle="tooltip" data-placement="top" title="ALL requirements must have an uploaded file." disabled>Submit Application</button>

                                    <button type="button" class="btn bg-default btn-flat btn-edit-new-application" data-toggle="modal" data-target="#modal-edit-new-application">Edit Application</button>

                                    <button type="button" class="btn bg-navy btn-flat btn-cancel-new-application" data-toggle="modal" data-target="#modal-cancel-new-application">Cancel Application</button>

                                    <button type="button" class="btn bg-default btn-flat btn-close-new-application"><a href="{{ route('privateschoolnewapplication') }}">Close</a></button>

                                  </p>
                                </div>
                                <!-- /.col -->
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="col-md-2">Status</label>
                                    <div class="col-sm-8" style="padding-left:0px">Draft
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 10%">
                                            </div>
                                        </div>                      
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <label class="col-md-4">Name of School:</label>
                                        <div class="col-md-8">School Name</div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <label class="col-md-4">Address of School:</label>
                                        <div class="col-md-8">School Address</div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <label class="col-md-4">Course (Grade Level Applying for?):</label>
                                        <div class="col-md-8">Kindergarten to Grade 6</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <label class="col-md-4">Principal/School Head:</label>
                                        <div class="col-md-8">Juan Dela Cruz</div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <label class="col-md-4">Contact Number:</label>
                                        <div class="col-md-8">+63999-123-4567</div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <label class="col-md-4">School Year:</label>
                                        <div class="col-md-8">2018 - 2019</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                              <div class="box">
                                <div class="box-header">
                                  <h3 class="box-title">New Application of Government Permit</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body table-responsive no-padding">
                                    <table class="table table-hover table-responsive table-bordered table-condensed">
                                        <thead>
                                            <tr>
                                                <th>Upload</th>
                                                <th width="500px">Requirements</th>
                                                <th></th>
                                                <th>Evaluation</th>
                                                <th>Inspection</th>
                                                <th>RO Review</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {!! $newApplicationRequirements !!} <!-- used to render text as HTML -->
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                              </div>
                              <!-- /.box -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

        <div class="modal fade" id="modal-cancel-new-application">
          <div class="modal-dialog">
            <div class="modal-content">
              <form method="POST" action="cancelnewapplication">
                {{ csrf_field() }}
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Cancel New Application</h4>
              </div>
              <div class="modal-body">
                <h4>Are you sure you want to cancel the application? This action cannot be undone.</h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary" id="cancel-new-application">Yes</button>
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

@include('layouts.footer')
@include('layouts.rightsidebar')
@endsection
