@extends('layouts.app')

@section('content')

@include('layouts.sidebar')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            {!! $applicationType !!}
        </h1>
        <!-- You can dynamically generate breadcrumbs here -->
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> GPRPS</a></li>
                <li class="active"> {{ $applicationType }}</li>
        </ol>
    </section>
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-private-schools">
                    <div class="box-header with-border">
                        <br>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="col-xs-12">
                          <div class="">
                            <div class="box-header">
                              <h3 class="box-title">List of Application of Government Permit</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body table-responsive no-padding">
                                <table class="table table-hover table-responsive table-bordered table-condensed" id="example1">
                                    <thead>
                                        <tr>
                                            <th style="width: 100px;">School Year</th>
                                            <th>School Name</th>
                                            <th style="width: 200px;">Grade Level Applying</th>
                                            <th style="width: 200px;">Status</th>
                                            <th style="width: 150px;">Last Date Updated</th>
                                            <th style="width: 50px;"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($applicationlists as $applicationlist)
                                            <tr>
                                                <td>{{ $applicationlist['schoolYear'] }}</td>
                                                <td>{{ $applicationlist['schoolName'] }}</td>
                                                <td>{{ $applicationlist['applicationFrom'] }} - {{ $applicationlist['applicationTo'] }}</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-striped {{ $applicationlist['progressClass'] }}" role="progressbar" aria-valuenow="{{ $applicationlist['valueNow'] }}" aria-valuemin="0" aria-valuemax="100" style="max-width: {{ $applicationlist['valueNow'] }}%"><strong class="title">{{ $applicationlist["applicationStatus"]}}</strong>
                                                        </div>
                                                    </div> 
                                                </td>
                                                <td>{{ $applicationlist['lastDateUpdated'] }}</td>
                                                <td>
                                                    <a href="{{ route('viewapplication', [$applicationlist['prStatusId'], $applicationTypeId, 'View Application']) }}">
                                                        <button type="button" class="btn btn-xs bg-olive btn-flat btn-view-application">
                                                            <span class="fa fa-eye"></span> View
                                                        </button>
                                                    </a>
                                                </td>
                                            </tr>

                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th><input class="form-control" type="text"></th>
                                            <th>
                                                <select class="form-control">
                                                    <option selected value=""></option>
                                                    <option value="Abra">Abra</option>
                                                    <option value="Apayao">Apayao</option>
                                                    <option value="Benguio City">Baguio City</option>
                                                    <option value="Benguet">Benguet</option>
                                                    <option value="Ifugao">Ifugao</option>
                                                    <option value="Kalinga">Kalinga</option>
                                                    <option value="Mt. Province">Mt. Province</option>
                                                    <option value="Tabuk City">Tabuk City</option>
                                                </select>
                                            </th>
                                            <th><input class="form-control" type="text"></th>
                                            <th><input class="form-control" type="text"></th>
                                            <th>
                                                <select class="form-control">
                                                    <option selected value=""></option>
                                                    <option value="Approved">Approved</option>
                                                    <option value="Cancelled">Cancelled</option>
                                                    <option value="Disapproved">Disapproved</option>
                                                    <option value="On Compliance">On Compliance</option>
                                                    <option value="On Process">on Process</option>
                                                    <option value="Removed">Removed</option>
                                                    <option value="Submitted">Submitted</option>
                                                </select>
                                            </th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- /.box-body -->
                          </div>
                          <!-- /.box -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div id="snackbar">{{ $status }}</div>
@include('layouts.footer')
@include('layouts.rightsidebar')
@endsection