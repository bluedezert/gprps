@extends('layouts.app')

@section('content')

@include('layouts.sidebar')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            {!! $applicationType !!}
        </h1>
        <!-- You can dynamically generate breadcrumbs here -->
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> GPRPS</a></li>
                <li class="active"> </li>
        </ol>
    </section>
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-private-schools">
                    <div class="box-header with-border">
                        <br>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                              <p>
                                <button type="button" class="btn bg-purple btn-flat btn-new-application" data-toggle="modal" data-target="#modal-set-new-application">{!! $newBtnText !!}</button>
                              </p>
                            </div>
                            <!-- /.col -->
                        </div>
                        <div class="col-xs-12">
                          <div class="box">
                            <div class="box-header">
                              <h3 class="box-title">List of Application of Government Permit</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body table-responsive no-padding">
                                <table class="table table-hover table-responsive table-bordered table-condensed" id="example1">
                                    <thead>
                                        <tr>
                                            <th style="width: 150px;">School Year</th>
                                            <th style="width: 300px;">Grade Level Applying</th>
                                            <th>Status</th>
                                            <th style="width: 200px;">Last Date Updated</th>
                                            <th style="width: 75px;"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($applicationlists as $applicationlist)
                                            <tr>
                                                <td>{{ $applicationlist['schoolYear'] }}</td>
                                                <td>{{ $applicationlist['applicationFrom'] }} - {{ $applicationlist['applicationTo'] }}</td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-striped {{ $applicationlist['progressClass'] }}" role="progressbar" aria-valuenow="{{ $applicationlist['valueNow'] }}" aria-valuemin="0" aria-valuemax="100" style="max-width: {{ $applicationlist['valueNow'] }}%"><strong class="title">{{ $applicationlist["applicationStatus"]}}</strong>
                                                        </div>
                                                    </div> 
                                                </td>
                                                <td>{{ $applicationlist['lastDateUpdated'] }}</td>
                                                <td>
                                                    <a href="{{ route('viewapplication', [$applicationlist['prStatusId'], $applicationTypeId, 'View Application']) }}">
                                                        <button type="button" class="btn btn-xs bg-olive btn-flat btn-view-application">
                                                            <span class="fa fa-eye"></span> View
                                                        </button>
                                                    </a>
                                                </td>
                                            </tr>

                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->
                          </div>
                          <!-- /.box -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- New Application Modal -->
        <div class="modal fade" id="modal-set-new-application">
          <div class="modal-dialog">
            <div class="modal-content">
              <form method="POST" action="{{ route('draftnewapplication') }}" onsubmit="return onFormNextSubmit();">
                {{ csrf_field() }}
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">New Application</h4>
              </div>
              <div class="modal-body">
                <input type="hidden" value="{{ $applicationTypeId }}" name="applicationType" id="applicationType">
                <h4>Applying for:</h4>
                <div class="row">
                    <div class="col-md-6 form-group">
                        <label>School Year: </label>
                        <select required class="form-control" name="sy" id="sy">
                            <option value="" selected disabled>Select School Year</option>
                            <option value="2020 - 2021">2020 - 2021</option>
                        </select>
                    </div>
                </div>
                <div class="row gradeleveldiv">
                    <div class="col-md-6 form-group">
                        <label>From</label>
                        <select required class="form-control" name="gradeLevelFrom" id="gradeLevelFrom">
                            <option value="" selected disabled>Select Grade From</option>
                            @foreach($gradeLevels as $gradeLevel)
                                <option value="{{ $gradeLevel['gradeLevelId'] }}">{{ $gradeLevel['description'] }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-6 form-group">
                        <label>To</label>
                        <select required class="form-control" name="gradeLevelTo" id="gradeLevelTo">
                            <option value="" selected disabled>Select Grade To</option>
                            @foreach($gradeLevels as $gradeLevel)
                                <option value="{{ $gradeLevel['gradeLevelId'] }}">{{ $gradeLevel['description'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="row categorydiv">
                    <div class="col-md-6 form-group">
                        <label>For Senior High Application</label>
                        <select required class="form-control" name="category" id="category">
                                <option value="0" selected> </option>
                            @foreach($categoriesshs as $categoryshs)
                                <option value="{{ $categoryshs['categoryId'] }}">{{ $categoryshs['category'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary bg-purple" id="draft-new-application">Next</button>
              </div>
            </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
<div id="snackbar">{{ $status }}</div>
@include('layouts.footer')
@include('layouts.rightsidebar')
@endsection