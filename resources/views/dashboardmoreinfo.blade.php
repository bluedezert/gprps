@extends('layouts.app')

@section('content')

@include('layouts.sidebar')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Dashboard <small> Application for SY {{ Config::get('constants.gprpsconst.application_school_year') }}</small>
        </h1>
        <!-- You can dynamically generate breadcrumbs here -->
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> GPRPS</a></li>
            <!--<li class="active"> Dashboard </li>-->
            <li class="active"> Dashboard</li>
        </ol>
    </section>
@if(Auth::user()->officeIDfk!=3)
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-private-schools">
                    <div class="box-header with-border">
                        <h3 class="box-title">Quick Stat</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                      <!-- Small boxes (Stat box) -->
                      <div class="row">
                        <div class="col-lg-3 col-xs-6">
                          <!-- small box -->
                          <div class="small-box bg-aqua">
                            <div class="inner">
                              <h3>{{ $onprocess['total'] }} <span style="font-size: x-large; font-weight: 300;">On Process</span></h3>

                              <p>Application/s</p>
                            </div>
                            <div class="icon">
                              <i class="fa fa-spinner"></i>
                            </div>
                            <a href="{{ route('dashboardmoreinfo', 4) }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                          </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                          <!-- small box -->
                          <div class="small-box bg-purple">
                            <div class="inner">
                              <h3>{{ $submitted['total'] }} <span style="font-size: x-large; font-weight: 300;">Submitted</span></h3>

                              <p>Application/s</p>
                            </div>
                            <div class="icon">
                              <i class="fa fa-paper-plane"></i>
                            </div>
                            <a href="{{ route('dashboardmoreinfo', 3) }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                          </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                          <!-- small box -->
                          <div class="small-box bg-green">
                            <div class="inner">
                              <h3>{{ $approved['total'] }} <span style="font-size: x-large; font-weight: 300;">Approved</span></h3>

                              <p>Application/s</p>
                            </div>
                            <div class="icon">
                              <i class="fa fa-check-circle"></i>
                            </div>
                            <a href="{{ route('dashboardmoreinfo', 5) }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                          </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                          <!-- small box -->
                          <div class="small-box bg-yellow">
                            <div class="inner">
                              <h3>{{ $disapproved['total'] }} <span style="font-size: x-large; font-weight: 300;">Disapproved</span></h3>

                              <p>Application/s</p>
                            </div>
                            <div class="icon">
                              <i class="fa fa-ban"></i>
                            </div>
                            <a href="{{ route('dashboardmoreinfo', 6) }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                          </div>
                        </div>
                        <!-- ./col -->
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-private-schools">
                    <div class="box-header with-border">
                        <h1>{{ $getApplicationStatusDesc }} <small>List of Schools</small></h1>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-pills" role="tablist">
                            <li role="presentation" class="active" style="background-color: #f1f1f1;"><a href="#private-school-elem" aria-controls="private-school-elem" role="tab" data-toggle="tab">Elementary</a></li>
                            <li role="presentation" style="background-color: #f1f1f1;"><a href="#private-school-sec" aria-controls="private-school-sec" role="tab" data-toggle="tab">Secondary</a></li>
                        </ul>
                        <br>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane table-responsive active" id="private-school-elem">
                                <table class="table table-hover table-responsive table-bordered table-condensed" id="schoolListElem">
                                    <thead>
                                        <tr>
                                            <th style="width: 150px;">Division</th>
                                            <th style="width: 100px;">School ID</th>
                                            <th>School Name</th>
                                            <th>Application Type</th>
                                            <th>Government P/R Number</th>
                                            <th>Series</th>
                                            <th>Last Date Updated</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($applicationlistelem as $school)
                                            <tr>
                                                <td>{{ $school['DIVISION'] }}</td>
                                                <td>{{ $school['SCHOOLID'] }}</td>
                                                <td>{{ $school['SCHOOLNAME'] }}</td>   
                                                <td>{{ $school['applicationType'] }}</td>           
                                                <td>{{ $school['governmentPRNumber'] }}</td>
                                                <td>{{ $school['seriesYear'] }}</td>
                                                <td>{{ $school['lastDateUpdated'] }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane table-responsive" id="private-school-sec">
                                <table class="table table-hover table-responsive table-bordered table-condensed" id="schoolListSec">
                                    <thead>
                                        <tr>
                                            <th style="width: 150px;">Division</th>
                                            <th style="width: 100px;">School ID</th>
                                            <th>School Name</th>
                                            <th>Application Type</th>
                                            <th>Government P/R Number</th>
                                            <th>Series</th>
                                            <th>Last Date Updated</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($applicationlistsec as $schoolsec)
                                            <tr>
                                                <td>{{ $schoolsec['DIVISION'] }}</td>
                                                <td>{{ $schoolsec['SCHOOLID'] }}</td>
                                                <td>{{ $schoolsec['SCHOOLNAME'] }}</td>   
                                                <td>{{ $schoolsec['applicationType'] }}</td>           
                                                <td>{{ $schoolsec['governmentPRNumber'] }}</td>
                                                <td>{{ $schoolsec['seriesYear'] }}</td>
                                                <td>{{ $schoolsec['lastDateUpdated'] }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endif
    <div id="snackbar">The Dashboard</div>
</div>
@include('layouts.footer')
@include('layouts.rightsidebar')

@endsection
